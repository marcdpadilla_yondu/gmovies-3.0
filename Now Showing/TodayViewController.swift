//
//  TodayViewController.swift
//  Now Showing
//
//  Created by Marc Darren Padilla on 10/1/17.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, URLSessionDelegate {
    
    
    @IBOutlet weak var posterBoard: UICollectionView!
    @IBOutlet weak var moreButton: UIButton!
    
    
    private var posterBoardItems : [MovieModel] = []
    private var downloadItems : [IndexPath] = []
    private var loadingScreen : LoaderView?
    private var gmoviesToken : String = ""
    private var urlSession = URLSession(configuration: .default)
    private var imageSession = URLSession()
    private var currentPage = 0
    private var maxPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        self.preferredContentSize = CGSize(width: self.view.frame.size.width, height: ((self.view.frame.size.width/4.0) * 1.48) + 65.0)
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        performWidgetUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        
        if (activeDisplayMode == NCWidgetDisplayMode.compact) {
            self.preferredContentSize = CGSize(width: maxSize.width, height: ((self.view.frame.size.width/4.0) * 1.48) + 65.0)
            print("compact")
        } else {
            let rows = ceilf(Float(posterBoard.numberOfItems(inSection: 0)) / 4.0)
            self.preferredContentSize = CGSize(width: maxSize.width, height: (((self.view.frame.size.width/4.0) * 1.48) + 50.0) * CGFloat(rows))
            print("preferred \(CGFloat(posterBoardItems.count) / 4.0)")
        }
        
    }
    
    //MARK: Collection view delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posterBoardItems.count > 12 ? 12 :  posterBoardItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "posterCell", for: indexPath)
        
        configureCell(cell: cell as! WidgetPosterCollectionViewCell, indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/4.0, height: ((self.view.frame.size.width/4.0) * 1.48) + 35.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = self.view.frame.size.width/4.0 * 4.0
        let totalSpacingWidth = CGFloat(2.5 * 3.0)
        
        let leftInset = (collectionView.frame.size.width - (totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(2.5, leftInset, 0, rightInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < collectionView.numberOfItems(inSection: 0) {
            let index = indexPath.row
            let movieObject = posterBoardItems[index]
            
            extensionContext?.open(URL(string: "gmovies://movie/\(movieObject.movieId!)")!, completionHandler: nil)
        } else {
            
        }
    }
    
    private func configureCell(cell: WidgetPosterCollectionViewCell, indexPath: IndexPath) {
        let b = posterBoardItems[indexPath.row]
        
        if b.poster != nil {
            cell.posterImage.image = b.poster
        } else {
            downloadImage(b: b, indexPath: indexPath)
        }
        
        cell.movieTitle.text = b.movieTitle!
    }
    
    @IBAction func moreAction(sender: UIButton) {
        extensionContext?.open(URL(string: "gmovies://")!, completionHandler: nil)
    }
    
    private func performWidgetUpdate() {
//        loadingScreen?.removeFromSuperview()
//        
//        self.view.addSubview(loadingScreen!)
//        loadingScreen?.frame = UIScreen.main.bounds
//        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
//                                  ])
//        
//        self.view.layoutIfNeeded()
//        
//        loadingScreen?.startAnimating()
        
        getContent()
    }
    
    private func getContent() {
        var urlRequest = URLRequest(url: URL(string: "\(Constants.gMoviesServer)/v1/token/get")!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        urlRequest.setValue("", forHTTPHeaderField: "gmovies-user")
        urlRequest.setValue("ios", forHTTPHeaderField: "gmovies-platform")
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                var result : NSDictionary? = nil
                do {
                    result = try JSONSerialization.jsonObject(with: data!) as? NSDictionary
                    
                    let status = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        self.gmoviesToken = result?.value(forKeyPath: "data.token") as! String
                        DispatchQueue.main.async {
                            self.downloadposterBoard()
                        }
                    } else {
                        print("token error")
                    }
                    
                } catch (let err) {
                    print(err.localizedDescription)
                }
            } else {
                print(error!.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    private func downloadposterBoard() {
        var urlRequest = URLRequest(url: URL(string: "\(Constants.gMoviesServer)/v1/movies")!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        urlRequest.setValue("", forHTTPHeaderField: "gmovies-user")
        urlRequest.setValue(gmoviesToken, forHTTPHeaderField: "gmovies-token")
        urlRequest.setValue("ios", forHTTPHeaderField: "gmovies-platform")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        urlRequest.httpMethod = "POST"
        do {
            let payload = try JSONSerialization.data(withJSONObject: ["status" : "now_showing", "theaters" : ""])
            
            urlRequest.httpBody = payload
        } catch (let error) {
            print("now showing widget payload", error.localizedDescription)
        }
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                var result : NSDictionary? = nil
                do {
                    result = try JSONSerialization.jsonObject(with: data!) as? NSDictionary
                    
                    let status = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        self.posterBoardItems.removeAll()
                        let results = result?.value(forKeyPath: "data.now_showing") as! [NSDictionary]

                        for d in results {
                            let b = MovieModel(data: d)
                            self.posterBoardItems.append(b)
                        }
                        
                        DispatchQueue.main.async {
                            self.posterBoard.reloadData()
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                        }
                    } else {
                        print("api error")
                    }
                    
                } catch (let err) {
                    print(err.localizedDescription)
                }
            } else {
                print(error!.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    private func downloadImage(b: MovieModel, indexPath: IndexPath) {
        
        if downloadItems.contains(indexPath) {
            return
        } else {
            downloadItems.append(indexPath)
        }
        
        imageSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue.main)
        let urlRequest : URLRequest = URLRequest(url: URL(string: b.posterUrl!)!, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60.0)
        
        
        let task : URLSessionDownloadTask = imageSession.downloadTask(with: urlRequest) { (url, response, error) in
            
            if error == nil {
                
                do {
                    let image = UIImage(data: try Data(contentsOf: url!))
                    
                    b.poster = image
                    
                    DispatchQueue.main.async {
                        if self.posterBoard.indexPathsForVisibleItems.contains(indexPath) {
                            self.posterBoard.performBatchUpdates({
                                self.posterBoard.reloadItems(at: [indexPath])
                            }, completion: nil)
                        }
                        
                        self.downloadItems = self.downloadItems.filter({ (i) -> Bool in
                            return i != indexPath
                        })
                    }
                    
                } catch (let err) {
                    print(err.localizedDescription)
                    self.downloadItems = self.downloadItems.filter({ (i) -> Bool in
                        return i != indexPath
                    })
                }
                
            } else {
                print(error!.localizedDescription)
                self.downloadItems = self.downloadItems.filter({ (i) -> Bool in
                    return i != indexPath
                })
            }
            
        }
        
        task.resume()
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust{
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential,credential);
        }
    }
    
}
