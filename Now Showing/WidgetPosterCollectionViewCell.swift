//
//  WidgetPosterCollectionViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 10/1/17.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

class WidgetPosterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        self.layer.borderWidth = 0.5
    }
}
