//
//  WidgetBannerCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 10/1/17.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

class WidgetBannerCell: UICollectionViewCell {
    
    
    @IBOutlet weak var bannerImage: UIImageView!
    
    
}
