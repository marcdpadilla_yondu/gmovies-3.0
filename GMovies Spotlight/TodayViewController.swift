//
//  TodayViewController.swift
//  GMovies Spotlight
//
//  Created by Marc Darren Padilla on 10/1/17.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UICollectionViewDelegate, UICollectionViewDataSource, URLSessionDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var carousel: UICollectionView!
    private var carouselItems : [BannerModel] = []

    private var downloadItems : [IndexPath] = []
    private var loadingScreen : LoaderView?
    private var gmoviesToken : String = ""
    private var urlSession = URLSession(configuration: .default)
    private var imageSession = URLSession()
    private var currentPage = 0
    private var maxPage = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        scrollPage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        
        performWidgetUpdate()
        if (activeDisplayMode == NCWidgetDisplayMode.compact) {
            self.preferredContentSize = maxSize
            print("compact")
        } else {
            self.preferredContentSize = CGSize(width: maxSize.width, height:  maxSize.width * 0.6)
            print("preferred")
        }
        
    }
    
    //MARK: Collection view delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carouselItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath)
        
        configureCell(cell: cell as! WidgetBannerCell, indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.width * 0.6)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        let bannerObject = carouselItems[index]
        
        if bannerObject.linkType?.caseInsensitiveCompare("movie") == .orderedSame {
            extensionContext?.open(URL(string: "gmovies://movie/\(bannerObject.objectId!)")!, completionHandler: nil)
        } else if bannerObject.linkType?.caseInsensitiveCompare("spotlight") == .orderedSame {
            extensionContext?.open(URL(string: "gmovies://spotlight/\(bannerObject.link!)")!, completionHandler: nil)
        } else if bannerObject.linkType?.caseInsensitiveCompare("external") == .orderedSame {
            extensionContext?.open(URL(string: bannerObject.link!)!, completionHandler: nil)
        }
    }
    
    private func configureCell(cell: WidgetBannerCell, indexPath: IndexPath) {
        let b = carouselItems[indexPath.row]
        
        if b.img != nil {
            cell.bannerImage.image = b.img
        } else {
            downloadImage(b: b, indexPath: indexPath)
        }
        
    }
    
    private func performWidgetUpdate() {
//        loadingScreen?.removeFromSuperview()
//        
//        self.view.addSubview(loadingScreen!)
//        loadingScreen?.frame = UIScreen.main.bounds
//        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
//            ])
//
//        self.view.layoutIfNeeded()
//        
//        loadingScreen?.startAnimating()
        
        getContent()
    }
    
    @objc private func scrollPage() {
        
        DispatchQueue.main.async {
            if self.carouselItems.count > 0 {
                self.carousel.scrollToItem(at: IndexPath(row: self.currentPage, section: 0), at: .left, animated: true)
            }
        }
        
        currentPage += 1
        if currentPage >= maxPage {
            currentPage = 0
        }
        
        self.perform(#selector(scrollPage), with: nil, afterDelay: 3.0)
    }
    
    private func getContent() {
        var urlRequest = URLRequest(url: URL(string: "\(Constants.gMoviesServer)/v1/token/get")!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        urlRequest.setValue("", forHTTPHeaderField: "gmovies-user")
        urlRequest.setValue("ios", forHTTPHeaderField: "gmovies-platform")
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                var result : NSDictionary? = nil
                do {
                    result = try JSONSerialization.jsonObject(with: data!) as? NSDictionary
                    
                    let status = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        self.gmoviesToken = result?.value(forKeyPath: "data.token") as! String
                        DispatchQueue.main.async {
                            self.downloadCarousel()
                        }
                    } else {
                        print("token error")
                    }
                    
                } catch (let err) {
                    print(err.localizedDescription)
                }
            } else {
                print(error!.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    private func downloadCarousel() {
        var urlRequest = URLRequest(url: URL(string: "\(Constants.gMoviesServer)/v1/carousel")!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        urlRequest.setValue("", forHTTPHeaderField: "gmovies-user")
        urlRequest.setValue(gmoviesToken, forHTTPHeaderField: "gmovies-token")
        urlRequest.setValue("ios", forHTTPHeaderField: "gmovies-platform")
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                var result : NSDictionary? = nil
                do {
                    result = try JSONSerialization.jsonObject(with: data!) as? NSDictionary
                    
                    let status = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        self.carouselItems.removeAll()
                        let results = result?.value(forKey: "data") as! [NSDictionary]
                        for d in results {
                            let b = BannerModel(data: d)
                            self.carouselItems.append(b)
                        }
                        
                        DispatchQueue.main.async {
                            self.maxPage = self.carouselItems.count
                            self.carousel.reloadData()
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                        }
                    } else {
                        print("api error")
                    }
                    
                } catch (let err) {
                    print(err.localizedDescription)
                }
            } else {
                print(error!.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    private func downloadImage(b: BannerModel, indexPath: IndexPath) {
        
        if downloadItems.contains(indexPath) {
            return
        } else {
            downloadItems.append(indexPath)
        }
    
        imageSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue.main)
        let urlRequest : URLRequest = URLRequest(url: URL(string: b.imgUrl!)!, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60.0)
        
        
        let task : URLSessionDownloadTask = imageSession.downloadTask(with: urlRequest) { (url, response, error) in
            
            if error == nil {
                
                do {
                    let image = UIImage(data: try Data(contentsOf: url!))
                    
                    b.img = image
                    
                    DispatchQueue.main.async {
                        if self.carousel.indexPathsForVisibleItems.contains(indexPath) {
                            self.carousel.performBatchUpdates({ 
                                self.carousel.reloadItems(at: [indexPath])
                            }, completion: nil)
                        }
                        
                        self.downloadItems = self.downloadItems.filter({ (i) -> Bool in
                            return i != indexPath
                        })
                    }
                    
                } catch (let err) {
                    print(err.localizedDescription)
                    self.downloadItems = self.downloadItems.filter({ (i) -> Bool in
                        return i != indexPath
                    })
                }
                
            } else {
                print(error!.localizedDescription)
                self.downloadItems = self.downloadItems.filter({ (i) -> Bool in
                    return i != indexPath
                })
            }
            
        }
        
        task.resume()
    }

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust{
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential,credential);
        }
    }

}
