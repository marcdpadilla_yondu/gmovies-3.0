//
//  CalendarMonthReusableView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CalendarMonthReusableView: UICollectionReusableView {

    @IBOutlet weak var monthName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
