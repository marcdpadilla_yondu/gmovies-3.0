//
//  SeatSelectionViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 10/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import SwiftSoup
import PopupDialog

class SeatSelectionViewController: UIViewController, SeatmapViewProtocol, CalendarViewProtocol, CinemaListProtocol, TimeTableProtocol, ProgressNavigationProtocol, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var seatMapContainer: UIView!
    @IBOutlet weak var seatmapHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var cinemaName: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet var promoCodeButtonUnderline: [UIView]!
    
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var cinemaButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    
    @IBOutlet weak var transactionWithSeatmap: UIStackView!
    @IBOutlet weak var transactionWithoutSeatmap: UIStackView!
    
    @IBOutlet var promoCodeTextField:[UITextField]!
    @IBOutlet var promoVerificationIcon:[UIImageView]!
    
    
    private var currentPopView : PopDropViewController?
    
    @IBOutlet weak var legendContainer: UIView!

    @IBOutlet weak var legendHeight: NSLayoutConstraint!
    
    @IBOutlet weak var continueButton: UIButton!
    var theaterObject : TheaterModel?
    var movieObject : MovieModel?
    var blockObject : BlockScreeningModel?
    var promoObject : PromoModel?
    
    private var seatMapViewController : SeatMapViewController?
    private var allowedDates : [Date]?
    
    private var selectedDate : Date?
    private var selectedCinema : CinemaModel?
    fileprivate var selectedTime : TimeModel?
    var selectedSeats : [SeatModel]?
    
    @IBOutlet var ticketPrice: [UILabel]!
    
    @IBOutlet weak var collectionView: UICollectionView!
     var centerSeats : Bool = true
    
    private var errorView : ErrorView?
    
    fileprivate var freeSeatsCount : Int = 1
    private var freeSeating : FreeSeatingView?
    
    private var maxSeats : Int = 10
    
    private var loadingScreen : LoaderView?
    
    fileprivate var currentPromoCode: String = ""
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    fileprivate var timer : Timer?
    fileprivate var ticker : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        selectedSeats = []
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        self.view.addSubview(loadingScreen!)
        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
            ])

        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
        
        if blockObject != nil {
            dateButton.isEnabled = false
            cinemaButton.isEnabled = false
            timeButton.isEnabled = false
            
            dateButton.setImage(nil, for: .normal)
            cinemaButton.setImage(nil, for: .normal)
            timeButton.setImage(nil, for: .normal)
            
            dateButton.setTitle(UIManager.seatmapDateFormatter(date: blockObject!.screeningDateTime!), for: .normal)
            cinemaButton.setTitle(blockObject?.theater?.cinemas?.first?.name!, for: .normal)
            timeButton.setTitle(blockObject?.screeningTimeAMPM, for: .normal)
            
            cinemaName.text = blockObject?.theater?.cinemas?.first?.name!.uppercased()
            movieTitle.text = blockObject?.movie?.movieTitle!.uppercased()
            
            getSeatMap()
            
            return
        }
        
        dateButton.setTitle(UIManager.seatmapDateFormatter(date: Date()), for: .normal)
        
        cinemaName.text = theaterObject!.name!.uppercased()
        movieTitle.text = movieObject!.movieTitle!.uppercased()
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showBanner(message: "Loading Schedules...", isPersistent: true, color: UIColor(netHex: 0x26c5a9), textColor: UIColor.white)
        
        GMoviesAPIManager.getMovieSchedule(movieId: (movieObject?.movieId!)!, theaterId: (theaterObject?.theaterId!)!) { (error, result) in
            if error == nil {
                let status : Int = result?.object(forKey: "status") as! Int
                //print(result)
                if status == 1 {
                    let dates : [NSDictionary] = result?.value(forKeyPath: "data.schedules.dates") as! [NSDictionary]
                    
                    self.allowedDates = []
                    for d : NSDictionary in dates {
                        let dateString : String = d.object(forKey: "date") as! String
                        self.allowedDates?.append(UIManager.scheduleDateStringToDate(dateString: dateString))
                    }
                    
                    let cinemas : [NSDictionary] = result?.value(forKeyPath: "data.schedules.cinemas") as! [NSDictionary]
                    
                    self.theaterObject?.cinemas?.removeAll()
                    
                    for c : NSDictionary in cinemas {
                        let 😂 : [NSDictionary] = c.object(forKey : "cinemas") as! [NSDictionary]
                        for ci : NSDictionary in 😂 {
                            let 🤓 : CinemaModel = CinemaModel(data : ci)
                            🤓.dateId = Int(c.object(forKey : "parent") as! String)!
                            self.theaterObject?.cinemas?.append(🤓)
                        }
                    }
                    
                    let times : [NSDictionary] = result?.value(forKeyPath: "data.schedules.times") as! [NSDictionary]
                    
                    for t : NSDictionary in times {
                        let 😂 : [NSDictionary] = t.object(forKey : "times") as! [NSDictionary]
                        for ti : NSDictionary in 😂 {
                            let 🤓 : TimeModel = TimeModel(data : ti)
                            🤓.dateCinemaId = t.object(forKey : "parent") as? String
                            let cinemaObject : CinemaModel = (self.theaterObject?.cinemas?.filter({ (c) -> Bool in
                                return c.dateCinemaId == 🤓.dateCinemaId
                            }).first)!
                            cinemaObject.times?.append(🤓)
                        }
                    }
                    
                    let paymentOptions : [NSDictionary] = result?.value(forKeyPath: "data.theater.payment_options") as! [NSDictionary]
                    self.theaterObject?.allowReseravation = result?.value(forKeyPath: "data.theater.allow_reservation") as! Bool
                    self.theaterObject?.convenienceFees = result?.value(forKeyPath: "data.theater.convenience_fees") as! NSDictionary?
                    self.theaterObject?.convenienceFeesMessage = result?.value(forKeyPath: "data.theater.convenience_fees_message") as! NSDictionary?
                    
                    self.theaterObject?.paymentOptions?.removeAll()
                    for p : NSDictionary in paymentOptions {
                        let po : PaymentOptionModel = PaymentOptionModel(data: p)
                        print(p)
                        self.theaterObject?.paymentOptions?.append(po)
                    }
                    
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        
                        self.selectedDate = self.allowedDates!.first
                        if self.allowedDates!.first == nil {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                                appDelegate.hideBanner()
                                self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
                                self.cinemaButton.isEnabled = false
                                self.timeButton.isEnabled = false
                                self.dateButton.isEnabled = false
                            }
                            return
                        }
                        self.cinemaButton.isEnabled = true
                        self.timeButton.isEnabled = true
                        self.dateButton.isEnabled = true
                        
                        let dateId : Int = UIManager.generateDateId(date: self.allowedDates!.first!)
                        
                        self.selectedCinema = self.theaterObject?.cinemas?.first(where: { (c) -> Bool in
                            return c.dateId == dateId
                        })
                        
                        self.selectedTime = self.selectedCinema?.times?.first(where: { (t) -> Bool in
                            return t.dateCinemaId == self.selectedCinema?.dateCinemaId
                        })
                        
                        if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                            self.transactionWithSeatmap.isHidden = false
                            self.transactionWithoutSeatmap.isHidden = true
                        }else {
                            self.transactionWithSeatmap.isHidden = true
                            self.transactionWithoutSeatmap.isHidden = false
                        }
                        
                        if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                            self.transactionWithSeatmap.isHidden = false
                            self.transactionWithoutSeatmap.isHidden = true
                        }else {
                            self.transactionWithSeatmap.isHidden = true
                            self.transactionWithoutSeatmap.isHidden = false
                        }
                        
                        if self.selectedDate == nil || self.selectedCinema == nil || self.selectedTime == nil {
                            appDelegate.showBanner(message: "Server Error", isPersistent: false)
                            self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
                            return
                        }
                        
                        let cinemaName : String = self.selectedCinema!.name!
                        
                        self.dateButton.setTitle(UIManager.seatmapDateFormatter(date: self.selectedDate!), for: .normal)
                        self.cinemaButton.setTitle(cinemaName, for: .normal)
                        
                        let string : NSString = NSString(string: "\(self.selectedTime!.name!) \(self.selectedTime!.variantString)")
                        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName : self.timeButton.titleLabel!.font, NSForegroundColorAttributeName : self.timeButton.titleLabel!.textColor])
                        attributedString.addAttributes([NSForegroundColorAttributeName : UIColor(netHex: 0x00A2FF)], range: string.range(of: (self.selectedTime?.variantString)!))
                        
                        self.timeButton.setAttributedTitle(attributedString, for: .normal)
                        
                        appDelegate.hideBanner()
                        
                        if self.theaterObject?.allowReseravation == true {
                            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                                self.getSeatMap()
                                if let message = self.selectedTime?.popcornMessage {
                                    if !message.isEmpty {
                                        self.showAlert(title: "GMovies", message: message, cancelText: "OK")
                                    }
                                }
                            } else {
                               self.showFreeSeating()
                                if let message = self.selectedTime?.popcornMessage {
                                    if !message.isEmpty {
                                        self.showAlert(title: "GMovies", message: message, cancelText: "OK")
                                    }
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                                self.loadErrorView(titleString: "E-Ticketing Unavailable", description: "Booking seats & buying tickets is unavailable for this cinema. You may view the schedules & proceed to the ticket counter to purchase tickets.")
                            }
                        }
                        self.reloadPrice()
                        
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        
                        let htmlMessage:String? = result!.value(forKey: "message") as? String
                        
                        var messageTitle: String = "Movie not available in this cinema."
                        var messageBody: String = "Please select another cinema or check back later."
                        
                        if htmlMessage != nil {
                            do {
                                let doc: Document = try! SwiftSoup.parse(htmlMessage!)
                                let title: Element = try! doc.select("b").first()!
                                messageTitle = try! title.text()
                                
                                let messageArray = htmlMessage!.components(separatedBy: "<br>")
                                messageBody = messageArray[1]
                            }catch Exception.Error( _, _){
                                
                            }catch {
                                
                            }
                        }
                        
                        self.loadErrorView(titleString: messageTitle, description: messageBody)
                        self.cinemaButton.isEnabled = false
                        self.timeButton.isEnabled = false
                        self.dateButton.isEnabled = false
                        appDelegate.hideBanner()
                    }
                }
                
            } else {
                DispatchQueue.main.async {
                    self.loadingScreen?.stopAnimating()
                    self.loadingScreen?.removeFromSuperview()
                    self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
                    appDelegate.showBanner(message: "Network Error", isPersistent: false)
                    self.cinemaButton.isEnabled = false
                    self.timeButton.isEnabled = false
                    self.dateButton.isEnabled = false
                }
            }
        }
    }
    
    private func displayLoader() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        centerSeats = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if parent != nil {
            (parent as! ProgressNavigationViewController).delegate = self
        }
        AnalyticsHelper.sendScreenShowEvent(name: "seat_selection")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.SeatMap)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        centerSeats = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : Private functions
    
    @objc private func showFreeSeating() {
        
        
        //3"5
        if UIScreen.main.bounds.maxY == 480 {
           seatmapHeight.constant = 265
        }
        //4"0
        if UIScreen.main.bounds.maxY == 568 {
           seatmapHeight.constant = 265
        }
        //4"7
        if UIScreen.main.bounds.maxY == 667 {
           seatmapHeight.constant = 350
        }
        //5"5
        if UIScreen.main.bounds.maxY == 736 {
           seatmapHeight.constant = 400
        }
        
        contentView.layoutIfNeeded()
        seatMapViewController?.view.removeFromSuperview()
        seatMapViewController = nil
        legendHeight.constant = 0.0
        
        freeSeating?.removeFromSuperview()
        freeSeating = nil
        freeSeating = Bundle.main.loadNibNamed("FreeSeatingView", owner: self, options: nil)?.first as? FreeSeatingView
        freeSeating?.translatesAutoresizingMaskIntoConstraints = false
        
        freeSeating?.minusButton.addTarget(self, action: #selector(self.seatcountAction), for: .touchUpInside)
        freeSeating?.plusButton.addTarget(self, action: #selector(self.seatcountAction), for: .touchUpInside)
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: freeSeating!, attribute: .leading, relatedBy: .equal, toItem: seatMapContainer, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: freeSeating!, attribute: .top, relatedBy: .equal, toItem: seatMapContainer, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: freeSeating!, attribute: .trailing, relatedBy: .equal, toItem: seatMapContainer, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: freeSeating!, attribute: .bottom, relatedBy: .equal, toItem: seatMapContainer, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        seatMapContainer?.addSubview(freeSeating!)
        seatMapContainer?.addConstraints(constraints)
        
        if blockObject == nil {
            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                selectedSeats?.removeAll()
            } else {
                freeSeatsCount = 1
            }
        }
        
        for icon in promoVerificationIcon {
            icon.image = nil
        }
        
        reloadPrice()
    }
    
    @objc private func seatcountAction(sender : UIButton) {
        if sender.tag == -1 {
            freeSeatsCount -= freeSeatsCount > 1 ? 1 : 0
        } else {
            freeSeatsCount += freeSeatsCount < 10 ? 1 : 0
        }
        if !currentPromoCode.isEmpty {
            verifyPromo(code: currentPromoCode) { (verified) in
                
            }
        }
        freeSeating?.countLabel.text = String(format: "%02d", freeSeatsCount)
        reloadPrice()
    }
    
    private func configureView() {
        scrollView.contentSize = contentView.frame.size
        seatMapContainer.layoutIfNeeded()
        dateButton.centerTextAndImage(spacing: 8)
        timeButton.centerTextAndImage(spacing: 8)
        cinemaButton.centerTextAndImage(spacing: 8)
    }
    
    @IBAction func dateButtonAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "date_button")
        let calendar : CalendarViewController = CalendarViewController(nibName: "CalendarViewController", bundle: nil)
        calendar.allowedDates = allowedDates
        calendar.delegate = self
        showPopup(view: sender as! UIView, viewController: calendar)
    }
    
    @IBAction func cinemaButtonAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "cinema_button")
        let list : CinemaListViewController = CinemaListViewController(nibName: "CinemaListViewController", bundle: nil)
        list.theaterObject = theaterObject
        if selectedDate == nil {
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showBanner(message: "Please select date", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
            return
        }
        list.currentDateId = UIManager.generateDateId(date: selectedDate!)
        list.delegate = self
        let count = theaterObject!.cinemas!.filter { (c) -> Bool in
            return c.dateId == UIManager.generateDateId(date: selectedDate!)
        }.count
        showPopup(view: sender as! UIView, viewController: list, height: CGFloat(count) * 44.0)
    }
    
    @IBAction func timeButtonAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "time_button")
        let list : TimeTableViewController = TimeTableViewController(nibName: "TimeTableViewController", bundle: nil)
        list.cinema = selectedCinema
        list.dateCinemaId = selectedCinema?.dateCinemaId
        list.delegate = self
        
        //print(selectedCinema, selectedCinema?.times)
        
        if selectedCinema == nil {
            return
        }
        
        if selectedCinema!.times == nil {
            return
        }
        
        showPopup(view: sender as! UIView, viewController: list, height: CGFloat(selectedCinema!.times!.count) * 44.0)
    }
    
    private func showPopup(view : UIView, viewController: UIViewController?, height: CGFloat = 240.0) {
        let pop : PopDropViewController = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: height)
        currentPopView = pop
        pop.showViewControllerFromView(viewController: viewController!, originView: view, offset: CGPoint(x: 0.0, y: 135.0))
    }
    
    @objc private func getSeatMap() {
        
        //3"5
        if UIScreen.main.bounds.maxY == 480 {
            legendHeight.constant = 30.0
            seatmapHeight.constant = 240.0
        }
        //4"0
        if UIScreen.main.bounds.maxY == 568 {
            legendHeight.constant = 30.0
            seatmapHeight.constant = 240
        }
        //4"7
        if UIScreen.main.bounds.maxY == 667 {
            legendHeight.constant = 30.0
            seatmapHeight.constant = 270.0
        }
        //5"5
        if UIScreen.main.bounds.maxY == 736 {
            legendHeight.constant = 50.0
            seatmapHeight.constant = 320.0
        }
        
        
        contentView.layoutIfNeeded()
        
        freeSeating?.removeFromSuperview()
        freeSeating = nil
        seatMapViewController?.view.removeFromSuperview()
        seatMapViewController = nil
        
        seatMapViewController = SeatMapViewController(nibName: "SeatMapViewController", bundle: nil)
        seatMapViewController?.view.translatesAutoresizingMaskIntoConstraints = false
        seatMapViewController?.delegate = self
        seatMapViewController?.maxSeats = maxSeats
        
        seatMapViewController?.willMove(toParentViewController: self)
        addChildViewController(seatMapViewController!)
        
        seatMapViewController?.didMove(toParentViewController: self)
        seatMapContainer.addSubview(seatMapViewController!.view)
        let constraints : [NSLayoutConstraint] = [NSLayoutConstraint(item: seatMapViewController!.view, attribute: .top, relatedBy: .equal, toItem: seatMapContainer, attribute: .top, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: seatMapViewController!.view, attribute: .left, relatedBy: .equal, toItem: seatMapContainer, attribute: .left, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: seatMapViewController!.view, attribute: .right, relatedBy: .equal, toItem: seatMapContainer, attribute: .right, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: seatMapViewController!.view, attribute: .bottom, relatedBy: .equal, toItem: seatMapContainer, attribute: .bottom, multiplier: 1.0, constant: 10.0)]
        seatMapContainer.addConstraints(constraints)
        
        //selectedSeatsHeight.constant = 100.0
        //ticketPriceHeight.constant = 44.0
        contentView.layoutIfNeeded()
        
        for inputPromo in promoCodeTextField {
            inputPromo.text = ""
            inputPromo.isEnabled = false
        }
        promoObject = nil
        currentPromoCode = ""
        for icon in promoVerificationIcon {
            icon.image = nil
        }
        
        selectedSeats?.removeAll()
        collectionView.reloadData()
        
        if blockObject == nil {
            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                selectedSeats?.removeAll()
            } else {
                freeSeatsCount = 1
            }
        }
        reloadPrice()
        seatMapViewController?.clearSeatmap()
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.hideBanner()
        appDelegate.showBanner(message: "Loading seat map...", isPersistent: true, color: UIColor(netHex: 0x26c5a9), textColor: UIColor.white)
        self.view.addSubview(loadingScreen!)
        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                  ])
        
        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
        
        if blockObject == nil {
            
            if selectedTime == nil {
                appDelegate.hideBanner()
                self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
                return
            }
            
            GMoviesAPIManager.getMovieSeatMap(scheduleId: selectedTime!.scheduleId!, theaterId: theaterObject!.theaterId!, cinemaId: "\(selectedCinema!.cinemaId!)", time: selectedTime!.timeId!) { (error, result) in
                if error == nil {

                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        let rawSeatmap : [[String]] = result?.value(forKeyPath: "data.seatmap") as! [[String]]
                        if rawSeatmap.count > 0 {
                            self.seatMapViewController?.setSeatMapData(rawData: result?.object(forKey: "data") as! NSDictionary)
                        } else {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                                self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
                            }
                        }
                    } else {
                        
                        //Parse error message
                        let htmlMessage:String? = result!.value(forKey: "message") as? String
                        
                        var messageTitle: String = "Oops!"
                        var messageBody: String = "Something went wrong."
                        
                        if htmlMessage != nil {
                            do {
                                let doc: Document = try! SwiftSoup.parse(htmlMessage!)
                                let title: Element = try! doc.select("b").first()!
                                messageTitle = try! title.text()
                                
                                let messageArray = htmlMessage!.components(separatedBy: "\n")
                                messageBody = messageArray[1]
                            }catch Exception.Error( _, _){
                                
                            }catch {
                                
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            self.loadErrorView(titleString: messageTitle, description: messageBody)
                        }
                        print(result?.value(forKey: "message") ?? "")
                        
                    }
                    
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        appDelegate.hideBanner()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        appDelegate.showBanner(message: "Network Error", isPersistent: false)
                        self.errorView?.retryButton.addTarget(self, action: #selector(self.getSeatMap), for: .touchUpInside)
                    }
                }
            }
        } else {
            GMoviesAPIManager.getMovieSeatMap(promoId : blockObject!.promoId!) { (error, result) in
                if error == nil {
                    //print(result)
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        
                        self.blockObject?.theater?.convenienceFees = result?.value(forKeyPath: "data.convenience_fees") as! NSDictionary?
                        self.blockObject?.theater?.convenienceFeesMessage = result?.value(forKeyPath: "data.convenience_fees_message") as! NSDictionary?
                        let paymentOptions : [NSDictionary] = result?.value(forKeyPath: "data.payment_options") as! [NSDictionary]
                        
                        self.blockObject?.theater?.paymentOptions?.removeAll()
                        for p : NSDictionary in paymentOptions {
                            let po : PaymentOptionModel = PaymentOptionModel(data: p)
                            //print(p)
                            self.blockObject?.theater?.paymentOptions?.append(po)
                        }
                        
                        let rawSeatmap : [[String]] = result?.value(forKeyPath: "data.seatmap") as! [[String]]
                        if rawSeatmap.count > 0 {
                            self.seatMapViewController?.setSeatMapData(rawData: result?.object(forKey: "data") as! NSDictionary)
                        } else {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                                self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
                            }
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
                        }
                        print(result?.value(forKey: "message") ?? "")
                    }

                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        appDelegate.hideBanner()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                    }
                    appDelegate.showBanner(message: "Network Error", isPersistent: false)
                    self.errorView!.retryButton.addTarget(self, action: #selector(self.getSeatMap), for: .touchUpInside)
                    
                }
            }
        }
    }
    
    private func loadErrorView(titleString : String, description : String, image: UIImage = UIImage(named: "error-popcorn")!) {
        
//        ticketPriceHeight.constant = 0.0
//        selectedSeatsHeight.constant = 0.0
        contentView.layoutIfNeeded()
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = contentView!
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.contentView!.addSubview(errorView!)
        self.contentView.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }
    
    private func reloadPrice() {
        if selectedTime == nil && blockObject == nil {
            for label in ticketPrice {
                label.text = "Php 0.00"
            }
            self.loadErrorView(titleString: "Oops!", description: "Something went wrong.")
            return
        } else if selectedTime != nil {
            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                let price : Int = (selectedTime?.price)! * (selectedSeats?.count)!
                for label in ticketPrice {
                    label.text = "Php \(price).00"
                }
            } else {
                let price : Int = (selectedTime?.price)! * freeSeatsCount
                for label in ticketPrice {
                    label.text = "Php \(price).00"
                }
            }
        } else if blockObject != nil {
            //print(blockObject!.price!)
            let price : Int = Int(Float(blockObject!.price!)!) * (selectedSeats?.count)!
            for label in ticketPrice {
                label.text = "Php \(price).00"
            }
        }
    }
    
    //MARK: Seatmap Delegate
    
    func didSelectSeat(sender s: SeatMapViewController, seatmapObject so: SeatModel) -> Bool {
        
        if (selectedSeats?.contains(so))! == false {
            if (selectedSeats?.count)! == maxSeats {
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.hideBanner()
                appDelegate.showBanner(message: "Up to \(maxSeats) seat\(maxSeats == 1 ? "" : "s") per transaction only", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
                self.collectionView.reloadData()
                return false
            }
            selectedSeats?.append(so)
            if !currentPromoCode.isEmpty {
                self.verifyPromo(code: currentPromoCode, completion: { (verified) in
                    
                })
            }
            for inputPromo in self.promoCodeTextField {
                inputPromo.isEnabled = true
            }
        } else {
            self.collectionView.reloadData()
            return false
        }
        
        selectedSeats = selectedSeats?.sorted(by: { (s1, s2) -> Bool in
            return s1.seatName!.localizedStandardCompare(s2.seatName!) == .orderedAscending
        })
        
        let index = selectedSeats?.index(where: { (s) -> Bool in
            return s.seatId == so.seatId
        })
        
//        s.view.isUserInteractionEnabled = false
//        collectionView.performBatchUpdates({
            self.collectionView.insertItems(at: [IndexPath(row: index!, section: 0)])
//        }, completion: { (_) in
//            s.view.isUserInteractionEnabled = true
//        })
        
        reloadPrice()
        
        return true
    }
    
    func didDeselectSeat(sender s: SeatMapViewController, seatmapObject so: SeatModel) {
        
        let index : Int? = selectedSeats?.index(where: { (s) -> Bool in
            return s.seatId == so.seatId
        })
        
        //print(so.seatId, index)
        
        if index != nil {
            self.selectedSeats = self.selectedSeats?.filter({ (s) -> Bool in
                
                return s.seatId! != so.seatId!
            })
            if !currentPromoCode.isEmpty {
                self.verifyPromo(code: self.currentPromoCode, completion: { (verified) in
                    
                })
            }else {
                for inputPromo in self.promoCodeTextField {
                    inputPromo.isEnabled = false
                }
            }
            
//            s.view.isUserInteractionEnabled = false
//            collectionView.performBatchUpdates({
                self.collectionView.deleteItems(at: [IndexPath(row: index!, section: 0)])
//                }) { (_) in
//                    s.view.isUserInteractionEnabled = true
//            }
        } else {
            self.collectionView.reloadData()
            print("nil")
        }
        
        reloadPrice()
    }
    
    //MARK: Calendar Delegate
    
    func didSelectDate(_ sender: CalendarViewController, selectedDate d: Date) {
        
    }
    
    func didDeselectDate(_ sender: CalendarViewController, deselectedDate d: Date) {
        
    }
    
    func didFinishedSelectingDate(_ sender: CalendarViewController, selectedDate d : Date) {
        selectedDate = d
        dateButton.setTitle(UIManager.seatmapDateFormatter(date: d), for: .normal)
        
        let dateId : Int = UIManager.generateDateId(date: self.selectedDate!)
        self.selectedCinema = ((self.theaterObject?.cinemas?.first(where: { (c) -> Bool in
            return c.dateId == dateId
        }))! as CinemaModel)
        
        self.selectedTime = (self.selectedCinema?.times?.first(where: { (t) -> Bool in
            return t.dateCinemaId == self.selectedCinema?.dateCinemaId
        }))! as TimeModel
        
        if let message = selectedTime?.popcornMessage {
            if !message.isEmpty {
                showAlert(title: "GMovies", message: message, cancelText: "OK")
            }
        }
        
        
        if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
            self.transactionWithSeatmap.isHidden = false
            self.transactionWithoutSeatmap.isHidden = true
        }else {
            self.transactionWithSeatmap.isHidden = true
            self.transactionWithoutSeatmap.isHidden = false
        }
        
        let cinemaName : String = self.selectedCinema!.name!
        
        self.cinemaButton.setTitle(cinemaName, for: .normal)
        
        let string : NSString = NSString(string: "\(selectedTime!.name!) \(selectedTime!.variantString)")
        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName : timeButton.titleLabel!.font, NSForegroundColorAttributeName : timeButton.titleLabel!.textColor])
        attributedString.addAttributes([NSForegroundColorAttributeName : UIColor(netHex: 0x00A2FF)], range: string.range(of: (selectedTime?.variantString)!))
        
        timeButton.setAttributedTitle(attributedString, for: .normal)
        
        currentPopView!.hide()
        currentPopView = nil
        
        if theaterObject?.allowReseravation == true {
            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                getSeatMap()
            } else {
                showFreeSeating()
            }
        } else {
            reloadPrice()
        }
    }
    
    //MARK: Cinema list delegate
    
    func didSelectCinema(cinema: CinemaModel) {
        selectedCinema = cinema
        cinemaButton.setTitle(selectedCinema?.name!, for: .normal)
        
        self.selectedTime = (self.selectedCinema?.times?.first(where: { (t) -> Bool in
            return t.dateCinemaId == self.selectedCinema?.dateCinemaId
        }))! as TimeModel
        
        if let message = selectedTime?.popcornMessage {
            if !message.isEmpty {
                showAlert(title: "GMovies", message: message, cancelText: "OK")
            }
        }
        
        if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
            self.transactionWithSeatmap.isHidden = false
            self.transactionWithoutSeatmap.isHidden = true
        }else {
            self.transactionWithSeatmap.isHidden = true
            self.transactionWithoutSeatmap.isHidden = false
        }
        
        let string : NSString = NSString(string: "\(selectedTime!.name!) \(selectedTime!.variantString)")
        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName : timeButton.titleLabel!.font, NSForegroundColorAttributeName : timeButton.titleLabel!.textColor])
        attributedString.addAttributes([NSForegroundColorAttributeName : UIColor(netHex: 0x00A2FF)], range: string.range(of: (selectedTime?.variantString)!))
        
        timeButton.setAttributedTitle(attributedString, for: .normal)
        
        currentPopView!.hide()
        currentPopView = nil
        
        if theaterObject?.allowReseravation == true {
            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                getSeatMap()
            } else {
                showFreeSeating()
            }
        }
    }
    
    //MARK: Time list delegate
    
    func didSelectTime(time: TimeModel) {
        selectedTime = time
        
        if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
            self.transactionWithSeatmap.isHidden = false
            self.transactionWithoutSeatmap.isHidden = true
        }else {
            self.transactionWithSeatmap.isHidden = true
            self.transactionWithoutSeatmap.isHidden = false
        }
        
        let string : NSString = NSString(string: "\(selectedTime!.name!) \(selectedTime!.variantString)")
        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName : timeButton.titleLabel!.font, NSForegroundColorAttributeName : timeButton.titleLabel!.textColor])
        attributedString.addAttributes([NSForegroundColorAttributeName : UIColor(netHex: 0x00A2FF)], range: string.range(of: (selectedTime?.variantString)!))
        
        timeButton.setAttributedTitle(attributedString, for: .normal)
        currentPopView!.hide()
        currentPopView = nil
        
        if theaterObject?.allowReseravation == true {
            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                getSeatMap()
            } else {
                showFreeSeating()
            }
        }
    }    
    
    //MARK: Collection view delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (selectedSeats?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : UICollectionViewCell? = nil
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seatCell", for: indexPath)
        
        configureCell(cell: cell as! SeatCell, indexPath: indexPath)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.300, delay: 0.0, options: .curveEaseOut, animations: {
            cell.alpha = 1.0
            }, completion: nil)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //3"5
        if UIScreen.main.bounds.maxY == 480 {
            return CGSize(width: 110, height: 110)
        }
        //4"0
        if UIScreen.main.bounds.maxY == 568 {
            return CGSize(width: 25, height: 20)
        }
        //4"7
        if UIScreen.main.bounds.maxY == 667 {
            return CGSize(width: 30, height: 25)
        }
        //5"5
        if UIScreen.main.bounds.maxY == 736 {
            return CGSize(width: 30, height: 25)
        }
        
        return CGSize(width: 30, height: 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    private func configureCell(cell : SeatCell, indexPath : IndexPath) {
        let seat : SeatModel = selectedSeats![indexPath.row]
        
        cell.seatName.text = seat.seatName
    }
    
    @IBAction func buyTicketsAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "buy_button")
        
        GAnalyticsHelper.sendEvent(category: GAEventCategory.BuyTicket, action: movieObject!.movieTitle!)
        
        if blockObject == nil {
            if theaterObject?.allowReseravation == false {
                //                self.loadErrorView()
                
                return
            }
            
            if self.selectedTime != nil {
                if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                    if (selectedSeats?.count)! == 0 {
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showBanner(message: "Please select your seats", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
                        return
                    }
                }
            } else {
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBanner(message: "Please select time", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
                return
            }
            
        } else {
            
            if (selectedSeats?.count)! == 0 {
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBanner(message: "Please select your seats", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
                return
            }
        }
        
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showLoginModal(previousViewController: nil, nextViewController: nil, showFrom: self)
            return
        }
        
        if currentPromoCode.isEmpty {
            
            self.promoObject = nil
            let pvc = (self.parent as! ProgressNavigationViewController).viewControllers?[1] as! PaymentViewController
            pvc.clearCurrentSelectedPayment()
            (self.parent as! ProgressNavigationViewController).setCurrentViewController(index: 1, animated: true)
        }else {
            self.verifyPromo(code: currentPromoCode, showAlertPopup: true, completion: { (verified) -> () in
                
                if verified {
                    
                    let pvc = (self.parent as! ProgressNavigationViewController).viewControllers?[1] as! PaymentViewController
                    pvc.clearCurrentSelectedPayment()
                    (self.parent as! ProgressNavigationViewController).setCurrentViewController(index: 1, animated: true)
                }
            })
        }
    }
    
    func showAlert(title: String, message: String, cancelText: String) {
        let popup = PopupDialog(title: title, message: message)
        let vc = popup.viewController as! PopupDialogDefaultViewController
        
        vc.titleFont = UIFont(name: "OpenSans", size: 16)!
        vc.titleColor = #colorLiteral(red: 0.3365190625, green: 0.3370802701, blue: 0.3537014127, alpha: 1)
        vc.titleTextAlignment = .center
        vc.messageFont = UIFont(name: "OpenSans", size: 14)!
        vc.messageColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        vc.messageTextAlignment = .center
        
        let cancelButton = CancelButton(title: cancelText, height: 50) {
            
        }
        
        popup.addButtons([cancelButton])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    //MARK: Progress nav delegate
    
    func willSelectViewController(navController: ProgressNavigationProtocol, destinationController: UIViewController) {
        if destinationController.isKind(of: PaymentViewController.self) == false {
            return
        }
        
        if blockObject != nil {
            
            (destinationController as! PaymentViewController).blockObject = blockObject
            (destinationController as! PaymentViewController).selectedSeatsController = self
            (destinationController as! PaymentViewController).promoObject = promoObject
            
            return
        }
        
        (destinationController as! PaymentViewController).dateObject = selectedDate
        (destinationController as! PaymentViewController).cinemaObject = selectedCinema
        (destinationController as! PaymentViewController).timeObject = selectedTime
        (destinationController as! PaymentViewController).selectedSeatsController = self
        (destinationController as! PaymentViewController).movieObject = movieObject
        (destinationController as! PaymentViewController).theaterObject = theaterObject
        (destinationController as! PaymentViewController).promoObject = promoObject
        
        if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") != ComparisonResult.orderedSame {
            (destinationController as! PaymentViewController).seatCount = freeSeatsCount
            
        }
    }
}

extension SeatSelectionViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.hideBanner()
        for icon in promoVerificationIcon {
            icon.image = nil
        }
        
        timer?.invalidate()
        timer = nil
        ticker = 0
        
        if string == "" && textField.text?.characters.count == 1 {
            currentPromoCode = ""
            promoObject = nil
            
            var seatCount = 0
            if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
                seatCount = (self.selectedSeats?.count)!
            }else {
                seatCount = freeSeatsCount
            }
            
            if seatCount == 0 {
                textField.text = ""
                for promoItem in promoCodeTextField {
                    promoItem.isEnabled = false
                }
                return false
            }
            
            return true
        }
        
        if string == "" {
            textField.deleteBackward()
        } else {
            textField.insertText(string.uppercased())
        }
        currentPromoCode = textField.text!
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        return true
    }
    
    @objc private func tick() {
        self.ticker += 1
        
        if self.ticker == 2 {
            self.timer?.invalidate()
            self.ticker = 0
            
            DispatchQueue.main.async {
                self.verifyPromo(code: self.currentPromoCode, completion: { (verified) -> () in
                    
                })
            }
        }
    }
}

extension SeatSelectionViewController
{
    func verifyPromo(code: String, showAlertPopup: Bool = false, completion: @escaping (Bool)->()) {
        
        var seatCount = 0
        if self.selectedTime!.seatingType?.caseInsensitiveCompare("reserved seating") == ComparisonResult.orderedSame {
            seatCount = (self.selectedSeats?.count)!
        }else {
            seatCount = freeSeatsCount
        }
        
        GMoviesAPIManager.verifyPromoCode2(mobile: AccountHelper.shared.currentUser!.mobileNumber!, userId: AccountHelper.shared.currentUser!.uuid!, promoCode: currentPromoCode, theaterId: theaterObject!.theaterId!, seatCount: seatCount, movie: movieObject!.movieTitle!, ticketPrice: 1, rushId: AccountHelper.shared.currentUser?.rushId ?? "", variant: selectedTime!.variantString, completion: { (error, result) in
            
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            if !showAlertPopup {
                appDelegate.hideBanner(animated: false)
            }
            
            if error == nil {
                
                let status = result?.value(forKeyPath: "response.status") as! Int
                if status == 1 {
                    
                    let details = result?.value(forKeyPath: "response.details") as! NSDictionary
                    self.promoObject = PromoModel(data: details)
                    self.promoObject?.code = self.currentPromoCode
                    
                    
                    DispatchQueue.main.async {
                        if (!showAlertPopup){
                            appDelegate.showBanner(message: "Promo code is valid", isPersistent: false, color: #colorLiteral(red: 0.2745098039, green: 0.5882352941, blue: 0.8980392157, alpha: 1), textColor: UIColor.white)
                        }
                        for icon in self.promoVerificationIcon {
                            icon.image = UIImage(named: "verify-correct")
                        }
                        completion(true)
                    }
                }else {
                    
                    self.promoObject = nil
                    
                    let msg = result?.value(forKeyPath: "response.details.msg") as! String
                    
                    DispatchQueue.main.async {
                        if showAlertPopup {
                            self.showAlert(title: "Error", message: msg, cancelText: "OK")
                        }else {
                            appDelegate.showBanner(message: msg, isPersistent: false, color: #colorLiteral(red: 0.3365190625, green: 0.3370802701, blue: 0.3537014127, alpha: 1), textColor: UIColor.white)
                        }
                        for icon in self.promoVerificationIcon {
                            icon.image = UIImage(named: "verify-wrong")
                        }
                        completion(false)
                    }
                }
                
            }else {

                self.promoObject = nil
                
                if showAlertPopup {
                    DispatchQueue.main.async {
                        self.showAlert(title: "Opps", message: "Something went wrong.", cancelText: "OK")
                    }
                }else {
                    DispatchQueue.main.async {
                        appDelegate.showBanner(message: "Something went wrong.", isPersistent: false, color: #colorLiteral(red: 0.3365190625, green: 0.3370802701, blue: 0.3537014127, alpha: 1), textColor: UIColor.white)
                    }
                }
                for icon in self.promoVerificationIcon {
                    icon.image = UIImage(named: "verify-wrong")
                }
                completion(false)
            }
        })
    }
}
