//
//  AccountHelper.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 10/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation

public class GMoviesUser: NSObject {
    
    var rushId:String?
    var lbid:String?
    var uuid:String?
    
    var fullName:String?
    var firstName:String?
    var lastName:String?
    var mobileNumber:String?
    var email:String?
    var birthdate:String?
    var fbid:String?
    var photo:Dictionary<String,String>?
    
    func serialize() -> Dictionary<String, Any> {
        var dict = Dictionary<String,Any>()
        
        if rushId != nil {
            dict.updateValue(rushId!, forKey: "rush_id")
        }
        if lbid != nil {
            dict.updateValue(lbid!, forKey: "lbid")
        }
        if uuid != nil {
            dict.updateValue(uuid!, forKey: "uuid")
        }
        if fullName != nil {
            dict.updateValue(fullName!, forKey: "fullname")
        }
        if firstName != nil {
            dict.updateValue(firstName!, forKey: "first_name")
        }
        if lastName != nil {
            dict.updateValue(lastName!, forKey: "last_name")
        }
        if mobileNumber != nil {
            dict.updateValue(mobileNumber!, forKey: "mobile")
        }
        if email != nil {
            dict.updateValue(email!, forKey: "email")
        }
        if birthdate != nil {
            dict.updateValue(birthdate!, forKey: "bday")
        }
        if fbid != nil {
            dict.updateValue(fbid!, forKey: "fbid")
        }
        if photo != nil {
            dict.updateValue(photo!, forKey: "photo")
        }
        
        return dict
    }
    
    func deserialize(data: Dictionary<String, Any>) {

        self.rushId = data["rush_id"] as? String
        self.lbid = data["lbid"] as? String
        self.uuid = data["uuid"] as? String
        self.fullName = data["fullname"] as? String
        self.firstName = data["first_name"] as? String
        self.lastName = data["last_name"] as? String
        self.mobileNumber = data["mobile"] as? String
        self.email = data["email"] as? String
        self.birthdate = data["bday"] as? String
        self.fbid = data["fbid"] as? String
        self.photo = data["photo"] as? Dictionary
    }
}

class AccountHelper
{
    
    public typealias RequestSuccessBlock = (NSDictionary?) -> Void
    public typealias RequestFailureBlock = (String) -> Void
    public let NotificationDidLoginToGMovies = Notification.Name("NotificationDidLoginToGMovies")
    public let NotificationDidRegisterToRush = Notification.Name("NotificationDidRegisterToRush")
    
    fileprivate let DEVICE_ID_KEY = "device_id"
    fileprivate let IS_LOGGED_IN_GMOVIES_KEY = "loggedin"
    fileprivate let IS_REGISTERED_IN_RUSH_KEY = "registered_rush"
    fileprivate let USER_SAVE_KEY = "user"
    fileprivate let DID_SHOW_JOIN_LOYALTY_PROGRAM_KEY = "did_show_join_loyalty_program"
    
    fileprivate var user:GMoviesUser?
    
    class var shared : AccountHelper {
        struct Static {
            static let instance : AccountHelper = AccountHelper()
        }
        return Static.instance
    }
    
    init() {
        if isLoggedInGMovies {
            //Load cached user
            let cachedUser: Dictionary? = UserDefaults.standard.dictionary(forKey: USER_SAVE_KEY)
            if cachedUser == nil {
                //Invalidate login
                UserDefaults.standard.set(false, forKey: IS_LOGGED_IN_GMOVIES_KEY)
                UserDefaults.standard.set(false, forKey: IS_REGISTERED_IN_RUSH_KEY)
                UserDefaults.standard.synchronize()
            }else {
                let user = GMoviesUser()
                user.deserialize(data: cachedUser!)
                self.user = user
            }
        }
    }
}

extension AccountHelper
{
    var currentUser: GMoviesUser? {
        return user
    }
    
    var isLoggedInGMovies: Bool {
        return UserDefaults.standard.bool(forKey: IS_LOGGED_IN_GMOVIES_KEY)
    }
    
    var isRegisteredInRush: Bool {
        return UserDefaults.standard.bool(forKey: IS_REGISTERED_IN_RUSH_KEY)
    }
    
    var didShowJoinLoyaltyProgram: Bool {
        return UserDefaults.standard.bool(forKey: DID_SHOW_JOIN_LOYALTY_PROGRAM_KEY)
    }
    
    func setDidShowJoinLoyaltyProgram(show: Bool) {
        UserDefaults.standard.set(show, forKey: DID_SHOW_JOIN_LOYALTY_PROGRAM_KEY)
    }
}

extension AccountHelper
{
    
    func logout ()
    {
        UserDefaults.standard.set(false, forKey: IS_LOGGED_IN_GMOVIES_KEY)
        UserDefaults.standard.set(false, forKey: IS_REGISTERED_IN_RUSH_KEY)
        UserDefaults.standard.set(nil, forKey: USER_SAVE_KEY)
        //UserDefaults.standard.set(false, forKey: DID_SHOW_JOIN_LOYALTY_PROGRAM_KEY)
        
        UserDefaults.standard.synchronize()
        self.user = nil
    }
    
    //11 digit mobile number starts with 0
    func requestOTP(mobileNumber: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        GMoviesAPIManager.login(mobileNum: mobileNumber, smsEmail: "sms") { (error, result) in
            
            if error == nil {
                let status : Int = result?.value(forKeyPath: "response.status") as! Int
                let msg : String = result?.value(forKeyPath: "response.msg") as! String
                if status == 0 {
                    DispatchQueue.main.async {
                        failure(msg)
                    }
                } else {
                    DispatchQueue.main.async {
                        successful(nil)
                    }
                }
            }else {
                DispatchQueue.main.async {
                    failure(error!.localizedDescription)
                }
            }
        }
    }
    
    //11 digit mobile number starts with 0
    func login(mobileNumber: String, otp: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {

        GMoviesAPIManager.verifyOTP(mobileNum: mobileNumber, pin: otp) { (error, result) in
            
            if error == nil {
                
                let status : Int = result?.value(forKeyPath: "response.status") as! Int
                if status == 1 {
                    //Successfully logged in
                    
                    let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                    self.saveUser(data: userData)
                    
                    UserDefaults.standard.set(true, forKey: self.IS_LOGGED_IN_GMOVIES_KEY)
                    NotificationCenter.default.post(name: self.NotificationDidLoginToGMovies, object: nil)
                    
                    let rushUserData = result?.value(forKeyPath: "response.rush") as? NSDictionary
                    
                    if let responseCode = rushUserData?.value(forKey: "error_code") as? String {
                        if responseCode == RushAPIResponseCode.successful.rawValue {
                            self.currentUser?.rushId = rushUserData?.value(forKeyPath: "data.uuid") as? String
                            UserDefaults.standard.set(true, forKey: self.IS_REGISTERED_IN_RUSH_KEY)
                            self.saveUser()
                            NotificationCenter.default.post(name: self.NotificationDidRegisterToRush, object: nil)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        successful(nil)
                    }
                    
                }else {
                    DispatchQueue.main.async {
                        failure("")
                    }
                }
            }else {
                DispatchQueue.main.async {
                    failure(error!.localizedDescription)
                }
            }
        }
    }
    
    func loginFb(fbid: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        GMoviesAPIManager.verifyFbId(fbid: fbid) { (error, result) in
            
        }
    }
    
    func register(mobile:String, otp:String, firstName:String, lastName:String, email:String, fbid:String = "0", loyalty:Bool, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock)
    {
        GMoviesAPIManager.register(mobileNum: mobile, pin: otp, firstName: firstName, lastName: lastName, email: email, fbid: fbid, loyalty: loyalty) { (error, result) in
            
            if error == nil {
                
                let status : Int = result?.value(forKeyPath: "response.status") as! Int
                if status == 1 {
                    //Successfully logged in
                    
                    let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                    self.saveUser(data: userData)
                    
                    UserDefaults.standard.set(true, forKey: self.IS_LOGGED_IN_GMOVIES_KEY)
                    UserDefaults.standard.synchronize()
                    
                    NotificationCenter.default.post(name: self.NotificationDidLoginToGMovies, object: nil)
                    
                    let rushUserData = result?.value(forKeyPath: "response.rush") as? NSDictionary
                    
                    if let responseCode = rushUserData?.value(forKey: "error_code") as? String {
                        if responseCode == RushAPIResponseCode.successful.rawValue {
                            self.currentUser?.rushId = rushUserData?.value(forKeyPath: "data.uuid") as? String
                            UserDefaults.standard.set(true, forKey: self.IS_REGISTERED_IN_RUSH_KEY)
                            self.saveUser()
                            NotificationCenter.default.post(name: self.NotificationDidRegisterToRush, object: nil)
                            
                            let rushPoints = result?.value(forKeyPath: "response.rush_points") as? NSDictionary
                            let badge = result?.value(forKeyPath: "response.badges") as? NSDictionary
                            
                            if rushPoints != nil && badge != nil {
                                
                                let earnedPoints = (rushPoints?.value(forKey: "points_earned") as? NSNumber)?.intValue ?? 0
                                let availablepoints = (rushPoints?.value(forKey: "user_points") as? NSNumber)?.intValue ?? 0
                                
                                let badgeName = badge?.value(forKey: "badge_name") as? String ?? "empty"
                                let badgeIconUrl = badge?.value(forKey: "badge_icon") as? String ?? "empty"
                                let groupBadgeIconUrl = badge?.value(forKey: "group_badge_icon") as? String ?? "empty"
                                
                                UserEngagementHelper.shared.present(earnedPoints: earnedPoints, availablePoints: availablepoints, badgeName: badgeName, badgeIconUrl: badgeIconUrl, groupBadgeIconUrl: groupBadgeIconUrl)
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        successful(nil)
                    }
                    
                }else {
                    DispatchQueue.main.async {
                        failure("")
                    }
                }
            }else {
                DispatchQueue.main.async {
                    failure(error!.localizedDescription)
                }
            }
        }
    }
    
    func registerToRush(successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        if !isLoggedInGMovies {
            print("Rush Registration Error: Login to GMovies first")
            return
        }
        
        RushAPI.shared.register(lbid: Constants.LBAPIToken, name: user!.fullName!, email: user!.email!, mobileNumber: user!.mobileNumber!, successful: { (response) in
            
            if response.statusCode == RushAPIStatusCode.successful {
                
                let status = (response.responseData! as NSDictionary).value(forKeyPath: "response.status") as! Int
                if status == 1 {
                    
                    let rushUserData = (response.responseData! as NSDictionary).value(forKeyPath: "response.rush") as? NSDictionary
                    
                    if rushUserData != nil {
                        let responseCode = rushUserData!.value(forKey: "error_code") as! String
                        
                        if responseCode == RushAPIResponseCode.successful.rawValue {
                            
                            let userData = rushUserData!.value(forKey: "data") as! NSDictionary
                            self.user?.rushId = userData.value(forKey: "uuid") as? String
                            self.saveUser()
                            UserDefaults.standard.set(true, forKey: self.IS_REGISTERED_IN_RUSH_KEY)
                            UserDefaults.standard.synchronize()
                            
                            let rushPoints = (response.responseData! as NSDictionary).value(forKeyPath: "response.rush_points") as? NSDictionary
                            let badge = (response.responseData! as NSDictionary).value(forKeyPath: "response.badges") as? NSDictionary
                            
                            let data: NSMutableDictionary = [:]
                            if rushPoints != nil && badge != nil {
                                
                                let earnedPoints = (rushPoints?.value(forKey: "points_earned") as? NSNumber)?.intValue ?? 0
                                let availablepoints = (rushPoints?.value(forKey: "user_points") as? NSNumber)?.intValue ?? 0
                                
                                let badgeName = badge?.value(forKey: "badge_name") as? String ?? "empty"
                                let badgeIconUrl = badge?.value(forKey: "badge_icon") as? String ?? "empty"
                                let groupBadgeIconUrl = badge?.value(forKey: "group_badge_icon") as? String ?? "empty"
                                
                                UserEngagementHelper.shared.present(earnedPoints: earnedPoints, availablePoints: availablepoints, badgeName: badgeName, badgeIconUrl: badgeIconUrl, groupBadgeIconUrl: groupBadgeIconUrl)
                            }
                            
                            DispatchQueue.main.async {
                                successful(data)
                            }
                        }else {
                            let errorMsg = (response.responseData! as NSDictionary).value(forKeyPath: "response.msg") as? String ?? "Error occured. Please try again."
                            DispatchQueue.main.async {
                                failure(errorMsg)
                            }
                        }
                    }else {
                        
                        DispatchQueue.main.async {
                            failure("Error occured. Please try again.")
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        failure("Error occured. Please try again.")
                    }
                }
                
            } else {
                print("Rush Registration Error: \(response.statusCode.message)")
                DispatchQueue.main.async {
                    failure("Error occured. Please try again.")
                }
            }
            
        }) { (error) in
            print("Rush Registration Error: \(error)")
            DispatchQueue.main.async {
                failure(error)
            }
        }
    }
}

extension AccountHelper
{
    func saveUser () {
        //Save user
        UserDefaults.standard.set(self.user?.serialize(), forKey: self.USER_SAVE_KEY)
        UserDefaults.standard.synchronize()
    }
    
    func saveUser(data: NSDictionary) {
        
        let mobile = data.value(forKey: "mobile") as! String
        let birthdate = data.value(forKey: "bday") as! String
        let fullName = data.value(forKey: "fullname") as! String
        let firstName = data.value(forKey: "first_name") as! String
        let lastName = data.value(forKey: "last_name") as! String
        let email = data.value(forKey: "email") as! String
        let fbid = data.value(forKey: "fbid") as! String
        
        let uuid = data.value(forKeyPath: "uuid") as! String
        let lbid = data.value(forKeyPath: "lbid") as! String
        let photo = data.value(forKeyPath: "photo") as! Dictionary<String,String>
        
        let user = GMoviesUser()
        user.mobileNumber = mobile
        user.birthdate = birthdate
        user.fullName = fullName
        user.firstName = firstName
        user.lastName = lastName
        user.email = email
        user.fbid = fbid
        user.uuid = uuid
        user.lbid = lbid
        user.photo = photo
        
        self.user = user
        
        UserDefaults.standard.set(self.user?.serialize(), forKey: self.USER_SAVE_KEY)
        UserDefaults.standard.synchronize()
    }
}
