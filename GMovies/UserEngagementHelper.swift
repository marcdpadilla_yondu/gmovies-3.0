//
//  UserEngagmentHelper.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 19/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation
import PopupDialog

struct UserEngagementCodes {
    
    static let inAppFeedback = "in-app-feedback"
    static let addWatchList = "add-watch-list"
    static let choosePreferredCinema = "choose-preffered-cinema"
    static let signUp = "sign-up"
    static let shareFbPage = "share-fb-page"
    static let shareOurBlogReelNews = "share-our-blog-reel-news"
}

class UserEngagementHelper {
    
    
    fileprivate lazy var viewControllerForPresentation: UIViewController = {
        let vc = UIViewController()
        vc.view.backgroundColor = UIColor.clear
        vc.view.isOpaque = false
        return vc
    }()
    
    fileprivate var earnedBadgeVC: EarnedPointsViewController?
    fileprivate var popupDialog: PopupDialog?
    
    
    class var shared : UserEngagementHelper {
        struct Static {
            static let instance : UserEngagementHelper = UserEngagementHelper()
        }
        return Static.instance
    }
    
    func earnBadgeFrom(engagement code:String, hideAvailablePoints:Bool = false) {
        
        if AccountHelper.shared.isRegisteredInRush {
            RushAPI.shared.getEarnedPointsFrom(lbid: Constants.LBAPIToken, rushId: AccountHelper.shared.currentUser!.rushId!, userEngament: code, successful: { (response) in
                
                guard response.statusCode == RushAPIStatusCode.successful else {
                    return
                }
                
                let responseData = (response.responseData! as NSDictionary)
                
                guard responseData.value(forKey: "status") as? Int == 1 else {
                    return
                }
                
                guard responseData.value(forKeyPath: "details.error_code") as? String == RushAPIResponseCode.successful.rawValue else {
                    return
                }
                
                guard let rushPoints = responseData.value(forKeyPath: "details.rush_points") as? NSDictionary else {
                    return
                }
                
                guard let badge = responseData.value(forKeyPath: "details.badges") as? NSDictionary else {
                    return
                }
                
                let earnedPoints = (rushPoints.value(forKey: "points_earned") as? NSNumber)?.intValue ?? 0
                let availablepoints = (rushPoints.value(forKey: "user_points") as? NSNumber)?.intValue ?? 0
                
                let badgeName = badge.value(forKey: "badge_name") as? String ?? "empty"
                let badgeIconUrl = badge.value(forKey: "badge_icon") as? String ?? "empty"
                let groupBadgeIconUrl = badge.value(forKey: "group_badge_icon") as? String ?? "empty"
                
                self.present(earnedPoints: earnedPoints, availablePoints: availablepoints, badgeName: badgeName, badgeIconUrl: badgeIconUrl, groupBadgeIconUrl: groupBadgeIconUrl, hideAvailablePoints: hideAvailablePoints
                )
                
            }, failure: { (error) in
                
            })
        }
    }
    
    func present (earnedPoints: Int, availablePoints: Int, badgeName: String, badgeIconUrl: String, groupBadgeIconUrl: String, hideAvailablePoints:Bool = false) {
        
        earnedBadgeVC = EarnedPointsViewController(nibName: "EarnedPointsViewController", bundle: nil)
        earnedBadgeVC?.earnedPoints = String(earnedPoints)
        earnedBadgeVC?.availablePoints = String(availablePoints)
        earnedBadgeVC?.badgeTitleText = badgeName
        earnedBadgeVC?.badgeIconUrl = badgeIconUrl
        earnedBadgeVC?.badgeGroupIconUrl = groupBadgeIconUrl
        earnedBadgeVC?.shouldHideAvailablePoints = hideAvailablePoints
        earnedBadgeVC?.delegate = self
        
        popupDialog = PopupDialog(viewController: earnedBadgeVC!, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
        
        let mainWindow = UIApplication.shared.keyWindow
        mainWindow?.addSubview((viewControllerForPresentation.view)!)
        
        //Make your transparent view controller present your actual view controller
        viewControllerForPresentation.present(popupDialog!, animated: true)
    }
}


extension UserEngagementHelper: EarnedPointsViewControllerDelegate {
    
    func onOkClicked() {
        viewControllerForPresentation.view.removeFromSuperview()
        popupDialog?.dismiss()
        popupDialog = nil
    }
}
