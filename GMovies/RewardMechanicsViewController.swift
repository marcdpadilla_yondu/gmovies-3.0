//
//  RewardMechanicsViewController.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 16/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

class RewardMechanicsViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var urlString: String = ""
        //http://gmovies.web.dev.digitalventures.ph/iframe/rush-faq
        switch Constants.BUILD_TYPE {
        case .Development:
            urlString = "http://www.gmovies.ph/iframe/rush-faq"
            break
        case .QualityAssurance:
            urlString = "http://www.gmovies.ph/iframe/rush-faq"
            break
        case .Production:
            urlString = "http://www.gmovies.ph/iframe/rush-faq"
            break
        }
        
        let url = URL(string: urlString)
        webView.loadRequest(URLRequest(url: url!))
    }
}
