//
//  RushLoyaltyCustomerAPI.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 24/02/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation


/** Status Codes */
public enum RushLoyaltyCustomerAPIStatusCode: Int {
    
    case successful = 200
    case requireValidToken = 400
    case unauthorizedRequest = 401
    case apiInternalError = 500
    
    var message: String {
        switch self {
        case .successful:
            return "Request is successful"
        case .requireValidToken:
            return "Request require valid token"
        case .unauthorizedRequest:
            return "Unauthorized request, app key and secret is invalid"
        case .apiInternalError:
            return "API internal error"
        }
    }
}

/** Error Codes */
public enum RushLoyaltyCustomerAPIResponseCode: String {
    
    case successful = "0x0"
    case invalidLoginDetails = "0x1" //Invalid customer login details.
    case nameIsRequired = "0x2"
    case emailIsInvalid = "0x3"
    case emailAlreadyExist =  "0x4"
    case mobileNumberIsInvalid = "0x5"
    case mobileNumberAlreadyExist = "0x6"
    case invalidMPin = "0x7" //Sorry, you have entered
    case cantGenerateUUID = "0x8"
    case cantProcessOTAC = "0x9"
    case invalidOTAC = "0xA"
    case mobileNumberAlreadyVerified = "0xB"
    case customerNotFound = "0xC"
    case emailAlreadyVerified = "0xD"
    case invalidOTEmailAC = "0xE"
    case rewardItemNotFound = "0xF"
    case insufficientPoints = "0x10"
    case imageIsNotReadable = "0x11"
    case invalidOldPin = "0x12"
    case messageIsEmpty = "0x13"
    case messageIsTooLong = "0x14"
    case newAndCurrentEmailIdentical = "0x15"
    case newAndCurrentMobileNumberIdentical = "0x16"
    case mismatchedPin = "0x17"
    case birthDateInvalid = "0x18"
    case mustBe18YearsOldOrAbove = "0x19"
    case fbUserNotRegistered = "0x20"
}

public struct RushLoyaltyCustomerAPIResponse
{
    var statusCode: RushLoyaltyCustomerAPIStatusCode
    var responseData: Dictionary<String,Any>?
}

class RushLoyaltyCustomerAPI
{
    fileprivate let DEV_BASE_URL = "http://rush.staging.digitalventures.ph"
    fileprivate let PROD_BASE_URL = "http://rush.ph"
    fileprivate let REQUEST_TIME_OUT:Float = 30.0
    
    fileprivate var currentToken:String?
    fileprivate var isDevelopment:Bool = false
    fileprivate var isDoneSetup:Bool = false
    fileprivate var appKey:String!
    fileprivate var appSecret:String!
    
    typealias RequestSuccessBlock = (RushLoyaltyCustomerAPIResponse) -> Void
    typealias RequestFailureBlock = (String) -> Void
    
    class var shared : RushLoyaltyCustomerAPI {
        struct Static {
            static let instance : RushLoyaltyCustomerAPI = RushLoyaltyCustomerAPI()
        }
        return Static.instance
    }
    
    fileprivate var baseUrl: String {
        return isDevelopment ? DEV_BASE_URL : PROD_BASE_URL
    }
    
    /* Call this before calling any function */
    func setup(appKey: String, appSecret: String, development: Bool) {
        
        if isDoneSetup {
            return
        }
        
        self.appKey = appKey
        self.appSecret = appSecret
        isDevelopment = development
        
        requestToken(successful: { (response) in
            
            if response.statusCode == .successful {
                self.currentToken = response.responseData?["token"] as? String
                self.isDoneSetup = true
                self.log(message: "Setup success")
            }else {
                self.log(message: ("Setup failed (\(response.responseData))"))
            }
            
        }) { (error) in
            self.log(message: error)
        }
    }
    
    /* Update token in case it is invalid. */
    func updateToken() {
        
        if !isDoneSetup {
            self.log(message: "Not yet setup.")
            return
        }
        
        requestToken(successful: { (response) in
            
            if response.statusCode == .successful {
                self.currentToken = response.responseData?["token"] as? String
                self.log(message: "Update token success")
            }else {
                self.log(message: ("Update token failed (\(response.responseData))"))
            }
            
        }) { (error) in
            self.log(message: error)
        }
    }
    
    /* Customer Registration */
    func registerCustomer(name: String, email: String, mobileNumber: String, birthdate: String?, gender: String?, mpin: String, fbid: String?, referralCode: String?, cardNumber: String?, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        if !isDoneSetup {
            self.log(message: "Not yet setup")
            return
        }
        
        let endPoint = "/api/dev/loyalty/customerapp/customer/register"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        var params = Dictionary<String, String> ()
        
        params.updateValue(name, forKey: "name")
        params.updateValue(email, forKey: "email")
        params.updateValue(mobileNumber, forKey: "mobile_no")
        params.updateValue(mpin, forKey: "mpin")
        
        if birthdate != nil {
            params.updateValue(birthdate!, forKey: "birthdate")
        }
        if gender != nil {
            params.updateValue(gender!, forKey: "gender")
        }
        if fbid != nil {
            params.updateValue(fbid!, forKey: "fb_id")
        }
        if referralCode != nil {
            params.updateValue(referralCode!, forKey: "referral_code")
        }
        if cardNumber != nil {
            params.updateValue(cardNumber!, forKey: "card_no")
        }
        
        //Create request
        let request = createRequest(url: requestUrl!, httpMethod: "POST", token: currentToken!, params: params)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                
                if response.statusCode == .requireValidToken {
                    self.updateToken()
                }
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    /* Customer Login */
    func loginCustomer(mobileNumber: String, mpin: String, fbId: String?, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        if !isDoneSetup {
            self.log(message: "Not yet setup")
            return
        }
        
        let endPoint = "/api/dev/loyalty/customerapp/customer/login"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        var params = Dictionary<String, String> ()
        
        params.updateValue(mobileNumber, forKey: "mobile_no")
        params.updateValue(mpin, forKey: "mpin")
        if fbId != nil {
            params.updateValue(fbId!, forKey: "fb_id")
        }
        
        //Create request
        let request = createRequest(url: requestUrl!, httpMethod: "POST", token: currentToken!, params:params)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                
                if response.statusCode == .requireValidToken {
                    self.updateToken()
                }
                successful(response)
                
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    /* Get customer details */
    func customerDetails(customerId: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        if !isDoneSetup {
            self.log(message: "Not yet setup")
            return
        }
        
        let endPoint = "/api/dev/loyalty/customerapp/customer/\(customerId)"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        
        //Create request
        let request = createRequest(url: requestUrl!, httpMethod: "GET", token: currentToken!)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                
                if response.statusCode == .requireValidToken {
                    self.updateToken()
                }
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    
    
    /* Request Token */
    fileprivate func requestToken(successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        let endPoint = "/api/dev/token"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        let params = ["app_key":self.appKey, "app_secret":self.appSecret]
        
        //Create the request
        let request = createRequest(url: requestUrl!, httpMethod: "POST", params: params)
        
        if request != nil {
            //Process the request
            process(urlRequest: request!, successful: successful, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
}

extension RushLoyaltyCustomerAPI {
    
    fileprivate func process(urlRequest: URLRequest, successful:RequestSuccessBlock?, failure:RequestFailureBlock?) {
        
        DispatchQueue.global(qos: .utility).async {
            //Send request
            let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if error != nil {
                    //failure
                    DispatchQueue.main.async {
                        failure?(error!.localizedDescription)
                    }
                }else {
                    //success
                    do {
                        
                        var statusCode: RushLoyaltyCustomerAPIStatusCode = .apiInternalError
                        
                        if let httpResponse = response as? HTTPURLResponse {
                            
                            statusCode = RushLoyaltyCustomerAPIStatusCode(rawValue: httpResponse.statusCode)!
                        }
                        
                        let data: Any = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                        let responseData: Dictionary<String,Any>? = data as? [String: Any]
                        
                        //Create the response struct
                        let responseStruct = RushLoyaltyCustomerAPIResponse(statusCode: statusCode,responseData: responseData)
                        DispatchQueue.main.async {
                            successful?(responseStruct)
                        }
                    }catch let jsonError {
                        
                        DispatchQueue.main.async {
                            failure?(jsonError.localizedDescription)
                        }
                    }
                }
            }
            dataTask.resume()
        }
    }
    
    fileprivate func createRequest(url: URL, httpMethod:String) -> URLRequest? {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request as URLRequest
    }
    
    fileprivate func createRequest(url:URL, httpMethod:String, params:[String:Any]) -> URLRequest? {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }catch _ {
            return nil
        }

        return request as URLRequest
    }
    
    fileprivate func createRequest(url:URL, httpMethod:String, token:String) -> URLRequest?
    {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        return request as URLRequest
    }
    
    fileprivate func createRequest(url:URL, httpMethod:String, token:String, params:[String:Any]) -> URLRequest? {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }catch _ {
            return nil
        }
        
        return request as URLRequest
    }
    
    fileprivate func log(message: String) {
        if isDevelopment {
            print("Rush Loyalty API: \(message)")
        }
    }
}

///* Check if token is valid */
//fileprivate func validateToken(appKey: String, appSecret: String, token: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
//    
//    let endPoint = "/api/dev/token"
//    let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
//    
//    //Create the request
//    let request = createRequest(url: requestUrl!, httpMethod: "POST")
//    
//    if request != nil {
//        //Process the request
//        process(urlRequest: request!, successful: successful, failure: failure)
//    }else {
//        failure("Invalid url")
//    }
//}
