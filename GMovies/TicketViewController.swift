//
//  TicketViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 9/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import PopupDialog

class TicketViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var ticket : TicketModel?
    private var layout = UICollectionViewFlowLayout()
    private var reuseIdentifier : String?

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var backToMainButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var backToMainWidth: NSLayoutConstraint!
    
    var hideNav : Bool = false
    var saveTickets : Bool = false
    var savedTickets : [IndexPath] = []
    
    private var loadingScreen : LoaderView?
    private var imageDownloaded : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        
        reuseIdentifier = ticket?.qrType?.caseInsensitiveCompare("barcode") == .orderedSame ? "ticketCellBar" : "ticketCell"
        
        collectionView?.setCollectionViewLayout(layout, animated: false)
        
        pager.numberOfPages = (ticket?.qrImage?.count)!
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        self.view.addSubview(loadingScreen!)
        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                  ])
        
        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if hideNav == false {
            backToMainWidth.constant = 0
        }
        self.navigationController?.setNavigationBarHidden(hideNav, animated: true)
        self.navigationItem.backBarButtonItem?.title = " "
        self.title = "TICKET"
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AnalyticsHelper.sendScreenShowEvent(name: "ticket")
        collectionView.performBatchUpdates({
            self.collectionView.reloadData()
        }, completion: { (_) in
//            self.saveTickets = true
//            if self.saveTickets == true {
//                DispatchQueue.main.async {
//                    for i in 0..<self.ticket!.qrCode!.count {
//                        let indexPath = IndexPath(item: i, section: 0)
//                        self.collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
//                        let ticketCell = self.collectionView.cellForItem(at: indexPath) as! TicketCollectionViewCell
//                        
//                    }
//                    self.saveTickets = false
//                    let indexPath = IndexPath(item: 0, section: 0)
//                    self.collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
//                }
//            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (ticket?.qrImage?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : UICollectionViewCell? = nil
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier!, for: indexPath)
        
        configureCell(cell: cell as! TicketCollectionViewCell, indexPath: indexPath)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        if imageDownloaded == true {
            loadingScreen?.stopAnimating()
            loadingScreen?.removeFromSuperview()
//            saveTickets = true
            if saveTickets == true {
                if savedTickets.contains(indexPath) == false {
                    UIImageWriteToSavedPhotosAlbum(UIImage(view: (cell as! TicketCollectionViewCell).ticketBg), nil, nil, nil)
                    savedTickets.append(indexPath)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height-2.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        pager.currentPage = page
    }
    
    //MARK: Private functions
    
    private func configureView() {
//        backToMainWidth.constant = (UIScreen.main.bounds.size.width/2.0)-0.5
        
    }
    
    private func configureCell(cell : TicketCollectionViewCell, indexPath: IndexPath) {
        
        cell.movieTitle.text = ticket!.movie?.movieTitle!
        
        if ticket?.qrType?.caseInsensitiveCompare("barcode") != .orderedSame {
            if ticket!.movie?.landscapePoster == nil {
                downloadPoster(to: ticket!, indexPath: indexPath)
            } else {
                cell.moviePoster.image = ticket!.movie?.landscapePoster
                imageDownloaded = true
            }
        } else {
            if ticket!.movie?.poster == nil {
                downloadPosterPortrait(to: ticket!, indexPath: indexPath)
            } else {
                cell.moviePoster.image = ticket!.movie?.poster
                imageDownloaded = true
            }
        }
        
        if ticket!.theaterLogo == nil {
            cell.theaterLogoHeight.constant = 0.0
            downloadLogo(to: ticket!, indexPath: indexPath)
        } else {
            cell.theaterLogoHeight.constant = 78.0
            cell.theaterLogo.image = ticket!.theaterLogo
        }
        
        cell.ticketBg.layoutIfNeeded()
        cell.qrCode.image = ticket!.qrImage![indexPath.row]
        cell.qrValue.text = ticket!.qrCode![indexPath.row]
        cell.theaterName.text = ticket!.theater!
        
        let string : NSString = NSString(string: "\(ticket!.cinema!)")
        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName : cell.cinema!.font, NSForegroundColorAttributeName : cell.cinema!.textColor])
//        attributedString.addAttributes([NSForegroundColorAttributeName : UIColor(netHex: 0x00A2FF)], range: string.range(of: "Variant"))
        
        cell.cinema.attributedText = attributedString
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "dd MMM yyyy"
        df.locale = Locale.current
        
        cell.date.text = df.string(from: ticket!.ticketDateStringToDate())
        
        df.dateFormat = "h:mm a"
        
        cell.time.text = df.string(from: ticket!.ticketDateStringToDate())
        
        if ticket!.qrCode!.count == 1 {
            
            if (ticket!.ticketCount == 1) {
                cell.ticketPrice.text = "Php \(ticket!.seatPrice!)"
                cell.convenienceFee.text = "Php \(ticket!.convenienceFee!)"
                cell.popcornFee.text = String(format: "Php %.2f", Float(ticket!.popcornPriceTotal))
                cell.popcornLabel.text = ticket!.popcornLabel
            }else {
                cell.ticketPrice.text = "Php \(ticket!.seatPrice!) x \(ticket!.ticketCount!)"
                cell.convenienceFee.text = "Php \(ticket!.convenienceFee!) x \(ticket!.ticketCount!)"
                cell.popcornFee.text = String(format: "Php %.2f x \(ticket!.ticketCount!)", Float(ticket!.popcornPrice))
                cell.popcornLabel.text = ticket!.popcornLabel
            }
            
            if ticket!.convenienceFeeTotal == "0" || ticket!.convenienceFeeTotal == "0.00" {
                cell.convenienceFeeLabel.isHidden = true
                cell.convenienceFee.isHidden = true
            } else {
                cell.convenienceFeeLabel.isHidden = false
                cell.convenienceFee.isHidden = false
            }
            
            //let subtotal : Float = (Float(ticket!.seatPrice!)! * Float(ticket!.qrCode!.count)) + Float(ticket!.convenienceFeeTotal!)! + Float(ticket!.popcornPriceTotal)
            let subtotal : Float = ticket!.subtotal
            
            if ticket!.popcornPriceTotal == 0 {
                cell.popcornLabel.isHidden = true
                cell.popcornFee.isHidden = true
            }else {
                cell.popcornLabel.isHidden = false
                cell.popcornFee.isHidden = false
            }
            
            cell.subtotal.text = "Php \(String(format: "%.2f", subtotal))"
            if ticket!.promo?.characters.count == 0 {
                cell.promoLabel.isHidden = true
                cell.promo.isHidden = true
            } else {
                cell.promoLabel.isHidden = false
                cell.promo.isHidden = false
            }
            
            if let promoType = ticket?.promoDetails?.value(forKey: "discount_type") as? String {
                if promoType.caseInsensitiveCompare("rush-rewards") == .orderedSame {
                    var discountedSeats = 0
                    var name = "Rush Reward"
                    if let seats = ticket!.promoDetails?.value(forKey: "discounted_seats") as? String {
                        discountedSeats =  NSString(string: seats).integerValue
                    }else if let seats = ticket!.promoDetails?.value(forKey: "discounted_seats") as? Int {
                        discountedSeats = seats
                    }
                    if let n = ticket?.promoDetails?.value(forKey: "name") as? String{
                        name = n
                    }
                    if discountedSeats > 1 {
                        cell.promoLabel.text = "\(name) (\(discountedSeats) Tickets)"
                    }else {
                        cell.promoLabel.text = "\(name) (\(discountedSeats) Ticket)"
                    }
                }
            }
            
            //cell.promo.text = "- Php \(ticket!.discount!)"
            cell.promo.text = String(format: "- Php %.2f", (ticket!.discount! as NSString).floatValue)
            cell.total.text = "Php \(ticket!.amount!)"
        } else {
            
            var discountType: String?
            var discountedSeats = 0
            var freeTickets = 0
            
            if let promoDetails = ticket!.promoDetails {
                
                discountType = promoDetails.object(forKey: "discount_type") as? String
                
                if discountType?.caseInsensitiveCompare("convenience_fee") == .orderedSame {
                    
                    if let seats = promoDetails.value(forKey: "discounted_seats") as? String {
                        discountedSeats =  NSString(string: seats).integerValue
                    }else if let seats = promoDetails.value(forKey: "discounted_seats") as? Int {
                        discountedSeats = seats
                    }
                }else if discountType?.caseInsensitiveCompare("special") == .orderedSame {
                    
                    let free = promoDetails.object(forKey: "discount_value") as! String
                    let freeArray = free.components(separatedBy: ";")
                    if freeArray.count == 2 {
                        freeTickets = (freeArray[1] as NSString).integerValue
                        
                        //let disc:Float = (ticketCost + Float(convenienceFeeValue)) * Float(freeCount)
                        
                    }
                }else if discountType?.caseInsensitiveCompare("rush-rewards") == .orderedSame {
                    if let seats = promoDetails.value(forKey: "discounted_seats") as? String {
                        discountedSeats =  NSString(string: seats).integerValue
                    }else if let seats = promoDetails.value(forKey: "discounted_seats") as? Int {
                        discountedSeats = seats
                    }
                }
            }
            
            cell.ticketPrice.text = "Php \(ticket!.seatPrice!)"
            cell.convenienceFee.text = "Php \(ticket!.convenienceFee!)"
            //cell.popcornFee.text = String(format: "Php %.2f", Float(ticket!.popcornPriceTotal/ticket!.qrCode!.count))
            cell.popcornFee.text = String(format: "Php %.2f", Float(ticket!.popcornPrice))
            
            cell.popcornLabel.text = ticket!.popcornLabel
            
            if ticket!.convenienceFeeTotal == "0" || ticket!.convenienceFeeTotal == "0.00" {
                cell.convenienceFeeLabel.isHidden = true
                cell.convenienceFee.isHidden = true
            } else {
                cell.convenienceFeeLabel.isHidden = false
                cell.convenienceFee.isHidden = false
            }
            
            let subtotal : Float = (Float(ticket!.seatPrice!)! + Float(ticket!.convenienceFee!)! + Float(ticket!.popcornPrice))
            
            if ticket!.popcornPrice == 0 {
                cell.popcornLabel.isHidden = true
                cell.popcornFee.isHidden = true
            }else {
                cell.popcornLabel.isHidden = false
                cell.popcornFee.isHidden = false
            }
            
            cell.subtotal.text = "Php \(String(format: "%.2f", subtotal))"
            
            //let discount : Float = Float(ticket!.discount!)! / Float(ticket!.qrCode!.count)
            var discount : Float = 0
            
            if discountType?.caseInsensitiveCompare("convenience_fee") == .orderedSame {
                
                if indexPath.row < ticket!.qrCode!.count - discountedSeats {
                    discount = 0
                }else {
                    discount = Float(ticket!.convenienceFee!)!
                }
                
            }else if discountType?.caseInsensitiveCompare("special") == .orderedSame {
                
                if indexPath.row < ticket!.qrCode!.count - freeTickets {
                    discount = 0
                }else {
                    discount = Float(ticket!.discount!)! / Float(freeTickets)
                }
            }else if discountType?.caseInsensitiveCompare("rush-rewards") == .orderedSame {
                
                if indexPath.row < ticket!.qrCode!.count - discountedSeats {
                    discount = 0
                }else {
                    discount = Float(ticket!.discount!)! / Float(discountedSeats)
                    //get promo name
                    cell.promoLabel.text = ticket!.promoDetails!.value(forKey: "name") as! String?
                }
                
            }else {
                discount = Float(ticket!.discount!)! / Float(ticket!.qrCode!.count)
            }
            
            if discount <= 0 {
                cell.promoLabel.isHidden = true
                cell.promo.isHidden = true
            } else {
                cell.promoLabel.isHidden = false
                cell.promo.isHidden = false
            }
            
            let nf = NumberFormatter()
            nf.positiveFormat = "0.##"
            if indexPath.row < ticket!.qrCode!.count - 1 {
                nf.roundingMode = .floor
            } else {
                nf.roundingMode = .ceiling
            }
            
            let fixedDiscount : Float = Float(nf.number(from: nf.string(from: NSNumber(value: discount))!)!)
            
            cell.promo.text = String(format: "- Php %.2f", discount)
            cell.total.text = "Php \(String(format: "%.2f", subtotal - fixedDiscount))"
        }
        
        //draw seats
        
        for v in cell.seatContainer.subviews {
            v.removeFromSuperview()
        }
        
        let width = cell.seatContainer.bounds.width
        let height = cell.seatContainer.bounds.height
        
        let seatHeight = (height - 2.0) / 2.0
        let seatWidth = (width - 8.0) / 5.0
        let wh = min(seatHeight, seatWidth)
        
        var x : CGFloat = 0.0
        var y : CGFloat = 0.0
        
        if ticket!.qrCode!.count == 1 {
            for i in 0..<ticket!.seats!.count {
                let l = UILabel(frame: CGRect(x: x, y: y, width: wh, height: wh))
                l.text = ticket!.seats![i]
                l.textColor = UIColor.white
                l.backgroundColor = UIManager.seatSelectedBg
                l.textAlignment = .center
                l.font = UIFont(name: "OpenSans", size: wh/2.5)
                l.layer.cornerRadius = 2.0
                l.clipsToBounds = true
                cell.seatContainer.addSubview(l)
                x += wh + 2.0
                if i % 4 == 0 && i > 0 {
                    x = 0.0
                    y += wh + 2.0
                }
            }
        } else {
            let l = UILabel(frame: CGRect(x: x, y: y, width: wh, height: wh))
            l.text = ticket!.seats![indexPath.row]
            l.textColor = UIColor.white
            l.backgroundColor = UIManager.seatSelectedBg
            l.textAlignment = .center
            l.font = UIFont(name: "OpenSans", size: wh/2.5)
            l.layer.cornerRadius = 2.0
            l.clipsToBounds = true
            cell.seatContainer.addSubview(l)
        }
    }
    
    private func downloadPoster(to: TicketModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: to.movie!.landscapePosterUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    to.movie!.landscapePoster = image
                    self.collectionView?.reloadItems(at: [indexPath])
                }
            } else {
                
            }
        }
    }
    
    private func downloadPosterPortrait(to: TicketModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: to.movie!.posterUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    to.movie!.poster = image
                    self.collectionView?.reloadItems(at: [indexPath])
                }
            } else {
                
            }
        }
    }
    
    private func downloadLogo(to: TicketModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: to.theaterLogoUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    to.theaterLogo = image
                    self.collectionView?.reloadItems(at: [indexPath])
                }
            } else {
                
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backToMainAction(_ sender: Any) {
//        self.navigationController?.popToRootViewController(animated: true)
        AnalyticsHelper.sendButtonEvent(name: "ticket_back_to_main")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        ((appDelegate.window?.rootViewController as! MNavigationViewController).viewControllers.last as! MTabBarViewController).setSelectedViewController(index: 0, animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func shareAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "ticket_share_button")
        let pop : PopDropViewController = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 132.0)
        let shareView : ShareViewController = ShareViewController(nibName: "ShareViewController", bundle: nil)
        shareView.mo = ticket!.movie
        shareView.t = ticket
        shareView.parentController = self
        
        pop.showViewControllerFromView(viewController: shareView, originView: sender as! UIButton, offset: CGPoint(x: 0.0, y: UIScreen.main.bounds.size.height - (sender as! UIButton).frame.size.height))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
