//
//  AnalyticsEventConstants.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 14/02/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation


public struct AnalyticsConstants
{
    static let IS_DEVELOPMENT_BUILD = false
    
    static var GATrackingId: String {
        get{
            if IS_DEVELOPMENT_BUILD {
                return "UA-91859214-1"
            }else {
                return "UA-49550413-2"
            }
        }
    }
}

public struct GAEventCategory {
    static let SignUp = "Sign Up"
    static let BuyTicket = "Buy Ticket"
    static let PayNow = "Pay Now"
    static let PageVisit = "Page Visit"
}

public struct GASignUpCategory {
    static let Facebook = "Facebook"
    static let Login = "Login"
    static let CreateAccount = "CreateAccount"
    static let Guest = "Guest"
}

public struct GAPageVisitCategory {
    static let SplashScreen = "Splash Screen"
    static let SignUp = "Sign Up"
    static let OTP = "OTP"
    static let Movies = "Movies"
    static let MovieDetails = "Movie Details"
    static let Cinemas = "Cinemas"
    static let SeatMap = "Seat Map"
    static let Spotlight = "Spotlight"
    static let MyProfile = "My Profile"
    static let MyRewards = "My Rewards"
    static let MyTickets = "My Tickets"
    static let Watchlist = "Watchlist"
}
