//
//  TicketModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 8/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TicketModel: NSObject {
    
    var amount : String?
    var cinema : String?
    var convenienceFeeTotal : String?
    var convenienceFee : String?
    var discount : String?
    var reservationDate : String?
    var movie : MovieModel?
    var promo : String?
    var qrCode : [String]?
    var qrImage : [UIImage?]?
    var qrType : String?
    var type : String?
    var seats : [String]?
    var subtotal : Float = 0
    var theater : String?
    var theaterLogoUrl : String?
    var theaterLogo : UIImage?
    var ticketCount : Int?
    var ticketStatus : String?
    var transactionId : String?
    var seatPrice : String?
    var share : NSDictionary?
    var popcornPriceTotal: Int = 0
    var popcornPrice: Int = 0
    var popcornLabel: String = "Popcorn Bucket"
    var promoDetails: NSDictionary?
    
    var ticketTimeString : String {
        get {
            let df = DateFormatter()
            df.dateFormat = "hh:mm a"
            df.locale = Locale.current
            
            return df.string(from: ticketDateStringToDate())
        }
    }
    
    func ticketDateStringToDate() -> Date {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.locale = Locale.current
        
        if reservationDate == nil {
            return Date()
        }
        
        return df.date(from: reservationDate!) == nil ? Date() : df.date(from: reservationDate!)!
    }
    
    init(data : NSDictionary) {
        //print(data)
        
        if let amt = data.value(forKey: "amount") as? String {
            amount = amt
        } else {
            amount = String(format: "%ld", data.value(forKey: "amount") as! Int)
        }
        cinema = data.value(forKey: "cinema") as? String
        if let cft = data.value(forKey: "convinience_fee") as? String {
            convenienceFeeTotal = cft
        } else {
            convenienceFeeTotal = String(format: "%ld", data.value(forKey: "convinience_fee") as! Int)
        }
        reservationDate = data.value(forKey: "datetime_reserve") as? String
        if let dis = data.value(forKey: "discount") as? String {
            discount = dis
        } else {
            discount = String(format: "%ld", data.value(forKey: "discount") as! Int)
        }
        movie = MovieModel(data: data.value(forKeyPath: "movie") as! NSDictionary)

        promo = data.value(forKey: "promo_code") as? String
        
        qrCode = data.value(forKey: "qr_code") as? [String]
        qrImage = []
        for base64 in data.value(forKey: "qr_image") as! [String] {
            let data = NSData(base64Encoded: base64, options: .ignoreUnknownCharacters)
            qrImage?.append(UIImage(data: data as! Data))
        }
        
        qrType = data.value(forKey: "qr_type") as? String
        convenienceFee = data.value(forKey: "reservation_fee") as? String
        type = data.value(forKey: "seating_type") as? String
        seats = data.value(forKey: "seats") as? [String]
        
        if let st = data.value(forKey: "subtotal") as? String {
            subtotal = Float(st) ?? 0.0
        } else if let st = data.value(forKey: "subtotal") as? Int {
            subtotal = Float(st) 
        } else if let st = data.value(forKey: "subtotal") as? Float {
            subtotal = st
        }
        
        theater = data.value(forKey: "theater") as? String
        theaterLogoUrl = data.value(forKey: "theater_logo") as? String
        if let tc = data.value(forKey: "ticket_count") as? Int {
            ticketCount = tc
        } else {
            ticketCount = Int(data.value(forKey: "ticket_count") as! String)
        }
        ticketStatus = data.value(forKey: "ticket_status") as? String
        transactionId = data.value(forKey: "transaction_id") as? String
        if let sp = data.value(forKey: "unit_price") as? String {
            seatPrice = String(format: "%.2f", (sp as NSString).floatValue)
        } else {
            seatPrice = String(format: "%.2f", Float(data.value(forKey: "unit_price") as! Int))
        }
        
        if let popcorn = data.value(forKey: "popcorn_price") as? String {
            popcornPriceTotal = Int(Float(popcorn)!)
        }else {
            popcornPriceTotal = data.value(forKey: "popcorn_price") as? Int ?? 0
        }
        
        if let popcorn = data.value(forKey: "popcorn") as? String {
            popcornPrice = Int(Float(popcorn)!)
        }else {
            popcornPrice = data.value(forKey: "popcorn") as? Int ?? 0
        }
        
        if let pLabel = data.value(forKey: "popcorn_label") as? String {
            popcornLabel = pLabel
        }
        
        share = data.value(forKey: "share") as? NSDictionary
        
        promoDetails = data.object(forKey: "promo_details") as? NSDictionary
    }
}
