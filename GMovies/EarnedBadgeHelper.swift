//
//  EarnedBadgeHelper.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 19/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation
import PopupDialog

class EarnedBadgeHelper
{
    class var shared : EarnedBadgeHelper {
        struct Static {
            static let instance : EarnedBadgeHelper = EarnedBadgeHelper()
        }
        return Static.instance
    }
    
    fileprivate var earnedBadgePopup: PopupDialog?
    fileprivate var viewControllerForPresentation: UIViewController?
    
    func present(points:NSDictionary, badge:NSDictionary, hideAvailablePoints: Bool = false) {
        
        //Create a custom view controller
        let earnedVC = EarnedPointsViewController(nibName: "EarnedPointsViewController", bundle: nil)
        earnedVC.delegate = self
        
        earnedBadgePopup = PopupDialog(viewController: earnedVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
        
        let earnedPoints = points.value(forKey: "points_earned") as! NSString
        let userPoints = points.value(forKey: "user_points") as! NSNumber
        
        earnedVC.earnedPoints = String(format:"%.3f", earnedPoints.doubleValue)
        earnedVC.availablePoints = String(format:"%.3f", userPoints.doubleValue)
        
        let badgeIcon = badge.value(forKey: "badge_icon") as! String
        let badgeGroupIcon = badge.value(forKey: "group_badge_icon") as! String
        let badgeName = badge.value(forKey: "badge_name") as! String
        
        earnedVC.badgeTitleText = badgeName
        earnedVC.badgeIconUrl = badgeIcon
        earnedVC.badgeGroupIconUrl = badgeGroupIcon
        earnedVC.shouldHideAvailablePoints = hideAvailablePoints
        
        //Create transparent host view for presenting the above view
        let mainWindow = UIApplication.shared.keyWindow
        viewControllerForPresentation = UIViewController()
        viewControllerForPresentation?.view.backgroundColor = UIColor.clear
        viewControllerForPresentation?.view.isOpaque = false
        mainWindow?.addSubview((viewControllerForPresentation?.view)!)
        
        //Make your transparent view controller present your actual view controller
        viewControllerForPresentation?.present(earnedBadgePopup!, animated: true)
    }
}

extension EarnedBadgeHelper: EarnedPointsViewControllerDelegate {
    func onOkClicked() {
        viewControllerForPresentation?.view.removeFromSuperview()
        earnedBadgePopup?.dismiss()
    }
}
