//
//  TicketsListViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 15/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TicketsListViewController: UITableViewController {
    
    private var errorView : ErrorView?
    
    private var tickets : [TicketModel]?
    private var currentPage : Int = 1
    private var maxPages : Int = Int.max

    private var selectedTicket : TicketModel?
    
    private var downloadTaskRunning : Bool = false
    
    private var currentPopDrop : PopDropViewController?
    
    private var loadingScreen : LoaderView?
    
    @IBOutlet weak var filterButton: UIButton!
    private var displayType : Int = 0
    private var currentFilter : String = "all"
    private var prevFilter : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tickets = []
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        parent?.title = "MY ACCOUNT"
        parent?.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "ticket_list")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.MyTickets)
        if tickets?.count == 0 || UserDefaults.standard.bool(forKey: "refreshTickets") == true {
            self.view.addSubview(loadingScreen!)
            self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                      ])
            
            self.view.layoutIfNeeded()
            
            loadingScreen?.startAnimating()
            
            downloadTickets(page: 1, type: currentFilter)
            
        } else {
            self.loadingScreen?.stopAnimating()
            self.loadingScreen?.removeFromSuperview()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        parent?.title = " "
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if currentFilter == "done" {
            let fTickets = tickets?.filter({ (t) -> Bool in
                
                return t.ticketStatus?.caseInsensitiveCompare("done") == .orderedSame
            })
            
            return (fTickets?.count)!
        } else if currentFilter == "upcoming" {
            let fTickets = tickets?.filter({ (t) -> Bool in
                return t.ticketStatus?.caseInsensitiveCompare("done") != .orderedSame
            })
            return (fTickets?.count)!
        }
        
        return (tickets?.count)!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TicketCell", for: indexPath)

        configureCell(cell: cell as! TicketViewCell, indexPath: indexPath)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var fTickets : [TicketModel]? = nil
        
        if currentFilter == "done" {
            fTickets = tickets?.filter({ (t) -> Bool in
                return t.ticketStatus?.caseInsensitiveCompare("done") == .orderedSame
            })
        } else if currentFilter == "upcoming" {
            fTickets = tickets?.filter({ (t) -> Bool in
                return t.ticketStatus?.caseInsensitiveCompare("done") != .orderedSame
            })
        } else {
            fTickets = tickets
        }
        
        selectedTicket = fTickets![indexPath.row]
        performSegue(withIdentifier: "showTicket", sender: self)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        let reloadDistance : CGFloat = 10.0
        if y > h + reloadDistance {
            if downloadTaskRunning == false {
                if maxPages > currentPage {
                    currentPage += 1
                    downloadTickets(page: currentPage, type: currentFilter)
                } else {
                    currentPage = maxPages
                }
            }
        }
    }
    
    //MARK: Private functions
    
    private func downloadTickets(page : Int, type : String) {
        
        if currentPage > maxPages {
            currentPage = maxPages
            return
        }
        
        if UserDefaults.standard.bool(forKey: "refreshTickets") == true {
            UserDefaults.standard.set(false, forKey: "refreshTickets")
            UserDefaults.standard.synchronize()
            tickets?.removeAll()
            self.tableView.reloadData()
        }
        
        //var cPage = page
        
        if type != prevFilter {
            prevFilter = type
            //cPage = 1
            currentPage = 1
            maxPages = 1
            tickets?.removeAll()
            self.view.addSubview(loadingScreen!)
            self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                      ])
            
            self.view.layoutIfNeeded()
            
            loadingScreen?.startAnimating()
        }
        
        downloadTaskRunning = true
        GMoviesAPIManager.getTickets(page: currentPage, type: type) { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    
                    self.maxPages = result?.value(forKeyPath: "data.links.max_page") as! Int
                    let tix : [NSDictionary] = result?.value(forKeyPath: "data.results") as! [NSDictionary]
                    
                    for t in tix {
                        //print(t)
                        let ticket : TicketModel = TicketModel(data: t)
                        if self.tickets?.contains(where: { (t1) -> Bool in
                            return t1.transactionId == ticket.transactionId
                        }) == false {
                            self.tickets?.append(ticket)
                        }
                    }
                    
                    if self.maxPages > 0 {
                        DispatchQueue.main.async {
                            self.errorView?.removeFromSuperview()
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            self.tableView.reloadData()
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            self.loadErrorView(titleString: "Your e-tickets will be saved here", description: "You can also access your tickets on your phone’s gallery.")
                        }
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        self.loadErrorView(titleString: "Your e-tickets will be saved here", description: "You can also access your tickets on your phone’s gallery.")
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.loadingScreen?.stopAnimating()
                    self.loadingScreen?.removeFromSuperview()
                    self.loadErrorView(titleString: "Your e-tickets will be saved here", description: "You can also access your tickets on your phone’s gallery.")
                }
                print(error!.localizedDescription)
            }
            
            self.downloadTaskRunning = false
        }
    }
    
    private func loadErrorView(titleString : String, description : String, image: UIImage = UIImage(named: "no-tickets")!) {

        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = tableView.subviews.first!
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 37.0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addSubview(errorView!)
        self.view.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }
    
    private func configureCell(cell : TicketViewCell, indexPath : IndexPath) {
        
        var fTickets : [TicketModel]? = nil
        
        if currentFilter == "done" {
            fTickets = tickets?.filter({ (t) -> Bool in
                return t.ticketStatus?.caseInsensitiveCompare("done") == .orderedSame
            })
        } else if currentFilter == "upcoming" {
            fTickets = tickets?.filter({ (t) -> Bool in
                return t.ticketStatus?.caseInsensitiveCompare("done") != .orderedSame
            })
        } else {
            fTickets = tickets
        }
        
        let ticket : TicketModel = fTickets![indexPath.row]
        
        cell.dateLabel.attributedText = UIManager.ticketListDateFormatter(date: ticket.ticketDateStringToDate())
        cell.movieTitle.text = ticket.movie?.movieTitle!
        cell.theaterName.text = ticket.theater!
        cell.cinemaName.text = ticket.cinema!
        cell.timeLabel.text = ticket.ticketTimeString
        cell.menuButton.tag = indexPath.row
        
        
        if cell.menuButton.actions(forTarget: self, forControlEvent: .touchUpInside) == nil{
            cell.menuButton.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)
        }
        
        
        if ticket.ticketStatus?.caseInsensitiveCompare("upcoming") != .orderedSame {
            cell.indicator.text = "DONE"
            cell.indicator.backgroundColor = UIColor(netHex: 0x696f80)
        } else {
            cell.indicator.text = "UPCOMING"
            cell.indicator.backgroundColor = UIManager.buttonRed
        }
        
        if ticket.movie?.landscapePoster == nil {
            downloadImage(ticket: ticket, indexPath: indexPath)
        } else {
            cell.moviePoster.image = ticket.movie?.landscapePoster
        }
        
        if let promoDetails = ticket.promoDetails {
            if let promoType = promoDetails.value(forKey: "discount_type") as? String {
                if promoType.caseInsensitiveCompare("rush-rewards") == .orderedSame {
                    cell.giftImageView.isHidden = false
                }else {
                    cell.giftImageView.isHidden = true
                }
            }else {
                cell.giftImageView.isHidden = true
            }
            
        }else {
            cell.giftImageView.isHidden = true
        }
    }
    
    @objc private func menuButtonAction(sender : UIButton) {
        AnalyticsHelper.sendButtonEvent(name: "ticket_list_menu_button")
        currentPopDrop?.hide()
        currentPopDrop = nil
        
        let share = ShareViewController(nibName: "ShareViewController", bundle: nil)
        if tickets![sender.tag].ticketStatus?.caseInsensitiveCompare("done") == .orderedSame {
            share.mode = 1
        }
        share.parentController = self
        share.mo = tickets![sender.tag].movie
        share.t = tickets![sender.tag]
        
        let mo = tickets![sender.tag].movie
        if mo?.movieId != nil {
            if (mo?.movieId?.characters.count)! > 0 && share.mode == 1 {
                currentPopDrop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 176.0)
            } else {
                currentPopDrop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 132.0)
            }
        } else{
            currentPopDrop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 132.0)
        }
        
        let rect = tableView.rectForRow(at: IndexPath(row: sender.tag, section: 0))
        
        currentPopDrop?.showViewControllerFromView(viewController: share, originView: sender, offset: CGPoint(x: 0, y: 64.0+rect.origin.y-tableView.contentOffset.y))
    }
    
    private func downloadImage(ticket : TicketModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: ticket.movie!.landscapePosterUrl!) { (error, image) in
            if error == nil {

                ticket.movie?.landscapePoster = image
                
                DispatchQueue.main.async {
                    if self.tableView.indexPathsForVisibleRows?.contains(indexPath) == true {
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                    } else {
                        
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    @IBAction func filterAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "ticket_list_filter_button")
        displayType += 1
        if displayType >= 3 {
            displayType = 0
        }
        
        if displayType == 0 {
            currentFilter = "all"
            filterButton.setTitle("All Tickets", for: .normal)
        } else if displayType == 1 {
            currentFilter = "upcoming"
            filterButton.setTitle("Upcoming", for: .normal)
        } else if displayType == 2 {
            currentFilter = "done"
            filterButton.setTitle("Done", for: .normal)
        }
        
        downloadTickets(page: 1, type: currentFilter)
        
        tableView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTicket" {
            let vc : TicketViewController = (segue.destination as! MNavigationViewController).topViewController as! TicketViewController
            vc.ticket = selectedTicket
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
