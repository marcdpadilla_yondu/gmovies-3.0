//
//  MovieListViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 5/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MovieListViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private var movies : [MovieModel]?
    private let reuseIdentifier = "movieCell"
    
    var theaterObject : TheaterModel?
    var comingSoonMode : Bool = false
    
    private var timer : Timer?
    private var ticker : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        movies = []
        
        downloadMovies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = theaterObject?.name!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "map_movie_list")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
        if timer != nil {
            timer?.invalidate()
            ticker = 0
        }
    }
    
    @objc private func tick() {
        self.ticker += 1
        
        if self.ticker == Constants.patience {
            self.timer?.invalidate()
            self.ticker = 0
            
            DispatchQueue.main.async {
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBanner(message: "Taking longer than expected... Please Wait.", isPersistent: false)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (movies?.count)!
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        configureCell(cell: cell as! MovieListViewCell, indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 145.0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let storyboard : UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let movieDetailNav : UINavigationController = storyboard.instantiateViewController(withIdentifier: "movieDetailNav") as! UINavigationController
        let movieDetail : MovieDetailViewController = movieDetailNav.topViewController as! MovieDetailViewController
        
        
        movieDetail.theaterObject = theaterObject
        movieDetail.movieObject = movies![indexPath.row]
        
        self.present(movieDetailNav, animated: true, completion: nil)
    }
    
    //MARK: Private functions
    
    private func configureCell(cell : MovieListViewCell, indexPath: IndexPath) {
        let movie : MovieModel? = movies![indexPath.row]
        
        cell.movieTitle.text = movie?.movieTitle!
        cell.advisory.text = movie?.advisoryRatingString!
        cell.genre.text = movie?.genre!
        cell.runtime.text = movie?.length!
        cell.cast.text = movie?.castList!
        cell.watchList.tag = indexPath.row
        
        //print(cell.watchList.actions(forTarget: self, forControlEvent: .touchUpInside))
        
        if cell.watchList.actions(forTarget: self, forControlEvent: .touchUpInside) == nil {
            cell.watchList.addTarget(self, action: #selector(self.watchlistButtonAction), for: .touchUpInside)
        }
        
        if movie?.watched == true {
            cell.watchList.backgroundColor = UIManager.buttonRed
            cell.watchList.setImage(UIImage(named: "watchlist-check"), for: .normal)
        } else {
            cell.watchList.backgroundColor = UIManager.watchButtonGray
            cell.watchList.setImage(UIImage(named: "watchlist-add"), for: .normal)
        }
        
        if movie!.poster == nil {
            downloadPoster(mo: movie!, indexPath: indexPath)
        } else {
            cell.poster.image = movie!.poster
        }
        
    }
    
    private func downloadPoster(mo: MovieModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: mo.posterUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    mo.poster = image
                    self.collectionView?.reloadItems(at: [indexPath])
                }
            } else {
                
            }
        }
    }
    
    private func downloadMovies() {
        if timer != nil {
            timer?.invalidate()
            ticker = 0
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MovieListViewController.tick), userInfo: nil, repeats: true)
        
        if comingSoonMode == false {
            GMoviesAPIManager.getMoviesNowShowing(theaterId: (theaterObject?.theaterId!)!) { (error, result) in
                if error == nil {
                    let status : Int = result?.object(forKey: "status") as! Int
                    
                    if status == 1 {
                        
                        let nowShowing : [NSDictionary]? = result?.value(forKeyPath: "data.now_showing") as? [NSDictionary]
                        
                        for movieRaw : NSDictionary in nowShowing! as [NSDictionary] {
                            let movie : MovieModel = MovieModel(data: movieRaw)
                            movie.showing = true
                            self.movies?.append(movie)
                        }
                        
                        DispatchQueue.main.async {
                            self.collectionView?.reloadData()
                            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.hideBanner()
                        }
                        
                    } else {
                        let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                } else {
                    print(error!.localizedDescription)
                }
                
                self.timer?.invalidate()
                self.timer = nil
                self.ticker = 0
            }
        } else {
            GMoviesAPIManager.getMoviesComingSoon(theaterId: (theaterObject?.theaterId!)!) { (error, result) in
                if error == nil {
                    let status : Int = result?.object(forKey: "status") as! Int
                    
                    if status == 1 {
                        
                        let nowShowing : [NSDictionary]? = result?.value(forKeyPath: "data.coming_soon") as? [NSDictionary]
                        
                        for movieRaw : NSDictionary in nowShowing! as [NSDictionary] {
                            let movie : MovieModel = MovieModel(data: movieRaw)
                            movie.showing = false
                            self.movies?.append(movie)
                        }
                        
                        DispatchQueue.main.async {
                            self.collectionView?.reloadData()
                            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.hideBanner()
                        }
                        
                    } else {
                        let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                } else {
                    print(error!.localizedDescription)
                }
                
                self.timer?.invalidate()
                self.timer = nil
                self.ticker = 0
            }
        }
    }
    
    @objc private func watchlistButtonAction(sender : MButton) {
        let movieObject : MovieModel = movies![sender.tag]
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if movieObject.watched == true {
            let alert = UIAlertController(title: "", message: "You’re about to remove this movie from your watchlist", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
                sender.isOn = true
            })
            let removeAction = UIAlertAction(title: "Remove", style: .default, handler: { (_) in
                sender.loadingIndicator(show: true)
                appDelegate.removeMovieFromWatchList(mo: movieObject, completion: { (success) -> Void? in
                    DispatchQueue.main.async {
                        sender.loadingIndicator(show: false)
                        sender.backgroundColor = UIManager.buttonRed
                        sender.setImage(UIImage(named: "watchlist-check"), for: .normal)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MovieModelUpdateRemove"), object: movieObject.movieId!)
                        self.collectionView?.performBatchUpdates({
                            self.collectionView?.reloadItems(at: [IndexPath(item: sender.tag, section: 0)])
                        }, completion: nil)
                        print("reloaded")
                    }
                })
            })
            alert.addAction(cancelAction)
            alert.addAction(removeAction)
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        } else {
            sender.loadingIndicator(show: true)
            appDelegate.addMovieToWatchList(mo: movieObject, completion: { (success) -> Void? in
                DispatchQueue.main.async {
                    sender.loadingIndicator(show: false)
                    sender.backgroundColor = UIManager.watchButtonGray
                    sender.setImage(UIImage(named: "watchlist-add"), for: .normal)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MovieModelUpdateAdd"), object: movieObject.movieId!)
                    self.collectionView?.performBatchUpdates({
                        self.collectionView?.reloadItems(at: [IndexPath(item: sender.tag, section: 0)])
                        }, completion: nil)
                    let popup = PopupViewController(nibName: "PopupViewController", bundle: nil, expandedHeight: 140.0, expandedWidth: 140.0)
                    let confirmView = Bundle.main.loadNibNamed("ConfirmationView", owner: self, options: nil)?.first as! ConfirmationView
                    confirmView.translatesAutoresizingMaskIntoConstraints = false
                    confirmView.confirmImage.image = UIImage(named: "added-watchlist")
                    confirmView.confirmMessage.text = "Saved to Watchlist!"
                    
                    popup.delegate = self
                    popup.showView(view: confirmView, offset: CGPoint.zero)
                    
                    
                }
            })
        }
    }
}

extension MovieListViewController: PopupViewControllerDelegate
{
    func onHidden(popup: PopupViewController) {
        if AccountHelper.shared.isRegisteredInRush {
            
            UserEngagementHelper.shared.earnBadgeFrom(engagement: "add-watch-list")
        }
    }
}
