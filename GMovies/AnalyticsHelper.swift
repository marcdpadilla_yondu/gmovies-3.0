//
//  AnalyticsHelper.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 11/12/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import Google
import Flurry_iOS_SDK

class AnalyticsHelper: NSObject {
    class func sendEvent(category : String, action : String, label : String, value : String) {
        let google = GAI.sharedInstance().defaultTracker
        
        google?.send([kGAIEventCategory : category, kGAIEventAction : action, kGAIEventLabel : label, kGAIEventValue : value])
        Flurry.logEvent(category, withParameters: ["action" : action, "label" : label, "value" : value], timed: false)
    }
    
    class func sendScreenShowEvent(name : String) {
        sendEvent(category: "screen", action: "screen_show", label: name, value: "")
    }
    
    class func sendButtonEvent(name : String) {
        sendEvent(category: "button", action: "button_press", label: name, value: "")
    }
}
