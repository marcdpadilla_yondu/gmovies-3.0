//
//  CategoryTableViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 23/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CategoryTableViewController: UITableViewController {
        
    private let identifier : String = "schedCinemaIdentifier"
    private var items : [NSDictionary] = [["id" : "customer_feedback", "name" : "Customer Feedback/Concerns"],
                                          ["id" : "partnerships", "name" : "Partnerships"],
                                          ["id" : "bulk_buy", "name" : "Bulk Buy"],
                                          ["id" : "others", "name" : "Others"]]
    
    var parentController : UIViewController?
    var delegate : CategoryProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.register(UINib(nibName: "SchedCinemaCell", bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SchedCinemaCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SchedCinemaCell
        
        let item : NSDictionary = items[indexPath.row]
        cell.cinemaName.text = item.value(forKey: "name") as! String
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let pop : PopDropViewController = self.parent as! PopDropViewController
        let cell = tableView.cellForRow(at: indexPath) as! SchedCinemaCell
        
        let item : NSDictionary = items[indexPath.row]
        delegate?.didSelectCategory(category: item)
        
        pop.hide()
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}

protocol CategoryProtocol {
    func didSelectCategory(category: NSDictionary)
}
