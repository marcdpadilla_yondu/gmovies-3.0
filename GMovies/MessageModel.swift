//
//  MessageModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 21/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MessageModel: NSObject {
    
    /*
     {
     "id": 1,
     "type": "watch_list",
     "message": "Sample Message",
     "payload": {
     "movie_id": "adADSADSQD"
     },
     "opened": "0",
     "created_at": "2016-11-21 00:00:00"
     }
    */
    
    
    var msgId : String?
    var type : String?
    var message : String?
    var movieId : String?
    var opened : Bool = false
    var createdAtString : String?
    
    var createdAtDate : Date {
        get {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            df.locale = Locale.current
            
            return df.date(from: createdAtString!)!
        }
    }
    
    var displayDate : String {
        get {
            let df = DateFormatter()
            df.dateFormat = "MMM d yyyy"
            df.locale = Locale.current
            
            return df.string(from: createdAtDate)
        }
    }
    
    var displayTime : String {
        get {
            let df = DateFormatter()
            df.dateFormat = "h:mm a"
            df.locale = Locale.current
            
            return df.string(from: createdAtDate)
        }
    }
    
    init(data : NSDictionary) {
        //print(data)
        msgId = String(format: "%ld", data.value(forKey: "id") as! Int)
        type = data.value(forKey: "type") as? String
        message = data.value(forKey: "message") as? String
        movieId = data.value(forKeyPath: "payload.movie_id") as? String
        if let op = data.value(forKey: "opened") as? String {
            opened = Int(op) == 1
        } else {
            opened = data.value(forKey: "opened") as! Int == 1
        }
        
        createdAtString = data.value(forKey: "created_at") as? String
    }
}
