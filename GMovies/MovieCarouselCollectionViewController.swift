//
//  MovieCarouselCollectionViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 31/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MovieCarouselCollectionViewController: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {
    
    private var sectionsCount : Int
    var items : [MovieModel]?
    private var layout : UICollectionViewFlowLayout
    private var reuseIdentifier : String
    var sectionTitle : String = ""
    
    var collectionView : UICollectionView?
    
    override init() {
        sectionsCount = 1
        layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 2.5
        layout.minimumInteritemSpacing = 2.5
        
        reuseIdentifier = "Cell"
        items = [MovieModel]()
        
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MovieCarouselCollectionViewController.notificationReceived), name: NSNotification.Name(rawValue: "MovieModelUpdateRemove"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MovieCarouselCollectionViewController.notificationReceived), name: NSNotification.Name(rawValue: "MovieModelUpdateAdd"), object: nil)
    }
    
    convenience init(sections sc : Int, reuseIdentifier ri : String) {
        self.init()
        
        sectionsCount = sc
        reuseIdentifier = ri
    }
    
    func setData(data : [MovieModel]?) {
        if data != nil {
            items = data
        }
        
        if collectionView != nil {
            collectionView?.setCollectionViewLayout(layout, animated: false)
            
            collectionView?.performBatchUpdates({ 
                self.collectionView?.reloadSections(IndexSet(integer: 0))
                }, completion: { (_) in
                    
            })
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return (items?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : UICollectionViewCell? = nil
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        configureCell(cell: cell as! PosterCollectionViewCell, indexPath: indexPath)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.300, delay: 0.0, options: .curveEaseOut, animations: { 
            cell.alpha = 1.0
            }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if sectionTitle.localizedCaseInsensitiveCompare("coming soon") == .orderedSame {
            return CGSize(width: UIScreen.main.bounds.size.width/2.75, height: ((UIScreen.main.bounds.size.width/2.75) * 1.48) + 50.0)
        }
        
        return CGSize(width: UIScreen.main.bounds.size.width/2.75, height: ((UIScreen.main.bounds.size.width/2.75) * 1.48) + 35.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 5.0, 0, 5.0)
    }
    
    //MARK: Private functions
    
    private func configureCell(cell : PosterCollectionViewCell, indexPath: IndexPath) {
        
        let movie : MovieModel = items![indexPath.row] as MovieModel
        
        cell.movieTitleLabel.text = movie.movieTitle!
        cell.alpha = 0.0
        cell.watchListButton.tag = indexPath.row
        if cell.watchListButton.target(forAction: #selector(MovieCarouselCollectionViewController.watchlistButtonAction), withSender: cell.watchListButton) == nil {
            
            cell.watchListButton.addTarget(self, action: #selector(MovieCarouselCollectionViewController.watchlistButtonAction), for: .touchUpInside)
        }
        
        if movie.showing == false {
            if movie.hasSchedule == false {
                cell.advanceBookingBg.isHidden = true
                cell.advanceBookingLabel.isHidden = true
            } else {
                cell.advanceBookingBg.isHidden = false
                cell.advanceBookingLabel.isHidden = false
            }
        } else {
            cell.advanceBookingBg.isHidden = true
            cell.advanceBookingLabel.isHidden = true
        }
        
        if movie.poster == nil {
            cell.posterImageView.image = nil
            downloadPoster(mo: movie, indexPath: indexPath)
        } else {
            cell.posterImageView.image = movie.poster
        }
        
        if movie.watched == true {
            cell.watchListButton.backgroundColor = UIManager.buttonRed
            cell.watchListButton.setImage(UIImage(named: "watchlist-check"), for: .normal)
        } else {
            cell.watchListButton.backgroundColor = UIManager.watchButtonGray
            cell.watchListButton.setImage(UIImage(named: "watchlist-add"), for: .normal)
        }
        
        if sectionTitle.localizedCaseInsensitiveCompare("coming soon") == .orderedSame {
            cell.releaseaDate.text = UIManager.comingSoonDateFormatter(dateString: movie.releaseDateString!)
            cell.releaseDateHeight.constant = 16.0
        } else {
            cell.releaseaDate.text = ""
            cell.releaseDateHeight.constant = 0.0
        }
    }
    
    private func downloadPoster(mo: MovieModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: mo.posterUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    mo.poster = image
                    if self.collectionView?.indexPathsForVisibleItems.contains(indexPath) == true {
                        self.collectionView?.reloadItems(at: [indexPath])
                    }
                }
            } else {
                
            }
        }
    }
    
    @objc private func watchlistButtonAction(sender : MButton) {
        
        AnalyticsHelper.sendButtonEvent(name: "movies_watchlist_button")
        let movieObject : MovieModel = items![sender.tag]
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            appDelegate.showLoginModal()
            return
        }
        
        if movieObject.watched == true {
            let alert = UIAlertController(title: "", message: "You’re about to remove this movie from your watchlist", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
                //sender.isOn = true
            })
            let removeAction = UIAlertAction(title: "Remove", style: .default, handler: { (_) in
                sender.loadingIndicator(show: true)
                appDelegate.removeMovieFromWatchList(mo: movieObject, completion: {[weak self, weak sender] (success) -> Void? in
                    DispatchQueue.main.async {
                        
                        if self == nil || sender == nil {
                            return
                        }
                        sender!.loadingIndicator(show: false)
                        
                        if success {
                            if self!.collectionView?.indexPathsForVisibleItems.contains(IndexPath(item: sender!.tag, section: 0)) == false {
                                return
                            }
                            self!.collectionView?.performBatchUpdates({
                                self?.collectionView?.reloadItems(at: [IndexPath(item: sender!.tag, section: 0)])
                            }, completion: nil)
                            print("reloaded")
                        }
                    }
                })
            })
            alert.addAction(cancelAction)
            alert.addAction(removeAction)
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        } else {
            
            sender.loadingIndicator(show: true)
            appDelegate.addMovieToWatchList(mo: movieObject, completion: {[weak self, weak sender] (success) -> Void? in
                DispatchQueue.main.async {
                    
                    if self == nil || sender == nil {
                        return
                    }
                    
                    sender!.loadingIndicator(show: false)
                    
                    if success {
                        
                        if self!.collectionView?.indexPathsForVisibleItems.contains(IndexPath(item: sender!.tag, section: 0)) == false {
                            return
                        }
                        
                        
                        self!.collectionView?.performBatchUpdates({
                            self!.collectionView?.reloadItems(at: [IndexPath(item: sender!.tag, section: 0)])
                        }, completion: nil)
                        
                        let popup = PopupViewController(nibName: "PopupViewController", bundle: nil, expandedHeight: 140.0, expandedWidth: 140.0)
                        let confirmView = Bundle.main.loadNibNamed("ConfirmationView", owner: self, options: nil)?.first as! ConfirmationView
                        confirmView.translatesAutoresizingMaskIntoConstraints = false
                        confirmView.confirmImage.image = UIImage(named: "added-watchlist")
                        confirmView.confirmMessage.text = "Saved to Watchlist!"
                        
                        popup.delegate = self
                        
                        popup.showView(view: confirmView, offset: CGPoint.zero)
                    }
                }
            })
        }
    }
    
    @objc private func notificationReceived(notif : Notification) {
        if notif.name == Notification.Name(rawValue: "MovieModelUpdateRemove") {
            let movieId : String = notif.object as! String
            
            let mo : MovieModel? = items?.first(where: { (mo1) -> Bool in
                return mo1.movieId == movieId
            })
            
            mo?.watched = false
            
            self.collectionView?.reloadData()
        } else if notif.name == Notification.Name(rawValue: "MovieModelUpdateAdd") {
            let movieId : String = notif.object as! String
            
            let mo : MovieModel? = items?.first(where: { (mo1) -> Bool in
                return mo1.movieId == movieId
            })
            
            mo?.watched = true
            
            self.collectionView?.reloadData()
        }
    }
}

extension MovieCarouselCollectionViewController: PopupViewControllerDelegate
{
    func onHidden(popup: PopupViewController) {
        if AccountHelper.shared.isRegisteredInRush {
            
            UserEngagementHelper.shared.earnBadgeFrom(engagement: "add-watch-list")
        }
    }
}
