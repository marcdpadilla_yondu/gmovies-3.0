//
//  ConfirmationView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 11/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class ConfirmationView: UIView {

    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var confirmMessage: UILabel!
    @IBOutlet weak var confirmImage: UIImageView!
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        bg.layer.cornerRadius = 5.0
    }

}
