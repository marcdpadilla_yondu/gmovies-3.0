//
//  RatingHelper.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 03/02/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation

public class RatingHelper {
    
    class var shared : RatingHelper {
        struct Static {
            static let instance : RatingHelper = RatingHelper()
        }
        return Static.instance
    }
    
    public func checkDidRate(result:@escaping (Bool)->()) {
        
        let lbid = UserDefaults.standard.value(forKeyPath: "user.lbid") as! String
        
        DispatchQueue.global(qos: .utility).async {
            
            GMoviesAPIManager.checkDidRate(lbid: lbid) { (error, response) in
                
                if error == nil {
                    
                    if let data = response?["response"] as! [String : Any]? {
                        let status = data["status"] as! Int
                        if status == 0 {
                            DispatchQueue.main.async {
                                result(false)
                            }
                        }else if status == 1 {
                            DispatchQueue.main.async {
                                result(true)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        result(true)
                    }
                }
            }
        }
    }
    
    func send(rating:Int, comment:String, transactioId:String, success:@escaping ()->Void, failure:@escaping (String)->Void) {
        
        let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
        let userId = user.value(forKey: "uuid") as! String
        let mobile = user.value(forKey: "mobile") as! String
        
        DispatchQueue.global(qos: .utility).async {
            
            GMoviesAPIManager.send(rating: rating, comment: comment, userId: userId, mobile: mobile, transactionId: transactioId, completion: { (error, response) in
                
                if error == nil {
                    if let data = response?["response"] as! [String : Any]? {
                        let status = data["status"] as! Int
                        if status == 0 {
                            DispatchQueue.main.async {
                                failure("Record not updated.")
                            }
                        }else if status == 1 {
                            DispatchQueue.main.async {
                                success()
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        failure(error!.localizedDescription)
                    }
                }
            })
        }
    }
}
