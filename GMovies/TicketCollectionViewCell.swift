//
//  TicketCollectionViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 9/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TicketCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var summaryButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var ticketHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ticketBg: UIView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var theaterLogo: UIImageView!
    @IBOutlet weak var theaterName: UILabel!
    @IBOutlet weak var cinema: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var seatContainer: UIView!
    @IBOutlet weak var qrCode: UIImageView!
    @IBOutlet weak var qrValue: UILabel!
    @IBOutlet weak var qrBg: UIView!
    
    @IBOutlet weak var ticketPrice: UILabel!
    @IBOutlet weak var convenienceFee: UILabel!
    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var promo: UILabel!
    @IBOutlet weak var promoLabel: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var convenienceFeeLabel: UILabel!
    
    @IBOutlet weak var popcornLabel: UILabel!
    @IBOutlet weak var popcornFee: UILabel!
    
    @IBOutlet weak var theaterLogoHeight: NSLayoutConstraint!
    
    private var line = CAShapeLayer()
    
    var isExpanded : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        qrBg.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        qrBg.layer.borderWidth = 0.5
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        cutOut(view: ticketBg)
        ticketHeight.constant = 505.0
        scrollView.layoutIfNeeded()
        scrollView.contentSize = scrollContentView.bounds.size
        line.strokeColor = UIColor(netHex: 0xdcdcdc).cgColor
        line.fillColor = nil
        line.lineDashPattern = [4, 2]
        ticketBg.layer.addSublayer(line)
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        path.addLine(to: CGPoint(x: ticketBg.frame.size.width, y: 0.0))
        line.path = path.cgPath
        line.frame = CGRect(x: 0.0, y: summaryButton.frame.origin.y - 3.0, width: ticketBg.frame.size.width, height: 2.0)
    }
    
    func cutOut(view : UIView) {
        
        let path = CGMutablePath()
        path.addArc(center: CGPoint(x: 0, y: 0), radius: 15, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.addArc(center: CGPoint(x: view.bounds.size.width/2.0, y: 0), radius: 15, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.addArc(center: CGPoint(x: view.bounds.size.width, y: 0), radius: 15, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.move(to: CGPoint(x: 0, y: summaryButton.frame.origin.y - 3.0))
        path.addArc(center: CGPoint(x: 0, y: summaryButton.frame.origin.y - 3.0), radius: 8, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.move(to: CGPoint(x: view.bounds.size.width, y: summaryButton.frame.origin.y - 3.0))
        path.addArc(center: CGPoint(x: view.bounds.size.width, y: summaryButton.frame.origin.y - 3.0), radius: 8, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        path.addRect(CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
//
        let maskLayer = CAShapeLayer()
        maskLayer.backgroundColor = UIColor.white.cgColor
        maskLayer.path = path;
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        view.layer.mask = maskLayer
        view.clipsToBounds = true
        
    }
    
    
    @IBAction func summaryAction(_ sender: Any) {
        ticketHeight.constant = 700.0
        scrollView.layoutIfNeeded()
        scrollView.contentSize = scrollContentView.bounds.size
        scrollView.setContentOffset(CGPoint(x: 0.0, y: 145.0), animated: true)
        isExpanded = true
    }
}
