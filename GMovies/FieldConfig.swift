//
//  FieldConfig.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class FieldConfig: NSObject {
    
    var label : String?
    var errorString : String?
    var error : Bool = false
    var placeholder : String?
    var prefix : String?
    var type : String = "default"
    var val : String = ""
    
    override init() {
        
    }
    
    init(label ps : String?, errorString es : String?) {
        label = ps
        errorString = es
    }
    
    init(label ps : String?, errorString es : String?, placeholder p : String?) {
        label = ps
        errorString = es
        placeholder = p
    }
    
    init(label ps : String?, errorString es : String?, placeholder p : String?, prefix pre : String?) {
        label = ps
        errorString = es
        placeholder = p
        prefix = pre
    }
}
