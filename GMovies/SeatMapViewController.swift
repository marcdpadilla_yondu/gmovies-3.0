//
//  SeatMapViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 8/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class SeatMapViewController: UIViewController, UIScrollViewDelegate {
    
    var seatmap : [[SeatModel]] = []
    var rowLetters : [String]?
    var maxSeats : Int = Constants.maxSeatsPerTransaction
    weak var delegate:SeatmapViewProtocol?
    fileprivate let screenFrame : CGRect = UIScreen.main.bounds
    
    @IBOutlet weak var scrollView: UIScrollView!
    var contentView: UIView?
    @IBOutlet weak var contentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewCenterX: NSLayoutConstraint!
    
    
    private var selectedSeatsCount : Int = 0
    private var timer : Timer?
    private var ticker : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        contentView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: screenFrame.size.width, height: screenFrame.size.height-64.0))
        contentView!.backgroundColor = UIManager.seatmapBg
        rowLetters = []
        
        scrollView.addSubview(contentView!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentView!.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        updateMinZoomScaleForSize(self.view.bounds.size)
    }
    
    func setSeatMapData(rawData : NSDictionary) {
        print(rawData)
        let rawSeatmap : [[String]] = rawData.value(forKeyPath: "seatmap") as! [[String]]
        let available : [String] = rawData.value(forKeyPath: "available.seats") as! [String]
        
        clearSeatmap()
        
        for row : [String] in rawSeatmap {
            var rs : [SeatModel] = []
            let l = SeatModel()
            l.state = .aisle
            rs.append(l)
            
            for col : String in row {
                let seat : SeatModel = SeatModel()
                seat.seatId = col
                seat.seatName = col
                if col.contains("(") == true {
                    seat.state = .aisle
                } else if available.contains(col) == true {
                    seat.state = .available
                } else {
                    seat.state = .taken
                }
                rs.append(seat)
                
                if col.contains("(") == false {
                    
                    let letter = seat.seatName?.components(separatedBy: .decimalDigits).joined(separator: "")
//                    print("1 \(letter)")
                    if rowLetters?.contains(letter!) == false {
                        rowLetters?.append(letter!)
                    }
                }
            }
            let r = SeatModel()
            r.state = .aisle
            rs.append(l)
            seatmap.append(rs)
        }
        
        generateSeatMap(seatMapData: seatmap)
    }
    
    func clearSeatmap() {

        selectedSeatsCount = 0
        
        contentView?.frame = CGRect(x: 0.0, y: 0.0, width: screenFrame.size.width, height: screenFrame.size.height-64.0)
        scrollView.layoutIfNeeded()
        scrollView.zoomScale = 1.0
        
        //print("clear1", contentView!.subviews.count, scrollView.subviews.count)
        
        if (contentView?.subviews.count)! > 0 {
            for v in (contentView?.subviews)! {
                v.removeFromSuperview()
            }
        }
        
        //print("clear2", contentView!.subviews.count, scrollView.subviews.count)
        seatmap.removeAll()
    }
    
    //MARK: Private functions
    
    fileprivate func generateSeatMap(seatMapData d : [[SeatModel]]) {
        
        let minAvailableWidth : CGFloat = contentView!.bounds.size.width

        //get max columns from seatMapData and calculate seat width
        //width = height
        //spacing = width / 2
        
        let cols : Int = (d.sorted { (s1, s2) -> Bool in
            return s2.count < s1.count
            }.first?.count)!
        
        let rows : Int = d.count
        let spacing : CGFloat = 2.0
        let width : CGFloat = (minAvailableWidth - (spacing * CGFloat(cols))) / (CGFloat(cols) /*+ (CGFloat(cols/2)) - 7.0*/)
        let height : CGFloat = width
        
        //draw!

        let screen : UILabel = UILabel(frame: CGRect(x: spacing, y: spacing, width: minAvailableWidth-(spacing * 2), height: 30.0))
        screen.layer.borderColor = UIManager.screenBorderColor.cgColor
        screen.layer.borderWidth = 0.5
        screen.backgroundColor = UIColor.clear
        screen.textAlignment = NSTextAlignment.center
        screen.font = UIFont(name: "OpenSans", size: 15.0)!
        screen.text = "MOVIE SCREEN"
        screen.textColor = UIManager.seatmapFontColor
        contentView?.addSubview(screen)
        
        var x : CGFloat = spacing
        var y : CGFloat = screen.frame.origin.y + screen.frame.size.height + height * 3
        
        var blankRow = 0
        var blankRowIndex = -1
        for j in 0 ..< rows {
            
            if d[j].count == d[j].filter({ (s) -> Bool in
                return s.state == .aisle
            }).count {
                blankRow += 1
                blankRowIndex = j
            }
            
            let gap : CGFloat = CGFloat(abs(cols - d[j].count)) / 2.0
//            print(gap)
            x += (gap * width)
            if gap >= 1 {
                x += (gap * spacing)
            }
            
            for i in 0 ..< cols {
                let b : MButton = MButton(type: .custom)
                b.frame = CGRect(x: x, y: y, width: width, height: height)
                b.layer.cornerRadius = 2.0
                b.radioMode = true
                b.offBgColor = UIManager.seatAvailableBg
                b.onBgColor = UIManager.seatSelectedBg
                b.onImage = UIImage(named: "seat-selected")
                
                if i >= d[j].count {
                    continue
                }
                
                let seat : SeatModel = d[j][i]
                b.setValue(seat, forKey: "payload")
                
                if seat.state != .aisle {
                    b.backgroundColor = UIManager.seatAvailableBg
                    if seat.state == .taken {
                        b.backgroundColor = UIManager.seatReservedBg
                        b.isEnabled = false
                    } else {
                        b.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                    }
                } else {
                    b.isEnabled = false
                    if (i == 0 || i == d[j].count - 1) && blankRowIndex != j {
                        b.titleLabel?.adjustsFontSizeToFitWidth = true
                        b.titleLabel?.font = UIFont(name: "OpenSans", size: height-3.0)!
                        
                        if j >= blankRow && j - blankRow < rowLetters!.count {
                            //print(rowLetters?.count, j, blankRow, rowLetters)
                            b.setTitle(rowLetters![j - blankRow], for: UIControlState())
                        } else {
                            b.setTitle(rowLetters!.last!, for: UIControlState())
                        }
                        b.setTitleColor(UIManager.seatmapFontColor, for: .disabled)
                        b.backgroundColor = UIManager.seatmapBg
                    }
                }
                
                contentView!.addSubview(b)
                x += width + spacing
            }
            y += spacing + height
            x = spacing
        }
        
        
        //recalculate contentview height
        
        contentView!.frame = CGRect(x: 0.0, y: 0.0, width: view.bounds.size.width, height: y + spacing*2 + height)
        updateMinZoomScaleForSize(self.view.bounds.size)
        print("generate", contentView!.subviews.count, scrollView.subviews.count)
    }
    
    @objc fileprivate func buttonAction(_ sender : MButton) {
        let seat : SeatModel = sender.value(forKey: "payload") as! SeatModel
        
        if selectedSeatsCount < 0 {
            selectedSeatsCount = 0
        }
        print(sender.isOn)
        
        if sender.isOn {
            selectedSeatsCount += 1
            if selectedSeatsCount <= Constants.maxSeatsPerTransaction {
                if delegate?.didSelectSeat(sender: self, seatmapObject: seat) == false {
                    selectedSeatsCount -= 1
                    sender.isOn = false
                }
                
            } else {
                selectedSeatsCount = Constants.maxSeatsPerTransaction
                sender.isOn = false
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBanner(message: "Up to \(maxSeats) seat\(maxSeats == 1 ? "" : "s") per transaction only", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
            }
            
        } else {
            if selectedSeatsCount > 0 {
                delegate?.didDeselectSeat(sender: self, seatmapObject: seat)
                selectedSeatsCount -= 1
                sender.isOn = false
            } else {
                selectedSeatsCount = 0
                sender.isOn = false
            }
        }
        
    }
    
    fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / contentView!.bounds.width
        let heightScale = size.height / contentView!.bounds.height
        
        let minScale = min(widthScale, heightScale)
        
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 3.0
        
        scrollView.setZoomScale(minScale, animated: true)

    }
    
    fileprivate func updateConstraintsForSize(_ size: CGSize) {
        
        let yOffset = max(0, (size.height - contentView!.frame.height) / 2)
        let xOffset = max(0, (size.width - contentView!.frame.width) / 2)
        
        var frame : CGRect = contentView!.frame
        frame.origin.x = xOffset
        frame.origin.y = yOffset
        contentView!.frame = frame
        
        view.layoutIfNeeded()
    }
    
    //MARK: Scroll view delegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return contentView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateConstraintsForSize(view.bounds.size)
    }
}

protocol SeatmapViewProtocol : class {
    func didSelectSeat(sender s : SeatMapViewController, seatmapObject so : SeatModel) -> Bool
    func didDeselectSeat(sender s : SeatMapViewController, seatmapObject so : SeatModel)
}
