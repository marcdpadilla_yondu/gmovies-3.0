//
//  BlockScreeningModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 17/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class BlockScreeningModel: NSObject {
    /*
     {
     "available_date" = "2016-11-30";
     mechanics = "";
     name = "Doctor Strange Blocked Screening";
     poster = "";
     "poster_landscape" = "";
     price = "0.00";
     "promo_id" = "a60758ae-adb6-4b8a-9bab-0ea11a70c02b";
     "start_time" = "20:00";
     theater =             {
     address = "Makati Avenue, Makati City";
     cinema = "Cinema 3";
     "link_name" = "greenbelt-3";
     name = "Greenbelt 3";
     "theater_logo" = "http://lh3.googleusercontent.com/HsTHRgr4qJZQ5UQD0W6Ar-ofJ8aya5FRTIwzxX376G48hILG0vs1ZLbwW5cCMPnCwvqiPstmFC5kXoUHQ0X_X12yNCeOXKd8nZF9";
     };
     }
    */
    
    var availableDate : String?
    var mechanics : String?
    var name : String?
    var posterUrl : String?
    var poster : UIImage?
    var landscapePosterUrl : String?
    var landscapePoster : UIImage?
    var price : String?
    var promoId : String?
    var startTime : String?
    var theater : TheaterModel?
    var movie : MovieModel?
    
    var screeningDateTime : Date? {
        get {
            let df = DateFormatter()
            df.locale = Locale.current
            df.dateFormat = "yyyy-MM-dd HH:mm"
            
            return df.date(from: "\(availableDate!) \(startTime!)")
        }
    }
    
    var screeningDateTimeString : String {
        get {
            let df = DateFormatter()
            df.locale = Locale.current
            df.dateFormat = "dd MMM - hh:mm a"
            
            return "\(df.string(from: screeningDateTime ?? Date()))"
        }
    }
    
    var screeningTimeAMPM : String {
        get {
            let df = DateFormatter()
            df.locale = Locale.current
            df.dateFormat = "hh:mm a"
            
            return "\(df.string(from: screeningDateTime!))"
        }
    }
    
    var screeningDateString : String {
        get {
            let df = DateFormatter()
            df.locale = Locale.current
            df.dateFormat = "dd MMM yyyy"
            
            return "\(df.string(from: screeningDateTime!))"
        }
    }
    
    init(data : NSDictionary) {
        //print(data)
        availableDate = data.value(forKey: "available_date") as? String
        mechanics = data.value(forKey: "mechanics") as? String
        name = data.value(forKey: "name") as? String
        posterUrl = data.value(forKey: "poster") as? String
        landscapePosterUrl = data.value(forKey: "poster_landscape") as? String
        price = data.value(forKey: "price") as? String
        promoId = data.value(forKey: "promo_id") as? String
        startTime = data.value(forKey: "start_time") as? String
        
        theater = TheaterModel(data: data.value(forKey: "theater") as! NSDictionary)
        
        let c = CinemaModel()
        c.name = data.value(forKeyPath: "theater.cinema") as! String
        
        theater?.cinemas?.append(c)
        
        movie = MovieModel(data: data.value(forKey: "movie") as! NSDictionary)
    }
}
