//
//  RewardMechanicModel.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 21/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation

public struct RewardMechanicsModel {
    
    var title: String!
    var details: String!
    
    public init(title: String, details: String) {
        self.title = title
        self.details = details
    }
}
