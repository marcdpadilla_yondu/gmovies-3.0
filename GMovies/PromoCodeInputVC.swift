//
//  PromoCodeInputVC.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 11/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit



@objc protocol PromoCodeInputVCDelegate {
    @objc func onCancelClicked()
    @objc func onOkClicked(promoCode: String?)
}

class PromoCodeInputVC: UIViewController {
    
    @IBOutlet weak var promoCodeTextField: UITextField!
    
    var delegate: PromoCodeInputVCDelegate?
    var defaultText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.promoCodeTextField.autocapitalizationType = .allCharacters
        promoCodeTextField.delegate = self
        promoCodeTextField.text = defaultText
    }
    
    
    @IBAction func onClickCancel(_ sender: Any) {
        delegate?.onCancelClicked()
        let popup : PopupViewController = self.parent as! PopupViewController
        popup.hide()
    }
    
    @IBAction func onClickOk(_ sender: Any) {
        delegate?.onOkClicked(promoCode: promoCodeTextField.text)
        let popup : PopupViewController = self.parent as! PopupViewController
        popup.hide()
    }
}

extension PromoCodeInputVC: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let lowercaseCharRange:NSRange =  (string as NSString).rangeOfCharacter(from: .lowercaseLetters)
        if lowercaseCharRange.location != NSNotFound {
            let nsString = textField.text as NSString?
            textField.text = nsString?.replacingCharacters(in: range, with: string.uppercased())
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        promoCodeTextField.resignFirstResponder()
        
        return true
    }
}
