//
//  CalendarItem.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 4/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CalendarItem: NSObject {
    
    /*
     {
     date = "2016-10-11";
     id = 20161011;
     label = "Oct 11, Tue";
     }
    */
    
    var enabled : Bool = false
    var selected : Bool = false
    var current : Bool = false
    var date : Date?
    
    var dateId : Int = 0
    
    override init() {
        super.init()
        date = Date()
    }
    
    convenience init(dateObject d : Date) {
        self.init()
        date = d
    }
    
    convenience init(dateString : String) {
        self.init()
        
        date = UIManager.scheduleDateStringToDate(dateString: dateString)
        enabled = true
    }
    
    fileprivate func generateDateString(format f : String) -> String {
        let df : DateFormatter = DateFormatter()
        df.dateFormat = f
        
        return df.string(from: date!)
    }
    
    func dateNumber() -> String {
        return generateDateString(format: "d")
    }
    
    func monthName() -> String {
        return generateDateString(format: "MMMM")
    }
    
    func yearNumber() -> String {
        return generateDateString(format: "yyyy")
    }
}
