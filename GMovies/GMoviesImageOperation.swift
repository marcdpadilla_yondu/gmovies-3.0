//
//  GMoviesImageOperation.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 23/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class GMoviesImageOperation: GMoviesOperation, URLSessionDelegate {
    var completion : (_ error : Error?, _ result : UIImage?) -> Void?
    var url : NSURL?
    
    override init() {
        completion = {_,_ in return}
        
        super.init()
    }
    
    convenience init(URL urlString: String, completion : @escaping (_ error : Error?, _ result : UIImage?) -> Void) {
        self.init()
        
        url = NSURL(string: urlString)
        self.completion = completion
    }
    
    override func start() {
        super.start()
        
        let session : URLSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue.main)
        let urlRequest : URLRequest = URLRequest(url: url! as URL, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60.0)
        
        
        let task : URLSessionDownloadTask = session.downloadTask(with: urlRequest) { (url, response, error) in
            
            //print(response)
            if error == nil {
                
                do {
                    let image = UIImage(data: try Data(contentsOf: url!))
                    self.completion(nil, image)
                } catch (let err) {
                    self.completion(err, nil)
                }
                
            } else {
                self.completion(error, nil)
            }
            
            self.finish()
        }
        
        task.resume()
    }
    
    override func cancel() {
        super.cancel()
        self.finish()
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust{
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential,credential);
        }
    }
}
