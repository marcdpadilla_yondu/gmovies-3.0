//
//  PaymentLoaderViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PaymentLoaderViewController: UIViewController, UINavigationControllerDelegate, PaymentResultDelegate {
    
    private var isPaymentGatewayLoaded : Bool = false
    private var paymentGatewayUrl : String?
    private var errorView : ErrorView?
    private var tx : String?
    
    var paymentRoot : UIViewController?
    
    var selectedPayment : PaymentOptionModel?
    var theaterObject : TheaterModel?
    var movieObject : MovieModel?
    var cinemaObject : CinemaModel?
    var timeObject : TimeModel?
    var dateObject : Date?
    var reservedSeating : Bool = false
    var seatCount : Int = 0
    var selectedSeatsController : SeatSelectionViewController?
    var email : String?
    var promo : String?
    
    var blockObject : BlockScreeningModel?
    
    var cancelTransaction : Bool = false
    
    //gcash
    var gcash : String?
    var otp : String?
    
    //mpass
    var username : String?
    var password : String?
    
    //claim_code
    var claimCode : String?
    
    var paymentRequestRunning : Bool = false
    
    var delegate : PaymentLoaderProtocol?

    @IBOutlet weak var loaderView: LoaderView!
    private var paymentView : UIView?
    private let paymentSdk = Ipay()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loaderView.loaderImages = UIManager.paymentLoader()
        if paymentRequestRunning == false {
            sendPayment()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.delegate = self
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.title = selectedPayment!.name!.uppercased()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "payment_loader")
        if blockObject == nil {
            reservedSeating = timeObject!.seatingType?.caseInsensitiveCompare("reserved seating") == .orderedSame
        } else {
            reservedSeating = true
        }
        
        if reservedSeating == true {
            seatCount = selectedSeatsController!.selectedSeats!.count
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func sendPayment() {
        let user : NSDictionary = UserDefaults.standard.object(forKey: "user") as! NSDictionary
        
        var seats : [String] = []
        if reservedSeating == true {
            for s : SeatModel in selectedSeatsController!.selectedSeats! {
                seats.append(s.seatId!)
            }
        }
        
        let seatPayload : Any = reservedSeating == false ? seatCount : seats
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        
        var payload : NSMutableDictionary? = nil
        
        if blockObject == nil {
            payload = ["first_name" : user.value(forKey: "first_name") as! String,
            "last_name" : user.value(forKey: "last_name") as! String,
            "email" : email!,
            "mobile" : user.value(forKey: "mobile") as! String,
            "seats" : seatPayload,
            "discount" : promo == nil ? "" : promo!,
            "schedule" : ["show_date" : df.string(from: dateObject!),
            "show_time" : timeObject!.timeId!,
            "movie" : movieObject!.movieId!,
            "cinema" : String(cinemaObject!.cinemaId!),
            "theater" : theaterObject!.theaterId!]
            ]
        } else {
            payload = ["first_name" : user.value(forKey: "first_name") as! String,
                       "last_name" : user.value(forKey: "last_name") as! String,
                       "email" : email!,
                       "mobile" : user.value(forKey: "mobile") as! String,
                       "seats" : seatPayload,
                       "discount" : promo == nil ? "" : promo!,
                       "schedule" : ["promo_id" : blockObject!.promoId!]
            ]
        }
        
        if gcash != nil {
            payload?.addEntries(from: ["gcash_account" : gcash!, "otp" : otp!])
        }
        
        if username != nil {
            payload?.addEntries(from: ["username" : username!, "password" : password ?? ""])
        }
        
        if claimCode != nil {
            payload?.addEntries(from: ["claim_code" : claimCode!])
        }
        
//        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.showBanner(message: "Processing your transaction", isPersistent: true, color: UIColor(netHex: 0x26c5a9), textColor: UIColor.white)
        
        paymentRequestRunning = true
        
        loaderView.startAnimating()
        
        var paymentId : String = selectedPayment!.optionId!
        
        GMoviesAPIManager.sendPayment(paymentOptionId: paymentId, params: payload!, completion: { (error, result) in
            if error == nil {
                //print(result)
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    let transactionId : String? = result?.value(forKeyPath: "data.result.id") as? String
                    if transactionId != nil {
                        self.checkState(transactionId: transactionId!)
                    }
                } else {
                    print(result?.value(forKeyPath: "data.message") ?? "")
                }
            } else {
                print(error!.localizedDescription)
            }
        })
    }
    
    @objc private func checkState(transactionId : String) {
        
        if cancelTransaction == true {
            return
        }
        
        tx = transactionId
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        GMoviesAPIManager.checkState(paymentOptionId: selectedPayment!.optionId!, transactionId: transactionId) { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                //print(result)
                if status == 1 {
                    let state : String = result?.value(forKeyPath: "data.state") as! String
                    print(state)
                    DispatchQueue.main.async {
                        switch (state) {
                        case "TX_RESERVATION_OK":
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_GENERATE_TICKET":
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            
                            break
                        case "TX_RESERVATION_HOLD":
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_PREPARE":
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_START":
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_STARTED":
                            self.isPaymentGatewayLoaded = false
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_PAYNAMICS_PAYMENT_HOLD":
                            if self.isPaymentGatewayLoaded == false {
                                self.loadPaymentWebView(url: result?.value(forKeyPath: "data.url") as! String)
                            }
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_CLIENT_PAYMENT_HOLD":
                            if self.isPaymentGatewayLoaded == false {
                                self.loadPaymentWebView(url: result?.value(forKeyPath: "data.url") as! String)
                            }
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_EXTERNAL_PAYMENT_HOLD":
                            //ipay88
                            let url = result?.value(forKeyPath: "data.url") as! String
                            if self.isPaymentGatewayLoaded == false {
                                GMoviesAPIManager.loadCustomEndpoint(endpoint: url, completion: { (error, result) in
                                    if error == nil {
                                        let status = result?.value(forKey: "status") as! Int
                                        if status == 1 {
                                            //print(result)
                                            
                                            /*
                                             paymentsdk = [[Ipay alloc] init];
                                             paymentsdk.delegate = self;
                                             IpayPayment *payment = [[IpayPayment alloc] init];
                                             [payment setPaymentId:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"PaymentId"]];
                                             [payment setMerchantKey:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"MerchantKey"]];
                                             [payment setMerchantCode:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"MerchantCode"]];
                                             [payment setRefNo:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"RefNo"]];
                                             [payment setAmount:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"Amount"]];
                                             [payment setCurrency:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"Currency"]];
                                             [payment setProdDesc:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"ProdDesc"]];
                                             [payment setUserName:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"UserName"]];
                                             [payment setUserEmail:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"UserEmail"]];
                                             [payment setUserContact:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"UserContact"]];
                                             [payment setRemark:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"Remark"]];
                                             [payment setLang:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"Lang"]];
                                             [payment setCountry:@"PH"];
                                             [payment setBackendPostURL:[[_responseDictionary objectForKey:@"payment_info"]objectForKey:@"BackendURL"]];
                                            */
                                            
                                            
                                            self.paymentSdk.delegate = self
                                            let payment = IpayPayment()
                                            payment.paymentId = result?.value(forKeyPath: "data.payment_info.PaymentId") as! String
                                            payment.merchantKey = result?.value(forKeyPath: "data.payment_info.MerchantKey") as! String
                                            payment.merchantCode = result?.value(forKeyPath: "data.payment_info.MerchantCode") as! String
                                            payment.refNo = result?.value(forKeyPath: "data.payment_info.RefNo") as! String
                                            payment.amount = result?.value(forKeyPath: "data.payment_info.Amount") as! String
                                            payment.currency = result?.value(forKeyPath: "data.payment_info.Currency") as! String
                                            payment.prodDesc = result?.value(forKeyPath: "data.payment_info.ProdDesc") as! String
                                            payment.userName = result?.value(forKeyPath: "data.payment_info.UserName") as! String
                                            payment.userEmail = result?.value(forKeyPath: "data.payment_info.UserEmail") as! String
                                            payment.userContact = result?.value(forKeyPath: "data.payment_info.UserContact") as! String
                                            payment.remark = result?.value(forKeyPath: "data.payment_info.Remark") as! String
                                            payment.lang = result?.value(forKeyPath: "data.payment_info.Lang") as! String
                                            payment.country = "PH"
                                            payment.backendPostURL = result?.value(forKeyPath: "data.payment_info.BackendURL") as! String
                                            
                                            DispatchQueue.main.async {
                                                self.paymentView = self.paymentSdk.checkout(payment)
                                                self.loadPaymentWebView()
                                            }
                                            
                                        } else {
                                            
                                        }
                                    } else {
                                        print(error!.localizedDescription)
                                        DispatchQueue.main.async {
                                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                                        }
                                    }
                                    
//                                    DispatchQueue.main.async {
//                                        self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
//                                    }
                                })
                            } else {
//                                print(error!.localizedDescription)
                                DispatchQueue.main.async {
                                    self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                                }
                            }
                            break
                        case "TX_FINALIZE":
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_FINALIZE_HOLD":
                            self.perform(#selector(self.checkState), with: transactionId, afterDelay: 3)
                            break
                        case "TX_DONE":
//                            self.loaderView.actionButton.isHidden = false
//                            self.loaderView.actionButton.addTarget(self, action: #selector(self.goToTicket), for: .touchUpInside)
//                            self.loaderView.loaderTitle.text = "Thanks for booking with GMovies!"
//                            self.loaderView.loaderSubtitle.text = "Simply present your e-ticket at the cinema door. Your ticket is saved in the app and in your photo gallery."
//                            self.loaderView.LoaderImageView.stopAnimating()
//                            self.loaderView.LoaderImageView.image = UIImage(named: "success-ticket")
//                            if self.navigationController?.visibleViewController != self {
//                                self.navigationController?.popToViewController(self, animated: true)
//                            }
                            if self.self.navigationController?.visibleViewController != self {
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                            
                            self.delegate?.didFinishTransaction(success: true, transactionId: transactionId)
                            appDelegate.showBanner(message: "Success!", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
                            self.paymentRequestRunning = false
                            
                            break
                            
                        default:
                            
                            self.delegate?.didFinishTransaction(success: false, transactionId: transactionId)
                            
                            if result!.value(forKeyPath: "data.message") != nil {
                                let alert = UIAlertController(title: "Error", message: result!.value(forKeyPath: "data.message") as? String, preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                    self.navigationController?.popToViewController(self.paymentRoot!, animated: true)
                                })
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                                appDelegate.showBanner(message: "Something went wrong", isPersistent: false)
                            }
                            
                            self.paymentRequestRunning = false
                            self.loadErrorView()
                            
                            break
                            
                        }
                    }
                } else {
                    print(result?.value(forKeyPath: "data.message"))
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    @objc private func goToTicket(sender : UIButton) {
        self.navigationController?.popToViewController(paymentRoot!, animated: true)
    }
    
    private func loadPaymentWebView(url : String) {
        isPaymentGatewayLoaded = true
        paymentGatewayUrl = url
        performSegue(withIdentifier: "showPaymentGateway", sender: self)
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showBanner(message: "Enter your credit card details", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
    }
    
    private func loadPaymentWebView() {
        isPaymentGatewayLoaded = true
        performSegue(withIdentifier: "showPaymentGateway", sender: self)
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showBanner(message: "Enter your credit card details", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
    }
    
    private func loadErrorView() {
        
        view.layoutIfNeeded()
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = UIManager.configureErrorView(parentController: self, superview: self.view)
        errorView!.errorTitle.text = "Oh no!"
        errorView!.errorDesc.text = "Sorry, the movie schedule is not yet available in this cinema. Try later or select a different cinema."
        errorView!.errorIcon.image = UIImage(named: "error-popcorn")
        errorView!.retryButton.isHidden = false
        
        errorView!.retryButton.setTitle("BACK TO PAYMENT", for: .normal)
        errorView!.retryButton.addTarget(self, action: #selector(self.retryButtonAction), for: .touchUpInside)
    }
    
    @objc private func retryButtonAction(sender : UIButton) {
        self.navigationController?.popToViewController(paymentRoot!, animated: true)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPaymentGateway" {
            let paymentGateway : PaymentGatewayViewController = segue.destination as! PaymentGatewayViewController
            if paymentView == nil {
                paymentGateway.url = paymentGatewayUrl
            } else {
                paymentGateway.ipay88View = paymentView
            }
            paymentGateway.paymentRoot = paymentRoot
            paymentGateway.title = self.title
            paymentGateway.showBackButton = true
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController.isKind(of: PaymentViewController.self) {
            cancelTransaction = true
            print(viewController)
        }
    }
    
    //ipay88 delegate
    
    func paymentSuccess(_ refNo: String!, withTransId transId: String!, withAmount amount: String!, withRemark remark: String!, withAuthCode authCode: String!) {
        
        print("ipay88 success \(tx)")
        self.checkState(transactionId: tx!)
    }
    
    func paymentFailed(_ refNo: String!, withTransId transId: String!, withAmount amount: String!, withRemark remark: String!, withErrDesc errDesc: String!) {
        print("ipay88 failed \(tx)")
        self.checkState(transactionId: tx!)
    }
    
    func paymentCancelled(_ refNo: String!, withTransId transId: String!, withAmount amount: String!, withRemark remark: String!, withErrDesc errDesc: String!) {
        print("ipay88 cancelled \(tx) \(transId)")
        self.checkState(transactionId: tx!)
    }
    
    func requerySuccess(_ refNo: String!, withMerchantCode merchantCode: String!, withAmount amount: String!, withResult result: String!) {
        
    }
    
    func requeryFailed(_ refNo: String!, withMerchantCode merchantCode: String!, withAmount amount: String!, withErrDesc errDesc: String!) {
        
    }
}

protocol PaymentLoaderProtocol {
    func didFinishTransaction(success : Bool, transactionId : String)
}
