//
//  CinemaListViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 12/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CinemaListViewController: UITableViewController {
    
    var theaterObject : TheaterModel?
    var currentDateId : Int?
    private let cellIdentifier : String = "schedCinemaIdentifier"
    
    var delegate : CinemaListProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.register(UINib(nibName: "SchedCinemaCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (theaterObject?.cinemas?.filter({ (c) -> Bool in
            return c.dateId == currentDateId!
        }).count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        configureCell(cell: cell as! SchedCinemaCell, indexPath: indexPath)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            let cinema = theaterObject?.cinemas?.filter({ (c) -> Bool in
                return c.dateId == currentDateId!
            })[indexPath.row]

            
            delegate?.didSelectCinema(cinema: cinema!)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: Private functions
    
    private func configureCell(cell : SchedCinemaCell, indexPath : IndexPath) {
        let cinema : CinemaModel = (theaterObject?.cinemas?.filter({ (c) -> Bool in
            return c.dateId == currentDateId!
        })[indexPath.row])!
        
        cell.cinemaName.text = cinema.name!
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}

protocol CinemaListProtocol {
    func didSelectCinema(cinema : CinemaModel)
}
