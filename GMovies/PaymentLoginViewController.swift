//
//  PaymentLoginViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 7/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PaymentLoginViewController: UITableViewController {

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var paymentLogo: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var noteHolder: UIView!
    
    
    var fields : [FieldConfig]?
    
    var paymentRoot : UIViewController?
    
    var selectedPayment : PaymentOptionModel?
    var theaterObject : TheaterModel?
    var movieObject : MovieModel?
    var cinemaObject : CinemaModel?
    var timeObject : TimeModel?
    var dateObject : Date?
    var reservedSeating : Bool = false
    var seatCount : Int = 0
    var selectedSeatsController : SeatSelectionViewController?
    var email : String?
    var promo : String?
    
    var blockObject : BlockScreeningModel?
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        fields = [FieldConfig(label: "SPACE", errorString: "")]
        
        if selectedPayment?.optionId?.localizedCaseInsensitiveContains("gcash") == true {
            let f = FieldConfig(label: "ENTER YOUR REGISTERED GCASH NO.", errorString: "This field is required", placeholder: "917XXXXXXXX", prefix: "+63")
            f.type = "mobile"
            let mobile = UserDefaults.standard.value(forKeyPath: "user.mobile") as! String
            f.val = mobile.substring(from: mobile.index(mobile.startIndex, offsetBy: 1))
            
            fields?.append(f)
            
            paymentLogo.image = UIImage(named: "gcash-logo")
            submitButton.setTitle("PROCEED", for: .normal)
            helpButton.isHidden = false
            AnalyticsHelper.sendScreenShowEvent(name: "payment_gcash_login")
        } else if selectedPayment?.optionId?.localizedCaseInsensitiveContains("mpass") == true {
            let f = FieldConfig(label: "USERNAME", errorString: "Invalid username")
            let p = FieldConfig(label: "PASSWORD", errorString: "This field is required")
            p.type = "password"
            
            fields?.append(contentsOf: [f,p])
            paymentLogo.image = UIImage(named: "mpass-logo")
            submitButton.setTitle("LOG IN", for: .normal)
            helpButton.isHidden = true
            AnalyticsHelper.sendScreenShowEvent(name: "payment_mpass_login")
        } else if selectedPayment?.optionId?.localizedCaseInsensitiveContains("claim_code") == true {
            let f = FieldConfig(label: "ENTER YOUR CLAIM CODE", errorString: "This field is required")
            noteHolder.isHidden = false
            fields?.append(f)
            paymentLogo.image = UIImage(named: "claimcode-logo")
            submitButton.setTitle("SUBMIT", for: .normal)
            helpButton.isHidden = true
            AnalyticsHelper.sendScreenShowEvent(name: "payment_claim_code")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = selectedPayment!.name!.uppercased()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    fileprivate func configureView() {
        UIManager.roundify(submitButton)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (fields?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell?
        
        if ((indexPath as NSIndexPath).row == 0) {
            cell = tableView.dequeueReusableCell(withIdentifier: "space", for: indexPath) as! TextFieldTableViewCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! TextFieldTableViewCell
            
            configureCell(cell as! TextFieldTableViewCell, indexPath: indexPath)
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath as NSIndexPath).row == 0 {
            return 50.0
        }
        
        return 80.0
    }
    
    fileprivate func configureCell(_ cell : TextFieldTableViewCell, indexPath : IndexPath) {
        let labelString = fields![(indexPath as NSIndexPath).row].label!
        let f : FieldConfig = fields![indexPath.row]
        
        cell.prompt.isHidden = true
        cell.label.text = labelString
        
        cell.separator.layer.borderWidth = 0.25
        cell.separator.layer.borderColor = UIColor(netHex: 0x777f99).cgColor
        
        cell.placeholder = fields![(indexPath as NSIndexPath).row].placeholder
        cell.pre = fields![(indexPath as NSIndexPath).row].prefix
        cell.prefix.text = fields![(indexPath as NSIndexPath).row].prefix
        
        cell.maxLength = 0
        cell.inputField.keyboardType = .default
        cell.inputField.isSecureTextEntry = false
        
        if selectedPayment?.optionId?.localizedCaseInsensitiveContains("claim_code") == true {
            cell.capitalize = true
        }
        
        if selectedPayment?.optionId?.localizedCaseInsensitiveContains("gcash") == true {
            cell.inputField.text = f.val
            cell.expand(state: true)
        }
        
        if f.type == "mobile" {
            cell.maxLength = 10
            cell.inputField.keyboardType = .numberPad
        } else if f.type == "password" {
            cell.inputField.isSecureTextEntry = true
        }
        
    }
    
    private func validateFields() -> Bool {
        
        var isValid : Bool = true
        for i : Int in 1 ..< (fields?.count)! {
            
            let f : FieldConfig = fields![i]
            
            let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: i, section: 0)) as! TextFieldTableViewCell
            if cell.inputField.text?.characters.count == 0 {
                cell.prompt.text = "Field is required"
                cell.prompt.isHidden = false
                cell.shake()
                isValid = false
            }
            
            if f.type == "mobile" {
                if Int(cell.inputField.text!) == nil {
                    cell.prompt.text = "Invalid mobile number"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                } else if (cell.inputField.text?.characters.count)! < 10 {
                    cell.prompt.text = "Invalid mobile number"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                }
            }
        }
        
        return isValid
    }
    
    @IBAction func submitAction(_ sender: AnyObject) {
        if validateFields() {
            
            if selectedPayment?.optionId == "gcash" {
                AnalyticsHelper.sendButtonEvent(name: "payment_gcash_submit")
                let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: 1, section: 0)) as! TextFieldTableViewCell
                GMoviesAPIManager.gcashLogin(mobileNum: cell.inputField.text!, completion: { (error, result) in
                    if error == nil {
                        let status : Int = result?.value(forKeyPath: "response.status") as! Int
                        if status == 0 {
                            DispatchQueue.main.async {
                                cell.prompt.text = (result?.value(forKeyPath: "response.msg") as! String)
                                cell.prompt.isHidden = false
                                cell.shake()
                            }
                        } else {
                            self.performSegue(withIdentifier: "showGCashVerify", sender: self)
                        }
                        
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            } else {
                performSegue(withIdentifier: "showPaymentLoader", sender: self)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGCashVerify" {
            let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: 1, section: 0)) as! TextFieldTableViewCell
            let v : VerifyGCashViewController = segue.destination as! VerifyGCashViewController
            v.mobileNumber = cell.inputField.text!
            
            v.selectedPayment = selectedPayment
            if blockObject == nil {
                v.theaterObject = theaterObject
                v.movieObject = movieObject
                v.cinemaObject = cinemaObject
                v.timeObject = timeObject
                v.dateObject = dateObject
            } else {
                v.blockObject = blockObject
            }
            v.reservedSeating = reservedSeating
            v.seatCount = seatCount
            v.selectedSeatsController = selectedSeatsController
            v.email = email
            v.promo = promo
            v.paymentRoot = paymentRoot
            
        } else if segue.identifier == "showPaymentLoader" {
            let paymentLoader : PaymentLoaderViewController = segue.destination as! PaymentLoaderViewController
            
            paymentLoader.selectedPayment = selectedPayment
            if blockObject == nil {
                paymentLoader.theaterObject = theaterObject
                paymentLoader.movieObject = movieObject
                paymentLoader.cinemaObject = cinemaObject
                paymentLoader.timeObject = timeObject
                paymentLoader.dateObject = dateObject
            } else {
                paymentLoader.blockObject = blockObject
            }
            paymentLoader.reservedSeating = reservedSeating
            paymentLoader.seatCount = seatCount
            paymentLoader.selectedSeatsController = selectedSeatsController
            paymentLoader.email = email
            paymentLoader.promo = promo
            paymentLoader.paymentRoot = paymentRoot
            paymentLoader.delegate = (paymentRoot as! ProgressNavigationViewController).viewControllers?[1] as! PaymentViewController
            
            if selectedPayment?.optionId!.localizedCaseInsensitiveContains("mpass") == true {
                AnalyticsHelper.sendButtonEvent(name: "payment_mpass_submit")
                let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: 1, section: 0)) as! TextFieldTableViewCell
                let cell1 : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: 2, section: 0)) as! TextFieldTableViewCell
                paymentLoader.username = cell.inputField.text
                paymentLoader.password = cell1.inputField.text
            } else if selectedPayment?.optionId!.localizedCaseInsensitiveContains("claim_code") == true {
                AnalyticsHelper.sendButtonEvent(name: "payment_claim_code_submit")
                let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: 1, section: 0)) as! TextFieldTableViewCell
                paymentLoader.claimCode = cell.inputField.text
            }
        } else if segue.identifier == "showGCashHelp" {
            let help = segue.destination as! PaymentGatewayViewController
            help.url = "\(Constants.gMoviesWebIP)/iframe/payment/gcash_help"
            help.showBackButton = true
            help.title = "GCASH"
            help.paymentRoot = self
            
        }
    }

}
