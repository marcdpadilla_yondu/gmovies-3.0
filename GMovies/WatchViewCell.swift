//
//  WatchViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 17/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class WatchViewCell: UITableViewCell {
    
    
    @IBOutlet weak var advanceBooking: UIButton!
    @IBOutlet weak var abBg: UIView!
    @IBOutlet weak var watchButton: MButton!
    @IBOutlet weak var posterCell: UIImageView!
    
    @IBOutlet weak var audienceRating: UIButton!
    @IBOutlet weak var runtime: UILabel!
    @IBOutlet weak var genren: UILabel!
    @IBOutlet weak var advisory: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var rottenRating: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        advisory.layer.borderWidth = 0.5
        advisory.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        advisory.layer.cornerRadius = 3.0
        advanceBooking.isHidden = true
        abBg.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
