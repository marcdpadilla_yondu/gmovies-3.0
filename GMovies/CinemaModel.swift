//
//  CinemaModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 11/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CinemaModel: NSObject {
    /*
     {
     "cinema_id" = 3;
     id = "20161011-3";
     label = "Cinema 3";
     }
    */
    
    var cinemaId : String?
    var dateCinemaId : String?
    var dateId : Int = 0
    var name : String?
    var times : [TimeModel]?
    
    override init() {
        super.init()
    }
    
    init (data : NSDictionary) {
        super.init()
//        print(data)
        cinemaId = data.object(forKey: "cinema_id") as? String
        dateCinemaId = data.object(forKey: "id") as? String
        name = data.object(forKey: "label") as? String
//        dateId = data.object(forKey: "parent") as! Int
        times = []
    }
}
