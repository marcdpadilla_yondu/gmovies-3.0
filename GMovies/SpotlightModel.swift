//
//  SpotlightModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class SpotlightModel: NSObject {
    /*
     "title": "10 Movie Roles That Jennifer Lawrence will Totally Nail!",
     "subtitle": "Forbes ranked Jennifer Lawrence as the World's Highest-Paid Actress(2015). Here are 10 priceless roles we know she would totally kill.",
     "slug": "10-movie-roles-that-jennifer-lawrence-will-totally-nail"
     "featured_image" : "sample.png"
    */
    
    private var _title : String?
    private var _subtitle: String?
    
    var slug : String?
    var imgUrl : String?
    var img : UIImage?
    var listImgUrl : String?
    var listImg : UIImage?
    var share : NSDictionary?
    
    var title : String? {
        get {
            return _title == nil ? "N/A" : _title!
        }
        
        set (t) {
            _title = t
        }
    }
    
    var subtitle : String? {
        get {
            return _subtitle == nil ? "N/A" : _subtitle!
        }
        
        set (st) {
            _subtitle = st
        }
    }
    
    init(data : NSDictionary) {
        super.init()
        title = data.value(forKey: "title") as? String
        subtitle = data.value(forKey: "subtitle") as? String
        imgUrl = data.value(forKey: "photo_url") as? String
        listImgUrl = data.value(forKey: "list_photo_url") as? String
        slug = data.value(forKey: "slug") as? String
        share = data.value(forKey: "share") as? NSDictionary
    }
}
