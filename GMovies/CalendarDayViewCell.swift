//
//  CalendarDayViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CalendarDayViewCell: UICollectionViewCell {

    @IBOutlet weak var dayLetter: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
