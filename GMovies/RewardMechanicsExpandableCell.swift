//
//  RewardMechanicsExpandableCell.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 21/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

class RewardMechanicsExpandableCell: YNExpandableCell {

    static let ID = "RewardMechanicsExpandableCell"
    
    @IBOutlet var titleLabel: UILabel!
    
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func initView() {
        
        let width = UIScreen.main.bounds.size.width
        let height = self.frame.size.height
        
        //let margins = self.container.layoutMarginsGuide
        //pageViewController?.view.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 0).isActive = true
        
        self.normalCustomAccessoryType = UIImageView(frame: CGRect(x: width - 46, y: height/2, width: 26, height: 26))
        self.normalCustomAccessoryType.image = UIImage(named: "mechanics-plus")
        self.selectedCustomAccessoryType = UIImageView(frame: CGRect(x: width - 46, y: height/2, width: 26, height: 26))
        self.selectedCustomAccessoryType.image = UIImage(named: "mechanics-minus")
        self.selectedCustomAccessoryType.isHidden = true
        
        self.contentView.addSubview(self.normalCustomAccessoryType)
        self.contentView.addSubview(self.selectedCustomAccessoryType)
        
        let margins = self.contentView.layoutMarginsGuide
        
        self.normalCustomAccessoryType.translatesAutoresizingMaskIntoConstraints = false
        self.selectedCustomAccessoryType.translatesAutoresizingMaskIntoConstraints = false
        
        self.normalCustomAccessoryType.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 0).isActive = true
        self.normalCustomAccessoryType.centerYAnchor.constraint(equalTo: margins.centerYAnchor, constant: 0).isActive = true
        self.normalCustomAccessoryType.widthAnchor.constraint(equalToConstant: 26).isActive = true
        self.normalCustomAccessoryType.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        self.selectedCustomAccessoryType.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 0).isActive = true
        self.selectedCustomAccessoryType.centerYAnchor.constraint(equalTo: margins.centerYAnchor, constant: 0).isActive = true
        self.selectedCustomAccessoryType.widthAnchor.constraint(equalToConstant: 26).isActive = true
        self.selectedCustomAccessoryType.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        self.selectionStyle = .none
    }
}
