//
//  MovieModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 22/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MovieModel: NSObject {
    var movieId : String?
    var movieTitle : String?
    var movieLinkName : String?
    var landscapePosterUrl : String?
    var landscapePoster : UIImage?
    var posterUrl : String?
    var poster : UIImage?
    var releaseDateString : String?
    var hasSchedule : Bool = false
    var theaters : [String]?
    var watched : Bool = false
    var showing : Bool = false
    
    var advisoryRating : String?
    var genre : String?
    var cast : [String]?
    var runtime : Float?
    
    var synopsis : String?
    
    var rottenRating : Int?
    var audienceRating : Int?
    var rottenSource : String?
    var audienceSource : String?
    var youtube : String?
    var averageRating : Int = 0
    var yourRating : Int = 0
    
    var share : NSDictionary?
    
    var rottenRatingPercent : Int? {
        get {
            if rottenRating == nil || rottenRating == -1 {
                return 0
            }
            
            return rottenRating
        }
    }
    
    var audienceRatingPercent : Int? {
        get {
            if audienceRating == nil || audienceRating == -1 {
                return 0
            }
            
            return audienceRating
        }
    }
    
    var advisoryRatingString : String? {
        get {
            if advisoryRating == nil {
                return "N/A"
            }
            
            return advisoryRating
        }
    }
    
    var length : String? {
        get {
            if runtime == 0.0 {
                return "runtime N/A"
            }
            
            let hours : Int = Int(runtime! / 60.0)
            let mins : Int = Int(runtime! - Float(hours * 60))
            
            if hours == 0 {
                return "\(mins)min\(mins != 1 ? "s" : "")"
            }
            
            return "\(hours)hr\(hours != 1 ? "s" : "") \(mins)min\(mins != 1 ? "s" : "")"
        }
    }
    
    var castList : String? {
        get {
            if cast == nil {
                return "Cast N/A"
            }
            
            return NSArray(array: cast!).componentsJoined(by: ", ")
        }
    }
    
    var synopsisString : String? {
        get {
            if synopsis == nil {
                return "N/A"
            }
            
            return synopsis
        }
    }
    
    var youtubeLink : String {
        get {
            if youtube == nil {
                return "https://www.youtube.com"
            }
            return "https://www.youtube.com/watch?v=\(youtube!)"
        }
    }
    
    init(data : NSDictionary) {
        //print(data)
        movieId = data.object(forKey: "movie_id") as? String
        movieTitle = data.object(forKey: "canonical_title") as? String
        releaseDateString = data.object(forKey: "release_date") as? String
        landscapePosterUrl = data.object(forKey: "poster_landscape") as? String
        posterUrl = data.object(forKey: "poster") as? String
        if data.object(forKey: "has_schedules") != nil {
            hasSchedule = (data.object(forKey: "has_schedules") as? Bool)!
        }
        theaters = data.object(forKey: "theaters") as? [String]
        watched = data.object(forKey: "watch_list") as? Bool == nil ? false : data.object(forKey: "watch_list") as! Bool
        showing = data.object(forKey: "is_showing") as? Bool == nil ? false : data.object(forKey: "is_showing") as! Bool
        movieLinkName = data.object(forKey: "link_name") as? String
        
        advisoryRating = data.value(forKeyPath: "advisory_rating") as? String
        genre = data.value(forKeyPath: "genre") as? String
        synopsis = data.value(forKeyPath: "synopsis") as? String
        rottenRating = data.value(forKeyPath: "ratings.critics") as? Int
        audienceRating = data.value(forKeyPath: "ratings.audience") as? Int
        if let rc = data.value(forKeyPath: "ratings.critics_comment") as? String {
            rottenSource = rc
        } else {
            rottenSource = "plus"
        }
        
        if let ac = data.value(forKeyPath: "ratings.audience_comment") as? String {
            audienceSource = ac
        } else {
            audienceSource = "plus"
        }
        cast = data.value(forKeyPath: "cast") as? [String]
        if data.value(forKeyPath: "runtime_mins") != nil {
            runtime = Float((data.value(forKeyPath: "runtime_mins") as? String)!)
        }
        
        share = data.value(forKey: "share") as? NSDictionary
        if data.value(forKey: "average_rating") != nil && (data.value(forKey: "average_rating") as AnyObject).isKind(of: NSNull.self) == false {
            averageRating = data.value(forKeyPath: "average_rating") as! Int
        }
        
        if data.value(forKey: "your_rating") != nil {
            yourRating = data.value(forKeyPath: "your_rating") as! Int
        }
    }
}
