//
//  CityCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {
    
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cinemaCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
