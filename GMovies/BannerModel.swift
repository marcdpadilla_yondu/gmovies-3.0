//
//  BannerModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class BannerModel: NSObject {
    /*
    "image": "http://static.srcdn.com/wp-content/uploads/doctor-strange-trailer-poster-comic-con.jpg",
    "link_type": "movies",
    "object_id": "0660b5a2-8c12-44f0-932a-48432a7e5022",
    "link_url": ""
     */
    
    var imgUrl : String?
    var img : UIImage?
    var linkType : String?
    var objectId : String?
    var link : String?
    
    init(data : NSDictionary) {
        //print(data)
        imgUrl = data.object(forKey: "photo_url") as? String
        linkType = data.object(forKey: "link_type") as? String
        objectId = data.object(forKey: "object_id") as? String
        link = data.object(forKey: "link_url") as? String
    }
}
