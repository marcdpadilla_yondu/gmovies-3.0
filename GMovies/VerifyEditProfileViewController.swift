//
//  VerifyEditProfileViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 15/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FBSDKCoreKit

class VerifyEditProfileViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var otpField: UITextField!
    
    @IBOutlet weak var firstDigit: UILabel!
    @IBOutlet weak var secondDigit: UILabel!
    @IBOutlet weak var thirdDigit: UILabel!
    @IBOutlet weak var fourthDigit: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var resendPrompt: UILabel!
    
    
    @IBOutlet weak var firstLine: UIView!
    @IBOutlet weak var secondLine: UIView!
    @IBOutlet weak var thirdLine: UIView!
    @IBOutlet weak var fourthLine: UIView!
    
    private var myAccountTab : MTabBarViewController?
    
    
    @IBOutlet weak var indicator: UIButton!
    
    var userDetails : NSDictionary?
    
    var mobileNumber : String?
    var base64Photo : String?
    var removePhoto : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        indicator.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        otpField.becomeFirstResponder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "edit_profile_otp")
        otpField.becomeFirstResponder()
        
        perform(#selector(self.showResend), with: nil, afterDelay: 8.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        NotificationCenter.default.removeObserver(self, name: nil, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc private func applicationDidBecomeActive(notif : Notification) {
        if notif.name == Notification.Name.UIApplicationDidBecomeActive {
            otpField.becomeFirstResponder()
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        indicator.isHidden = true
        
        if string == "" {
            if textField.text?.characters.count == 1 {
                firstDigit.text = string
            } else if textField.text?.characters.count == 2 {
                secondDigit.text = string
            } else if textField.text?.characters.count == 3 {
                thirdDigit.text = string
            } else if textField.text?.characters.count == 4 {
                fourthDigit.text = string
            }
        } else {
            if textField.text?.characters.count == 0 {
                firstDigit.text = string
            } else if textField.text?.characters.count == 1 {
                secondDigit.text = string
            } else if textField.text?.characters.count == 2 {
                thirdDigit.text = string
            } else if textField.text?.characters.count == 3 {
                fourthDigit.text = string
                sendDetails(pin: "\(textField.text!)\(string)")
            } else {
                return false
            }
        }
        
        return true
    }
    
    //MARK : Private methods
    
    private func sendDetails(pin : String) {
        
        if userDetails?.value(forKey: "fbid") as! String != "0" {
            GMoviesAPIManager.editProfile(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, mobileNum: userDetails?.value(forKey: "mobile") as! String, pin: pin, firstName: userDetails?.value(forKey: "first_name") as! String, lastName: userDetails?.value(forKey: "last_name") as! String, email: userDetails?.value(forKey: "email") as! String, fbid: userDetails?.value(forKey: "fbid") as! String, bday: userDetails?.value(forKey: "bday") as! String, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    
                    if status == 1 {
                        DispatchQueue.main.async {
                            self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
                            self.indicator.isHidden = false
                        }
                        
                        UserDefaults.standard.set(true, forKey: "loggedin")
                        UserDefaults.standard.synchronize()
                        
                        let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                        AccountHelper.shared.saveUser(data: userData)
                        
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Success", message: "Your profile has been updated", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                
                            })
                            
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                            
                            GMoviesAPIManager.editPhoto(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, photo: self.base64Photo!, completion: { (error, result) in
                                
                                if error == nil {
                                    let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                                    let newUser : NSMutableDictionary = user.mutableCopy() as! NSMutableDictionary
                                    let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                    
                                    photo.setValue(self.base64Photo!, forKey: "mobile")
                                    newUser.setValue(photo, forKeyPath: "photo")
                                    UserDefaults.standard.setValue(newUser, forKey: "user")
                                    UserDefaults.standard.set(newUser, forKey: "user")
                                    UserDefaults.standard.synchronize()
                                    print("woof!")
                                } else {
                                    print(error!.localizedDescription)
                                }
                                
                            })
                        }
                        
                        DispatchQueue.main.async {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            self.firstDigit.shake()
                            self.secondDigit.shake()
                            self.thirdDigit.shake()
                            self.fourthDigit.shake()
                            self.firstLine.shake()
                            self.secondLine.shake()
                            self.thirdLine.shake()
                            self.fourthLine.shake()
                            self.indicator.isHidden = false
                            self.indicator.shake()
                            
                        }
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        } else {
            GMoviesAPIManager.editProfile(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, mobileNum: userDetails?.value(forKey: "mobile") as! String, pin: pin, firstName: userDetails?.value(forKey: "first_name") as! String, lastName: userDetails?.value(forKey: "last_name") as! String, email: userDetails?.value(forKey: "email") as! String, bday: userDetails?.value(forKey: "bday") as! String , completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    
                    if status == 1 {
                        DispatchQueue.main.async {
                            self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
                            self.indicator.isHidden = false
                        }
                        
                        UserDefaults.standard.set(true, forKey: "loggedin")
                        UserDefaults.standard.synchronize()
                        
                        let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                        AccountHelper.shared.saveUser(data: userData)
                        
                        let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                        
                        DispatchQueue.main.async {
                            
                            let alert = UIAlertController(title: "Success", message: "Your profile has been updated", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                
                            })
                            
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                            
                            if self.removePhoto == false {
                                GMoviesAPIManager.editPhoto(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, photo: self.base64Photo!, completion: { (error, result) in
                                    
                                    if error == nil {
                                        let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                                        let newUser : NSMutableDictionary = user.mutableCopy() as! NSMutableDictionary
                                        let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                        
                                        photo.setValue(self.base64Photo!, forKey: "mobile")
                                        newUser.setValue(photo, forKeyPath: "photo")
                                        UserDefaults.standard.setValue(newUser, forKey: "user")
                                        UserDefaults.standard.set(newUser, forKey: "user")
                                        UserDefaults.standard.synchronize()
                                        print("woof!")
                                    } else {
                                        print(error!.localizedDescription)
                                    }
                                    
                                })
                            } else {
                                GMoviesAPIManager.removePhoto(lbid: user.value(forKey: "lbid") as! String, completion: { (error, result) in
                                    if error == nil {
                                        let status = result?.value(forKeyPath: "response.status") as! Int
                                        if status == 1 {
                                            self.removePhoto = false
                                            let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                                            let newUser : NSMutableDictionary = user.mutableCopy() as! NSMutableDictionary
                                            let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                            
                                            photo.setValue("", forKey: "mobile")
                                            photo.setValue("", forKey: "website")
                                            newUser.setValue(photo, forKeyPath: "photo")
                                            UserDefaults.standard.setValue(newUser, forKey: "user")
                                            UserDefaults.standard.set(newUser, forKey: "user")
                                            UserDefaults.standard.synchronize()
                                        }
                                    } else {
                                        
                                    }
                                })
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            self.firstDigit.shake()
                            self.secondDigit.shake()
                            self.thirdDigit.shake()
                            self.fourthDigit.shake()
                            self.firstLine.shake()
                            self.secondLine.shake()
                            self.thirdLine.shake()
                            self.fourthLine.shake()
                            self.indicator.isHidden = false
                            self.indicator.shake()
                            self.indicator.setImage(UIImage(named: "verify-wrong"), for: .normal)
                            self.indicator.isHidden = false
                        }
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        }
    }
    
    private func configureView() {
        UIManager.buttonBorder(button: resendButton, width: 0.25, color: UIColor.gray)
        UIManager.roundify(resendButton)
        
        resendButton.alpha = 0
        resendPrompt.alpha = 0
        mobileLabel.text = "+63\(mobileNumber!)"
    }
    
    @objc private func showResend() {
        UIView.animate(withDuration: 0.250, animations: {
            self.resendButton.alpha = 1.0
            self.resendPrompt.alpha = 1.0
        }, completion: { (_) in
            
        })
    }
    
    private func hideResend() {
        UIView.animate(withDuration: 0.150, animations: {
            self.resendButton.alpha = 0.0
            self.resendPrompt.alpha = 0.0
        }, completion: { (_) in
            self.perform(#selector(self.showResend), with: nil, afterDelay: 8.0)
        })
    }
    
    @IBAction func resendAction(_ sender: AnyObject) {
        hideResend()
        //        firstDigit.shake()
        //        secondDigit.shake()
        //        thirdDigit.shake()
        //        fourthDigit.shake()
        //        firstLine.shake()
        //        secondLine.shake()
        //        thirdLine.shake()
        //        fourthLine.shake()
        GMoviesAPIManager.editProfile(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, mobileNum: userDetails?.value(forKey: "mobile") as! String) { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKeyPath: "response.status") as! Int
                if status != 1 {
                    //print(result?.value(forKeyPath: "response.msg"))
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }

}
