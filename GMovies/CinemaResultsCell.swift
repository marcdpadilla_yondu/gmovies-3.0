//
//  CinemaResultsCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 29/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CinemaResultsCell: UITableViewCell {
    
    @IBOutlet weak var cinemaName: UILabel!
    @IBOutlet weak var cinemaAddress: UILabel!
    @IBOutlet weak var heartButton: MButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        heartButton.radioMode = true
        heartButton.onImage = UIImage(named: "preferred-cinema")
        heartButton.offImage = UIImage(named: "unselected-cinema")
        
//        if UserDefaults.standard.bool(forKey: "loggedin") == false {
//            heartButton.isHidden = true
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
