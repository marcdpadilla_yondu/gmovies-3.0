//
//  CinemaResultsTableViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 29/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CinemaResultsTableViewController: NSObject, UITableViewDelegate, UITableViewDataSource {
    var tableView : UITableView?
    var parent : UIViewController?
    var dataSource : [TheaterModel]?
    private var reuseIdentifier : String = "resultCell"
    private var bookableOnly : Bool = false
    var delegate : CinemaResultsProtocol?
    
    override init() {
        super.init()
        tableView = nil
        dataSource = []
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bookableOnly {
            return (dataSource?.filter({ (t) -> Bool in
                let theater = t
                
                return theater.isActive == true
            }).count)!
        }
        return (dataSource?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        configureCell(cell: cell as! CinemaResultsCell, indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if delegate != nil {
            delegate?.didSelectTheater(theater: dataSource![indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func setData(data : [TheaterModel]) {
        dataSource = data
        
        if tableView != nil {
            tableView?.reloadData()
        }
    }
    
    func filterData(bookable : Bool) {
        if dataSource?.count == 0 {
            return
        }
        
        bookableOnly = bookable
        if tableView != nil {
            tableView?.reloadData()
        }
    }
    
    //MARK : Private function
    
    private func configureCell(cell : CinemaResultsCell, indexPath: IndexPath) {
        var filtered : [TheaterModel]? = nil
        if bookableOnly {
            filtered = dataSource?.filter({ (t) -> Bool in
                let theater = t 
                
                return theater.isActive == true
            })
        } else {
            filtered = dataSource
        }
        let theater : TheaterModel = filtered![indexPath.row] as TheaterModel
        
        cell.cinemaName.text = theater.name!
        cell.cinemaAddress.text = theater.address!
        if theater.isPreferred {
            cell.heartButton.setImage(UIImage(named: "preferred-cinema"), for: .normal)
        } else {
            cell.heartButton.setImage(UIImage(named: "unselected-cinema"), for: .normal)
        }
        cell.heartButton.tag = indexPath.row
        
        if cell.heartButton.actions(forTarget: self, forControlEvent: .touchUpInside) == nil {
            cell.heartButton.addTarget(self, action: #selector(self.heartButtonAction), for: .touchUpInside)
        }
        
    }
    
    @objc private func heartButtonAction(sender : MButton) {
        AnalyticsHelper.sendButtonEvent(name: "heart_button")
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            appDelegate.showLoginModal()
            return
        }
        
        var filtered : [TheaterModel]? = nil
        if bookableOnly {
            filtered = dataSource?.filter({ (t) -> Bool in
                let theater = t
                
                return theater.isActive == true
            })
        } else {
            filtered = dataSource
        }
        let to : TheaterModel = filtered![sender.tag] as TheaterModel
        
        if to.isPreferred == true {
            sender.isOn = true
            let alert = UIAlertController(title: "", message: "You’re about to remove this cinema on your preferred cinema list", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
                sender.isOn = true
            })
            let removeAction = UIAlertAction(title: "Remove", style: .default, handler: { (_) in
                sender.loadingIndicator(show: true)
                appDelegate.removeTheaterFromPreferred(to: to, completion: { (success) -> Void? in
                    DispatchQueue.main.async {
                        sender.loadingIndicator(show: false)
                        to.isPreferred = false
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TheaterModelUpdateRemove"), object: to.theaterId!)
                        sender.setImage(UIImage(named: "unselected-cinema"), for: .normal)
                        //print("removed theater")
                        self.tableView?.reloadData()
                    }
                })
            })
            alert.addAction(cancelAction)
            alert.addAction(removeAction)
            
            parent?.present(alert, animated: true, completion: nil)
        } else {
            sender.loadingIndicator(show: true)
            appDelegate.addTheaterToPreferred(to: to, completion: { (success) -> Void in
                if success == true {
                    DispatchQueue.main.async {
                        sender.loadingIndicator(show: false)
                        to.isPreferred = true
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TheaterModelUpdateAdd"), object: to.theaterId!)
                        sender.setImage(UIImage(named: "preferred-cinema"), for: .normal)
                        //print("added theater")
                        
                        let popup = PopupViewController(nibName: "PopupViewController", bundle: nil, expandedHeight: 140.0, expandedWidth: 140.0)
                        let confirmView = Bundle.main.loadNibNamed("ConfirmationView", owner: self, options: nil)?.first as! ConfirmationView
                        confirmView.translatesAutoresizingMaskIntoConstraints = false
                        confirmView.confirmImage.image = UIImage(named: "added-cinema")
                        confirmView.confirmMessage.text = "Saved to preferred!"
                        
                        popup.delegate = self
                        popup.showView(view: confirmView, offset: CGPoint.zero)
                        self.tableView?.reloadData()
                    }
                } else {
                    DispatchQueue.main.async {
                        sender.loadingIndicator(show: false)
                        sender.setImage(UIImage(named: "unselected-cinema"), for: .normal)
                    }
                }
                
            })
        }
    }
}

protocol CinemaResultsProtocol {
    func didSelectTheater(theater : TheaterModel)
}

extension CinemaResultsTableViewController: PopupViewControllerDelegate
{
    func onHidden(popup: PopupViewController) {
        if AccountHelper.shared.isRegisteredInRush {
            
            UserEngagementHelper.shared.earnBadgeFrom(engagement: UserEngagementCodes.choosePreferredCinema)
        }
    }
}
