//
//  ReelRewardsViewController.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 16/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit
import Nuke
import ReachabilitySwift

class ReelRewardsViewController: UIViewController {
    
    
    enum BadgeFilter: String {
        case lock = "lock"
        case unlocked = "unlock"
        case all = "mix"
    }
    
    @IBOutlet weak var availablePoints: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let availablePointsText = "Your Available Points is "
    fileprivate var currentPage: Int = 1
    fileprivate var maxPage: Int = 1
    fileprivate var currentFilter: BadgeFilter = .all
    fileprivate var downloadTaskRunning = false
    fileprivate var noBadgeIcon: UIImage?
    fileprivate var errorView: ErrorView?
    
    lazy var popDrop: PopDropViewController = {
        return PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 10)
    }()
    
    lazy var filtersTable: FilterTableViewController = {
        return FilterTableViewController()
    }()
    
    let badgeCellReuseIdentifier = "BadgeCollectionViewCellReuseIdentifier"
    
    var badgesData:[BadgeModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.register(BadgeCollectionViewCell.self, forCellWithReuseIdentifier: badgeCellReuseIdentifier)
        self.collectionView.register(UINib(nibName:"BadgeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: badgeCellReuseIdentifier)
        noBadgeIcon = UIImage(named: "nobadge")
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: ReachabilityChangedNotification,object: appDelegate.reachability)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        if AccountHelper.shared.currentUser?.rushId != nil {
            RushAPI.shared.getUserPoints(lbid: Constants.LBAPIToken, uuid: AccountHelper.shared.currentUser!.rushId!, successful: { (response) in
                print("")
                
                if (response.statusCode == RushAPIStatusCode.successful) {
                    
                    let responseCode = (response.responseData! as NSDictionary).value(forKeyPath: "response.details.error_code") as? String
                    
                    if responseCode == RushAPIResponseCode.successful.rawValue{
                        if let data = (response.responseData! as NSDictionary).value(forKeyPath: "response.details.data") as? Int {
                            self.availablePoints.text =  String(format: "\(self.availablePointsText) %d", data)
                        }
                    }
                }
                
            }) { (error) in
                print("")
            }
        }
        
        if AccountHelper.shared.currentUser?.rushId != nil {
            downloadFirstPage()
        }
    }
    
    @IBAction func onFilterClicked(_ sender: UIButton) {
        
        let filters:[String] = ["All", "Lock", "Unlocked"]
        popDrop.height = CGFloat(filters.count) * 44.0
        
        popDrop.showViewControllerFromView(viewController: filtersTable, originView: sender, offset: CGPoint(x: 40, y: 95))
        filtersTable.data = filters
        filtersTable.delegate = self
    }
}

extension ReelRewardsViewController: UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return badgesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: badgeCellReuseIdentifier,
                                                      for: indexPath) as! BadgeCollectionViewCell
        cell.groupIconImageView.image = nil
        cell.badgeIconImageView.image = noBadgeIcon
        
        let badgeData = badgesData[indexPath.row]
        
        weak var weakGroupBadgeIconView = cell.groupIconImageView
        weak var weakBadgeIconView = cell.badgeIconImageView
        
        if let url = URL(string: badgeData.badgeGroupIconUrl) {
            Nuke.loadImage(with: url, into: cell.groupIconImageView, handler: { (response, isFromCache) in
                if response.value != nil {
                    weakGroupBadgeIconView?.image = response.value
                }
            })
        }
        
        if let url = URL(string: badgeData.badgeIconUrl) {
            Nuke.loadImage(with: url, into: cell.badgeIconImageView, handler: { (response, isFromCache) in
                if response.value != nil {
                    weakBadgeIconView?.image = response.value
                }
            })
        }
        
        cell.descLabel.text = badgeData.desc
        cell.pointsLabel.text =  String(format: "%d Points", badgeData.points)
        cell.delegate = self
        cell.tag = indexPath.row
        
        if badgeData.isFlipped {
            cell.showDetails()
        }else {
            cell.hideDetails()
        }
        
        return cell
    }
}

extension ReelRewardsViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //3"5
        if UIScreen.main.bounds.maxY == 480 {
            return CGSize(width: 110, height: 110)
        }
        //4"0
        if UIScreen.main.bounds.maxY == 568 {
            return CGSize(width: 130, height: 130)
        }
        //4"7
        if UIScreen.main.bounds.maxY == 667 {
            return CGSize(width: 150, height: 150)
        }
        //5"5
        if UIScreen.main.bounds.maxY == 736 {
            return CGSize(width: 180, height: 180)
        }
        
        return CGSize(width: 130, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

extension ReelRewardsViewController: UICollectionViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        let reloadDistance : CGFloat = 10.0
        if y > h + reloadDistance {
            if downloadTaskRunning == false {
                
                if maxPage > currentPage {
                    currentPage += 1
                    downloadBadges(page: currentPage, filter: currentFilter, completion: { (badges, page) in
                        self.badgesData.append(contentsOf: badges)
                        if self.badgesData.count > 0 {
                            self.errorView?.removeFromSuperview()
                        }
                        self.collectionView.reloadData()
                    })
                } else {
                    currentPage = maxPage
                }
            }
        }
    }
}

extension ReelRewardsViewController: FilterTableViewControllerDelegate
{
    func onFilterSelected(filter: String) {
        
        var badgeFilter: BadgeFilter = .all
        if filter.lowercased() == "all" {
            badgeFilter = BadgeFilter.all
        }else if filter.lowercased() == "unlocked" {
            badgeFilter = .unlocked
        }else if filter.lowercased() == "lock" {
            badgeFilter = .lock
        }
        currentPage = 0
        maxPage = 1
        currentFilter = badgeFilter
        badgesData.removeAll()
        collectionView.reloadData()
        
        if AccountHelper.shared.currentUser?.rushId != nil {
            downloadFirstPage()
        }
    }
}

extension ReelRewardsViewController {
    
    func downloadBadges (page: Int, filter: BadgeFilter, completion: @escaping ([BadgeModel], Int) -> ()) {
        
        downloadTaskRunning = true
        
        RushAPI.shared.getUserBadges(lbid: Constants.LBAPIToken, rushId: AccountHelper.shared.currentUser!.rushId!, page: page, type: filter.rawValue, successful: { (response) in
            
            var badges:[BadgeModel] = []
            
            if response.statusCode == RushAPIStatusCode.successful {
                let status = (response.responseData! as NSDictionary).value(forKey: "status") as! Int
                if status == 1 {
                    
                    let responseCode = (response.responseData! as NSDictionary).value(forKeyPath: "details.error_code") as? String
                    if responseCode == RushAPIResponseCode.successful.rawValue {
                        let data = (response.responseData! as NSDictionary).value(forKeyPath: "details.data") as? [NSDictionary]
                        self.maxPage = (response.responseData! as NSDictionary).value(forKeyPath: "details.max_page") as! Int
                        
                        if data != nil {
                            for d in data! {
                                let badge = BadgeModel(data: d)
                                badges.append(badge)
                            }
                        }
                    }
                }
            }
            
            self.downloadTaskRunning = false
            completion(badges, page)
            
        }, failure: { (error) in
            self.downloadTaskRunning = false
            completion([], page)
        })
    }
    
    func loadErrorView(titleString : String, description : String, image: UIImage) {
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = self.view
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 37.0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addSubview(errorView!)
        self.view.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }
    
    @objc fileprivate func reachabilityChanged(note: NSNotification) {
        
        if note.name == ReachabilityChangedNotification {
            let reachability = note.object as! Reachability
            
            if reachability.isReachable {
                
                errorView?.removeFromSuperview()
                errorView = nil
                downloadFirstPage()
                
            } else {
                DispatchQueue.main.async {
                    self.loadErrorView(titleString: "No Internet", description: "Please check your network settings and try again", image: UIImage(named: "no-internet")!)
                }
            }
        }
    }
    
    func downloadFirstPage() {
        
        if downloadTaskRunning == false {
            self.badgesData.removeAll()
            self.collectionView.reloadData()
            currentPage = 1
            downloadBadges(page: currentPage, filter: currentFilter, completion: { (badges, page) in
                self.badgesData.append(contentsOf: badges)
                self.collectionView.reloadData()
                if self.badgesData.count == 0 {
                    
                    if self.currentFilter == .all {
                        self.loadErrorView(titleString: "Coming soon", description: "", image: self.noBadgeIcon!)
                    }else {
                        self.loadErrorView(titleString: "No badge available", description: "", image: self.noBadgeIcon!)
                    }
                    
                }else {
                    self.errorView?.removeFromSuperview()
                }
            })
        }
    }
}

extension ReelRewardsViewController: BadgeCollectionViewCellDelegate
{
    func onBadgeTapped(index: Int) {
        let badge = badgesData[index]
        badge.isFlipped = true
    }
    
    func onBadgeDetailsTapped(index: Int) {
        let badge = badgesData[index]
        badge.isFlipped = false
    }
}
