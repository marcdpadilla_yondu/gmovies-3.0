//
//  VerifyMobileViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 1/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FBSDKCoreKit

class VerifyMobileViewController: UITableViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var otpField: UITextField!
    
    @IBOutlet weak var firstDigit: UILabel!
    @IBOutlet weak var secondDigit: UILabel!
    @IBOutlet weak var thirdDigit: UILabel!
    @IBOutlet weak var fourthDigit: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var resendPrompt: UILabel!
    
    
    @IBOutlet weak var firstLine: UIView!
    @IBOutlet weak var secondLine: UIView!
    @IBOutlet weak var thirdLine: UIView!
    @IBOutlet weak var fourthLine: UIView!
    
    private var myAccountTab : MTabBarViewController?
    
    private var newlyRegisteredLoyaltyUser = false
    
    
    @IBOutlet weak var indicator: UIButton!
    
    var userDetails : NSDictionary?
    
    var fromLogin : Bool = false
    var mobileNumber : String?
    var resendAttempt : Int = 0
    
    var modalMode : Bool = false
    var joinLoyaltyProgramAutomatically = false
    var previousViewController : UIViewController?
    var nextViewController : UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        indicator.isHidden = true
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        otpField.becomeFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
        
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.OTP)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        otpField.becomeFirstResponder()
        
        perform(#selector(showResend), with: nil, afterDelay: 8.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        NotificationCenter.default.removeObserver(self, name: nil, object: nil)
        resendAttempt = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc private func applicationDidBecomeActive(notif : Notification) {
        if notif.name == Notification.Name.UIApplicationDidBecomeActive {
            otpField.becomeFirstResponder()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        indicator.isHidden = true
        
        if string == "" {
            if textField.text?.characters.count == 1 {
                firstDigit.text = string
            } else if textField.text?.characters.count == 2 {
                secondDigit.text = string
            } else if textField.text?.characters.count == 3 {
                thirdDigit.text = string
            } else if textField.text?.characters.count == 4 {
                fourthDigit.text = string
            }
        } else {
            if textField.text?.characters.count == 0 {
                firstDigit.text = string
            } else if textField.text?.characters.count == 1 {
                secondDigit.text = string
            } else if textField.text?.characters.count == 2 {
                thirdDigit.text = string
            } else if textField.text?.characters.count == 3 {
                fourthDigit.text = string
                if fromLogin == false {
                    sendDetails(pin: "\(textField.text!)\(string)")
                } else {
                    sendMobile(pin: "\(textField.text!)\(string)")
                }
            } else {
                return false
            }
        }
        
        return true
    }
    
    //MARK : Private methods
    
    private func checkPhoto() {
        let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
        if (user.value(forKeyPath: "photo.mobile") as! String).characters.count == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let req : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "picture.type(large)"])
            req.start { (conn, result, error) in
                if error == nil {
                    let url = (result as! NSDictionary).value(forKeyPath: "picture.data.url") as! String
                    DispatchQueue.main.async {
                        GMoviesAPIManager.downloadPoster(urlString: url, completion: { (error, image) in
                            if error == nil {
                                let imgData = UIImageJPEGRepresentation(image!, 1.0)
                                let user : NSDictionary? = UserDefaults.standard.value(forKey: "user") as? NSDictionary
                                let newUser : NSMutableDictionary = user?.mutableCopy() as! NSMutableDictionary
                                let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                photo.setValue(imgData?.base64EncodedString(), forKey: "mobile")
                                newUser.setValue(photo, forKeyPath: "photo")
                                UserDefaults.standard.set(newUser, forKey: "user")
                                UserDefaults.standard.synchronize()
                                
                                DispatchQueue.main.async {
                                    GMoviesAPIManager.editPhoto(lbid: user?.value(forKey: "lbid") as! String, photo: user?.value(forKeyPath: "photo.mobile") as! String, completion: { (error, result) in
                                        
                                        print("woof!")
                                        
                                    })
                                }
                                
                            } else {
                                print(error!.localizedDescription)
                            }
                        })
                    }
                } else {
                    print(error!.localizedDescription)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        } else if (user.value(forKeyPath: "photo.mobile") as! String).characters.count != 0 {
            
        }
    }
    
    private func sendDetails(pin : String) {
        
        let mobile = userDetails?.value(forKey: "mobile") as! String
        let firstName = userDetails?.value(forKey: "first_name") as! String
        let lastName = userDetails?.value(forKey: "last_name") as! String
        let email = userDetails?.value(forKey: "email") as! String
        
        let shouldRegisterToLoyaltyProgram: Bool = self.userDetails?.value(forKey: "register_loyalty_program") as! Bool
        
        if userDetails?.value(forKey: "fbid") as! String != "0" {
            
            let fbid = userDetails?.value(forKey: "fbid") as! String
            
            AccountHelper.shared.register(mobile: mobile, otp: pin, firstName: firstName, lastName: lastName, email: email, fbid: fbid, loyalty:shouldRegisterToLoyaltyProgram, successful: { data in
                
                self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
                self.indicator.isHidden = false
                
                UserDefaults.standard.set(true, forKey: "showTutorial")
                UserDefaults.standard.synchronize()
                
                self.checkPhoto()
                
                if self.modalMode == false {
                    if UserDefaults.standard.bool(forKey: "showTutorial") == true {
                        self.newlyRegisteredLoyaltyUser = AccountHelper.shared.isRegisteredInRush
                        self.performSegue(withIdentifier: "showTutorial", sender: nil)
                    } else {
                        self.loadHomeStoryboard()
                    }
                } else {
                    
                    if self.joinLoyaltyProgramAutomatically && AccountHelper.shared.isRegisteredInRush {
                        
                        let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
                        successRegistration.delegate = self
                        successRegistration.userName = AccountHelper.shared.currentUser?.firstName
                        successRegistration.dismissButtonTitle = "Go to Rewards"
                        self.navigationController?.setNavigationBarHidden(true, animated: false)
                        self.show(successRegistration, sender: self)
                        
                    }else {
                        if self.previousViewController == nil {
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            if self.previousViewController != self.nextViewController {
                                _ = self.previousViewController?.navigationController?.pushViewController(self.nextViewController!, animated: true)
                            } else {
                                _ = self.previousViewController?.navigationController?.popToViewController(self.previousViewController!, animated: true)
                            }
                        }
                    }
                }
                
            }, failure: { (error) in
                
                self.firstDigit.shake()
                self.secondDigit.shake()
                self.thirdDigit.shake()
                self.fourthDigit.shake()
                self.firstLine.shake()
                self.secondLine.shake()
                self.thirdLine.shake()
                self.fourthLine.shake()
                self.indicator.isHidden = false
                self.indicator.shake()

            })
            
        }else {
            
            AccountHelper.shared.register(mobile: mobile, otp: pin, firstName: firstName, lastName: lastName, email: email, loyalty: shouldRegisterToLoyaltyProgram, successful: { data in
                
                self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
                self.indicator.isHidden = false
                
                UserDefaults.standard.set(true, forKey: "showTutorial")
                UserDefaults.standard.synchronize()
                
                if self.modalMode == false {
                    if UserDefaults.standard.bool(forKey: "showTutorial") == true {
                        self.newlyRegisteredLoyaltyUser = AccountHelper.shared.isRegisteredInRush
                        self.performSegue(withIdentifier: "showTutorial", sender: nil)
                    } else {
                        self.loadHomeStoryboard()
                    }
                } else {
                    
                    if self.joinLoyaltyProgramAutomatically && AccountHelper.shared.isRegisteredInRush {
                        
                        let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
                        successRegistration.delegate = self
                        successRegistration.userName = AccountHelper.shared.currentUser?.firstName
                        successRegistration.dismissButtonTitle = "Go to Rewards"
                        self.navigationController?.setNavigationBarHidden(true, animated: false)
                        self.show(successRegistration, sender: self)
                        
                    }else {
                        if self.previousViewController == nil {
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            if self.previousViewController != self.nextViewController {
                                _ = self.previousViewController?.navigationController?.pushViewController(self.nextViewController!, animated: true)
                            } else {
                                _ = self.previousViewController?.navigationController?.popToViewController(self.previousViewController!, animated: true)
                            }
                        }
                    }
                }
                
            }, failure: { (error) in
                
                self.firstDigit.shake()
                self.secondDigit.shake()
                self.thirdDigit.shake()
                self.fourthDigit.shake()
                self.firstLine.shake()
                self.secondLine.shake()
                self.thirdLine.shake()
                self.fourthLine.shake()
                self.indicator.isHidden = false
                self.indicator.shake()
                self.indicator.setImage(UIImage(named: "verify-wrong"), for: .normal)
                self.indicator.isHidden = false
            })
            
        }
    }
    
    private func sendMobile(pin : String) {
        
        AccountHelper.shared.login(mobileNumber: mobileNumber!, otp: pin, successful: { data in
            
            self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
            self.indicator.isHidden = false
            
            AnalyticsHelper.sendEvent(category: "event", action: "login", label: "login_success", value: "OTP")
            if AccountHelper.shared.isRegisteredInRush {
                self.goToNextScreen()
                
            }else {
                if self.joinLoyaltyProgramAutomatically {
                    AccountHelper.shared.registerToRush(successful: { data in
                        
                        let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
                        successRegistration.delegate = self
                        successRegistration.userName = AccountHelper.shared.currentUser?.firstName
                        successRegistration.dismissButtonTitle = "Go to Rewards"
                        self.navigationController?.setNavigationBarHidden(true, animated: false)
                        self.show(successRegistration, sender: self)
                        
                    }) { (error) in
                        self.goToNextScreen()
                    }
                }else if !AccountHelper.shared.didShowJoinLoyaltyProgram {
                    
                    let loyaltyVC = JoinLoyaltyProgramViewController()
                    loyaltyVC.showJoinNowButton = true
                    loyaltyVC.showNotNowButton = true
                    loyaltyVC.delegate = self
                    self.show(loyaltyVC, sender: self)
                    AccountHelper.shared.setDidShowJoinLoyaltyProgram(show: true)
                }else {
                    self.goToNextScreen()
                }
            }
            
        }) { (error) in
            
            self.firstDigit.shake()
            self.secondDigit.shake()
            self.thirdDigit.shake()
            self.fourthDigit.shake()
            self.firstLine.shake()
            self.secondLine.shake()
            self.thirdLine.shake()
            self.fourthLine.shake()
            self.indicator.isHidden = false
            self.indicator.shake()
            self.indicator.setImage(UIImage(named: "verify-wrong"), for: .normal)
            self.indicator.isHidden = false
            AnalyticsHelper.sendEvent(category: "event", action: "login", label: "login_fail", value: "OTP")
        }
    }
    
    fileprivate func goToNextScreen(selectRewards:Bool = false)
    {
        if self.modalMode == false {
            if UserDefaults.standard.bool(forKey: "showTutorial") == true {
                
                self.performSegue(withIdentifier: "showTutorial", sender: nil)
            } else {
                self.loadHomeStoryboard(selectRewards: selectRewards)
            }
        } else {
            
            if self.previousViewController == nil {
                self.dismiss(animated: true, completion: nil)
            } else {
                if self.previousViewController != self.nextViewController {
                    self.previousViewController?.navigationController?.pushViewController(self.nextViewController!, animated: true)
                } else {
                   _ = self.previousViewController?.navigationController?.popToViewController(self.previousViewController!, animated: true)
                }
            }
        }
    }
    
    private func configureView() {
        UIManager.buttonBorder(button: resendButton, width: 0.25, color: UIColor.gray)
        UIManager.roundify(resendButton)
        
        resendButton.alpha = 0
        resendPrompt.alpha = 0
        mobileLabel.text = fromLogin == false ? userDetails?.object(forKey: "mobile") as? String : "+63\(mobileNumber!)"
    }
    
    @objc private func showResend() {
        UIView.animate(withDuration: 0.250, animations: {
            self.resendButton.alpha = 1.0
            self.resendPrompt.alpha = 1.0
            }, completion: { (_) in
                
        }) 
    }
    
    private func hideResend() {
        UIView.animate(withDuration: 0.150, animations: {
            self.resendButton.alpha = 0.0
            self.resendPrompt.alpha = 0.0
        }, completion: { (_) in
            self.perform(#selector(VerifyMobileViewController.showResend), with: nil, afterDelay: 8.0)
        }) 
    }
    
    @IBAction func resendAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "resend_otp")
        hideResend()
//        firstDigit.shake()
//        secondDigit.shake()
//        thirdDigit.shake()
//        fourthDigit.shake()
//        firstLine.shake()
//        secondLine.shake()
//        thirdLine.shake()
//        fourthLine.shake()
        if fromLogin == false {
            GMoviesAPIManager.register(mobileNum: userDetails?.value(forKey: "mobile") as! String) { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    if status != 1 {
                        print(result?.value(forKeyPath: "response.msg"))
                    }
                } else {
                    print(error!.localizedDescription)
                }
            }
        } else {
            if resendAttempt < 3 {
                GMoviesAPIManager.login(mobileNum: mobileNumber!, smsEmail : "sms") { (error, result) in
                    if error == nil {
                        let status : Int = result?.value(forKeyPath: "response.status") as! Int
                        if status != 1 {
                            print(result?.value(forKeyPath: "response.msg"))
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                }
            } else {
                let alert = UIAlertController(title: "", message: "Would you like to send the OTP to your email instead?", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "No", style: .default, handler: { (_) in
                    AnalyticsHelper.sendEvent(category: "event", action: "resend_otp", label: "mobile", value: "")
                    GMoviesAPIManager.login(mobileNum: self.mobileNumber!, smsEmail : "sms") { (error, result) in
                        if error == nil {
                            let status : Int = result?.value(forKeyPath: "response.status") as! Int
                            if status != 1 {
                                print(result?.value(forKeyPath: "response.msg"))
                            }
                        } else {
                            print(error!.localizedDescription)
                        }
                    }
                })
                let yesAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                    AnalyticsHelper.sendEvent(category: "event", action: "resend_otp", label: "email", value: "")
                    GMoviesAPIManager.login(mobileNum: self.mobileNumber!, smsEmail : "email") { (error, result) in
                        if error == nil {
                            let status : Int = result?.value(forKeyPath: "response.status") as! Int
                            if status != 1 {
                                print(result?.value(forKeyPath: "response.msg"))
                            }
                        } else {
                            print(error!.localizedDescription)
                        }
                    }
                })
                alert.addAction(noAction)
                alert.addAction(yesAction)
                
                present(alert, animated: true, completion: nil)
            }
            
            resendAttempt += 1
        }
    }
    
    fileprivate func loadHomeStoryboard(selectRewards: Bool = false) {
        var storyboard : UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let tab : MTabBarViewController = storyboard.instantiateInitialViewController() as! MTabBarViewController
        
        let home : UINavigationController = storyboard.instantiateViewController(withIdentifier: "HomeViewNavigation") as! UINavigationController
        home.title = "Movies"
        home.tabImageOn = UIImage(named: "movies-active")
        home.tabImageOff = UIImage(named: "movies")
        home.tabTextColorOn = UIColor(netHex: 0x435082)
        home.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Cinemas", bundle: nil)
        let cinemas : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        cinemas.title = "Cinemas"
        cinemas.tabImageOn = UIImage(named: "cinemas-active")
        cinemas.tabImageOff = UIImage(named: "cinemas")
        cinemas.tabTextColorOn = UIColor(netHex: 0x435082)
        cinemas.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Spotlight", bundle: nil)
        let spotlight : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        spotlight.title = "Spotlight"
        spotlight.tabImageOn = UIImage(named: "spotlight-active")
        spotlight.tabImageOff = UIImage(named: "spotlight")
        spotlight.tabTextColorOn = UIColor(netHex: 0x435082)
        spotlight.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Rewards", bundle: nil)
        let vc2 : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        vc2.title = "Rewards"
        vc2.tabImageOn = UIImage(named: "myrewards-active")
        vc2.tabImageOff = UIImage(named: "myrewards")
        vc2.tabTextColorOn = UIColor(netHex: 0x435082)
        vc2.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
        let myAccount : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        myAccount.title = " "
        myAccount.tabImageOn = UIImage(named: "myaccount-active")
        myAccount.tabImageOff = UIImage(named: "myaccount")
        myAccount.tabTextColorOn = UIColor(netHex: 0x435082)
        myAccount.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        myAccountTab = (myAccount.topViewController as? MTabBarViewController)
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            myAccountTab?.initialViewControllerIndex = 2
        }
//        myAccountTab!.delegate = self
        myAccountTab!.isIndicatorEnabled = true
        myAccountTab!.indicatorHugsContent = true
        let ticketList : TicketsListViewController = storyboard.instantiateViewController(withIdentifier: "TicketListView") as! TicketsListViewController
        myAccountTab!.title = "My Account"
        ticketList.tabImageOn = UIImage(named: "tickets-active")
        ticketList.tabImageOff = UIImage(named: "tickets")
        
        let watchList : WatchListViewController = storyboard.instantiateViewController(withIdentifier: "WatchListView") as! WatchListViewController
        watchList.tabImageOn = UIImage(named: "watchlist-active")
        watchList.tabImageOff = UIImage(named: "watchlist-1")
        
        let profileView : ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileView") as! ProfileViewController
        profileView.tabImageOn = UIImage(named: "profile-active")
        profileView.tabImageOff = UIImage(named: "profile")
        
        if selectRewards {
            tab.initialViewControllerIndex = 3
        }
        
        myAccountTab!.viewControllers = [ticketList, watchList, profileView]
        
        tab.viewControllers = [home, cinemas, spotlight, vc2, myAccount]
        
        self.navigationController?.pushViewController(tab, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTutorial" {
            let tvc: TutorialViewController = segue.destination as! TutorialViewController
            tvc.newlyRegisteredLoyaltyUser = newlyRegisteredLoyaltyUser
        }
    }
}

extension VerifyMobileViewController: JoinLoyaltyProgramViewControllerProtocol
{
    func onJoinNowClicked() {
        
        AccountHelper.shared.registerToRush(successful: { data in
            
            let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
            successRegistration.delegate = self
            successRegistration.userName = AccountHelper.shared.currentUser?.firstName
            successRegistration.dismissButtonTitle = "Go to Rewards"
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.show(successRegistration, sender: self)
            
        }) { (error) in
            self.goToNextScreen()
        }
    }
    
    func onNotNowClicked() {
        goToNextScreen()
    }
}

extension VerifyMobileViewController: LoyaltyRegistrationSuccessVCDelegate
{
    func didClickDismiss(sender: LoyaltyRegistrationSuccessVC) {
        self.loadHomeStoryboard(selectRewards: true)
    }
}
