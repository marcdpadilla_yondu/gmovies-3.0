//
//  SectionLabelTableViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 31/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class SectionLabelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sectionTitle: UILabel!
    @IBOutlet weak var filterButtonAction: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        filterButtonAction.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        filterButtonAction.layer.borderWidth = 0.5
        
//        if UserDefaults.standard.bool(forKey: "loggedin") == false {
//            filterButtonAction.isHidden = true
//        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
