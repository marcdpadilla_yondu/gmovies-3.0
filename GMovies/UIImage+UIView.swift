//
//  UIImage+UIView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 14/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import Foundation
import CoreGraphics
import QuartzCore

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}
