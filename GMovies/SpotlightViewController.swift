//
//  SpotlightViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FBSDKShareKit
import Social

class SpotlightViewController: UITableViewController, MEVFloatingButtonDelegate {
    
    private var items : [SpotlightModel] = []
    
    private var errorView : ErrorView?
    private var moreBelow : MEVFloatingButton = MEVFloatingButton()
    
    var fromBanner : Bool = false
    var spotlightUrl : String?
    var maxPages : Int = 1
    var currentPage : Int = 1
    var downloadTaskRunning : Bool = false
    
    private var loadingScreen : LoaderView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        self.view.addSubview(loadingScreen!)
        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                  ])
        
        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
        
        if items.count == 0 {
            downloadSpotlight(page: 1)
        }
        
        moreBelow.animationType = .MEVFloatingButtonAnimationFromBottom
        moreBelow.displayMode = .always
        moreBelow.position = .bottomCenter
        moreBelow.backgroundColor = UIColor.white
        moreBelow.imageColor = UIColor(netHex: 0xda2424)
        moreBelow.imagePadding = 2.0
        moreBelow.verticalOffset = 0.0
        moreBelow.layer.cornerRadius = 14.0
        moreBelow.shadowColor = UIColor.black
        moreBelow.shadowOffset = CGSize.zero
        moreBelow.shadowRadius = 8.0
        moreBelow.shadowOpacity = 0.85
        
        let label = UIButton(type: .custom)
        label.titleLabel?.font = UIFont(name: "OpenSans", size: 11.0)
        label.setTitle("More News", for: .normal)
        label.setImage(UIImage(named: "floating-button"), for: .normal)
        label.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5.0)
        label.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5.0, bottom: 0, right: 0.0)
        label.sizeToFit()
        var frame = label.frame
        frame.size.width += 25.0
        frame.size.height += 8.0
        label.frame = frame
        
        moreBelow.image = UIImage(cgImage: UIImage(view: label).cgImage!, scale: UIScreen.main.scale, orientation: UIImageOrientation.up)
        
        self.tableView.setFloatingButton(moreBelow)
        self.tableView.floatingButtonDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "SPOTLIGHT"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "spotlight")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.Spotlight)
        if fromBanner == true {
            performSegue(withIdentifier: "showSpotlight", sender: self)
        }
        
        
        for v : UIView in moreBelow.subviews {
            print("floating", v)
            v.layer.cornerRadius = 14.0
            
            for vs : UIView in v.subviews {
                vs.layer.cornerRadius = 14.0
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func downloadSpotlight(page : Int) {
        
        if currentPage > maxPages {
            currentPage = maxPages
            return
        }
        
        downloadTaskRunning = true
        GMoviesAPIManager.getSpotlight(page: currentPage) { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                
                if status == 1 {
                    //print(result)
                    self.maxPages = result?.value(forKeyPath: "data.links.max_page") as! Int
                    let results : [NSDictionary] = result?.value(forKeyPath: "data.results") as! [NSDictionary]
                    
                    for d in results {
                        let sl : SpotlightModel = SpotlightModel(data: d)
                        self.items.append(sl)
                    }
                    
                    for s in results {
                        let sp : SpotlightModel = SpotlightModel(data: s)
                        if self.items.contains(where: { (t1) -> Bool in
                            return t1.slug == sp.slug
                        }) == false {
                            self.items.append(sp)
                        }
                    }
                    
                    if self.maxPages > 0 {
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            self.errorView?.removeFromSuperview()
                            self.tableView.reloadData()
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            print(error!.localizedDescription)
                            self.loadErrorView()
                        }
                    }
                    
                    
                } else {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        print(error!.localizedDescription)
                        self.loadErrorView()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.loadingScreen?.stopAnimating()
                    self.loadingScreen?.removeFromSuperview()
                    print(error!.localizedDescription)
                    self.loadErrorView()
                }
            }
            
            self.downloadTaskRunning = false
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "spotlightIdentifier", for: indexPath)

        configureCell(cell: cell as! SpotlightViewCell, indexPath: indexPath)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK: Private functions
    
    private func loadErrorView() {
        tableView.separatorStyle = .none
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = UIManager.configureErrorView(parentController: self)
        errorView!.errorTitle.text = "Oops!"
        errorView!.errorDesc.text = "Something went wrong"
        errorView!.errorIcon.image = UIImage(named: "error-popcorn")
        errorView?.retryButton.isHidden = false
    }
    
    private func configureCell(cell : SpotlightViewCell, indexPath : IndexPath) {
        let sl : SpotlightModel = items[indexPath.row];
        
        cell.spotlightTitle.text = "\(sl.title!)\n\(sl.subtitle!)"
        cell.spotlightImage.image = nil
        if sl.listImg == nil {
            downloadImage(so: sl, indexPath: indexPath)
        } else {
            cell.spotlightImage.image = sl.listImg
        }
        
        cell.fbShareButton.tag = indexPath.row
        cell.twitterShareButton.tag = indexPath.row
        cell.emailShareButton.tag = indexPath.row
        
        if cell.fbShareButton.actions(forTarget: self, forControlEvent: .touchUpInside) == nil {
            cell.fbShareButton.addTarget(self, action: #selector(self.shareAction), for: .touchUpInside)
        }
        
        if cell.twitterShareButton.actions(forTarget: self, forControlEvent: .touchUpInside) == nil {
            cell.twitterShareButton.addTarget(self, action: #selector(self.shareAction), for: .touchUpInside)
        }
        
        if cell.emailShareButton.actions(forTarget: self, forControlEvent: .touchUpInside) == nil {
            cell.emailShareButton.addTarget(self, action: #selector(self.shareAction), for: .touchUpInside)
        }
    }
    
    private func downloadImage(so : SpotlightModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: so.listImgUrl!) { (error, img) in
            if error == nil {
                
                DispatchQueue.main.async {
                    so.listImg = img
                    if self.tableView.indexPathsForVisibleRows!.contains(indexPath) {
                        self.tableView.beginUpdates()
                        self.tableView.reloadRows(at: self.tableView.indexPathsForVisibleRows!, with: .fade)
                        self.tableView.endUpdates()
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSpotlight" {
            
            if fromBanner == false {
                let indexPath = tableView.indexPathForSelectedRow
                let sp = items[indexPath!.row]
                tableView.deselectRow(at: indexPath!, animated: true)
                
                let vc : SpotlightViewerController = segue.destination as! SpotlightViewerController
                print("\(Constants.gMoviesWebIP)/iframe/spotlight/\(sp.slug!)")
                vc.title = sp.title!
                vc.url = "\(Constants.gMoviesWebIP)/iframe/spotlight/\(sp.slug!)"
            } else {
                let vc : SpotlightViewerController = segue.destination as! SpotlightViewerController
                vc.url = spotlightUrl
            }
            
            fromBanner = false
        }
    }
    
    @objc private func shareAction(sender : UIButton) {
        AnalyticsHelper.sendButtonEvent(name: "spotlight_share")
        let cell = sender.superview?.superview?.superview as! SpotlightViewCell
        let indexPath = tableView.indexPath(for: cell)
        let t = items[indexPath!.row]
        if cell.fbShareButton == sender {
            let fbShare : FBSDKShareLinkContent = FBSDKShareLinkContent()
            fbShare.contentURL = URL(string: t.share!.value(forKey: "url") as! String)!
            fbShare.contentTitle = t.share!.value(forKey: "title") as! String
            fbShare.contentDescription = t.share!.value(forKey: "message") as! String
            
            let fbShareDialog : FBSDKShareDialog = FBSDKShareDialog()
            fbShareDialog.fromViewController = self
            fbShareDialog.shareContent = fbShare
            fbShareDialog.mode = .automatic
            fbShareDialog.delegate = self
            fbShareDialog.show()
            
        } else if cell.emailShareButton == sender {
            let fbShare : FBSDKShareLinkContent = FBSDKShareLinkContent()
            fbShare.contentURL = URL(string: t.share!.value(forKey: "url") as! String)!
            fbShare.contentTitle = t.share!.value(forKey: "title") as! String
            fbShare.contentDescription = t.share!.value(forKey: "message") as! String
            
            let fbShareDialog : FBSDKMessageDialog = FBSDKMessageDialog()
            fbShareDialog.shareContent = fbShare
            fbShareDialog.show()
        } else if cell.twitterShareButton == sender {
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
                
                let tweetShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                tweetShare.add(URL(string: t.share!.value(forKey: "url") as! String)!)
                tweetShare.setInitialText(t.share!.value(forKey: "message") as! String)
                
                self.present(tweetShare, animated: true, completion: nil)
                
            } else {
                
                let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to tweet.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    if let url = URL(string:UIApplicationOpenSettingsURLString) {
                        UIApplication.shared.openURL(url)
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        let reloadDistance : CGFloat = 0.0
        if y > h + reloadDistance {
            if downloadTaskRunning == false {
                if maxPages > currentPage {
                    currentPage += 1
                    downloadSpotlight(page: currentPage)
                } else {
                    currentPage = maxPages
                }
            }
        }
        
        
        if y + scrollView.bounds.size.height < scrollView.contentSize.height || currentPage < maxPages {
            moreBelow.displayMode = .always
        } else {
            moreBelow.displayMode = .none
        }
    }
    
    //MARK: floating button delegates
    func floatingButton(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        
        if y < scrollView.contentSize.height {
            moreBelow.displayMode = .always
            scrollView.setContentOffset(CGPoint(x: 0.0, y: y), animated: true)
        } else {
            moreBelow.displayMode = .none
            if downloadTaskRunning == false {
                if maxPages > currentPage {
                    currentPage += 1
                    downloadSpotlight(page: currentPage)
                } else {
                    currentPage = maxPages
                }
            }
        }
        
    }

}

extension SpotlightViewController: FBSDKSharingDelegate
{
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        if AccountHelper.shared.isRegisteredInRush {
            UserEngagementHelper.shared.earnBadgeFrom(engagement: UserEngagementCodes.shareOurBlogReelNews)
        }
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
}
