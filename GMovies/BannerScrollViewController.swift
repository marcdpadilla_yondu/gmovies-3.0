//
//  BannerScrollViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 5/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import ReachabilitySwift

class BannerScrollViewController: UIViewController, UIScrollViewDelegate {
    
    var items : [BannerModel]?
    fileprivate var views : [UIView]?
    fileprivate var pageControlUsed : Bool = false
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bannerScroll: UIScrollView!
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var contentViewRightConstraint: NSLayoutConstraint!
    
    private var autoScrollStarted : Bool = false
    private var dragged : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        items = []
        views = []
        pager.numberOfPages = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: ReachabilityChangedNotification,object: appDelegate.reachability)
        
        if items?.count == 0 {
            downloadBanners()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: nil, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private methods
    
    @objc private func reachabilityChanged(note: NSNotification) {

        let reachability = note.object as! Reachability

        if reachability.isReachable {
            items?.removeAll()
            views?.removeAll()
            pager.numberOfPages = 0
            downloadBanners()
        }
    }
    
    fileprivate func configureView() {
        let pages : Int = items!.count
        contentViewRightConstraint.constant = -(UIScreen.main.bounds.size.width*CGFloat(pages-1))
        //print("contentsize constant", contentViewRightConstraint.constant)
        bannerScroll.layoutIfNeeded()
        bannerScroll.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height)
    }
    
    private func downloadBanners() {
        GMoviesAPIManager.getCarousel { (error, result) in
            if error == nil {
                
                let status : Int = result?.object(forKey: "status") as! Int
                
                if status == 1 {
                    let data : [NSDictionary] = result?.object(forKey: "data") as! [NSDictionary]
                    //print(result)
                    for banner : NSDictionary in data as [NSDictionary] {
                        let b : BannerModel = BannerModel(data: banner)
                        self.items?.append(b)
                    }
                    
                    DispatchQueue.main.async {
                        self.pager.numberOfPages = (self.items?.count)!
                        self.generateViews()
                        self.configureView()
                        
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.hideBanner()
                    }
                    
                } else {
                    let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                //print(error!.localizedDescription)
            }
        }
    }
    
    @objc fileprivate func scrollPage() {
        if dragged {
            dragged = false
            self.perform(#selector(self.scrollPage), with: nil, afterDelay: 3.0)
            return
        }
        pageControlUsed = true
        var page : Int = pager.currentPage+1
        var frame : CGRect = UIScreen.main.bounds
        var reachedEnd : Bool = false
        if (page >= views!.count) {
            page = 0
            pager.currentPage = 0
            reachedEnd = true
        } else {
            pager.currentPage = page
        }
        
        frame.origin.x = frame.size.width*(CGFloat(page))
        frame.origin.y = 0
        
        if reachedEnd == false {
            bannerScroll.scrollRectToVisible(frame, animated: true)
            self.perform(#selector(scrollPage), with: nil, afterDelay: 3.0)
        } else {
            UIView.animateKeyframes(withDuration: 1.0, delay: 0.0, options: UIViewKeyframeAnimationOptions(), animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.333, animations: {
                    self.contentView.alpha = 0.0
                })
                UIView.addKeyframe(withRelativeStartTime: 0.334, relativeDuration: 0.333, animations: {
                    self.bannerScroll.scrollRectToVisible(frame, animated: false)
                })
                UIView.addKeyframe(withRelativeStartTime: 0.666, relativeDuration: 0.333, animations: {
                    self.contentView.alpha = 1.0
                })
                }, completion: { (_) in
                    
                    self.perform(#selector(self.scrollPage), with: nil, afterDelay: 3.0)
            })
        }
        
    }
    
    @objc private func tapAction(tap : UITapGestureRecognizer) {
        let index = tap.view?.tag
        let bannerObject = items![index!]
        
        //print("tapped")
        
        if bannerObject.linkType?.caseInsensitiveCompare("movie") == .orderedSame {
            
            GMoviesAPIManager.getMovieDetail(movieId: bannerObject.objectId!, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    
                    if status == 1 {
                        DispatchQueue.main.async {
                            let movieObject : MovieModel = MovieModel(data : result?.value(forKey: "data") as! NSDictionary)
                            let storyboard : UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
                            let movieDetailNav : UINavigationController = storyboard.instantiateViewController(withIdentifier: "movieDetailNav") as! UINavigationController
                            let movieDetail : MovieDetailViewController = movieDetailNav.topViewController as! MovieDetailViewController
                            
                            
                            movieDetail.movieObject = movieObject
                            
                            self.present(movieDetailNav, animated: true, completion: nil)
                        }
                    }
                } else {
                    //print(error!.localizedDescription)
                }
            })
            
        } else if bannerObject.linkType?.caseInsensitiveCompare("spotlight") == .orderedSame {
            let tab : MTabBarViewController = self.parent?.parent?.parent as! MTabBarViewController
            (tab.viewControllers?[2] as! MNavigationViewController).popToRootViewController(animated: false)
            let spotlight = (tab.viewControllers?[2] as! MNavigationViewController).topViewController as! SpotlightViewController
            spotlight.fromBanner = true
            spotlight.spotlightUrl = bannerObject.link
            //print(bannerObject.link)
            tab.setSelectedViewController(index: 2, animated: true)
        } else if bannerObject.linkType?.caseInsensitiveCompare("external") == .orderedSame {
            UIApplication.shared.openURL(URL(string: bannerObject.link!)!)
        } else if bannerObject.linkType?.caseInsensitiveCompare("store_link") == .orderedSame {
            UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/id582901861")!)
        }
    }
    
    fileprivate func generateViews() {
        self.view.layoutIfNeeded()
        var c : Int = 0
        let pageWidth : CGFloat = UIScreen.main.bounds.size.width
        for _ in items! {
            let v : UIImageView = UIImageView(frame: CGRect(x: CGFloat(c)*pageWidth, y: 0.0, width: pageWidth, height: bannerScroll.frame.size.height))
            v.translatesAutoresizingMaskIntoConstraints = false
            var constraints : [NSLayoutConstraint] = []
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapAction))
            v.addGestureRecognizer(tapGesture)
            v.tag = c
            v.isUserInteractionEnabled = true
            
            contentView.addSubview(v)
            views!.append(v)
            
            constraints.append(NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: v, attribute: .top, multiplier: 1.0, constant: 0.0))
            constraints.append(NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: v, attribute: .bottom, multiplier: 1.0, constant: 0.0))
            constraints.append(NSLayoutConstraint(item: v, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: pageWidth))
            constraints.append(NSLayoutConstraint(item: v, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: v.frame.size.height))
            
            if c == 0 {
                constraints.append(NSLayoutConstraint(item: contentView, attribute: .leading, relatedBy: .equal, toItem: v, attribute: .leading, multiplier: 1.0, constant: 0.0))
            } else if c == items!.count-1 {
                constraints.append(NSLayoutConstraint(item: contentView, attribute: .trailing, relatedBy: .equal, toItem: v, attribute: .trailing, multiplier: 1.0, constant: 0.0))
                let pv : UIView = views![c-1]
                constraints.append(NSLayoutConstraint(item: v, attribute: .left, relatedBy: .equal, toItem: pv, attribute: .right, multiplier: 1.0, constant: 0.0))
            } else {
                let pv : UIView = views![c-1]
                constraints.append(NSLayoutConstraint(item: v, attribute: .left, relatedBy: .equal, toItem: pv, attribute: .right, multiplier: 1.0, constant: 0.0))
            }
            
            contentView.addConstraints(constraints)
            
            let b : BannerModel = items![c]
            if b.img == nil {
                
                GMoviesAPIManager.downloadPoster(urlString: b.imgUrl!, completion: { (error, image) in
                    if error == nil {
                        b.img = image
                        //print(b.imgUrl!)
                        DispatchQueue.main.async {
                            v.image = image
                            
                            if self.autoScrollStarted == false {
                                self.perform(#selector(self.scrollPage), with: nil, afterDelay: 3.0)
                                self.autoScrollStarted = true
                            }
                        }
                    } else {
                        //print(error?.localizedDescription)
                    }
                })
            } else {
                DispatchQueue.main.async {
                    v.image = b.img
                    if self.autoScrollStarted == false {
                        self.perform(#selector(self.scrollPage), with: nil, afterDelay: 3.0)
                        self.autoScrollStarted = true
                    }
                }
            }
            
            c+=1
        }
        
        contentView.layoutIfNeeded()
    }
    
    @IBAction func pagerAction(_ sender: AnyObject) {
        scrollPage()
    }
    
    //MARK: Scroll View Delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if pageControlUsed == false {
            let pageWidth : CGFloat = UIScreen.main.bounds.size.width
            let page : CGFloat = floor((bannerScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1
            self.pager.currentPage = Int(page)
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        dragged = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
