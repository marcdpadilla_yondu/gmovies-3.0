//
//  TicketViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 17/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TicketViewCell: UITableViewCell {
    
    private lazy var __once: () = {
        DispatchQueue.main.async(execute: {
            self.addGradient()
        });
    }()
    
    @IBOutlet weak var dateBg: UIView!
    @IBOutlet weak var theaterName: UILabel!
    @IBOutlet weak var indicator: UILabel!
    @IBOutlet weak var cinemaName: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var startsIn: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var moviePoster: UIImageView!
    
    @IBOutlet weak var giftImageView: UIImageView!
    @IBOutlet weak var startsInHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        indicator.layer.cornerRadius = 3.0
        dateBg.layer.cornerRadius = 2.0
        startsInHeight.constant = 0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        _ = __once
        
    }
    
    private func addGradient() {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = moviePoster.bounds
        gradient.colors = [UIColor(red: 0, green: 0, blue: 0, alpha: 0.85).cgColor, UIColor.clear.cgColor, UIColor.clear.cgColor]
        moviePoster.layer.sublayers?.removeAll()
        moviePoster.layer.insertSublayer(gradient, at: 0)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
