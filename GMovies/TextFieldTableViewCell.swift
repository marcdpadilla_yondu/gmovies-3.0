//
//  TextFieldTableViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 25/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class TextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var prompt: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var prefix: UILabel!
    
    
    @IBOutlet weak var labelFieldConstraint : NSLayoutConstraint!
    @IBOutlet weak var prefixLeftConstraint: NSLayoutConstraint!
    
    var placeholder : String?
    var pre : String?
    var maxLength : Int = 0 //infinite
    
    var isExpanded : Bool = false
    var capitalize : Bool = false
    
    var delegate : TextfieldCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func expand(state : Bool) {
        self.contentView.layoutIfNeeded()
        if state == true {
            isExpanded = true
            
            prefix.alpha = 0.0
            
            self.labelFieldConstraint.constant = -self.label.frame.size.height-5.0
            if self.pre == nil {
                self.prefixLeftConstraint.constant = -38.0
            } else {
                self.prefixLeftConstraint.constant = 0.0
                self.prefix.isHidden = false
                self.prefix.alpha = 1.0
            }
            self.contentView.layoutIfNeeded()
            
            if self.placeholder != nil {
                self.inputField.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.lightGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14.0)!])
            }
            
            
        } else {
            isExpanded = false
            
            self.contentView.layoutIfNeeded()
            
            self.labelFieldConstraint.constant = 0
            self.inputField.attributedPlaceholder = NSAttributedString(string: "")
            
            if self.pre == nil {
                self.prefixLeftConstraint.constant = -38.0
            } else {
                self.prefixLeftConstraint.constant = 0.0
                self.prefix.alpha = 0.0
            }
            
            self.contentView.layoutIfNeeded()
            
            if self.pre != nil {
                self.prefix.isHidden = true
            }
            
            
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if delegate != nil {
            return delegate!.shouldBeginEditing(cell: self)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.contentView.layoutIfNeeded()
        
        isExpanded = true
        
        prefix.alpha = 0.0
        
        delegate?.didBeginEditing(cell: self)
        
        UIView.animate(withDuration: 0.150, animations: {
            self.labelFieldConstraint.constant = -self.label.frame.size.height-5.0
            if self.pre == nil {
                self.prefixLeftConstraint.constant = -38.0
            } else {
                self.prefixLeftConstraint.constant = 0.0
                self.prefix.isHidden = false
                self.prefix.alpha = 1.0
            }
            self.contentView.layoutIfNeeded()
            
            }, completion: { (_) in
                
                if self.placeholder != nil {
                    self.inputField.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.lightGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 14.0)!])
                }
        }) 
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        delegate?.didEndEditing(cell: self)
        
        if inputField.text?.characters.count > 0 {
            return
        }
        
        isExpanded = false
        
        self.contentView.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.150, animations: {
            self.labelFieldConstraint.constant = 0
            self.inputField.attributedPlaceholder = NSAttributedString(string: "")
            
            if self.pre == nil {
                self.prefixLeftConstraint.constant = -38.0
            } else {
                self.prefixLeftConstraint.constant = 0.0
                self.prefix.alpha = 0.0
            }
            
            self.contentView.layoutIfNeeded()
            
        }, completion: { (_) in
            
            if self.pre != nil {
                self.prefix.isHidden = true
            }
        }) 
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        prompt.text = ""
        
        if (textField.text?.characters.count)! + 1 > maxLength && string != "" && maxLength > 0 {
            return false
        }
        
        if capitalize == true {
            if string == "" {
                textField.deleteBackward()
            } else {
                textField.insertText(string.uppercased())
            }
            return false
        }
        
        return true
    }
    
}

protocol TextfieldCellDelegate {
    func didBeginEditing(cell : TextFieldTableViewCell)
    func didEndEditing(cell : TextFieldTableViewCell)
    func shouldBeginEditing(cell : TextFieldTableViewCell) -> Bool
}
