//
//  AllCinemasTableViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class AllCinemasTableViewController: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private var cinemas : NSMutableDictionary?
    var tableView : UITableView?
    var preferred : [String] = []
    
    private let reuseIdentifier : String = "sectionCell"
    private let defaultCell : String = "cell"
    private let valueCell : String = "valueCell"
    
    weak var delegate : AllCinemasControllerProtocol?
    var movieObject : MovieModel?
    
    override init() {
        super.init()
        
        cinemas = ["Top" : ["My Preferred Cinemas", "All Cinemas"]]
        UserDefaults.standard.addObserver(self, forKeyPath: "loggedin", options: .new, context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived), name: NSNotification.Name(rawValue: "TheaterModelUpdateRemove"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived), name: NSNotification.Name(rawValue: "TheaterModelUpdateAdd"), object: nil)
        
        //print("allcinemas", "downloading")
        GMoviesAPIManager.getTheaters() { (error, result) in
            (UIApplication.shared.delegate as! AppDelegate).loadProgress += 1
            (UIApplication.shared.delegate as! AppDelegate).setValue((UIApplication.shared.delegate as! AppDelegate).loadProgress, forKey: "loadProgress")
            
            if error == nil {
                let status : Int = result?.object(forKey: "status") as! Int

                if status == 1 {
                    let regions : NSDictionary = result?.value(forKeyPath: "data.theaters") as! NSDictionary
                    var theaters : [TheaterModel] = []
                    for regionKey : String in regions.allKeys as! [String] {
                        let cities : NSDictionary = regions.object(forKey: regionKey) as! NSDictionary
                        for city : String in cities.allKeys as! [String] {
                            let cinemas : [NSDictionary] = cities.object(forKey: city) as! [NSDictionary]
                            for cinema : NSDictionary in cinemas {
                                let c : TheaterModel = TheaterModel(data: cinema)
                                theaters.append(c)
                            }
                        }
                        
                        self.cinemas?.setValue(theaters, forKey: regionKey)
                        theaters.removeAll()
                    }
                    
                    DispatchQueue.main.async {
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.hideBanner()
                        self.downloadPreferred()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didInitialize"), object: nil)
                        
                        if self.tableView != nil {
                            self.tableView?.reloadData()
                        }
                    }
                    
                } else {
                    let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                        
                    }))
                    
                    let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
            } else {
                let alert : UIAlertController = UIAlertController(title: "Error", message: error!.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                    
                }))
                
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
                //print(error!.localizedDescription)
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return (cinemas?.allKeys.count)!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionView : RegionCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! RegionCell
        
        let key : String = cinemas?.allKeys[section] as! String
        
        if section != 0 {
            
            let filteredRegion : NSArray? = ((cinemas?.object(forKey: key) as! NSArray).filter { (theater) -> Bool in
                if self.movieObject == nil {
                    return key == (theater as! TheaterModel).regionKey!
                }
                return key == (theater as! TheaterModel).regionKey! && self.movieObject?.theaters?.contains((theater as! TheaterModel).theaterId!) == true
                } as NSArray)
            
            if (filteredRegion?.count)! > 0 {
                let theaterModel : TheaterModel? = filteredRegion![0] as? TheaterModel
                sectionView.regionLabel.text = theaterModel?.region!
            } else {
                sectionView.regionLabel.text = ""
            }
            
            return sectionView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0
        }
        
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key : String = cinemas?.allKeys[section] as! String
        
        var count : Int = 2
        
        if section != 0 {
            
            let filteredRegion : NSArray = ((cinemas?.object(forKey: key) as! NSArray).filter { (theater) -> Bool in
                if self.movieObject == nil {
                    return key == (theater as! TheaterModel).regionKey!
                }
                return key == (theater as! TheaterModel).regionKey! && self.movieObject?.theaters?.contains((theater as! TheaterModel).theaterId!) == true
            } as NSArray)
            
//            print("filteredRegion \(key)")
            
            count = (filteredRegion.value(forKeyPath: "@distinctUnionOfObjects.city") as! NSArray).count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = nil
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: defaultCell, for: indexPath)
            let c = cinemas?.object(forKey: "Top") as! NSArray
            cell?.textLabel?.text = (c.object(at: indexPath.row) as! String)
            
            if indexPath.row == 0 {
                cell?.imageView?.image = UIImage(named: "preferred-cinema")
            } else {
                cell?.imageView?.image = nil
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: valueCell, for: indexPath)
            configureCityCell(cell: cell as! CityCell, indexPath: indexPath)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if delegate != nil {
            
            let key : String = cinemas?.allKeys[indexPath.section] as! String
            
            if indexPath.section != 0 {
                
                let filteredRegion : NSArray = ((cinemas?.object(forKey: key) as! NSArray).filter { (theater) -> Bool in
                    
                    let cell : CityCell = tableView.cellForRow(at: indexPath) as! CityCell
                    
                    if self.movieObject == nil {
                        return cell.cityLabel.text == (theater as! TheaterModel).city!
                    }
                    
                    return cell.cityLabel.text == (theater as! TheaterModel).city! && self.movieObject?.theaters?.contains((theater as! TheaterModel).theaterId!) == true
                    
                    } as NSArray).sorted(by: { (t1, t2) -> Bool in
                        return (t1 as! TheaterModel).city!.caseInsensitiveCompare((t2 as! TheaterModel).city!) == ComparisonResult.orderedAscending
                    }) as NSArray
                
                    delegate?.didSelectDataSource!(data: filteredRegion as! [TheaterModel])
                
            } else {
                
                let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell
                
                if cell.textLabel?.text?.caseInsensitiveCompare("all cinemas") == .orderedSame {
                    
                    if movieObject == nil {
                        delegate?.didSelectDataSource!(data: getAllCinemas()!)
                    }else {
                        delegate?.didSelectDataSource!(data: getAvailableCinemas()!)
                    }
                    
                    //delegate?.didSelectDataSource!(data: getAllCinemas()!)
                    
                } else {
                    
                    delegate?.didSelectDataSource!(data: getPreferredCinemas()!)
                }
            }
            
        }
    }
    
    func getAllCinemas() -> [TheaterModel]? {
        let allCinemas : NSMutableArray =  NSMutableArray(array: cinemas?.object(forKey: cinemas?.allKeys[1] as! String) as! NSArray)
        allCinemas.addObjects(from: (cinemas?.object(forKey: cinemas?.allKeys[2] as! String) as! NSArray).flatMap({ $0 as? TheaterModel}))
        
        return allCinemas.flatMap({ $0 as? TheaterModel})
    }
    
    func getAvailableCinemas() -> [TheaterModel]? {
        return self.getAllCinemas()?.filter({ (theater) -> Bool in
            
            if self.movieObject!.theaters!.contains(theater.theaterId!) {
                return true
            }else {
                return false
            }
        })
    }

    
    func getPreferredCinemas() -> [TheaterModel]? {
        
        return getAllCinemas()?.filter({ (t) -> Bool in
            
            if self.preferred.contains(t.theaterId!) {
                t.isPreferred = true
                return true
            } else {
                t.isPreferred = false
            }
            
            return false
        })
    }
    
    private func downloadPreferred() {
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            return
        }
        
        
        GMoviesAPIManager.getPreferred { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    self.preferred.removeAll()
                    self.preferred = result?.value(forKey: "data") as! [String]
                    
                    self.getAllCinemas()?.filter({ (t) -> Bool in
                        
                        if self.preferred.contains(t.theaterId!) {
                            t.isPreferred = true
                            return true
                        } else {
                            t.isPreferred = false
                        }
                        
                        return false
                    })
                    
                    
                } else {
                    
                }
            } else {
                //print(error!.localizedDescription)
            }
            
            DispatchQueue.main.async {
                self.tableView?.reloadData()
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loggedin" {
            downloadPreferred()
        }
    }
    
    //MARK : Private functions
    
    @objc private func notificationReceived(notif : Notification) {
        if notif.name == Notification.Name(rawValue: "TheaterModelUpdateRemove") {
            let theaterId : String = notif.object as! String
            
            preferred.remove(at: preferred.index(where: { (s) -> Bool in
                return s == theaterId
            })!)
            
        } else if notif.name == Notification.Name(rawValue: "TheaterModelUpdateAdd") {
            let theaterId : String = notif.object as! String
            //print(theaterId)
            preferred.append(theaterId)
            
        }
    }
    
    private func configureCityCell(cell : CityCell, indexPath : IndexPath) {
        let key : String = cinemas?.allKeys[indexPath.section] as! String
        
        var cityName : String? = nil
        var cinemaCount : Int = 0
        
        if indexPath.section != 0 {
            
            let filteredRegion : NSArray = ((cinemas?.object(forKey: key) as! NSArray).filter { (theater) -> Bool in
                if self.movieObject == nil {
                    return key == (theater as! TheaterModel).regionKey!
                }
                return key == (theater as! TheaterModel).regionKey! && self.movieObject?.theaters?.contains((theater as! TheaterModel).theaterId!) == true
                } as NSArray)
            
            let cities : NSArray = filteredRegion.value(forKeyPath: "@distinctUnionOfObjects.city") as! NSArray
            cityName = cities.sorted(by: { (t1, t2) -> Bool in
                return (t1 as! String).caseInsensitiveCompare((t2 as! String)) == ComparisonResult.orderedAscending
            })[indexPath.row] as? String
            
            cinemaCount = ((cinemas?.object(forKey: key) as! NSArray).filter { (theater) -> Bool in
                if self.movieObject == nil {
                    return cityName == (theater as! TheaterModel).city!
                }
                return cityName == (theater as! TheaterModel).city! && self.movieObject?.theaters?.contains((theater as! TheaterModel).theaterId!) == true
            }).count
        }
        
        cell.cityLabel.text = cityName
        
        cell.cinemaCount.text = "\(cinemaCount) Cinema\(cinemaCount == 1 ? "" : "s")"
    }
    
    private func configureSectionCell(cell : RegionCell, indexPath : IndexPath) {
        
    }
}

@objc protocol AllCinemasControllerProtocol {
    @objc optional func didSelectDataSource(data : [TheaterModel])
}
