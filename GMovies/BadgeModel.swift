//
//  BadgeModel.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 12/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation

class BadgeModel: NSObject
{
    var id: Int
    var name: String
    var desc: String
    var points: Int
    var badgeGroupIconUrl: String
    var badgeIconUrl: String
    var isFlipped:Bool = false
    
    init(data : NSDictionary) {
        
        id = data.value(forKey: "id") as! Int
        name = data.value(forKey: "name") as! String
        desc = data.value(forKey: "description") as! String
        
        points = (data.value(forKey: "points") as! NSNumber).intValue
        badgeIconUrl = data.value(forKey: "badge_icon") as! String
        badgeGroupIconUrl = data.value(forKey: "group_badge_icon") as! String
        
        if let grayBadgeGroupIconUrl = data.value(forKey: "badge_gray_icon") as? String {
            if !grayBadgeGroupIconUrl.isEmpty {
                badgeGroupIconUrl = grayBadgeGroupIconUrl
            }else {
                badgeGroupIconUrl = data.value(forKey: "group_badge_icon") as! String
            }
        }else {
            badgeGroupIconUrl = data.value(forKey: "group_badge_icon") as! String
        }
    }
}
