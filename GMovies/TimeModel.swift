//
//  TimeModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 11/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TimeModel: NSObject {
    /*
     parent = "20161011-3";
     {
     id = "19:25";
     label = "07:25 PM";
     price = 270;
     "schedule_id" = "3::20161011,j_UZKDwbQXyuPZR8DGeikg,None";
     "seating_type" = "Reserved Seating";
     variant = "<null>";
     }
    */
    
    var dateCinemaId : String?
    var timeId : String?
    var name : String?
    var price : Int = 0
    var scheduleId : String?
    var seatingType : String?
    var variant : String?
    var popcornPrice : Int = 0
    var popcornLabel : String = "Popcorn Bucket"
    var popcornMessage : String?
    
    var priceString : String {
        get {
            return "Php \(price).00"
        }
    }
    
    var variantString : String {
        get {
            if variant == nil {
                return ""
            }
            return variant!
        }
    }
    
    init (data : NSDictionary) {
        super.init()
        
//        dateCinemaId = data.object(forKey: "parent") as? String
        timeId = data.object(forKey: "id") as? String
        name = data.object(forKey: "label") as? String
        
        if let amt = data.value(forKey: "price") as? Int {
            price = amt
        } else if let amt = data.object(forKey: "price") as? String {
            price = Int(amt) ?? Int(Float(amt) ?? 0)
            //price = Int(data.object(forKey: "price") as! String)!
        } else if let amt = data.object(forKey: "price") as? Float {
            price = Int(amt)
        }
        
        scheduleId = data.object(forKey: "schedule_id") as? String
        seatingType = data.object(forKey: "seating_type") as? String
        variant = data.object(forKey: "variant") as? String
        
        if let popcorn = data.object(forKey: "popcorn_price") as? String {
            popcornPrice = Int(popcorn) ?? 0
        } else {
            popcornPrice = data.value(forKey: "popcorn_price") as? Int ?? 0
        }
        
        if let pLabel = data.value(forKey: "popcorn_label") as? String {
            popcornLabel = pLabel
        }
        
        popcornMessage = data.value(forKey: "popcorn_msg") as? String
    }
}
