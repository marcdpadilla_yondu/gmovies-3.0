//
//  ProgressNavigationViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 12/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class ProgressNavigationViewController: UIViewController {
    
    private lazy var __once: () = {
            DispatchQueue.main.async(execute: {
                self.layoutButtons()
            });
        }()
    
    var currentViewControllerIndex : Int = 0
    var viewControllers : [UIViewController]?
    var stepButtons : [UIButton]?
    var lines : [UIView]?
    var lock : Bool = false
    var currentViewController : UIViewController?
    var destinationViewController : UIViewController?
    var token : Int = 0
    
    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var progressContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    
    var delegate : ProgressNavigationProtocol?
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        stepButtons = []
        lines = []
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        _ = self.__once
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Private functions
    
    fileprivate func layoutButtons() {
            
        var bContainerFrame : CGRect = progressContainer.bounds
        bContainerFrame.size.width -= 40.0
        let buttonWidth : CGFloat = 30.0
        let spacing : CGFloat = fabs((CGFloat((viewControllers?.count)!) * buttonWidth)-bContainerFrame.size.width) / (CGFloat((viewControllers?.count)!)-1)
        
        var x : CGFloat = 40.0
        var i : Int = 0
        
        for vc : UIViewController in viewControllers! {
            let b = UIButton(type: .custom)
            
            b.translatesAutoresizingMaskIntoConstraints = false
            
            b.setTitle(vc.tabBarItem.title, for: UIControlState())
            b.frame = CGRect(x: x, y: 0, width: buttonWidth, height: buttonWidth)
            b.tag = i
//            b.addTarget(self, action: #selector(MTabBarViewController.buttonAction(sender:)), for: .touchUpInside)
            b.setImage(vc.tabImageOn, for: .disabled)
            b.setImage(vc.tabImageOn, for: .focused)
            b.setImage(vc.tabImageOff, for: UIControlState())
            b.setImage(vc.tabImageOn, for: .highlighted)
            
            stepButtons?.append(b)
            progressContainer.addSubview(b)

//            x += 40.0
            
            progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: buttonWidth))
            
            if (i == 0) {
                b.isEnabled = false
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .leading, relatedBy: .equal, toItem: progressContainer, attribute: .leading, multiplier: 1.0, constant: x/2.0))
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .top, relatedBy: .equal, toItem: progressContainer, attribute: .top, multiplier: 1.0, constant: 0.0))
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .bottom, relatedBy: .equal, toItem: progressContainer, attribute: .bottom, multiplier: 1.0, constant: 0.0))
                if (viewControllers?.count == 1) {
                    progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .trailing, relatedBy: .equal, toItem: progressContainer, attribute: .trailing, multiplier: 1.0, constant: -(x/2.0)))
                }
            } else if (i == (viewControllers?.count)!-1) {
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .trailing, relatedBy: .equal, toItem: progressContainer, attribute: .trailing, multiplier: 1.0, constant: -(x/2.0)))
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .top, relatedBy: .equal, toItem: progressContainer, attribute: .top, multiplier: 1.0, constant: 0.0))
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .bottom, relatedBy: .equal, toItem: progressContainer, attribute: .bottom, multiplier: 1.0, constant: 0.0))
                if (viewControllers?.count == 2) {
                    let pb : UIButton = stepButtons![i-1] as UIButton
                    
                    progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .left, relatedBy: .equal, toItem: pb, attribute: .right, multiplier: 1.0, constant: spacing))
                }
            } else {
                let pb : UIButton = stepButtons![i-1] as UIButton
                
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .left, relatedBy: .equal, toItem: pb, attribute: .right, multiplier: 1.0, constant: spacing))
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .top, relatedBy: .equal, toItem: progressContainer, attribute: .top, multiplier: 1.0, constant: 0.0))
                progressContainer.addConstraint(NSLayoutConstraint(item: b, attribute: .bottom, relatedBy: .equal, toItem: progressContainer, attribute: .bottom, multiplier: 1.0, constant: 0.0))
            }
            
            i += 1
        }
        
        setCurrentViewController(index: 0, animated: false)
        
        for i in 0..<stepButtons!.count-1 {
            let l = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.5))
            l.backgroundColor = UIColor(netHex: 0x566184)
            
            l.translatesAutoresizingMaskIntoConstraints = false
            
            let b1 = stepButtons![i]
            let b2 = stepButtons![i+1]
            
            progressContainer.addSubview(l)
            lines?.append(l)
            
            progressContainer.addConstraint(NSLayoutConstraint(item: b1, attribute: .trailing, relatedBy: .equal, toItem: l, attribute: .leading, multiplier: 1.0, constant: 0.0))
            progressContainer.addConstraint(NSLayoutConstraint(item: b2, attribute: .leading, relatedBy: .equal, toItem: l, attribute: .trailing, multiplier: 1.0, constant: 0.0))
            progressContainer.addConstraint(NSLayoutConstraint(item: b1, attribute: .centerY, relatedBy: .equal, toItem: l, attribute: .centerY, multiplier: 1.0, constant: 0.0))
            l.addConstraint(NSLayoutConstraint(item: l, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 0.5))
        }
        
    }
    

    // MARK: - Navigation
    
    func setCurrentViewController(index i : Int, animated: Bool) {
        if (i < 0 || viewControllers?.count == 0 ||  stepButtons?.count == 0) {
            return
        }
        
        let pb : UIButton = stepButtons![currentViewControllerIndex]
        let b : UIButton = stepButtons![i]
        let pv : UIViewController = viewControllers![currentViewControllerIndex]
        let vc : UIViewController = viewControllers![i]
        
        
        delegate?.willSelectViewController!(navController: self, destinationController: vc)
        
        if (animated == false) {
            lock = true
            currentViewControllerIndex = i
            
            if (currentViewController != nil) {
                currentViewController?.willMove(toParentViewController: nil)
                currentViewController?.view.removeFromSuperview()
                currentViewController?.removeFromParentViewController()
            }
            
            destinationViewController = viewControllers![currentViewControllerIndex]
            
            destinationViewController!.view.frame = containerView.bounds
            self.addChildViewController(destinationViewController!)
            containerView.addSubview(destinationViewController!.view)
            destinationViewController!.didMove(toParentViewController: self)
            
            currentViewController = destinationViewController!
            destinationViewController = nil
            
            //            pb.setImage(pv.tabImageOff, forState: .Normal)
            //            b.setImage(vc.tabImageOn, forState: .Normal)
            if pv.tabBgColorOff != nil {
                pb.backgroundColor = pv.tabBgColorOff
            }
            
            if vc.tabBgColorOn != nil {
                b.backgroundColor = vc.tabBgColorOn
            }
            
            navigationBar.layoutIfNeeded()
            lock = false
        } else {
            
            if (currentViewControllerIndex == i) {
                return
            }
            
            let p = currentViewControllerIndex
            currentViewControllerIndex = i
            
            destinationViewController = viewControllers![currentViewControllerIndex]
            var startingX : CGFloat = containerView.frame.width
            var endingX : CGFloat = -containerView.frame.width
            var containerFrame = containerView.bounds
            if (currentViewControllerIndex < p) {
                startingX = -containerView.frame.width
                endingX = containerView.frame.width
            }
            
            containerFrame.origin.x = startingX
            destinationViewController?.view.frame = containerFrame
            containerView.addSubview((destinationViewController?.view)!)
            var pFrame = containerFrame
            pFrame.origin.x = endingX
            var nFrame = containerFrame
            nFrame.origin.x = 0
            
            //            self.destinationViewController?.view.alpha = 0.0
            lock = true
            
            UIView.animate(withDuration: 0.250, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                
                self.currentViewController?.view.frame = pFrame
                self.destinationViewController?.view.frame = nFrame
                //                self.currentViewController?.view.alpha = 0.0
                //                self.destinationViewController?.view.alpha = 1.0
                if pv.tabBgColorOff != nil {
                    pb.backgroundColor = pv.tabBgColorOff
                }
                
                if vc.tabBgColorOn != nil {
                    b.backgroundColor = vc.tabBgColorOn
                }
                pb.setImage(pv.tabImageOff, for: UIControlState())
                b.setImage(vc.tabImageOn, for: UIControlState())
                self.navigationBar.layoutIfNeeded()
                
                }, completion: { (_) in
                    if (self.currentViewController != nil) {
                        self.currentViewController?.willMove(toParentViewController: nil)
                        self.currentViewController?.view.removeFromSuperview()
                        self.currentViewController?.removeFromParentViewController()
                    }
                    
                    self.addChildViewController(self.destinationViewController!)
                    self.destinationViewController!.didMove(toParentViewController: self)
                    
                    self.currentViewController = self.destinationViewController!
                    self.destinationViewController = nil
                    
                    self.lock = false
            })
        }
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        if currentViewControllerIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        } else if currentViewControllerIndex == 2 {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            setCurrentViewController(index: currentViewControllerIndex - 1, animated: true)
        }
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}

@objc protocol ProgressNavigationProtocol {
    @objc optional func willSelectViewController(navController : ProgressNavigationViewController, destinationController : UIViewController)
}
