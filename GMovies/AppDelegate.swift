//
//  AppDelegate.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 25/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import ReachabilitySwift
import FBSDKCoreKit
import FBSDKLoginKit
import Fabric
import Crashlytics
import Flurry_iOS_SDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var bannerView : BannerView?
    private var bannerConstraintTop : NSLayoutConstraint?
    
    var allCinemasController : AllCinemasTableViewController?
    let reachability = Reachability()
    
    var loadProgress : Float = 0.0


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        reachability?.whenReachable = { _ in
            DispatchQueue.main.async {
                self.showBanner(message: "Connected!", isPersistent: false, color : UIColor(netHex: 0x189aec))
                self.window?.isUserInteractionEnabled = true
            }
        }
        
        reachability?.whenUnreachable = { _ in
            DispatchQueue.main.async {
                self.showBanner(message: "No Internet Connection", isPersistent: true)
                self.window?.isUserInteractionEnabled = false
            }
        }
        
        application.applicationIconBadgeNumber = 0
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        
        GMSServices.provideAPIKey(Constants.GoogleMapsKey)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
//        UserDefaults.standard.set(false, forKey: "loggedin")
//        UserDefaults.standard.set(nil, forKey: "user")
//        
//        FBSDKLoginManager().logOut()
        
        let notification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
        
        if notification != nil {
            UserDefaults.standard.set(notification!, forKey: "remoteNotification")
            UserDefaults.standard.synchronize()
        }
        
        UserDefaults.standard.set(false, forKey: "maintenance")
        UserDefaults.standard.synchronize()
        
        checkMaintenance()
        
        switch Constants.BUILD_TYPE {
        case .Development:
            RushAPI.shared.buildType = RushAPI.BuildType.Development
            break
        case .QualityAssurance:
            RushAPI.shared.buildType = RushAPI.BuildType.QualityAssurance
            break
        case .Production:
            RushAPI.shared.buildType = RushAPI.BuildType.Production
            break
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        //print(deviceTokenString)
        
        UserDefaults.standard.setValue(deviceTokenString, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Push notif failed")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        if handled == false {
            if url.host != nil {
                if url.host! == "movie" {
                    UserDefaults.standard.set(url.lastPathComponent, forKey: "launchedWithMovieId")
                    UserDefaults.standard.synchronize()
                } else if url.host! == "spotlight" {
                    UserDefaults.standard.set(url.lastPathComponent, forKey: "launchedWithSpotlight")
                    UserDefaults.standard.synchronize()
                    //print("spotlight", url.lastPathComponent)
                }
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if application.applicationState == .background || application.applicationState == .inactive {
            let notification = userInfo[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
            
            if notification != nil {
                UserDefaults.standard.set(notification!, forKey: "remoteNotification")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        var currentViewController = window?.rootViewController
//        
//        if currentViewController == nil {
//            print("current == nil")
//            return UIInterfaceOrientationMask(rawValue: UIInterfaceOrientationMask.portrait.rawValue)
//        }
//        
//        while (currentViewController!.presentedViewController != nil) {
//            currentViewController = currentViewController!.presentedViewController
//        }
//        
//        
//        let className = String(describing: currentViewController?.self())
//        
//        if className == "MPInlineVideoFullscreenViewController" || className == "MPMoviePlayerViewController" || className == "AVFullScreenViewController" {
//            print("current == video")
//            return UIInterfaceOrientationMask(rawValue: UIInterfaceOrientationMask.allButUpsideDown.rawValue)
//        }
//        
//        print("current == \(className)")
//        
//        return UIInterfaceOrientationMask(rawValue: UIInterfaceOrientationMask.portrait.rawValue)
//    }
    
    func requestAPIToken() {
        GMoviesAPIManager.authenticateAPI { (error : Error?, result : NSDictionary?) in
            //print("test", result)
            if error == nil {
                let status : Int = result!.object(forKey: "status") as! Int
                if status == 1 {
                    let data : NSDictionary = result!.object(forKey: "data") as! NSDictionary
                    UserDefaults.standard.set(data.object(forKey: "token")!, forKey: "GMoviesAPIToken")
                    UserDefaults.standard.synchronize()
                    
                    if self.allCinemasController == nil {
                        self.allCinemasController = AllCinemasTableViewController()
                    }
                } else {
                    let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                        
                    }))
                    
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
            } else {
                //print(error!.localizedDescription)
            }
        }
    }
    
    func showLoginModal(previousViewController : UIViewController? = nil, nextViewController : UIViewController? = nil, showFrom : UIViewController? = nil, completion: ((WelcomeViewController) -> ())? = nil) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNav : MNavigationViewController = storyboard.instantiateInitialViewController() as! MNavigationViewController
        let loginView : WelcomeViewController = loginNav.topViewController as! WelcomeViewController
        loginView.modalMode = true
        loginView.previousViewController = previousViewController
        loginView.nextViewController = nextViewController
        
        if previousViewController == nil {
            if showFrom == nil {
                window?.rootViewController?.present(loginNav, animated: true, completion: {
                    completion?(loginView)
                })
            } else {
                showFrom?.present(loginNav, animated: true, completion: {
                    completion?(loginView)
                })
            }
        } else {
            previousViewController?.navigationController?.pushViewController(loginView, animated: true)
        }
    }
    
    func addMovieToWatchList(mo : MovieModel, completion:@escaping (_ success: Bool) -> Void? = { _ in return }) {
        if UserDefaults.standard.bool(forKey: "loggedin") == true {
            GMoviesAPIManager.addToWatchList(movieId: mo.movieId!, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        mo.watched = true
                        completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                }
            })
        } else {
            completion(false)
            showLoginModal()
        }
    }
    
    func removeMovieFromWatchList(mo : MovieModel, completion:@escaping (_ success: Bool) -> Void? = { _ in return }) {
        if UserDefaults.standard.bool(forKey: "loggedin") == true {
            GMoviesAPIManager.removeFromWatchList(movieId: mo.movieId!, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        //print("Removed")
                        mo.watched = false
                        completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                    //print(error!.localizedDescription)
                }
            })
        } else {
            completion(false)
            showLoginModal()
        }
    }
    
    func addTheaterToPreferred(to : TheaterModel, completion:@escaping (_ success: Bool) -> Void? = { _ in return }) {
        if UserDefaults.standard.bool(forKey: "loggedin") == true {
            GMoviesAPIManager.addToPreferred(theaterId: to.theaterId!, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        //print("Added")
                        to.isPreferred = true
                        completion(true)
                    } else {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "", message: result?.value(forKey: "message") as! String, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                
                            })
                            alert.addAction(okAction)
                            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                        completion(false)
                    }
                } else {
                    completion(false)
                    //print(error!.localizedDescription)
                }
            })
        } else {
            completion(false)
            showLoginModal()
        }
    }
    
    func removeTheaterFromPreferred(to : TheaterModel, completion:@escaping (_ success: Bool) -> Void? = { _ in return }) {
        if UserDefaults.standard.bool(forKey: "loggedin") == true {
            GMoviesAPIManager.removeFromPreferred(theaterId: to.theaterId!, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        //print("Removed")
                        to.isPreferred = false
                        completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                    //print(error!.localizedDescription)
                }
            })
        } else {
            completion(false)
            showLoginModal()
        }
    }
    
    func getConfig() {
        GMoviesAPIManager.getServices { (error, result) in
            if error == nil {
                
                let results : [NSDictionary] = result?.value(forKey: "results") as! [NSDictionary]
                //print(results)
                
                for d in results {
                    if d.value(forKey: "code") as! String == "CRASHLYTICS" {
                        if d.value(forKey: "status") as! String == "active" {
//                            UserDefaults.standard.set(true, forKey: "crashlytics")
                            Fabric.with([Crashlytics.self])
                            // Configure tracker from GoogleService-Info.plist.
                            var configureError:NSError?
                            GGLContext.sharedInstance().configureWithError(&configureError)
                            assert(configureError == nil, "Error configuring Google services: \(configureError)")
                            
                            // Optional: configure GAI options.
                            let gai = GAI.sharedInstance()
                            gai?.trackUncaughtExceptions = true  // report uncaught exceptions
                            //gai?.logger.logLevel = GAILogLevel.verbose  // remove before app release
                            
                            Flurry.startSession("G9J7Q6GQJDHSQ5HMJZ9X")
                            //Flurry.logEvent("Started Application");
                            
                            /*
                             let tracker = GAI.sharedInstance().defaultTracker
                             tracker.set(kGAIScreenName, value: name)
                             
                             let builder = GAIDictionaryBuilder.createScreenView()
                             tracker.send(builder.build() as [NSObject : AnyObject])
                            */
                        } else {
//                            UserDefaults.standard.set(false, forKey: "crashlytics")
                        }
                        
//                        UserDefaults.standard.synchronize()
                    }
                }
            } else {
                //print(error!.localizedDescription)
            }
            
            DispatchQueue.main.async {
                self.loadProgress += 1
                self.setValue(self.loadProgress, forKey: "loadProgress")
                self.checkAppStoreVersion()
            }
            
        }
    }
    
    func checkMaintenance() {
        UserDefaults.standard.set(false, forKey: "maintenance")
        UserDefaults.standard.synchronize()
        GMoviesAPIManager.getMaintenanceMode { (error, result) in
            if error == nil {
                //print(result)
                let enabled : Int = Int(result?.value(forKey: "enabled") as! String) ?? 0
                if enabled == 1 {
                    UserDefaults.standard.set(true, forKey: "maintenance")
                } else {
                    UserDefaults.standard.set(false, forKey: "maintenance")
                }
                
                UserDefaults.standard.synchronize()
            } else {
                //print(error!.localizedDescription)
                UserDefaults.standard.set(false, forKey: "maintenance")
                UserDefaults.standard.synchronize()
            }
            
            DispatchQueue.main.async {
                self.loadProgress += 1
                self.setValue(self.loadProgress, forKey: "loadProgress")
                self.getConfig()
            }
        }
    }
    
    func checkAppStoreVersion() {
        GMoviesAPIManager.checkAppStoreVersion { (error, result) in
            if error == nil {
                let results : [NSDictionary] = result?.value(forKey: "results") as! [NSDictionary]
                
                if results.count > 0 {
                    let version : String = results.first?.value(forKey: "version") as! String
                    let releaseNotes : String = results.first?.value(forKey: "releaseNotes") as! String
                    let localVersion : String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
                    
                    if version.compare(localVersion) == ComparisonResult.orderedDescending {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "GMovies \(version)", message: releaseNotes, preferredStyle: .alert)
                            let updateAction = UIAlertAction(title: "Update", style: .default, handler: { (_) in
                                UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/id582901861")!)
                            })
                            alert.addAction(updateAction)
                            
                            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                
            } else {
                //print(error!.localizedDescription)
            }
            
            DispatchQueue.main.async {
                self.loadProgress += 1
                self.setValue(self.loadProgress, forKey: "loadProgress")
                if Constants.GMoviesAPIToken == nil {
                    self.requestAPIToken()
                } else {
                    if self.allCinemasController == nil {
                        self.allCinemasController = AllCinemasTableViewController()
                    }
                }
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        reachability?.stopNotifier()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if UserDefaults.standard.string(forKey: "appVersion") == nil {
            UserDefaults.standard.set(true, forKey: "showTutorial")
            let localVersion : String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            UserDefaults.standard.set(localVersion, forKey: "appVersion")
            UserDefaults.standard.synchronize()
        } else {
            let localVersion : String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            let savedVersion = UserDefaults.standard.string(forKey: "appVersion")
            
            if localVersion.compare(savedVersion!) == ComparisonResult.orderedDescending {
                UserDefaults.standard.set(localVersion, forKey: "appVersion")
                UserDefaults.standard.set(true, forKey: "showTutorial")
                UserDefaults.standard.synchronize()
            }
        }
        
        do {
            try self.reachability?.startNotifier()
        } catch {
            //print("Unable to start notifier")
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    //MARK: - Custom methods
    
    func configureBannerView() {
        if bannerView == nil {
            bannerView = Bundle.main.loadNibNamed("BannerView", owner: self, options: nil)?.first as? BannerView
            bannerView!.translatesAutoresizingMaskIntoConstraints = false
            
            var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
            constraints.append(NSLayoutConstraint(item: bannerView!, attribute: .leading, relatedBy: .equal, toItem: window!, attribute: .leading, multiplier: 1.0, constant: 0))
            bannerConstraintTop = NSLayoutConstraint(item: bannerView!, attribute: .top, relatedBy: .equal, toItem: window!, attribute: .top, multiplier: 1.0, constant: 0)
            constraints.append(bannerConstraintTop!)
            constraints.append(NSLayoutConstraint(item: bannerView!, attribute: .trailing, relatedBy: .equal, toItem: window!, attribute: .trailing, multiplier: 1.0, constant: 0))
            
            window?.addSubview(bannerView!)
            window?.addConstraints(constraints)
            
            var frame : CGRect = bannerView!.frame
            frame.origin.y = -frame.size.height
            bannerView!.frame = frame
        }
        
        window?.layoutIfNeeded()
    }
    
    func showBanner(message : String?, isPersistent : Bool, color : UIColor = UIColor(netHex: 0x56585b), textColor : UIColor = UIColor(netHex: 0xffffff)) {
        configureBannerView()
        window?.rootViewController!.view.bringSubview(toFront: bannerView!)
        bannerView?.messageLabel.text = message
        bannerView?.show(constraint: bannerConstraintTop!)
        UIView.animate(withDuration: 0.250) { 
            self.bannerView?.backgroundColor = color
            self.bannerView?.messageLabel.textColor = textColor
        }
        
        if isPersistent == false {
            self.perform(#selector(AppDelegate.hideBanner), with: nil, afterDelay: 5.0)
        }
    }
    
    func hideBanner(animated:Bool = true) {
        //configureBannerView()
        bannerView?.hide(constraint: bannerConstraintTop!, animated: animated)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppDelegate.hideBanner), object: nil)
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "ph.com.globetel.GMovies" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "GMovies", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

