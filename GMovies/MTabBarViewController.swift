//
//  MTabBarViewController.swift
//  Crisis Management
//
//  Created by Marc Darren Padilla on 15/7/16.
//  Copyright © 2016 Marc. All rights reserved.
//

import UIKit

class MTabBarViewController: UIViewController {
    
    private lazy var __once: () = {
            DispatchQueue.main.async(execute: { 
                self.layoutButtons()
            });
        }()
    
    @IBOutlet weak var tabBar: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var tabBarBottom: NSLayoutConstraint!
    
    
    fileprivate var lock : Bool = false
    var tabButtons : [UIButton]?
    fileprivate var indicator : UIView?
    fileprivate var indicatorLeftConstraint : NSLayoutConstraint?
    var currentViewController : UIViewController?
    var destinationViewController : UIViewController?
    var viewControllers : [UIViewController]?
    var selectedIndex : Int = 0
    var token : Int = 0
    var isIndicatorEnabled : Bool = false
    var indicatorHugsContent : Bool = false
    
    var delegate : MTabBarProtocol?
    var initialViewControllerIndex : Int = 0
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        viewControllers = []
        tabButtons = []
        
//        tabBar.backgroundColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        hideTabBar(animated: animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        _ = self.__once
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showTabBar(animated: Bool) {
        self.view.layoutIfNeeded()
        tabBarBottom.constant = 0.0
        if animated {
            UIView.animate(withDuration: 0.150, delay: 0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
                }, completion: { (_) in
                    
            })
        } else {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideTabBar(animated: Bool) {
        self.view.layoutIfNeeded()
        tabBarBottom.constant = -tabBar.frame.size.height
        if animated {
            UIView.animate(withDuration: 0.150, delay: 0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
                }, completion: { (_) in
                    
            })
        } else {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Private functions
    
    fileprivate func setupButton(_ button: UIButton) {
        let spacing: CGFloat = 3.0
        let imageSize: CGSize = button.imageView!.image == nil ? CGSize.zero : button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        let labelString = NSString(string: button.titleLabel!.text == nil ? "" : button.titleLabel!.text!)
        if button.imageView!.image == nil {
            button.titleLabel?.font = UIFont(name: "OpenSans", size: 14.0)!
        }
        let titleSize = labelString.size(attributes: [NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        button.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
    }
    
    fileprivate func layoutButtons() {
        
        let bContainerFrame : CGRect = tabBar.bounds
        let buttonWidth : CGFloat = (viewControllers?.count)! > 0 ? bContainerFrame.size.width / CGFloat((viewControllers?.count)!) : bContainerFrame.size.width
        
        var x : CGFloat = 0.0
        var i : Int = 0
        var imgWidth : CGFloat = 0.0
        
        for vc : UIViewController in viewControllers! {
            let b = UIButton(type: .custom)
            
            b.translatesAutoresizingMaskIntoConstraints = false
            
            b.setTitle(vc.tabBarItem.title, for: UIControlState())
            b.frame = CGRect(x: x, y: 0, width: buttonWidth, height: tabBar.frame.size.height)
            b.tag = i
            b.addTarget(self, action: #selector(MTabBarViewController.buttonAction(sender:)), for: .touchUpInside)
            b.titleLabel?.font = UIFont(name: "OpenSans", size: 10.0)!
            b.titleLabel?.adjustsFontSizeToFitWidth = true
            b.setTitleColor(UIColor.lightGray, for: UIControlState())
            b.setTitleColor(UIColor.black, for: .focused)
            b.setTitleColor(UIColor.black, for: .highlighted)
            b.setTitleColor(UIColor.black, for: .disabled)
            b.setImage(vc.tabImageOn, for: .disabled)
            b.setImage(vc.tabImageOn, for: .focused)
            b.setImage(vc.tabImageOff, for: UIControlState())
            b.setImage(vc.tabImageOn, for: .highlighted)
            
            if vc.tabImageOn != nil {
                imgWidth = (vc.tabImageOn?.size.width)!
            }
            
            if vc.tabTextColorOff != nil {
                b.setTitleColor(vc.tabTextColorOff, for: UIControlState())
            }
            
            if vc.tabTextColorOn != nil {
                b.setTitleColor(vc.tabTextColorOn, for: .focused)
                b.setTitleColor(vc.tabTextColorOn, for: .highlighted)
                b.setTitleColor(vc.tabTextColorOn, for: .disabled)
            }
            
            if vc.tabBgColorOff != nil {
                b.backgroundColor = vc.tabBgColorOff
            }
            
            setupButton(b)
            
            tabButtons?.append(b)
            tabBar.addSubview(b)
            x += buttonWidth
            
            tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: buttonWidth))
            
            if (i == 0) {
                b.isEnabled = false
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .leading, relatedBy: .equal, toItem: tabBar, attribute: .leading, multiplier: 1.0, constant: 0.0))
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .top, relatedBy: .equal, toItem: tabBar, attribute: .top, multiplier: 1.0, constant: 0.0))
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .bottom, relatedBy: .equal, toItem: tabBar, attribute: .bottom, multiplier: 1.0, constant: 0.0))
                if (viewControllers?.count == 1) {
                    tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .trailing, relatedBy: .equal, toItem: tabBar, attribute: .trailing, multiplier: 1.0, constant: 0.0))
                }
            } else if (i == (viewControllers?.count)!-1) {
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .trailing, relatedBy: .equal, toItem: tabBar, attribute: .trailing, multiplier: 1.0, constant: 0.0))
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .top, relatedBy: .equal, toItem: tabBar, attribute: .top, multiplier: 1.0, constant: 0.0))
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .bottom, relatedBy: .equal, toItem: tabBar, attribute: .bottom, multiplier: 1.0, constant: 0.0))
                if (viewControllers?.count == 2) {
                    let pb : UIButton = tabButtons![i-1] as UIButton
                    
                    tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .left, relatedBy: .equal, toItem: pb, attribute: .right, multiplier: 1.0, constant: 0.0))
                }
            } else {
                let pb : UIButton = tabButtons![i-1] as UIButton
                
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .left, relatedBy: .equal, toItem: pb, attribute: .right, multiplier: 1.0, constant: 0.0))
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .top, relatedBy: .equal, toItem: tabBar, attribute: .top, multiplier: 1.0, constant: 0.0))
                tabBar.addConstraint(NSLayoutConstraint(item: b, attribute: .bottom, relatedBy: .equal, toItem: tabBar, attribute: .bottom, multiplier: 1.0, constant: 0.0))
            }
            
            i += 1
        }
        
        if indicator == nil && isIndicatorEnabled == true {
            
            var initialX : CGFloat = 0.0
            if imgWidth != 0 {
                imgWidth += 10
                initialX = (imgWidth/2.0)
                
            } else {
                imgWidth = buttonWidth
            }
            
            indicator = UIView(frame: CGRect(x: 0.0, y: tabBar.frame.size.height-3.0, width: imgWidth, height: 3.0))
            indicator?.translatesAutoresizingMaskIntoConstraints = false
            indicator!.backgroundColor = UIColor(netHex: 0xd81515)
            tabBar!.addSubview(indicator!)
            indicatorLeftConstraint = NSLayoutConstraint(item: tabBar, attribute: .leading, relatedBy: .equal, toItem: indicator, attribute: .leading, multiplier: 1.0, constant: initialX)
            tabBar.addConstraint(indicatorLeftConstraint!)
            tabBar.addConstraint(NSLayoutConstraint(item: tabBar, attribute: .bottom, relatedBy: .equal, toItem: indicator, attribute: .bottom, multiplier: 1.0, constant: 0.0))
            tabBar.addConstraint(NSLayoutConstraint(item: indicator!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 3.0))
            tabBar.addConstraint(NSLayoutConstraint(item: indicator!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: imgWidth))
            tabBar.layoutIfNeeded()
            
        }
        
        setSelectedViewController(index: initialViewControllerIndex, animated: false)
    }
    
    func setSelectedViewController(index i : Int, animated: Bool) {
        if (i < 0 || viewControllers?.count == 0 ||  tabButtons?.count == 0) {
            initialViewControllerIndex = 0
            return
        }
        
        if delegate != nil {
            if delegate!.shouldSelectViewControllerAtIndex?(tabController: self, index: i) == false {
                return
            }
        }
        
        
        for b : UIButton in tabButtons! as [UIButton] {
            b.isEnabled = true
        }
        
        tabButtons![i].isEnabled = false
        
        let pb : UIButton = tabButtons![selectedIndex]
        let b : UIButton = tabButtons![i]
        let pv : UIViewController = viewControllers![selectedIndex]
        let vc : UIViewController = viewControllers![i]
        
        if isIndicatorEnabled == true && indicator != nil {
            if indicatorHugsContent == false {
                indicatorLeftConstraint!.constant = CGFloat(i) * b.frame.size.width * -1
            } else {
                let offset = (CGFloat(i) * b.frame.size.width) + ((b.frame.size.width/2.0) - ((indicator?.frame.size.width)!/2.0))
                indicatorLeftConstraint!.constant = offset * -1
            }
        }
        
        if (animated == false) {
            lock = true
            selectedIndex = i
            
            if (currentViewController != nil) {
                currentViewController?.willMove(toParentViewController: nil)
                currentViewController?.view.removeFromSuperview()
                currentViewController?.removeFromParentViewController()
            }
            
            destinationViewController = viewControllers![selectedIndex]
            
            destinationViewController!.view.frame = containerView.bounds
            destinationViewController!.willMove(toParentViewController: self)
            self.addChildViewController(destinationViewController!)
            containerView.addSubview(destinationViewController!.view)
            destinationViewController!.didMove(toParentViewController: self)
            
            currentViewController = destinationViewController!
            destinationViewController = nil
            
//            pb.setImage(pv.tabImageOff, forState: .normal)
//            b.setImage(vc.tabImageOn, forState: .normal)
            if pv.tabBgColorOff != nil {
                pb.backgroundColor = pv.tabBgColorOff
            }
            
            if vc.tabBgColorOn != nil {
                b.backgroundColor = vc.tabBgColorOn
            }
            pb.setImage(pv.tabImageOff, for: UIControlState())
            b.setImage(vc.tabImageOn, for: UIControlState())
            self.tabBar.layoutIfNeeded()
            
            tabBar.layoutIfNeeded()
            lock = false
        } else {
            
            if (selectedIndex == i) {
                return
            }
            
            let p = selectedIndex
            selectedIndex = i
            
            destinationViewController = viewControllers![selectedIndex]
            var startingX : CGFloat = containerView.frame.width
            var endingX : CGFloat = -containerView.frame.width
            var containerFrame = containerView.bounds
            if (selectedIndex < p) {
                startingX = -containerView.frame.width
                endingX = containerView.frame.width
            }
            
            containerFrame.origin.x = startingX
            destinationViewController?.view.frame = containerFrame
            containerView.addSubview((destinationViewController?.view)!)
            var pFrame = containerFrame
            pFrame.origin.x = endingX
            var nFrame = containerFrame
            nFrame.origin.x = 0
            
//            self.destinationViewController?.view.alpha = 0.0
            lock = true
            
            destinationViewController!.willMove(toParentViewController: self)
            if (self.currentViewController != nil) {
                self.currentViewController?.willMove(toParentViewController: nil)
                self.currentViewController?.view.removeFromSuperview()
                self.currentViewController?.removeFromParentViewController()
            }
            
            self.addChildViewController(self.destinationViewController!)
            self.destinationViewController!.didMove(toParentViewController: self)
            
            UIView.animate(withDuration: 0.150, delay: 0, options: .curveEaseOut, animations: {
                
                self.currentViewController?.view.frame = pFrame
                self.destinationViewController?.view.frame = nFrame
//                self.currentViewController?.view.alpha = 0.0
//                self.destinationViewController?.view.alpha = 1.0
                if pv.tabBgColorOff != nil {
                    pb.backgroundColor = pv.tabBgColorOff
                }
                
                if vc.tabBgColorOn != nil {
                    b.backgroundColor = vc.tabBgColorOn
                }
                pb.setImage(pv.tabImageOff, for: UIControlState())
                b.setImage(vc.tabImageOn, for: UIControlState())
                self.tabBar.layoutIfNeeded()
                
                }, completion: { (_) in
                    
                    self.currentViewController = self.destinationViewController!
                    self.destinationViewController = nil
                    
                    self.lock = false
            })
        }
        
        if delegate != nil {
            delegate!.didSelectViewControllerAtIndex?(tabController: self, index: selectedIndex)
        }
    }
    
    func buttonAction(sender : AnyObject?) {
        if lock == true {
            return
        }
        
        let s : UIButton = sender as! UIButton
        setSelectedViewController(index: s.tag, animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

@objc protocol MTabBarProtocol {
    @objc optional func shouldSelectViewControllerAtIndex(tabController : MTabBarViewController, index : Int) -> Bool
    @objc optional func didSelectViewControllerAtIndex(tabController : MTabBarViewController, index : Int)
}
