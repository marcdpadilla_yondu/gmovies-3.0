//
//  GMoviesDataOperation.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 21/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class GMoviesDataOperation: GMoviesOperation {
    
    var completion : (_ error : Error?, _ result : NSDictionary?) -> Void?
    var url : NSURL?
    var params : NSDictionary?
    var method : String = "GET"
    var headers : NSDictionary?
    
    private var retryCount : Int = 0
    
    override init() {
        completion = {_,_ in return}
        
        super.init()
    }
    
    convenience init(url urlString: String, parameters : NSDictionary?, httpMethod : String? = "GET", completion : @escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        self.init()
        
        url = NSURL(string: urlString)
        params = parameters
        method = httpMethod!
        self.completion = completion
    }
    
    override func start() {
        super.start()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let session : URLSession = URLSession.shared
        
        var urlRequest : URLRequest = URLRequest(url: url! as URL, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60.0)
        if params != nil {
            urlRequest.httpMethod = method
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            urlRequest.setValue("application/json, text/plain, text/html", forHTTPHeaderField: "Accept")
//            urlRequest.setValue("compress, gzip", forHTTPHeaderField: "Accept-Encoding")
            if method == "POST" {
                do {
                    let payload = try JSONSerialization.data(withJSONObject: params!)

                    urlRequest.httpBody = payload
                } catch (let error) {
                    print("GMoviesDataOperation payload", error.localizedDescription)
                }
            }
        }
        
        if headers != nil {
            for key : String in headers?.allKeys as! [String] {
                urlRequest.setValue(headers!.object(forKey: key as Any) as! String!, forHTTPHeaderField: key)
                print(headers!.object(forKey: key as Any) as! String!, key)
            }
        }
        
        if Constants.GMoviesAPIToken != nil {
            urlRequest.setValue(Constants.GMoviesAPIToken!, forHTTPHeaderField: "gmovies-token")
        }
        
        urlRequest.setValue(UserDefaults.standard.bool(forKey: "loggedin") == false ? "" : UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, forHTTPHeaderField: "gmovies-user")
        
        print(urlRequest)
        
        let task : URLSessionDataTask = session.dataTask(with: urlRequest as URLRequest) { (data, response, error) in
            var result : NSDictionary? = nil
            
//            sleep(15)
            
            //print(response)
            
            if error == nil {
                let httpResponse : HTTPURLResponse = response as! HTTPURLResponse
                
                do {
//                    print(String(data: data!, encoding: .utf8))
                    
                    if httpResponse.statusCode == 403 {
                        result = try JSONSerialization.jsonObject(with: data!) as? NSDictionary
                        let status : Int = Int((result?.object(forKey: "status") as? String)!)!
                        
                        if status == 0 {
                            // reauthenticate token
                            print("retrying")
//                            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
//                            DispatchQueue.main.async {
//                                appDelegate.showBanner(message: "Something went wrong. Retrying...", isPersistent: false)
//                            }
                            
                            if GMoviesAPIManager.checkOperationExistence(identifier: "tokenOperation") == true {
                                sleep(2)
                                self.start()
                                return
                            }
                            
                            GMoviesAPIManager.authenticateAPI { (error : Error?, result : NSDictionary?) in
                                if error == nil {
                                    let status : Int = result!.object(forKey: "status") as! Int
                                    if status == 1 {
                                        let data : NSDictionary = result!.object(forKey: "data") as! NSDictionary
                                        UserDefaults.standard.set(data.object(forKey: "token")!, forKey: "GMoviesAPIToken")
                                        UserDefaults.standard.synchronize()
                                        print("reauthenticated retrying", self.url)
                                        self.start()
                                    } else {
                                        //print(result)
                                        print("token error")
                                    }
                                } else {
                                    print(error!.localizedDescription)
                                }
                            }
                            
                            return
                            
                        } else {
                            result = try JSONSerialization.jsonObject(with: data!) as? NSDictionary
                            self.completion(nil, result)
                        }
                    } else if httpResponse.statusCode == 404 {
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        DispatchQueue.main.async {
                            appDelegate.showBanner(message: "Server Error", isPersistent: false)
                        }
                        self.completion(NSError(domain: "ph.com.globetel.GMovies", code: 404, userInfo: [NSLocalizedFailureReasonErrorKey : NSLocalizedString("Server Error", comment: "")]), nil)
                    } else if httpResponse.statusCode == 500 {
                        
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        
                        if self.retryCount < Constants.maxRetry {
                            self.retryCount += 1
                            sleep(3)
                            self.start()
                            return
                        } else {
                            DispatchQueue.main.async {
                                appDelegate.showBanner(message: "Server Error", isPersistent: false)
                            }
                            
                            self.completion(NSError(domain: "ph.com.globetel.GMovies", code: 500, userInfo: [NSLocalizedFailureReasonErrorKey : NSLocalizedString("Server Error", comment: "")]), nil)
                        }
                    } else {
                        result = try JSONSerialization.jsonObject(with: data!) as? NSDictionary
                        DispatchQueue.main.async {
                            self.completion(nil, result)
                        }
                    }
                    
//                    self.completion(nil, result)
                    
                } catch (let err) {
                    //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue))
                    print("GMoviesDataOperation", err.localizedDescription)
                    self.completion(err, result)
                }
                
            } else {
                
                if self.retryCount < Constants.maxRetry {
                    self.retryCount += 1
                    sleep(3)
                    self.start()
                    return
                } else {
                    print("GMoviesDataOperation", error!.localizedDescription)
                    self.completion(error, nil)
                }
                
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            self.finish()
        }
        
        task.resume()
    }
    
    override func cancel() {
        super.cancel()
        self.finish()
    }
    
    //MARK: Private functions
    
    private func parseParameters(params : NSDictionary?) -> String {
        
        if params != nil {
            do {
                let jsonData : Data = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
                
                return String(data: jsonData, encoding: .utf8)!
            } catch (let error) {
                print(error.localizedDescription)
            }
        }
        
        return "{}"
    }
    
}
