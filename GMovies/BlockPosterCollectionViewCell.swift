//
//  BlockPosterCollectionViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 18/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class BlockPosterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var advanceBookingLabel: UIButton!
    @IBOutlet weak var advanceBookingBg: UIView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var theaterName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        self.layer.borderWidth = 0.5
        
        //        if UserDefaults.standard.bool(forKey: "loggedin") == false {
        //            watchListButton.isHidden = true
        //        }
    }
}
