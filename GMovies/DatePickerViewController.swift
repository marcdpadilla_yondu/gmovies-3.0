//
//  DatePickerViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 15/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var delegate : DatePickerProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        datePicker.maximumDate = Date()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func datePickerDidChanged(_ sender: Any) {
        
        delegate?.didChangeDate(datePicker: self, date: datePicker.date)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol DatePickerProtocol {
    func didChangeDate(datePicker: DatePickerViewController, date: Date)
}
