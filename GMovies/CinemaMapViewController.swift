//
//  CinemaMapViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 7/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import GoogleMaps
import ReachabilitySwift

class CinemaMapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UITextFieldDelegate, AllCinemasControllerProtocol {
    
    private var locationManager : CLLocationManager?
    private var didCenterMap : Bool = false
    
    private var cinemas : [NSDictionary]?
    private var nearbyCinemas : [TheaterModel]?
    private var markers : [GMSMarker]?
    private var timer : Timer?
    private var ticker : Int = 0
    private var drawerCollapsed : Bool = false
    
    @IBOutlet weak var resultsCount: UILabel!
    @IBOutlet weak var drawer: UIView!
    @IBOutlet weak var drawerContainer: UIView!
    @IBOutlet weak var drawerTop: NSLayoutConstraint!
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var filterButton: MButton!
    @IBOutlet weak var resultsTableView: UITableView!
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var recentContainer: UIView!
    @IBOutlet weak var recentTableView: UITableView!
    
    @IBOutlet weak var recentContainerBottom: NSLayoutConstraint!
    
    private var loadingScreen : LoaderView?
    
    private var errorView : ErrorView?
    
    private var lastLocation: CGPoint = CGPoint.zero
    private var expandedDrawerPosition : CGPoint = CGPoint.zero
    private var resultsController : CinemaResultsTableViewController?
    private var fromMarker : Bool = false
    private var theaterFromMarker : TheaterModel?
    
    override func loadView() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
//        let camera = GMSCameraPosition.camera(withLatitude: -37.787359, longitude: 122.408227, zoom: 15.0)
//        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        mapView.delegate = self
//        mapView.isMyLocationEnabled = true
//        view = mapView
        
        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
//        marker.map = mapView
        super.loadView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        let camera = GMSCameraPosition.camera(withLatitude: 14.553449, longitude: 121.049992, zoom: 15.0)
//        let mv = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.camera = camera
//        mapView = mv
        
        filterButton.radioMode = true
        filterButton.offImage = UIImage(named: "all-cinemas")
        filterButton.onImage = UIImage(named: "bookable-cinemas")
        
        resultsController = CinemaResultsTableViewController()
        resultsController?.parent = self
        resultsController?.tableView = resultsTableView
        resultsTableView.delegate = resultsController
        resultsTableView.dataSource = resultsController
        
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager?.delegate = self
        
        searchView.layer.shadowColor = UIColor.black.cgColor
        searchView.layer.shadowRadius = 2.0
        searchView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        searchView.layer.shadowOpacity = 0.4
        
        cinemas = []
        nearbyCinemas = []
        markers = []
        downloadNearbyTheaters()
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        recentTableView.delegate = appDelegate.allCinemasController
        recentTableView.dataSource = appDelegate.allCinemasController
        appDelegate.allCinemasController?.tableView = recentTableView
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        lastLocation = drawer.center
        expandedDrawerPosition = drawer.frame.origin
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "CINEMAS"
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.allCinemasController?.movieObject = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "map")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.Cinemas)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidAppear), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: ReachabilityChangedNotification,object: appDelegate.reachability)
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager?.requestWhenInUseAuthorization()
            return
        } else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager?.startUpdatingLocation()
        }
        
        appDelegate.allCinemasController?.delegate = self
        recentTableView.reloadData()
        
        if UserDefaults.standard.bool(forKey: "showHeartPopup") == true {
            UserDefaults.standard.set(false, forKey: "showHeartPopup")
            UserDefaults.standard.synchronize()
            
            let alert = UIAlertController(title: "", message: "Tap the ♡ to save your preferred cinema", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK, got it", style: .default, handler: { (_) in
                
            })
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.title = ""
        self.searchField.resignFirstResponder()
        NotificationCenter.default.removeObserver(self, name: nil, object: nil)
        
        if timer != nil {
            timer?.invalidate()
            ticker = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc private func reachabilityChanged(note: NSNotification) {
        
        if note.name == ReachabilityChangedNotification {
            let reachability = note.object as! Reachability
            
            if reachability.isReachable {
                
                errorView?.removeFromSuperview()
                errorView = nil
                
                downloadNearbyTheaters()
                
            } else {
                DispatchQueue.main.async {
                    self.loadErrorView(titleString: "No Internet", description: "Please check your network settings and try again")
                }
            }
        }
    }
    
    private func loadErrorView(titleString : String, description : String, image: UIImage = UIImage(named: "no-internet")!) {
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = self.view
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addSubview(errorView!)
        self.view.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }
    
    // MARK: Location Delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
            break
            
        case .denied, .restricted:
            
            let alertController = UIAlertController(
                
                title: "Location Access Disabled",
                message: "In order to determine the cinemas near you, please open this app settings and set the location permission to 'When In Use'",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            break
            
        default:
            
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if didCenterMap == false {
            let location : CLLocation = locations[0]
            let camera = GMSCameraUpdate.setTarget(location.coordinate, zoom: 15.0)
            
            mapView.moveCamera(camera)
//            didCenterMap = true
        locationManager?.stopUpdatingLocation()
//        } else {
        
//        }
    }
    
    //MARK: Private functions
    
    @objc private func keyboardDidAppear(notif : Notification) {
        let info = notif.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        self.recentContainerBottom.constant = keyboardFrame.size.height - 44.0
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @objc private func keyboardDidHide(notif : Notification) {
        self.recentContainerBottom.constant = 0.0
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    private func downloadNearbyTheaters() {
        
        if timer != nil {
            timer?.invalidate()
            ticker = 0
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
        
        var coords : CLLocationCoordinate2D? = nil
        if locationManager?.location == nil {
            coords = CLLocationCoordinate2D(latitude: 14.553449, longitude: 121.049992)
        } else {
            coords = (locationManager?.location?.coordinate)!
        }
        
        resultsCount.text = "SEARCHING NEARBY CINEMAS"
        
        self.view.addSubview(loadingScreen!)
        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                  ])
        
        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
        
        GMoviesAPIManager.getTheaters(coords: coords!) { (error, result) in
            if error == nil {
                let status : Int = result?.object(forKey: "status") as! Int
                
                if status == 1 {
                    //print(result)
                    let theaters : [NSDictionary] = result?.value(forKeyPath: "data.results.theaters") as! [NSDictionary]
                    let preferred : [String] = result?.value(forKeyPath: "data.preferred") as! [String]
                    self.nearbyCinemas?.removeAll()
                    for theater : NSDictionary in theaters as [NSDictionary] {
                        let t : TheaterModel = TheaterModel(data: theater)
                        if preferred.contains(t.theaterId!) {
                            t.isPreferred = true
                        }
                        self.nearbyCinemas?.append(t)
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.hideBanner()
                        
                        self.resultsController?.setData(data: self.nearbyCinemas!)
                        
                        self.markTheaters(theaters: self.nearbyCinemas!, bookable: false)
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                    }
                    
                    let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                DispatchQueue.main.async {
                    self.loadingScreen?.stopAnimating()
                    self.loadingScreen?.removeFromSuperview()
                }
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBanner(message: error!.localizedDescription, isPersistent: false)
            }
            
            self.timer?.invalidate()
            self.timer = nil
            self.ticker = 0
        }
    }
    
    @objc private func tick() {
        self.ticker += 1
        
        if self.ticker == Constants.patience {
            self.timer?.invalidate()
            self.ticker = 0
            
            DispatchQueue.main.async {
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBanner(message: "Taking longer than expected... Please Wait.", isPersistent: false)
            }
        }
    }
    
    @IBAction func panGestureAction(_ sender: AnyObject) {
        let panGesture : UIPanGestureRecognizer = sender as! UIPanGestureRecognizer
        
        let translation =  panGesture.translation(in: view)
        
        self.view.endEditing(true)
        
        var filtered : [TheaterModel]? = nil
        if filterButton.isOn {
            filtered = nearbyCinemas?.filter({ (t) -> Bool in
                let theater = t
                
                return theater.isActive == true
            })
        } else {
            filtered = nearbyCinemas
        }
        
        if panGesture.state == UIGestureRecognizerState.ended {
            let velocity = panGesture.velocity(in: view)
            
            if velocity.y > 0 {
                UIView.animate(withDuration: 0.200, delay: 0, options: .curveEaseOut, animations: {
                    self.drawer.frame.origin.y = self.view.frame.size.height - 41.0
                    
                    }, completion: { (_) in
                        panGesture.setTranslation(CGPoint.zero, in: self.view)
                        
                        let theater = filtered?.first
                        if theater?.location == nil {
                            self.drawerCollapsed = true
                            return
                        }
                        let camera = GMSCameraUpdate.setTarget((theater?.location)!, zoom: 15.0)
                        
                        self.mapView.animate(with: camera)
                        
                        self.drawerCollapsed = true
                })
            } else {
                UIView.animate(withDuration: 0.200, delay: 0, options: .curveEaseOut, animations: {
                    self.drawer.frame.origin = self.expandedDrawerPosition
                    
                    }, completion: { (_) in
                        panGesture.setTranslation(CGPoint.zero, in: self.view)
                        
                        let theater = filtered?.first
                        
                        if theater == nil {
                            self.drawerCollapsed = false
                            return
                        }
                        if theater?.location == nil {
                            self.drawerCollapsed = false
                            return
                        }
                        
                        var point = self.mapView.projection.point(for: (theater?.location)!)
                        point.y += (self.drawer.frame.origin.y / 2.0) - 40.0
                        let camera = GMSCameraUpdate.setTarget(self.mapView.projection.coordinate(for: point), zoom: 15.0)
                        
                        self.mapView.animate(with: camera)
                        self.drawerCollapsed = false
                })
            }
        } else if panGesture.state == UIGestureRecognizerState.changed {
            UIView.animate(withDuration: 0.100, delay: 0, options: .curveEaseOut, animations: {
                self.drawer.frame.origin.y += translation.y
                if self.drawer.frame.origin.y > self.view.frame.size.height - 41.0 {
                    self.drawer.frame.origin.y = self.view.frame.size.height - 41.0
                    self.drawerCollapsed = true
                } else if self.drawer.frame.origin.y < self.expandedDrawerPosition.y {
                    self.drawer.frame.origin = self.expandedDrawerPosition
                    self.drawerCollapsed = false
                }
                
                }, completion: { (_) in
                    
            })
            
            panGesture.setTranslation(CGPoint.zero, in: view)
        }
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesBegan(touches, with: event)
//        lastLocation = drawer.center
//    }
//    
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesEnded(touches, with: event)
//        drawer.frame.origin.y = expandedDrawerPosition.y
//    }
    
    @IBAction func filterButtonAction(_ sender: AnyObject) {
        let button : MButton = sender as! MButton
        resultsController?.filterData(bookable: button.isOn)
        markTheaters(theaters: nearbyCinemas!, bookable: button.isOn)
    }
    
    private func markTheaters(theaters : [TheaterModel], bookable : Bool) {
        
        markers?.removeAll()
        mapView.clear()
        
        var filtered : [TheaterModel]? = nil
        if bookable {
            filtered = nearbyCinemas?.filter({ (t) -> Bool in
                let theater = t
                
                return theater.isActive == true
            })
        } else {
            filtered = theaters
        }
        
        self.resultsCount.text = "\((filtered?.count)!) CINEMA\((filtered?.count)! != 1 ? "S" : "") NEARBY"
        
        for theater in filtered! {
            if theater.location == nil {
                continue
            }
            let marker : GMSMarker = GMSMarker(position: theater.location!)
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.groundAnchor = CGPoint(x: 0.5, y: 1.0)
            marker.icon = nil
            let markerView : CinemaMarker = Bundle.main.loadNibNamed("CinemaMarker", owner: self, options: nil)?.first as! CinemaMarker
            markerView.isActive = theater.isActive
            markerView.coordinates = theater.location
            markerView.cinemaName.text = theater.name!
            markerView.cinemaAddress.text = theater.address!
            markerView.theaterObject = theater
            marker.iconView = markerView
            marker.map = self.mapView
            markers?.append(marker)
        }
        
        let theater = filtered!.first
        if theater?.location != nil {
            var point = mapView.projection.point(for: (theater?.location)!)
            point.y += (drawer.frame.origin.y / 2.0) - 40.0
            let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point), zoom: 15.0)
            
            mapView.animate(with: camera)
        }
    }
    
    //MARK: Map View delegates
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        self.view.endEditing(true)
        
        let custom : CinemaMarker = marker.iconView as! CinemaMarker
        var point = mapView.projection.point(for: marker.position)
        point.y = (drawer.frame.origin.y / 2.0) + 60.0
        let camera = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point), zoom: 18.0)
        
        if (custom.isExpanded == false) {
            custom.expand()
            
            mapView.animate(with: camera)
        } else {
            
            fromMarker = true
            theaterFromMarker = (marker.iconView as! CinemaMarker).theaterObject
            self.performSegue(withIdentifier: "showMovieList", sender: nil)
            custom.collapse()
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            let theater : TheaterModel = (appDelegate.allCinemasController?.getAllCinemas()?.filter({ (t) -> Bool in
//                return t.name == custom.cinemaName.text
//            }).first)!
            
            
        }
        
        mapView.selectedMarker = marker
        
        if drawerCollapsed == false {
            UIView.animate(withDuration: 0.200, delay: 0, options: .curveEaseOut, animations: {
                self.drawer.frame.origin.y = self.view.frame.size.height - 41.0
                
                }, completion: { (_) in
                    self.drawerCollapsed = true
            })
        }
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        self.view.endEditing(true)
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        self.view.endEditing(true)
    }
    
    //MARK: Textfield : Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        recentContainer.isHidden = false
        recentTableView.reloadData()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        recentContainer.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        recentContainer.isHidden = true
        textField.resignFirstResponder()
        
        if (textField.text?.characters.count)! > 0 {
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let filteredArray : [TheaterModel]? = appDelegate.allCinemasController?.getAllCinemas()?.filter({ (theater) -> Bool in
                
                return (theater.address?.localizedCaseInsensitiveContains(textField.text!))! || (theater.name?.localizedCaseInsensitiveContains(textField.text!))!
            }).sorted(by: { (t1, t2) -> Bool in
                
                return t1.name?.compare(t2.name!) == ComparisonResult.orderedAscending
            })
            
            nearbyCinemas = filteredArray
            resultsController?.setData(data: nearbyCinemas!)
            markTheaters(theaters: nearbyCinemas!, bookable: filterButton.isOn)
            recentContainer.isHidden = true
        }
        
        return true
    }
    
    //MARK : all cinemas controller delegate
    
    func didSelectDataSource(data: [TheaterModel]) {
        nearbyCinemas = data
        resultsController?.setData(data: nearbyCinemas!)
        markTheaters(theaters: nearbyCinemas!, bookable: filterButton.isOn)
        recentContainer.isHidden = true
        self.searchField.resignFirstResponder()
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieList" {
            let vc : MTabBarViewController = segue.destination as! MTabBarViewController
            vc.isIndicatorEnabled = true
            
            var tm : TheaterModel? = nil
            if fromMarker == true {
                fromMarker = false
                tm = theaterFromMarker
            } else {
                let p : CinemaResultsCell = sender as! CinemaResultsCell
                let c : CinemaResultsTableViewController = (p.superview?.superview as! UITableView).delegate as! CinemaResultsTableViewController
                let i : IndexPath =  (p.superview?.superview as! UITableView).indexPath(for: p)!
                tm = c.dataSource![i.row]
            }
            
            let storyboard : UIStoryboard = UIStoryboard(name: "Cinemas", bundle: nil)
            let nowshowing : MovieListViewController = storyboard.instantiateViewController(withIdentifier: "NowShowingView") as! MovieListViewController
            nowshowing.theaterObject = tm
            nowshowing.title = "Now Showing"
            nowshowing.tabTextColorOn = UIColor.white
            nowshowing.tabTextColorOff = UIColor(netHex: 0xaaaab2)
            nowshowing.tabBgColorOn = UIColor(netHex: 0x28314e)
            nowshowing.tabBgColorOff = UIColor(netHex: 0x28314e)
            
            let comingsoon : MovieListViewController = storyboard.instantiateViewController(withIdentifier: "NowShowingView") as! MovieListViewController
            comingsoon.theaterObject = tm
            comingsoon.title = "Coming Soon"
            comingsoon.tabTextColorOn = UIColor.white
            comingsoon.tabTextColorOff = UIColor(netHex: 0xaaaab2)
            comingsoon.tabBgColorOn = UIColor(netHex: 0x28314e)
            comingsoon.tabBgColorOff = UIColor(netHex: 0x28314e)
            comingsoon.comingSoonMode = true
            
            vc.title = tm!.name!
            vc.viewControllers = [nowshowing, comingsoon]
            
        }
    }

}
