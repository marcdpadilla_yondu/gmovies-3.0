//
//  MyReviewsListViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 16/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MyReviewsListViewController: UITableViewController {

    private var reviews : [ReviewModel] = []
    private var hideContent : Bool = false
    private var errorView : ErrorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "MY REVIEWS"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "review_list")
        downloadReviews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return reviews.count
    }
    
    private func downloadReviews() {
        GMoviesAPIManager.getReviews { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    let data : [NSDictionary] = result?.value(forKey: "data") as! [NSDictionary]
                    
                    if data.count == 0 {
                        DispatchQueue.main.async {
                            self.loadErrorView(titleString: "No Reviews", description: "You haven't reviewed any movies yet", image: UIImage(named: "no-reviews")!)
                        }
                    } else {
                        
                        self.reviews.removeAll()
                        for r in data {
                            let rev = ReviewModel(data: r)
                            
                            self.reviews.append(rev)
                        }
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.errorView?.removeFromSuperview()
                        }
                        
                    }
                    
                } else {
                    
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    private func loadErrorView(titleString : String, description : String, image: UIImage = UIImage(named: "no-internet")!) {
        hideContent = true
        tableView.separatorStyle = .none
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = tableView.subviews.first!
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addSubview(errorView!)
        self.view.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewIdentifier", for: indexPath)

        configureCell(cell: cell as! MyReviewCell, indexPath: indexPath)

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func configureCell(cell : MyReviewCell, indexPath : IndexPath) {
        let review : ReviewModel = reviews[indexPath.row]
        
        cell.movieTitle.text = review.movie?.movieTitle!
        cell.rating.rating = Double(review.rating)
        cell.comment.text = review.comment!
        cell.createdDate.text = review.createdAtString
        
        if review.movie?.poster != nil {
            cell.poster.image = review.movie?.poster
        } else {
            downloadPoster(review: review, indexPath: indexPath)
        }
    }
    
    private func downloadPoster(review : ReviewModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: review.movie!.posterUrl!, completion: {(error, image) in
            
            if error == nil {
                review.movie?.poster = image
                DispatchQueue.main.async {
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                }
            } else {
                print(error!.localizedDescription)
            }
            
        })
    }

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
