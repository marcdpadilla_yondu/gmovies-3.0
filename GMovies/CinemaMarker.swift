//
//  CinemaMarker.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 28/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import CoreLocation

class CinemaMarker: UIView {
    
    @IBOutlet weak var cinemaAddress: UILabel!
    @IBOutlet weak var cinemaName: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var pointer: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailViewWidth: NSLayoutConstraint!
    @IBOutlet weak var pointerCenter: NSLayoutConstraint!
    
    @IBOutlet weak var cinemaNameLeft: NSLayoutConstraint!

    @IBOutlet weak var cinemaAddressLeft: NSLayoutConstraint!
    
    var isExpanded : Bool = false
    var isActive : Bool = false
    var coordinates : CLLocationCoordinate2D?
    
    private let inactiveColor : UIColor = UIColor(netHex: 0xc3c6d0)
    private let activeColor : UIColor = UIColor(netHex: 0xd81515)
    
    var theaterObject : TheaterModel?
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let path : UIBezierPath = UIBezierPath()
        
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        path.addLine(to: CGPoint(x: 15.0, y: 0.0))
        path.addLine(to: CGPoint(x: 7.5, y: 12.0))
        
        path.close()
        
        let mask : CAShapeLayer = CAShapeLayer()
        mask.frame = pointer.bounds
        mask.path = path.cgPath
        
        pointer.layer.mask = mask
        
        iconView.layer.cornerRadius = 10.0
        detailView.layer.cornerRadius = 10.0
        detailView.layer.borderWidth = 1.0
        detailView.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        detailView.layer.borderWidth = 0.5
        
        if isActive == false {
            iconView.backgroundColor = inactiveColor
            pointer.backgroundColor = inactiveColor
        } else {
            iconView.backgroundColor = activeColor
            pointer.backgroundColor = activeColor
        }
    }
    
    func expand() {
        detailViewWidth.constant = 200.0
        pointerCenter.constant = (iconView.frame.size.width/2.0) + 7.5
        cinemaNameLeft.constant = iconView.frame.size.width + 5.0
        cinemaAddressLeft.constant = iconView.frame.size.width + 5.0
        var f : CGRect = frame
        f.size.width = 300.0
        UIView.animate(withDuration: 0.250, animations: { 
            self.layoutIfNeeded()
            self.frame = f
            self.pointer.backgroundColor = UIColor.white
            }) { (_) in
                self.isExpanded = true
        }
    }
    
    func collapse() {
        detailViewWidth.constant = iconView.frame.size.width
        pointerCenter.constant = 0.0
        cinemaNameLeft.constant = 0.0
        cinemaAddressLeft.constant = 0.0
        var f : CGRect = frame
        f.size.width = 70.0
        UIView.animate(withDuration: 0.250, animations: {
            self.layoutIfNeeded()
            self.frame = f
            self.pointer.backgroundColor = self.isActive ? self.activeColor : self.inactiveColor
        }) { (_) in
            self.isExpanded = false
        }
    }
}
