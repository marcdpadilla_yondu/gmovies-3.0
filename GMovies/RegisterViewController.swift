//
//  RegisterViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 25/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class RegisterViewController: UITableViewController, TextfieldCellDelegate {
    
    
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var termsAndConditionsCheckbox: MButton!
    @IBOutlet weak var termsAndConditionsTextView: AttributedTextView!
    
    @IBOutlet weak var loyaltyProgramCheckbox: MButton!
    @IBOutlet weak var loyaltyProgramTextView: AttributedTextView!
    
    var facebookDetails : NSDictionary?
    
    var fields : [FieldConfig]?
    
    var modalMode : Bool = false
    var checkJoinLoyaltyProgram = false
    var hideJoinLoyaltyProgramCheckbox = false
    var disableJoinLoyaltyProgramCheckbox = false
    var previousViewController : UIViewController?
    var nextViewController : UIViewController?
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.tableView?.addGestureRecognizer(tapGesture)
        
        fields = [FieldConfig(label: "SPACE", errorString: ""), FieldConfig(label: "FIRST NAME", errorString: "This field is required"),
        FieldConfig(label: "LAST NAME", errorString: "This field is required"),
        FieldConfig(label: "MOBILE NO", errorString: "This field is required", placeholder: "917xxxxxxxx", prefix: "+63"),
        FieldConfig(label: "EMAIL", errorString: "This field is required")]
        
        if facebookDetails != nil {
            for i in 1..<fields!.count {
                let fc = fields![i]
                if i == 1 {
                    fc.val = facebookDetails?.object(forKey: "first_name") as! String
                } else if i == 2 {
                    fc.val = facebookDetails?.object(forKey: "last_name") as! String
                } else if i == 3 {
                    
                } else if i == 4 {
                    fc.val = facebookDetails?.object(forKey: "email") as? String == nil ? "" : facebookDetails?.object(forKey: "email") as! String
                }
            }
            
        }
        
        configureView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "CREATE ACCOUNT"
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.SignUp)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        UIManager.roundify(submitButton)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    fileprivate func configureView() {
        
        termsAndConditionsCheckbox.offImage = UIImage(named: "terms-box-unchecked")
        termsAndConditionsCheckbox.onImage = UIImage(named: "terms-box-checked")
        termsAndConditionsCheckbox.radioMode = true
        
        loyaltyProgramCheckbox.offImage = UIImage(named: "terms-box-unchecked")
        loyaltyProgramCheckbox.onImage = UIImage(named: "terms-box-checked")
        loyaltyProgramCheckbox.radioMode = true
        loyaltyProgramCheckbox.isOn = true
        if hideJoinLoyaltyProgramCheckbox {
            loyaltyProgramCheckbox.isOn = checkJoinLoyaltyProgram
        }
        loyaltyProgramCheckbox.isEnabled = !disableJoinLoyaltyProgramCheckbox
        loyaltyProgramCheckbox.isHidden = hideJoinLoyaltyProgramCheckbox
        loyaltyProgramTextView.isHidden = hideJoinLoyaltyProgramCheckbox
        
        termsAndConditionsTextView.attributer = "I accept GMovies' "
            .color(UIColor(hex: 11187148))
            .font(UIFont(name: "OpenSans", size: 12))
            .append("Terms and Conditions")
            .font(UIFont(name: "OpenSans-Bold", size: 12))
            .underline
            .makeInteract({ (_) in
                self.performSegue(withIdentifier: "showTerms", sender: self)
            })
            .setLinkColor(UIColor.white)
        
        loyaltyProgramTextView.attributer = "I agree to join "
            .color(UIColor(hex: 11187148))
            .font(UIFont(name: "OpenSans", size: 12))
            .append("Reel Rewards")
            .font(UIFont(name: "OpenSans-Bold", size: 12))
            .underline
            .makeInteract({ (_) in
                let loyaltyProgramVC = JoinLoyaltyProgramViewController()
                loyaltyProgramVC.title = "BACK TO REGISTRATION"
                loyaltyProgramVC.showJoinNowButton = false
                loyaltyProgramVC.showNotNowButton = false
                self.show(loyaltyProgramVC, sender: self)
            })
            .setLinkColor(UIColor.white)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (fields?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell?
        
        if ((indexPath as NSIndexPath).row == 0) {
            cell = tableView.dequeueReusableCell(withIdentifier: "space", for: indexPath) as! TextFieldTableViewCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! TextFieldTableViewCell
            
            configureCell(cell as! TextFieldTableViewCell, indexPath: indexPath)
        }

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath as NSIndexPath).row == 0 {
            return 50.0
        }
        
        return 80.0
    }
    
    fileprivate func configureCell(_ cell : TextFieldTableViewCell, indexPath : IndexPath) {
        let labelString = fields![(indexPath as NSIndexPath).row].label!
        let fc = fields![indexPath.row]
        
        cell.prompt.isHidden = true
        cell.label.text = labelString
        cell.delegate = self
        
        cell.separator.layer.borderWidth = 0.25
        cell.separator.layer.borderColor = UIColor(netHex: 0x777f99).cgColor
        
        cell.placeholder = fields![(indexPath as NSIndexPath).row].placeholder
        cell.pre = fields![(indexPath as NSIndexPath).row].prefix
        cell.prefix.text = fields![(indexPath as NSIndexPath).row].prefix
        
        if (indexPath as NSIndexPath).row == 3 {
            cell.maxLength = 10
            cell.inputField.keyboardType = .numberPad
        } else {
            cell.maxLength = 0
            cell.inputField.keyboardType = .default
        }
        
        if facebookDetails != nil {
            if indexPath.row == 1 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            } else if indexPath.row == 2 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            } else if indexPath.row == 3 {
                cell.inputField.text = fc.val
                cell.inputField.becomeFirstResponder()
            } else if indexPath.row == 4 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            }
            
        }
        
        fc.val = cell.inputField.text?.compare(fc.val) != .orderedSame ? cell.inputField.text! : fc.val
        cell.inputField.text = fc.val
    }
    
    private func validateFields() -> Bool {
        
        var isValid : Bool = true
        for i : Int in 1 ..< (fields?.count)! {
            tableView.scrollToRow(at: IndexPath(item: i, section: 0), at: .none, animated: false)
            let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: i, section: 0)) as! TextFieldTableViewCell
            if cell.inputField.text?.characters.count == 0 {
                cell.prompt.text = "Field is required"
                cell.prompt.isHidden = false
                cell.shake()
                isValid = false
            }
            
            if i == 3 {
                
                if cell.inputField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) != nil {
                    cell.prompt.text = "Invalid mobile number"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                } else if (cell.inputField.text?.characters.count)! < 10 {
                    cell.prompt.text = "Invalid mobile number"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                }
            }
            
            if i == 4 {
                if cell.inputField.text?.contains("@") == false || cell.inputField.text?.contains(".") == false {
                    cell.prompt.text = "Invalid email"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                }
            }
        }
        
        if termsAndConditionsCheckbox.isOn == false {
            isValid = false
            termsAndConditionsCheckbox.shake()
        }
        
        return isValid
    }
    
    @IBAction func submitAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "register_submit")
        if validateFields() {
            let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: 3, section: 0)) as! TextFieldTableViewCell
            GMoviesAPIManager.register(mobileNum: cell.inputField.text!, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    if status == 0 {
                        DispatchQueue.main.async {
                            cell.prompt.text = (result?.value(forKeyPath: "response.msg") as! String)
                            cell.prompt.isHidden = false
                            cell.shake()
                        }
                    } else {
                        
                        self.performSegue(withIdentifier: "showVerify", sender: nil)
                        
                    }
                    
                } else {
                    print(error!.localizedDescription)
                }
            })
        }
        
    }

    @IBAction func termsAndConditionsCheckboxClicked(_ sender: Any) {
        
    }
    
    @IBAction func loyaltyProgramCheckboxClicked(_ sender: Any) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVerify" {
            let v : VerifyMobileViewController = segue.destination as! VerifyMobileViewController
            let userDetails : NSMutableDictionary? = NSMutableDictionary()
            for i : Int in 1 ..< (fields?.count)! {
                tableView.scrollToRow(at: IndexPath(item: i, section: 0), at: .none, animated: false)
                let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: i, section: 0)) as! TextFieldTableViewCell
                if i == 1 {
                    userDetails?.setValue(cell.inputField.text!, forKey: "first_name")
                } else if i == 2 {
                    if cell.inputField.text!.isEmpty {
                        userDetails?.setValue("-", forKey: "last_name")
                    }else {
                        userDetails?.setValue(cell.inputField.text!, forKey: "last_name")
                    }
                } else if i == 3 {
                    userDetails?.setValue("+63\(cell.inputField.text!)", forKey: "mobile")
                } else if i == 4 {
                    userDetails?.setValue(cell.inputField.text!, forKey: "email")
                }
            }
            
            if facebookDetails != nil {
                userDetails?.setValue(facebookDetails?.value(forKey: "id") as! String, forKey: "fbid")
            } else {
                userDetails?.setValue("0", forKey: "fbid")
            }
            if loyaltyProgramCheckbox.isOn {
                userDetails?.setValue(true, forKey: "register_loyalty_program")
            }else {
                userDetails?.setValue(false, forKey: "register_loyalty_program")
            }
            
            v.userDetails = userDetails
            v.modalMode = modalMode
            v.joinLoyaltyProgramAutomatically = checkJoinLoyaltyProgram
            v.previousViewController = previousViewController
            v.nextViewController = nextViewController
            
        } else if segue.identifier == "showTerms" {
            let v = (segue.destination as! MNavigationViewController).topViewController as! TANDCViewController
            v.url = "\(Constants.gMoviesWebIP)/iframe/page/terms"
        }
    }
    
    func shouldBeginEditing(cell: TextFieldTableViewCell) -> Bool {
        
        return true
    }
    
    func didBeginEditing(cell: TextFieldTableViewCell) {
        
    }
    
    func didEndEditing(cell: TextFieldTableViewCell) {
        if tableView.indexPath(for: cell) != nil {
            let indexPath = tableView.indexPath(for: cell)! as IndexPath
            let fc = fields![indexPath.row]
            fc.val = cell.inputField.text!
        }
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        tableView.endEditing(true)
    }
    
    
}
