//
//  MovieDetailViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 23/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import Cosmos
import FBSDKShareKit

class MovieDetailViewController: UIViewController, YTPlayerViewDelegate {

    
    @IBOutlet weak var blockInfo: UIStackView!
    @IBOutlet weak var blockDateLabel: UILabel!
    @IBOutlet weak var blockCinemaLabel: UILabel!
    @IBOutlet weak var blockTimeLabel: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var moviePoster: UIImageView!
    var movieObject : MovieModel?
    var theaterObject : TheaterModel?
    var blockObject : BlockScreeningModel?
    
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var advisoryWidth: NSLayoutConstraint!
    @IBOutlet weak var advisory: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var movieLength: UILabel!
    
    @IBOutlet weak var posterFrame: UIView!
    @IBOutlet weak var miniPoster: UIImageView!
    @IBOutlet weak var synopsisLabel: UILabel!
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var synopsisView: UIView!
    
    @IBOutlet weak var ratingsContainer: UIView!
    @IBOutlet weak var midRatingBox: UIView!
    
    @IBOutlet weak var castLabel: UILabel!
    @IBOutlet weak var cast: UILabel!
    @IBOutlet weak var mechanics: UILabel!
    
    
    @IBOutlet weak var rottenRating: UIButton!
    @IBOutlet weak var audienceRating: UIButton!
    
    @IBOutlet weak var audienceStarRating: CosmosView!
    @IBOutlet weak var yourStarRating: CosmosView!
    
    @IBOutlet weak var trailerView: YTPlayerView!
    
    
    @IBOutlet weak var castHeight: NSLayoutConstraint!
    @IBOutlet weak var castTop: NSLayoutConstraint!
    @IBOutlet weak var ratingsHeight: NSLayoutConstraint!
    @IBOutlet weak var synopsisHeight: NSLayoutConstraint!
    @IBOutlet weak var synopsisBottom: NSLayoutConstraint!    
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var buyTicketsButton: UIButton!
    @IBOutlet weak var watchButton: UIButton!
    @IBOutlet weak var watchButtonWidth: NSLayoutConstraint!
    
    private let gradient1: CAGradientLayer = CAGradientLayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        advisory.layer.borderWidth = 0.5
        advisory.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        advisory.layer.cornerRadius = 3.0
        
        posterFrame.layer.borderWidth = 0.5
        posterFrame.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        
//        ratingsContainer.layer.borderWidth = 0.5
//        ratingsContainer.layer.borderColor = UIColor(netHex: 0xc0c0c0).cgColor
//        
//        midRatingBox.layer.borderWidth = 0.5
//        midRatingBox.layer.borderColor = UIColor(netHex: 0xc0c0c0).cgColor
        
        moviePoster.alpha = 0.0
        
        if blockObject != nil {
            self.advisory.text = self.blockObject?.movie!.advisoryRatingString!
            self.genre.text = self.movieObject?.genre == nil ? "" : self.movieObject?.genre!
            self.synopsisLabel.text = self.blockObject?.movie!.synopsisString!
            self.movieLength.text = self.blockObject?.movie!.length!
            self.mechanics.text = self.blockObject?.mechanics!
            movieTitle.text = blockObject?.movie?.movieTitle?.uppercased()
            watchButtonWidth.constant = 0.0
            ratingsHeight.constant = 0.5
            ratingsContainer.isHidden = true
            castTop.constant += 10.0
            cast.isHidden = true
            castLabel.isHidden = true
            trailerView.isHidden = true
            
            blockDateLabel.text = blockObject?.screeningDateString
            blockCinemaLabel.text = "\(blockObject!.theater!.name!), \(blockObject!.theater!.cinemas!.first!.name!)"
            blockTimeLabel.text = blockObject?.screeningTimeAMPM
            
            blockInfo.isHidden = false
            buyTicketsButton.setTitle("CLAIM TICKETS", for: .normal)
            
            return
        }
        
        blockInfo.isHidden = true

        movieTitle.text = movieObject?.movieTitle?.uppercased()
        mechanics.isHidden = true
        
        switch self.movieObject!.rottenSource! {
        case "certified fresh":
            self.rottenRating.setImage(UIImage(named: "fresh"), for: .normal)
            break
        case "upright":
            self.rottenRating.setImage(UIImage(named: "popcorn-full"), for: .normal)
            break
        case "spilled":
            self.rottenRating.setImage(UIImage(named: "popcorn-spilled"), for: .normal)
            break
        case "rotten":
            self.rottenRating.setImage(UIImage(named: "rotten"), for: .normal)
            break
        case "fresh":
            self.rottenRating.setImage(UIImage(named: "tomatoe"), for: .normal)
            break
        default:
            self.rottenRating.setImage(UIImage(named: "plus"), for: .normal)
        }
        
        self.audienceRating.setTitle("\(self.movieObject!.audienceRatingPercent!)%", for: .normal)
        
        switch self.movieObject!.audienceSource! {
        case "certified fresh":
            self.audienceRating.setImage(UIImage(named: "fresh"), for: .normal)
            break
        case "upright":
            self.audienceRating.setImage(UIImage(named: "popcorn-full"), for: .normal)
            break
        case "spilled":
            self.audienceRating.setImage(UIImage(named: "popcorn-spilled"), for: .normal)
            break
        case "rotten":
            self.audienceRating.setImage(UIImage(named: "rotten"), for: .normal)
            break
        case "fresh":
            self.audienceRating.setImage(UIImage(named: "tomatoe"), for: .normal)
            break
        default:
            self.audienceRating.setImage(UIImage(named: "plus"), for: .normal)
        }
        
        if movieObject?.cast == nil || movieObject?.synopsis == nil || movieObject?.theaters?.count == 0 {
            getFullMovieDetails()
        } else {
            self.advisory.text = self.movieObject?.advisoryRatingString!
            self.genre.text = self.movieObject?.genre == nil ? "" : self.movieObject?.genre!
            self.synopsisLabel.text = self.movieObject?.synopsisString!
            self.movieLength.text = self.movieObject?.length!
            self.cast.text = self.movieObject?.castList!
            self.rottenRating.setTitle("\(self.movieObject!.rottenRatingPercent!)%", for: .normal)
            self.audienceRating.setTitle("\(self.movieObject!.audienceRatingPercent!)%", for: .normal)
            
            getFullMovieDetails()
        }
        
        // Do any additional setup after loading the view.
        
        if movieObject?.hasSchedule == false {
            buyTicketsButton.backgroundColor = UIColor(netHex: 0x0c7fc8)
            watchButtonWidth.constant = 0.0
            if movieObject?.watched == false {
                buyTicketsButton.setTitle("NOTIFY ME", for: .normal)
                buyTicketsButton.setImage(nil, for: .normal)
                buyTicketsButton.setTitleColor(UIColor.white, for: .normal)
                buyTicketsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                buyTicketsButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            } else {
                buyTicketsButton.setTitle("WATCHING", for: .normal)
                buyTicketsButton.setImage(UIImage(named: "added"), for: .normal)
                buyTicketsButton.setTitleColor(UIColor(netHex: 0x4aa8e3), for: .normal)
                buyTicketsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
                buyTicketsButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            }
        }
        
        if movieObject?.watched == false {
            watchButton.setImage(UIImage(named: "watchlist-add"), for: .normal)
        } else {
            watchButton.setImage(UIImage(named: "added"), for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        if blockObject == nil {
            if movieObject!.landscapePoster == nil {
                downloadPoster()
            } else {
                self.moviePoster.image = movieObject?.landscapePoster
                moviePoster.alpha = 1.0
            }
            
            if movieObject!.poster == nil {
                downloadMiniPoster()
            } else {
                self.miniPoster.image = movieObject?.poster
            }
        } else {
            if blockObject!.landscapePoster == nil {
                downloadPoster()
            } else {
                self.moviePoster.image = blockObject?.landscapePoster
                moviePoster.alpha = 1.0
            }
            
            if blockObject!.poster == nil {
                downloadMiniPoster()
            } else {
                self.miniPoster.image = blockObject?.poster
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "movie_detail")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.MovieDetails)
        
        ratingsContainer.layer.addBorder(edge: .top, color: UIColor(netHex: 0xdcdcdc), thickness: 0.5)
        ratingsContainer.layer.addBorder(edge: .bottom, color: UIColor(netHex: 0xdcdcdc), thickness: 0.5)
        midRatingBox.layer.addBorder(edge: .left, color: UIColor(netHex: 0xdcdcdc), thickness: 0.5)
        midRatingBox.layer.addBorder(edge: .right, color: UIColor(netHex: 0xdcdcdc), thickness: 0.5)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Private functions
    
    private func configureView() {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = moviePoster.bounds
        gradient1.frame = moviePoster.bounds
        
        if blockObject != nil {
            mechanics.preferredMaxLayoutWidth = UIScreen.main.bounds.size.width - 16.0
            
            gradient.colors = [UIColor.clear.cgColor, UIColor(netHex: 0x28314E).cgColor]
            gradient1.colors = [UIColor.clear.cgColor, UIColor.clear.cgColor, UIColor(netHex: 0x28314E).cgColor]
            
            var mechanicsSize = NSString(string : blockObject!.mechanics!).boundingRect(with: CGSize(width: mechanics.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : synopsisLabel.font!], context: nil).size
            mechanicsSize.height += 20.0
            
            if mechanicsSize.height > mechanics.frame.size.height {
                castHeight.constant = mechanicsSize.height
                contentViewHeight.constant += abs(mechanicsSize.height - 102.0)
                
                self.scrollView.layoutIfNeeded()
            }
            
        } else {
            gradient.colors = [UIColor.clear.cgColor, UIColor.clear.cgColor, UIColor.white.cgColor]
            gradient1.colors = [UIColor.clear.cgColor, UIColor.clear.cgColor, UIColor.white.cgColor]
        }
        
        moviePoster.layer.sublayers?.removeAll()
        moviePoster.layer.insertSublayer(gradient, at: 0)
        self.scrollView.contentSize = self.contentView.frame.size
        
        if trailerView.layer.sublayers?.contains(gradient1) == false {
            trailerView.layer.addSublayer(gradient1)
        }
        
//        let gradient1: CAGradientLayer = CAGradientLayer()
//        gradient1.frame = synopsisLabel.bounds
//        gradient1.frame.size.height = 30.0
//        gradient1.frame.origin.y = synopsisLabel.bounds.height - 30.0
//        gradient1.colors = [UIColor.clear.cgColor, UIColor.clear.cgColor, UIColor.white.cgColor]
//        synopsisLabel.layer.sublayers?.removeAll()
//        synopsisLabel.layer.insertSublayer(gradient1, at: 0)
        
//        contentView.frame = UIScreen.main.bounds
//        print(contentView.frame)
    }
    
    private func downloadPoster() {
        if blockObject == nil {
            GMoviesAPIManager.downloadPoster(urlString: movieObject!.landscapePosterUrl!, completion: { (error, image) in
                if error == nil {
                    self.movieObject?.landscapePoster = image
                    DispatchQueue.main.async {
                        self.moviePoster.alpha = 0.0
                        self.moviePoster.image = image
                        UIView.animate(withDuration: 0.300, animations: {
                            self.moviePoster.alpha = 1.0
                        }, completion: { (_) in
    //                        let image = UIImage(view: self.view)
    //                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                        })
                    }
                }
            })
        } else {
            GMoviesAPIManager.downloadPoster(urlString: blockObject!.landscapePosterUrl!, completion: { (error, image) in
                if error == nil {
                    self.blockObject?.landscapePoster = image
                    DispatchQueue.main.async {
                        self.moviePoster.alpha = 0.0
                        self.moviePoster.image = image
                        UIView.animate(withDuration: 0.300, animations: {
                            self.moviePoster.alpha = 1.0
                        }, completion: { (_) in
                            //                        let image = UIImage(view: self.view)
                            //                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                        })
                    }
                } else {
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.300, animations: {
                            self.moviePoster.alpha = 1.0
                        }, completion: { (_) in
                            //                        let image = UIImage(view: self.view)
                            //                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                        })
                    }
                }
            })
        }
    }
    
    private func downloadMiniPoster() {
        if blockObject == nil {
            GMoviesAPIManager.downloadPoster(urlString: movieObject!.posterUrl!, completion: { (error, image) in
                if error == nil {
                    self.movieObject?.poster = image
                    DispatchQueue.main.async {
                        self.miniPoster.alpha = 0.0
                        self.miniPoster.image = image
                        UIView.animate(withDuration: 0.300, animations: {
                            self.miniPoster.alpha = 1.0
                        })
                    }
                }
            })
        } else {
            GMoviesAPIManager.downloadPoster(urlString: blockObject!.posterUrl!, completion: { (error, image) in
                if error == nil {
                    self.blockObject?.poster = image
                    DispatchQueue.main.async {
                        self.miniPoster.alpha = 0.0
                        self.miniPoster.image = image
                        UIView.animate(withDuration: 0.300, animations: {
                            self.miniPoster.alpha = 1.0
                        })
                    }
                }
            })
        }
    }
    
    private func getFullMovieDetails() {
        GMoviesAPIManager.getMovieDetail(movieId: movieObject!.movieId!) { (error, result) in
            if error == nil {
                let status : Int? = result?.value(forKeyPath: "status") as? Int
                
                if status == 1 {
                    //print(result)
                    self.movieObject?.advisoryRating = result?.value(forKeyPath: "data.advisory_rating") as? String
                    self.movieObject?.youtube = result?.value(forKeyPath: "data.trailer") as? String
//                    self.movieObject?.youtube = "ArYTqj1zRBo"
                    self.movieObject?.genre = result?.value(forKeyPath: "data.genre") as? String
                    self.movieObject?.synopsis = result?.value(forKeyPath: "data.synopsis") as? String
                    //print(result?.value(forKeyPath: "data.synopsis") as? String)
                    self.movieObject?.rottenRating = result?.value(forKeyPath: "data.ratings.critics") as? Int
                    self.movieObject?.audienceRating = result?.value(forKeyPath: "data.ratings.audience") as? Int
                    self.movieObject?.cast = result?.value(forKeyPath: "data.cast") as? [String]
                    self.movieObject?.runtime = Float((result?.value(forKeyPath: "data.runtime_mins") as? String)!)
                    self.movieObject?.theaters = result?.value(forKeyPath: "data.theaters") as? [String]
                    if result?.value(forKeyPath: "data.average_rating") != nil && (result?.value(forKeyPath: "data.average_rating") as AnyObject).isKind(of: NSNull.self) == false {
                        self.movieObject?.averageRating = result?.value(forKeyPath: "data.average_rating") as! Int
                    }
                    
                    if result?.value(forKeyPath: "data.your_rating") != nil && (result?.value(forKeyPath: "data.your_rating") as AnyObject).isKind(of: NSNull.self) == false {
                        self.movieObject?.yourRating = result?.value(forKeyPath: "data.your_rating") as! Int
                    }
                } else {
                    
                }
                
                DispatchQueue.main.async {
                    self.advisory.text = self.movieObject?.advisoryRatingString!
                    self.genre.text = self.movieObject?.genre == nil ? "" : self.movieObject?.genre!
                    self.synopsisLabel.text = self.movieObject?.synopsisString!
                    self.cast.text = self.movieObject?.castList!
                    self.movieLength.text = self.movieObject?.length!
                    self.rottenRating.setTitle("\(self.movieObject!.rottenRatingPercent!)%", for: .normal)
                    self.audienceRating.setTitle("\(self.movieObject!.audienceRatingPercent!)%", for: .normal)
                    self.audienceStarRating.rating = Double(self.movieObject?.averageRating ?? 0)
                    self.yourStarRating.rating = Double(self.movieObject!.yourRating)
                    
                    if self.movieObject?.youtube != nil {
                        if (self.movieObject?.youtube?.characters.count)! > 0 {
                            self.trailerView.load(withVideoId: self.movieObject!.youtube!)
                            self.trailerView.delegate = self
                            self.trailerView.isHidden = false
                        }
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func readMoreFunction(_ sender: AnyObject) {
        
        let synopsis = blockObject == nil ? movieObject?.synopsis : blockObject?.movie?.synopsis
        
        var synopsisSize = NSString(string : synopsis!).boundingRect(with: CGSize(width: synopsisLabel.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName : synopsisLabel.font!], context: nil).size
        synopsisSize.height += 20.0
        
        if synopsisSize.height > synopsisLabel.frame.size.height {
            synopsisBottom.constant = synopsisLabel.frame.size.height-synopsisSize.height
            synopsisHeight.constant += abs(synopsisLabel.frame.size.height-synopsisSize.height)
            contentViewHeight.constant += abs(synopsisBottom.constant)
            
            readMoreButton.isHidden = true
            
            UIView.animate(withDuration: 0.300, delay: 0.0, options: .curveEaseOut, animations: { 
                self.scrollView.layoutIfNeeded()
                }, completion: { (_) in
                    print("contentsize", self.scrollView.contentSize, self.contentView.frame.size,self.synopsisView.frame.size)
                    self.scrollView.contentSize = self.contentView.frame.size
                    print("contentsize", self.scrollView.contentSize, self.contentView.frame.size, self.contentView.frame.size)
            })
        }
    }
    
    @IBAction func buyTicketsAction(_ sender: AnyObject) {
        if movieObject?.hasSchedule == false {
            watchlistAction(sender)
        } else {
            AnalyticsHelper.sendButtonEvent(name: "movie_detail_buy_button")
            if theaterObject != nil || blockObject != nil {
                var storyboard = UIStoryboard(name: "Booking", bundle: nil)
                let nav : ProgressNavigationViewController = storyboard.instantiateInitialViewController() as! ProgressNavigationViewController
                let seatSelection : SeatSelectionViewController = storyboard.instantiateViewController(withIdentifier: "SelectSeatsView") as! SeatSelectionViewController
                seatSelection.movieObject = movieObject
                seatSelection.theaterObject = theaterObject
                seatSelection.tabImageOn = UIImage(named: "schedule-active")
                seatSelection.tabImageOff = UIImage(named: "schedule")
                seatSelection.blockObject = blockObject
                
                let payment : PaymentViewController = storyboard.instantiateViewController(withIdentifier: "PaymentView") as! PaymentViewController
                payment.movieObject = movieObject
                payment.theaterObject = theaterObject
                payment.tabImageOn = UIImage(named: "payment-active")
                payment.tabImageOff = UIImage(named: "payment")
                
                storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
                let ticket : TicketViewController = storyboard.instantiateViewController(withIdentifier: "TicketView") as! TicketViewController
                
                ticket.tabImageOn = UIImage(named: "ticket-active")
                ticket.tabImageOff = UIImage(named: "ticket")
                
                nav.viewControllers = [seatSelection, payment, ticket];
                
                self.navigationController?.pushViewController(nav, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
                let theaterSelection : TheaterListViewController = storyboard.instantiateViewController(withIdentifier: "TheaterListView") as! TheaterListViewController
                theaterSelection.movieObject = movieObject
                
                self.navigationController?.pushViewController(theaterSelection, animated: true)
            }
        }
    }
    
    @IBAction func watchlistAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "movie_detail_watchlist_button")
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if UserDefaults.standard.bool(forKey: "loggedin") == true {
            if movieObject!.watched == true {
                let alert = UIAlertController(title: "", message: "You’re about to remove this movie from your watchlist", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
                    if sender.isKind(of: MButton.self) == false {
                        return
                    }
                    (sender as! MButton).isOn = true
                })
                let removeAction = UIAlertAction(title: "Remove", style: .default, handler: { (_) in
                    (sender as! UIButton).loadingIndicator(show: true)
                    appDelegate.removeMovieFromWatchList(mo: self.movieObject!, completion: { (success) -> Void? in
                        DispatchQueue.main.async {
                            (sender as! UIButton).loadingIndicator(show: false)
                            
                            if success == false {
                                return
                            }
                            
                            if self.movieObject?.hasSchedule == false {
                                self.buyTicketsButton.backgroundColor = UIColor(netHex: 0x0c7fc8)
                                self.watchButtonWidth.constant = 0.0
                                self.buyTicketsButton.setTitle("NOTIFY ME", for: .normal)
                                self.buyTicketsButton.setImage(nil, for: .normal)
                                self.buyTicketsButton.setTitleColor(UIColor.white, for: .normal)
                                self.buyTicketsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                                self.buyTicketsButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                            } else {
                                sender.setImage(UIImage(named: "watchlist-add"), for: .normal)
                            }
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MovieModelUpdateRemove"), object: self.movieObject!.movieId!)
                        }
                    })
                })
                alert.addAction(cancelAction)
                alert.addAction(removeAction)
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                (sender as! UIButton).loadingIndicator(show: true)
                appDelegate.addMovieToWatchList(mo: movieObject!, completion: { (success) -> Void? in
                    DispatchQueue.main.async {
                        (sender as! UIButton).loadingIndicator(show: false)
                        
                        if success == false {
                            return
                        }
                        
                        if self.movieObject?.hasSchedule == false {
                            self.buyTicketsButton.backgroundColor = UIColor(netHex: 0x0c7fc8)
                            self.watchButtonWidth.constant = 0.0
                            self.buyTicketsButton.setTitle("WATCHING", for: .normal)
                            self.buyTicketsButton.setImage(UIImage(named: "added"), for: .normal)
                            self.buyTicketsButton.setTitleColor(UIColor(netHex: 0x4aa8e3), for: .normal)
                            self.buyTicketsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
                            self.buyTicketsButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
                        } else {
                            sender.setImage(UIImage(named: "added"), for: .normal)
                        }
                        let popup = PopupViewController(nibName: "PopupViewController", bundle: nil, expandedHeight: 140.0, expandedWidth: 140.0)
                        let confirmView = Bundle.main.loadNibNamed("ConfirmationView", owner: self, options: nil)?.first as! ConfirmationView
                        confirmView.translatesAutoresizingMaskIntoConstraints = false
                        confirmView.confirmImage.image = UIImage(named: "added-watchlist")
                        confirmView.confirmMessage.text = "Saved to Watchlist!"
                        
                        popup.delegate = self
                        
                        popup.showView(view: confirmView, offset: CGPoint.zero)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MovieModelUpdateAdd"), object: self.movieObject!.movieId!)
                    }
                })
            }
        } else {
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
            let theaterSelection : TheaterListViewController = storyboard.instantiateViewController(withIdentifier: "TheaterListView") as! TheaterListViewController
            theaterSelection.movieObject = movieObject
            
            appDelegate.showLoginModal(previousViewController: self, nextViewController: self)
        }
    }
    
    @IBAction func shareAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "movie_detail_share_button")
        let pop : PopDropViewController = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 132.0)
        let shareView : ShareViewController = ShareViewController(nibName: "ShareViewController", bundle: nil)
        shareView.mo = movieObject
        shareView.parentController = self
        
        pop.showViewControllerFromView(viewController: shareView, originView: sender as! UIButton, offset: CGPoint(x: 0.0, y: UIScreen.main.bounds.size.height - sender.frame.size.height))
        
    }
    
    //MARK: YTPlayer Delegate
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
//        playerView.isHidden = false
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
//        print(state)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MovieDetailViewController: PopupViewControllerDelegate
{
    func onHidden(popup: PopupViewController) {
        if AccountHelper.shared.isRegisteredInRush {
            
            UserEngagementHelper.shared.earnBadgeFrom(engagement: "add-watch-list")
        }
    }
}
