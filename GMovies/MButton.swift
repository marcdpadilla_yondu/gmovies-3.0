//
//  MButton.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 25/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    var radioMode : Bool = false
    var onImage : UIImage?
    var offImage : UIImage?
    var onBgColor : UIColor?
    var offBgColor : UIColor?
    var onTextColor : UIColor?
    var offTextColor : UIColor?
    
    var payload : AnyObject?
    
    private var _isOn = false
    private var originalAlpha : CGFloat = 1.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        originalAlpha = self.alpha
    }
    
    var isOn : Bool {
        get {
            return _isOn
        }
        
        set (v) {
            _isOn = v
            if _isOn {
                setTitleColor(onTextColor, for: UIControlState())
                setImage(onImage, for: UIControlState())
                backgroundColor = onBgColor
            } else {
                setTitleColor(offTextColor, for: UIControlState())
                setImage(offImage, for: UIControlState())
                backgroundColor = offBgColor
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.alpha = 0.9
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.alpha = originalAlpha
        
        if touches.count >= 1 {
            let touch : UITouch = touches.first! as UITouch
            if self.frame.contains(touch.location(in: self.superview)) == false || touch.tapCount > 1{
                if radioMode == true {
                    isOn = !_isOn
                }
                super.touchesEnded(touches, with: event)
                return
            }
        }
        
        if radioMode == true {
            isOn = !_isOn
        }
        super.touchesEnded(touches, with: event)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        self.alpha = originalAlpha
    }
}
