//
//  ProfileViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 15/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import MessageUI

class ProfileViewController: UITableViewController {

    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var editProfileButton: UIButton!
    
    @IBOutlet weak var mobileLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var profileImageWidth: NSLayoutConstraint!
    
    private var mobileHeightConstant : CGFloat = 0.0
    
    private var menuItems : [NSDictionary] = [["name" : "My Reviews", "type" : "main"],["name" : "Messages", "type" : "main"],["name" : "About GMovies", "type" : "sub"],["name" : "Rate Our App", "type" : "sub"],["name" : "Contact Us", "type" : "sub"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        mobileHeightConstant = mobileLabelHeight.constant
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        parent?.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if UserDefaults.standard.bool(forKey: "loggedin") == false && menuItems.count > 2 {
            menuItems.removeSubrange(Range(uncheckedBounds: (0,2)))
            name.text = "Hello, Guest"
            mobile.text = ""
            editProfileButton.setTitle("Log In or Sign Up", for: .normal)
            editProfileButton.titleLabel?.adjustsFontSizeToFitWidth = true
            mobileLabelHeight.constant = 0.0
            
        } else if UserDefaults.standard.bool(forKey: "loggedin") == true {
            let user : NSDictionary? = UserDefaults.standard.value(forKey: "user") as? NSDictionary
            if user != nil {
                name.text = "\(user!.value(forKey: "first_name") as! String) \(user!.value(forKey: "last_name") as! String)"
                mobile.text = (user!.value(forKey: "mobile") as! String)
                mobileLabelHeight.constant = mobileHeightConstant
                editProfileButton.setTitle("Edit Profile", for: .normal)
                
                var base64Photo = user!.value(forKeyPath: "photo.mobile") as? String
                if base64Photo == nil {
                    base64Photo = ""
                }
                
                if FBSDKAccessToken.current() != nil && base64Photo!.characters.count == 0 {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    let req : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "picture.type(large)"])
                    req.start { (conn, result, error) in
                        if error == nil {
                            let url = (result as! NSDictionary).value(forKeyPath: "picture.data.url") as! String
                            DispatchQueue.main.async {
                                GMoviesAPIManager.downloadPoster(urlString: url, completion: { (error, image) in
                                    if error == nil {
                                        if image != nil {
                                            let imgData = UIImageJPEGRepresentation(image!, 1.0)
                                            let newUser : NSMutableDictionary = user!.mutableCopy() as! NSMutableDictionary
                                            let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                            
                                            photo.setValue(imgData!.base64EncodedString(), forKey: "mobile")
                                            newUser.setValue(photo, forKeyPath: "photo")
                                            UserDefaults.standard.setValue(newUser, forKey: "user")
                                            UserDefaults.standard.set(newUser, forKey: "user")
                                            UserDefaults.standard.synchronize()
                                            DispatchQueue.main.async {
                                                self.profileImage.image = image
                                                self.profileImage.clipsToBounds = true
                                            }
                                        } else {
                                            DispatchQueue.main.async {
                                                self.profileImage.image = UIImage(named: "guest-profile")
                                                self.profileImage.clipsToBounds = true
                                            }
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            self.profileImage.image = UIImage(named: "guest-profile")
                                            self.profileImage.clipsToBounds = true
                                        }
                                        print(error!.localizedDescription)
                                    }
                                })
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.profileImage.image = UIImage(named: "guest-profile")
                                self.profileImage.clipsToBounds = true
                            }
                            print(error!.localizedDescription)
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                } else if base64Photo!.characters.count != 0 {
                    let imgDataString = user!.value(forKeyPath: "photo.mobile") as! String
                    let imgData = NSData(base64Encoded: imgDataString, options: .ignoreUnknownCharacters)
                    DispatchQueue.main.async {
                        self.profileImage.image = UIImage(data: imgData as! Data)
                        self.profileImage.clipsToBounds = true
                    }
                } else if base64Photo!.characters.count != 0 {
                    GMoviesAPIManager.downloadPoster(urlString: user!.value(forKeyPath: "photo.website") as! String, completion: { (error, image) in
                        
                        if error == nil {
                            if image != nil {
                                let imgData = UIImageJPEGRepresentation(image!, 1.0)
                                let newUser : NSMutableDictionary = user?.mutableCopy() as! NSMutableDictionary
                                let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                photo.setValue(imgData?.base64EncodedString(), forKey: "mobile")
                                newUser.setValue(photo, forKeyPath: "photo")
                                UserDefaults.standard.setValue(newUser, forKey: "user")
                                UserDefaults.standard.synchronize()
                                DispatchQueue.main.async {
                                    self.profileImage.image = image
                                    self.profileImage.clipsToBounds = true
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.profileImage.image = UIImage(named: "guest-profile")
                                    self.profileImage.clipsToBounds = true
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.profileImage.image = UIImage(named: "guest-profile")
                                self.profileImage.clipsToBounds = true
                            }
                            print(error!.localizedDescription)
                        }
                        
                    })
                } else {
                    DispatchQueue.main.async {
                        self.profileImage.image = UIImage(named: "guest-profile")
                        self.profileImage.clipsToBounds = true
                    }

                }
            }
            
            if menuItems.count < 4 {
                menuItems.insert(["name" : "My Reviews", "type" : "main"], at: 0)
                menuItems.insert(["name" : "Messages", "type" : "main"], at: 1)
            }
        }
        
        headerView.layoutIfNeeded()
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "profile")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.MyProfile)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menuItems.count
    }
    
    //MARK: Private functions
    
    @IBAction func editProfileAction(_ sender: AnyObject) {
        
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            AnalyticsHelper.sendButtonEvent(name: "profile_login")
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showLoginModal()
        } else {
            AnalyticsHelper.sendButtonEvent(name: "profile_edit")
            performSegue(withIdentifier: "showEditProfile", sender: self)
        }
    }
    
    private func configureView() {
        UIManager.roundify(editProfileButton)
        UIManager.buttonBorder(button: editProfileButton, width: 0.25, color: UIColor(netHex: 0xbac2da))
        profileImage.layer.cornerRadius = profileImageWidth.constant/2.0
        profileImage.backgroundColor = UIColor.gray
    }
    
    private func configureCell(cell : UITableViewCell, indexPath : IndexPath) {
        
        let item : NSDictionary = menuItems[indexPath.row] as NSDictionary
        
        if indexPath.row >= 2 || UserDefaults.standard.bool(forKey: "loggedin") == false {
            cell.textLabel?.textColor = UIColor(netHex: 0xbac2da)
        } else {
            cell.textLabel?.textColor = UIColor.black
        }
        
        cell.textLabel?.text = (item.value(forKey: "name") as? String)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as UITableViewCell?
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "reuseIdentifier")
        }

        configureCell(cell: cell!, indexPath: indexPath)

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)
        
        print(cell!.textLabel!.text!)
        
        if cell!.textLabel!.text! == "My Reviews" {
            AnalyticsHelper.sendButtonEvent(name: "profile_reviews")
            performSegue(withIdentifier: "showMyReviews", sender: self)
        } else if cell!.textLabel!.text! == "Messages" {
            AnalyticsHelper.sendButtonEvent(name: "profile_messages")
            performSegue(withIdentifier: "showMessages", sender: self)
        } else if cell!.textLabel!.text! == "Contact Us" {
            AnalyticsHelper.sendButtonEvent(name: "profile_contact_us")
            performSegue(withIdentifier: "showContactForm", sender: self)
        } else if cell!.textLabel!.text! == "About GMovies" {
            AnalyticsHelper.sendButtonEvent(name: "profile_about_us")
            performSegue(withIdentifier: "showAboutView", sender: self)
        } else if cell!.textLabel!.text! == "Rate Our App" {
            AnalyticsHelper.sendButtonEvent(name: "profile_rate_our_app")
            UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/id582901861")!)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
