//
//  BannerView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 31/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class BannerView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    @IBOutlet weak var messageLabel: UILabel!
    
    func show() {
        UIView.animate(withDuration: 0.25, animations: {
            var frame : CGRect = self.frame
            frame.origin.y = 0.0
            self.frame = frame
        }) 
    }
    
    func show(constraint : NSLayoutConstraint) {
        constraint.constant = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.superview?.layoutIfNeeded()
        })
    }
    
    func hide(animated:Bool = true) {
        
        if animated {
            
            UIView.animate(withDuration: 0.25, animations: {
                var frame : CGRect = self.frame
                frame.origin.y = -frame.size.height
                self.frame = frame
            })
        }else {
            var frame : CGRect = self.frame
            frame.origin.y = -frame.size.height
            self.frame = frame
        }
    }
    
    func hide(constraint : NSLayoutConstraint, animated:Bool = true) {
        constraint.constant = -frame.size.height
        
        if animated {
            UIView.animate(withDuration: 0.25, animations: {
                self.superview?.layoutIfNeeded()
            })
        }else {
            self.superview?.layoutIfNeeded()
        }
    }
}
