//
//  CalendarViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit


class CalendarViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private lazy var __once: () = {
            let height : CGFloat = 44.0+20.0
            self.topBg = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: height))
            self.topBg!.backgroundColor = UIColor(netHex: 0xf2f4f8)
            
            self.collectionView?.insertSubview(self.topBg!, at: 0)
            
        }()
    
    fileprivate let dateIdentifier = "dateIdentifier"
    fileprivate let dayIdentifier = "dayIdentifier"
    fileprivate let monthIdentifier = "monthIdentifier"
    fileprivate let footerIdentifier = "footerButton"
    
    fileprivate var dayNames : [String]?
    fileprivate var dates : [CalendarItem]?
    private var selectedCalendarItem : CalendarItem?
    
    var selectedIndexPath : IndexPath?
    var token : Int = 0
    var topBg : UIView?
    
    weak var delegate : CalendarViewProtocol?
    
    var allowedDates : [Date]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UINib(nibName: "CalendarViewCell", bundle: nil), forCellWithReuseIdentifier: dateIdentifier)
        self.collectionView!.register(UINib(nibName: "CalendarDayViewCell", bundle: nil), forCellWithReuseIdentifier: dayIdentifier)
        self.collectionView!.register(UINib(nibName: "CalendarMonthReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: monthIdentifier)
        self.collectionView!.register(UINib(nibName: "CollectionButtonView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerIdentifier)
        // Do any additional setup after loading the view.
        
        dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        dates = []
        
        
        var calendar : Calendar = Calendar.current
        calendar.locale = NSLocale.current
        
        // today = first available date
        let today : Date = allowedDates?.sorted(by: { (d1, d2) -> Bool in
            return calendar.compare(d1, to: d2, toGranularity: .day) == ComparisonResult.orderedAscending
        }).first ?? Date()
        
        let comp : DateComponents = (calendar as NSCalendar).components([.weekday, .day], from: today)
        
        var firstAvailableDateSelected = false
        for i in -(comp.weekday!-1) ..< allowedDates!.count {
            let date : Date = (calendar as NSCalendar).date(byAdding: .day, value: i, to: today, options: [])!
            
            let ci : CalendarItem = CalendarItem(dateObject: date)
            if i == 0 {
                ci.current = false
                ci.selected = false
                ci.enabled = false
                ci.dateId = UIManager.generateDateId(date: date)
                selectedIndexPath = IndexPath(row: (comp.weekday!-1)+dayNames!.count, section: 0)
            } else if i < 0 {
                ci.enabled = false
            }
            
            if allowedDates?.contains(where: { (d) -> Bool in
                
                return calendar.compare(date, to: d, toGranularity: Calendar.Component.day) == ComparisonResult.orderedSame
            }) == true {
                if firstAvailableDateSelected == false {
                    firstAvailableDateSelected = true
                    ci.selected = true
                    selectedCalendarItem = ci
                }
                ci.enabled = true
            } else {
                ci.enabled = false
            }
            
            dates?.append(ci)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        _ = self.__once
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    //MARK: fix year diff
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning fix year difference
        let calendar : Calendar = Calendar.current
        let fcomp : DateComponents = (calendar as NSCalendar).components([.month, .year], from: (dates!.first!.date)! as Date)
        let tcomp : DateComponents = (calendar as NSCalendar).components([.month, .year], from: (dates!.last!.date)! as Date)
        
        let yDiff = tcomp.year! - fcomp.year!
        
        let sections : Int = (tcomp.month! + (yDiff * 12)) - fcomp.month!
        
        if fcomp.month! == tcomp.month! - 1 {
            return 1
        }
        
        return sections + 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let calendar : Calendar = Calendar.current
        let fcomp : DateComponents = (calendar as NSCalendar).components([.month, .year], from: (dates!.first!.date)! as Date)
        
        var items : [CalendarItem] = (dates?.filter({ (ci : CalendarItem) -> Bool in
            let comp: DateComponents = (calendar as NSCalendar).components([.month, .year], from: ci.date! as Date)
            if fcomp.month! == comp.month! - 1 {
                return true
            }
            return fcomp.month!+section == comp.month!
        }))!
        
        //items.removeAll()
        var itemsCount : Int = items.count
        
        if section > 0 {
            
            if items.count > 0 {
                let ci : CalendarItem = items.first!
                let comp : DateComponents = (calendar as NSCalendar).components([.weekday], from: ci.date! as Date)
                
                itemsCount += (comp.weekday!-1)
            } else {
                let comp : DateComponents = (calendar as NSCalendar).components([.weekday], from: Date())
                
                itemsCount += (comp.weekday!-1)
            }
        } else {
            itemsCount += dayNames!.count
        }
        
        return itemsCount
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : UICollectionViewCell?
        
        if (indexPath as NSIndexPath).section == 0 {
            if (indexPath as NSIndexPath).row < 7 {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: dayIdentifier, for: indexPath)
                configureDayCell(cell as! CalendarDayViewCell, indexPath: indexPath)
            } else {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: dateIdentifier, for: indexPath)
                configureDateCell(cell as? CalendarViewCell, indexPath: indexPath)
            }
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: dateIdentifier, for: indexPath)
            configureDateCell(cell as? CalendarViewCell, indexPath: indexPath)
        }
    
        // Configure the cell
    
        return cell!
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath as NSIndexPath).row < 7 && (indexPath as NSIndexPath).section == 0 {
            return
        }
        
        var ci : CalendarItem?
        let selectedIndex = (selectedIndexPath! as NSIndexPath).row
        //print((selectedIndexPath! as NSIndexPath).section)
        if (selectedIndexPath! as NSIndexPath).section == 0 {
        
            if (indexPath as NSIndexPath).section == 0 {
                ci = dates![(indexPath as NSIndexPath).row-7]
                if ci!.enabled == true {
                    ci = dates![selectedIndex-dayNames!.count]
                    ci!.selected = false
                    configureDateCell(collectionView.cellForItem(at: selectedIndexPath!) as? CalendarViewCell, indexPath: selectedIndexPath!)
                    
                    ci = dates![(indexPath as NSIndexPath).row-7]
                    ci!.selected = true
                    selectedCalendarItem = ci
                } else {
                    return
                }
            } else {
                let firstIndex : Int = (collectionView.numberOfItems(inSection: (indexPath as NSIndexPath).section-1))-(dayNames?.count)!
                ci = dates![firstIndex]
                let calendar : Calendar = Calendar.current
                let comp : DateComponents = (calendar as NSCalendar).components([.weekday], from: ci!.date! as Date)
                
                    let index = (indexPath as NSIndexPath).row + (firstIndex-(comp.weekday!-1))
                    ci = dates![index]
                    if ci!.enabled == true {
                        ci = dates![selectedIndex-dayNames!.count]
                        ci!.selected = false
                        
                        configureDateCell(collectionView.cellForItem(at: selectedIndexPath!) as? CalendarViewCell, indexPath: selectedIndexPath!)
                        ci = dates![index]
                        ci!.selected = true
                        selectedCalendarItem = ci
                    } else {
                        return
                    }
            }
            
        } else {
            if (indexPath as NSIndexPath).section == 0 {
                ci = dates![(indexPath as NSIndexPath).row-7]
                if ci!.enabled == true {
                    let firstIndex : Int = (collectionView.numberOfItems(inSection: (selectedIndexPath! as NSIndexPath).section-1))-(dayNames?.count)!
                    ci = dates![firstIndex]
                    let calendar : Calendar = Calendar.current
                    let comp : DateComponents = (calendar as NSCalendar).components([.weekday], from: ci!.date! as Date)
                    
                    let index = selectedIndex + (firstIndex-(comp.weekday!-1))
                    
                    ci = dates![index]
                    ci!.selected = false
                    //print(selectedIndexPath)
                    configureDateCell(collectionView.cellForItem(at: selectedIndexPath!) as? CalendarViewCell, indexPath: selectedIndexPath!)
                    
                    ci = dates![(indexPath as NSIndexPath).row-7]
                    ci!.selected = true
                    selectedCalendarItem = ci
                } else {
                    return
                }
            } else {
                let firstIndex : Int = (collectionView.numberOfItems(inSection: (indexPath as NSIndexPath).section-1))-(dayNames?.count)!
                ci = dates![firstIndex]
                let calendar : Calendar = Calendar.current
                let comp : DateComponents = (calendar as NSCalendar).components([.weekday], from: ci!.date! as Date)
                
                let index = (indexPath as NSIndexPath).row + (firstIndex-(comp.weekday!-1))
                ci = dates![index]
                if ci!.enabled == true {
                    ci = dates![selectedIndex + (firstIndex-(comp.weekday!-1))]
                    ci!.selected = false
                    
                    configureDateCell(collectionView.cellForItem(at: selectedIndexPath!) as? CalendarViewCell, indexPath: selectedIndexPath!)
                    ci = dates![index]
                    ci!.selected = true
                    selectedCalendarItem = ci
                } else {
                    return
                }
            }
        }
        
        //print(ci!.date)
        
        configureDateCell(collectionView.cellForItem(at: indexPath) as? CalendarViewCell, indexPath: indexPath)
        selectedIndexPath = indexPath
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var header : UICollectionReusableView? = nil
        
        if kind == UICollectionElementKindSectionHeader {
            header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: monthIdentifier, for: indexPath) as? CalendarMonthReusableView
            
            configureMonthHeader(header as! CalendarMonthReusableView, indexPath: indexPath)
        } else if kind == UICollectionElementKindSectionFooter {
            header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerIdentifier, for: indexPath) as! CollectionButtonView
            (header as! CollectionButtonView).okayButton.addTarget(self, action: #selector(CalendarViewController.okayButtonAction), for: .touchUpInside)
        }
        
        return header!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if section < collectionView.numberOfSections - 1 {
            return CGSize.zero
        }
        
        return CGSize(width: collectionView.frame.size.width, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width : CGFloat = (self.view.frame.size.width)/7.3
        
        let calendar : Calendar = Calendar.current
        let fcomp : DateComponents = (calendar as NSCalendar).components([.month, .year], from: (dates!.first!.date)! as Date)
        
        let items : [CalendarItem] = (dates?.filter({ (ci : CalendarItem) -> Bool in
            let comp: DateComponents = (calendar as NSCalendar).components([.month, .year], from: ci.date! as Date)
            return fcomp.month!+(indexPath as NSIndexPath).section == comp.month!
        }))!
        
        var height : CGFloat = (self.view.frame.size.height-28.0-(44.0*CGFloat(collectionView.numberOfSections)))/ceil(CGFloat(items.count)/7.3)
        
        height = width
        if (indexPath as NSIndexPath).row < 7 && (indexPath as NSIndexPath).section == 0 {
            height = 20.0
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width : CGFloat = self.view.frame.size.width
        let height : CGFloat = 44.0
        
        return CGSize(width: width, height: height)
    }
    
    //MARK: Private methods
    
    fileprivate func configureDayCell(_ cell: CalendarDayViewCell, indexPath: IndexPath) {
        cell.dayLetter.text = dayNames![(indexPath as NSIndexPath).row]
    }
    
    fileprivate func configureDateCell(_ cell: CalendarViewCell?, indexPath: IndexPath) {
        
        if cell == nil {
            return
        }
        
        var ci : CalendarItem?
        if (indexPath as NSIndexPath).section == 0 {
            ci = dates![(indexPath as NSIndexPath).row-7]
            cell!.dayNumber.text = ci!.dateNumber()
        } else {
            let firstIndex : Int = (collectionView?.numberOfItems(inSection: (indexPath as NSIndexPath).section-1))!-(dayNames?.count)!
            ci = dates![firstIndex]
            let calendar : Calendar = Calendar.current
            let comp : DateComponents = (calendar as NSCalendar).components([.weekday], from: ci!.date! as Date)
            
            if ((indexPath as NSIndexPath).row < (comp.weekday!-1)) {
                cell!.dayNumber.text = ""
                return
            } else {
                let index = (indexPath as NSIndexPath).row + (firstIndex-(comp.weekday!-1))
                ci = dates![index]
                cell!.dayNumber.text = ci?.dateNumber()
            }
        }
        
        if ci!.current == true {
            if ci!.selected == false {
                cell!.dayNumber.layer.borderColor = UIManager.buttonRed.cgColor
                cell!.dayNumber.layer.borderWidth = 0.25
                cell!.dayNumber.textColor = UIColor.black
                cell!.dayNumber.backgroundColor = UIColor.white
            } else {
                cell!.dayNumber.backgroundColor = UIManager.buttonRed
                cell!.dayNumber.textColor = UIColor.white
            }
        } else if ci!.selected == true {
            cell!.dayNumber.backgroundColor = UIManager.buttonRed
            cell!.dayNumber.textColor = UIColor.white
            cell!.dayNumber.layer.borderWidth = 0.0
        } else {
            cell!.dayNumber.backgroundColor = UIColor.white
            cell!.dayNumber.layer.borderWidth = 0.0
            if ci!.enabled == false {
                cell!.dayNumber.textColor = UIColor(netHex: 0xf2f4f8)
            } else {
                cell!.dayNumber.textColor = UIColor.black
            }
        }
    }
    
    fileprivate func configureMonthHeader(_ header : CalendarMonthReusableView, indexPath: IndexPath) {
        let calendar = Calendar.current
        let fcomp : DateComponents = (calendar as NSCalendar).components([.month, .year], from: (dates!.last!.date)! as Date)
        let items : [CalendarItem] = (dates?.filter({ (ci : CalendarItem) -> Bool in
            let comp: DateComponents = (calendar as NSCalendar).components([.month, .year], from: ci.date! as Date)
            
            if fcomp.month! == comp.month! - 1 {
                return true
            }
            
            return fcomp.month!+(indexPath as NSIndexPath).section == comp.month!
        }))!
        
        let ci : CalendarItem = items.last!
        header.backgroundColor = UIColor(netHex: 0xf2f4f8)
        
        header.monthName.text = "\(ci.monthName()) \(ci.yearNumber())"
    }
    
    @objc private func okayButtonAction() {
        
        delegate?.didFinishedSelectingDate(self, selectedDate: selectedCalendarItem!.date!)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
}

protocol CalendarViewProtocol : class {
    func didSelectDate(_ sender : CalendarViewController, selectedDate d : Date)
    func didDeselectDate(_ sender : CalendarViewController, deselectedDate d : Date)
    func didFinishedSelectingDate(_ sender : CalendarViewController, selectedDate d : Date)
}
