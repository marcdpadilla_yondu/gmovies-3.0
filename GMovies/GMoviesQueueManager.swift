//
//  GMoviesQueueManager.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 22/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
//import ReachabilitySwift

class GMoviesQueueManager: NSObject {
    private var queue : OperationQueue?
    private var imageQueue : OperationQueue?
    static let sharedInstance = GMoviesQueueManager()
    
    override init() {
        super.init()
        queue = OperationQueue()
//        queue?.maxConcurrentOperationCount = 1
        imageQueue = OperationQueue()
        imageQueue?.maxConcurrentOperationCount = 4
        
//        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
//        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: ReachabilityChangedNotification,object: appDelegate.reachability)
        
    }
    
    func addOperation(op : Operation) {
        self.queue!.addOperation(op)
    }
    
    func addDownloadOperation(op : Operation) {
        self.imageQueue?.addOperation(op)
    }
    
    func getQueue() -> OperationQueue {
        return queue!
    }
    
    func getImageQueue() -> OperationQueue {
        return imageQueue!
    }
    
//    func reachabilityChanged(note: NSNotification) {
//        
//        let reachability = note.object as! Reachability
//        
//        if reachability.isReachable {
//            queue?.isSuspended = false
//            imageQueue?.isSuspended = false
//            print("=====\n\nNETWORK OPERATIONS RESUMED\n\n=====")
//        } else {
//            queue?.isSuspended = true
//            imageQueue?.isSuspended = true
//            print("=====\n\nNETWORK OPERATIONS PAUSED\n\n=====")
//        }
//    }
}
