//
//  SeatCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 12/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class SeatCell: UICollectionViewCell {
    
    @IBOutlet weak var seatName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 3.0
    }
}
