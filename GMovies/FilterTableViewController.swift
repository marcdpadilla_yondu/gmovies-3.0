//
//  FilterTableViewController.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 30/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

protocol FilterTableViewControllerDelegate {
    func onFilterSelected(filter: String)
}

class FilterTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: FilterTableViewControllerDelegate?
    
    fileprivate var _data:[String] = []
    var data:[String] {
        get { return _data }
        set {
            _data = newValue
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        self.tableView.register(UINib(nibName:"FilterTableCell", bundle: nil), forCellReuseIdentifier: "filterCell")
    }
}

extension FilterTableViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterCell", for: indexPath) as! FilterTableCell
        cell.title.text = data[indexPath.row]
        return cell
    }
}

extension FilterTableViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let popUp : PopDropViewController = self.parent as! PopDropViewController
        
        let filter: String = data[indexPath.row]
        delegate?.onFilterSelected(filter: filter)
        
        popUp.hide()
    }
}
