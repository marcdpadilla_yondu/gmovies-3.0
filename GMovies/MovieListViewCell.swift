//
//  MovieListViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 5/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MovieListViewCell: UICollectionViewCell {
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var watchList: UIButton!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var advisory: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var runtime: UILabel!
    @IBOutlet weak var cast: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        advisory.layer.borderWidth = 0.5
        advisory.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        advisory.layer.cornerRadius = 3.0
    }
}
