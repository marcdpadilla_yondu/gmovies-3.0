//
//  RushAPI.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 24/04/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation

/** Response Codes */
public enum RushAPIResponseCode: String {
    case successful = "0x0"
}

/** Status Codes */
public enum RushAPIStatusCode: Int {
    
    case successful = 200
    case error = 500
    
    var message: String {
        switch self {
        case .successful:
            return "OK"
        case .error:
            return "ERROR"
        }
    }
}

public struct RushAPIResponse
{
    var statusCode: RushAPIStatusCode
    var responseData: Dictionary<String,Any>?
}

class RushAPI
{
    
    public enum BuildType: Int {
        case Development
        case QualityAssurance
        case Production
    }
    
    fileprivate let DEV_BASE_URL = "http://core.dev.digitalventures.ph"
    fileprivate let PROD_BASE_URL = "http://core.digitalventures.ph"
    fileprivate let QA_BASE_URL = "http://core.qa.digitalventures.ph"
    
    fileprivate let REQUEST_TIME_OUT:Float = 30.0
    
    public var buildType:BuildType = .Development
    
    typealias RequestSuccessBlock = (RushAPIResponse) -> Void
    typealias RequestFailureBlock = (String) -> Void
    
    class var shared : RushAPI {
        struct Static {
            static let instance : RushAPI = RushAPI()
        }
        return Static.instance
    }
    
    fileprivate var baseUrl: String {
        
        switch buildType {
        case .Development:
            return DEV_BASE_URL
        case .QualityAssurance:
            return QA_BASE_URL
        case .Production:
            return PROD_BASE_URL
        }
    }
    
    func register(lbid: String, name: String, email: String, mobileNumber: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        let endPoint = "/api/gmovies/rush/register"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        var params = Dictionary<String, String> ()
        
        params.updateValue(name, forKey: "name")
        params.updateValue(email, forKey: "email")
        params.updateValue(mobileNumber, forKey: "mobile_no")
        
        //Create request
        let request = createRequest(url: requestUrl!, httpMethod: "POST", token: lbid, params: params)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    func getUserPoints(lbid: String, uuid: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        let endPoint = "/api/gmovies/rush-rewards/points/\(uuid)"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        
        let request = createRequest(url: requestUrl!, httpMethod: "GET", token: lbid)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    func getEarnedPoints(lbid: String, uuid: String, transactionId: String, amount: Int, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        let endPoint = "/api/gmovies/rush-rewards/points/\(uuid)/earn"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        var params = Dictionary<String, Any> ()
        
        params.updateValue(amount, forKey: "amount")
        params.updateValue(transactionId, forKey: "or_no")
        
        //Create request
        let request = createRequest(url: requestUrl!, httpMethod: "POST", token: lbid, params: params)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    //Badges
    func getUserBadges(lbid: String, rushId: String, page: Int, type: String, badgeGroup: Int, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        let endPoint = "/api/gmovies/badges/\(rushId)/page/\(page)/\(type)/\(badgeGroup)"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        
        let request = createRequest(url: requestUrl!, httpMethod: "GET", token: lbid)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    func getUserBadges(lbid: String, rushId: String, page: Int, type: String = "mix", successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        let endPoint = "/api/gmovies/badges/\(rushId)/page/\(page)/\(type)"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        
        let request = createRequest(url: requestUrl!, httpMethod: "GET", token: lbid)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
    
    func getEarnedPointsFrom(lbid:String, rushId: String, userEngament code: String, successful: @escaping RequestSuccessBlock, failure: @escaping RequestFailureBlock) {
        
        let endPoint = "/api/gmovies/badges/user-engagements/\(rushId)/earn"
        let requestUrl = URL(string: "\(baseUrl)\(endPoint)")
        var params = Dictionary<String, Any> ()
        
        params.updateValue(code, forKey: "code")
        
        //Create request
        let request = createRequest(url: requestUrl!, httpMethod: "POST", token: lbid, params: params)
        
        if request != nil {
            //Process request
            process(urlRequest: request!, successful: { (response) in
                successful(response)
            }, failure: failure)
        }else {
            failure("Invalid url.")
        }
    }
}

extension RushAPI {
    
    fileprivate func process(urlRequest: URLRequest, successful:RequestSuccessBlock?, failure:RequestFailureBlock?) {
        
        DispatchQueue.global(qos: .utility).async {
            //Send request
            let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if error != nil {
                    //failure
                    DispatchQueue.main.async {
                        failure?(error!.localizedDescription)
                    }
                }else {
                    //success
                    do {
                        
                        var statusCode: RushAPIStatusCode = .error
                        
                        if let httpResponse = response as? HTTPURLResponse {
                            
                            statusCode = RushAPIStatusCode(rawValue: httpResponse.statusCode)!
                        }
                        
                        let data: Any = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                        let responseData: Dictionary<String,Any>? = data as? [String: Any]
                        
                        //Create the response struct
                        let responseStruct = RushAPIResponse(statusCode: statusCode,responseData: responseData)
                        DispatchQueue.main.async {
                            successful?(responseStruct)
                        }
                    }catch let jsonError {
                        
                        DispatchQueue.main.async {
                            failure?(jsonError.localizedDescription)
                        }
                    }
                }
            }
            dataTask.resume()
        }
    }
    
    fileprivate func createRequest(url: URL, httpMethod:String) -> URLRequest? {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request as URLRequest
    }
    
    fileprivate func createRequest(url:URL, httpMethod:String, params:[String:Any]) -> URLRequest? {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }catch _ {
            return nil
        }
        
        return request as URLRequest
    }
    
    fileprivate func createRequest(url:URL, httpMethod:String, token:String) -> URLRequest?
    {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("\(token)", forHTTPHeaderField: "LBAPIToken")
        
        return request as URLRequest
    }
    
    fileprivate func createRequest(url:URL, httpMethod:String, token:String, params:[String:Any]) -> URLRequest? {
        
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(REQUEST_TIME_OUT))
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("\(token)", forHTTPHeaderField: "LBAPIToken")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }catch _ {
            return nil
        }
        
        return request as URLRequest
    }
    
    fileprivate func log(message: String) {
        if buildType == .Development || buildType == .QualityAssurance {
            print("Rush API: \(message)")
        }
    }
}
