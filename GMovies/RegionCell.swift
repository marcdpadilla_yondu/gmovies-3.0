//
//  RegionCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class RegionCell: UITableViewCell {
    
    
    @IBOutlet weak var regionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
