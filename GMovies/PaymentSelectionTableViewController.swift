//
//  PaymentSelectionTableViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 2/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PaymentSelectionTableViewController: UITableViewController {
    
    var paymentOptions: [PaymentOptionModel]?
    
    var delegate : PaymentSelectionProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "payment_selection")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (paymentOptions?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)

        configureCell(cell: cell, indexPath: indexPath)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let po : PaymentOptionModel = paymentOptions![indexPath.row]
        
        delegate?.didSelectPaymentOption(po: po)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    private func configureCell(cell : UITableViewCell, indexPath : IndexPath) {
        let po : PaymentOptionModel = paymentOptions![indexPath.row]
        
        cell.textLabel?.text = po.name!
        cell.textLabel?.font = UIFont(name: "OpenSans", size: 12.0)
        
        if po.icon == nil {
            downloadIcon(po: po, indexPath: indexPath)
        } else {
            cell.imageView?.image = po.icon
            cell.imageView?.contentMode = .scaleAspectFit
        }
    }
    
    private func downloadIcon(po: PaymentOptionModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: po.iconUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    po.icon = image
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                }
            } else {
                
            }
        }
    }
}

protocol PaymentSelectionProtocol {
    func didSelectPaymentOption(po : PaymentOptionModel)
}
