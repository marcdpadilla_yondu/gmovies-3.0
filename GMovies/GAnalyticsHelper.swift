//
//  GAnalyticsHelper.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 14/02/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation

public class GAnalyticsHelper {
    
    static let IS_DEVELOPMENT_BUILD = false
    
    //Google Analytics
    static let GATRACKING_ID_DEV = "UA-91859214-1"
    static let GATRACKING_ID_PROD = "UA-49550413-2"
    
    class func sendEvent(category: String, action: String, label: String? = nil, value: NSNumber? = nil) {
        
        let tracker = GAI.sharedInstance().tracker(withTrackingId: trackingId())
        
        let gAParams:GAIDictionaryBuilder = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: value)
        tracker?.send(gAParams.build() as Dictionary)
    }
    
    class func trackingId() -> String {
        if IS_DEVELOPMENT_BUILD {
            return GATRACKING_ID_DEV
        }else {
            return GATRACKING_ID_PROD
        }
    }
}
