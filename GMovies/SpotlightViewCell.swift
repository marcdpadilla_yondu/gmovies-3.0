//
//  SpotlightViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class SpotlightViewCell: UITableViewCell {
    
    @IBOutlet weak var spotlightImage: UIImageView!
    @IBOutlet weak var spotlightTitle: UILabel!
    @IBOutlet weak var gradientView: UIView!
    
    @IBOutlet weak var fbShareButton: UIButton!
    @IBOutlet weak var twitterShareButton: UIButton!
    @IBOutlet weak var emailShareButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 170.0)
        gradient.colors = [UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.85).cgColor, UIColor.clear.cgColor]
        gradientView.layer.sublayers?.removeAll()
        gradientView.layer.insertSublayer(gradient, at: 0)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
