//
//  LoyaltyRegistrationSuccessVC.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 04/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

@objc protocol LoyaltyRegistrationSuccessVCDelegate {
    @objc func didClickDismiss(sender: LoyaltyRegistrationSuccessVC)
}

class LoyaltyRegistrationSuccessVC: UIViewController
{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dismissButton: EZUIButton!
    
    var userName: String?
    var dismissButtonTitle: String?
    
    var delegate: LoyaltyRegistrationSuccessVCDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (userName != nil) {
            self.titleLabel.text = "Welcome \(userName!) to Reel Rewards Program"
        }else {
            self.titleLabel.text = "Welcome to Reel Rewards Program"
        }
        
        dismissButton.setTitle(dismissButtonTitle, for: .normal)
    }

    @IBAction func onDismissClicked(_ sender: Any) {
        delegate?.didClickDismiss(sender: self)
    }
}
