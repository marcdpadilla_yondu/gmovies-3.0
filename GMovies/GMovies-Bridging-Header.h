//
//  GMovies-Bridging-Header.h
//  GMovies
//
//  Created by Marc Darren Padilla on 8/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

#import "Ipay.h"
#import "IpayPayment.h"
#import "YTPlayerView.h"
#import "UIView+MGBadgeView.h"
#import <Google/Analytics.h>
#import "UIScrollView+FloatingButton.h"
//#import "Flurry.h"
