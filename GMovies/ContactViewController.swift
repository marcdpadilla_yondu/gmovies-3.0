//
//  ContactViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 18/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import PopupDialog
import MessageUI

class ContactViewController: UITableViewController, CategoryProtocol {

    @IBOutlet weak var commentBg: UIView!
    @IBOutlet weak var commentField: UITextView!
    @IBOutlet weak var submitButton: MButton!
    @IBOutlet weak var emailBg: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var fullNameBg: UIView!
    @IBOutlet weak var subjectField: UITextField!
    @IBOutlet weak var subjectBg: UIView!
    @IBOutlet weak var categoryBg: UIView!
    @IBOutlet weak var categoryField: UIButton!
    
    @IBOutlet weak var helpButton: MButton!
    
    private var loadingScreen: LoaderView?
    
    private var currentPop :  PopDropViewController?
    private var selectedCategory : NSDictionary = ["id" : "customer_feedback", "name" : "Customer Feedback/Concerns"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        if UserDefaults.standard.bool(forKey: "loggedin") == true {
            let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
            
            fullNameField.text = "\(user.value(forKey: "first_name") as! String) \(user.value(forKey: "last_name") as! String)"
            emailField.text = user.value(forKey: "email") as! String
            
        }
        
        categoryField.setTitle("Customer Feedback/Concerns", for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "CONTACT US"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "contact_us")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    //MARK: Private functions
    
    private func configureView() {
        UIManager.roundify(submitButton)
        UIManager.roundify(helpButton)
        UIManager.buttonBorder(button: helpButton, width: 0.5, color: UIColor(netHex: 0x7681a2))
        commentBg.layer.cornerRadius = 3.0
        emailBg.layer.cornerRadius = 3.0
        fullNameBg.layer.cornerRadius = 3.0
        categoryBg.layer.cornerRadius = 3.0
        subjectBg.layer.cornerRadius = 3.0
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }
    
    @IBAction func submitAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "contact_us_submit")
        if validate() {
            
            self.view.addSubview(loadingScreen!)
            self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem:  tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem:  tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem:  tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem:  tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                      ])
            
             self.view.layoutIfNeeded()
            
            loadingScreen?.startAnimating()
            
            GMoviesAPIManager.sendConcern(name: fullNameField.text!, email: emailField.text!, subject: subjectField.text!, category: selectedCategory.value(forKey: "id") as! String, message: commentField.text!, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        DispatchQueue.main.async {
                            
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            
                            let alert = UIAlertController(title: "Feedback Sent!", message: "", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                            alert.addAction(okAction)
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()

                            
                            let alert = UIAlertController(title: "Error", message: "Failed to send feedback", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                            alert.addAction(okAction)
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()

                    }
                    //print(error!.localizedDescription)
                }
            })
        }
    }
    
    @IBAction func categoryAction(_ sender: Any) {
        currentPop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 176.0)
        let cat = CategoryTableViewController(nibName: "CategoryTableViewController", bundle: nil)
        cat.delegate = self
        cat.parentController = self
        
        currentPop?.showViewControllerFromView(viewController: cat, originView: sender as! UIButton, offset: CGPoint(x: 0, y: 64.0+categoryBg.frame.origin.y))
    }
    
    func didSelectCategory(category: NSDictionary) {
        selectedCategory = category
        categoryField.setTitle(category.value(forKey: "name") as! String, for: .normal)
    }
    
    func validate() -> Bool {
        if fullNameField.text?.characters.count == 0 {
            fullNameBg.shake()
            return false
        }
        
        if subjectField.text?.characters.count == 0 {
            subjectBg.shake()
            return false
        }
        
        if emailField.text?.characters.count == 0 {
            emailBg.shake()
            return false
        }
        
        if emailField.text?.contains("@") == false || emailField.text?.contains(".") == false {
            emailBg.shake()
            return false
        }
        
        if commentField.text.characters.count == 0 {
            commentBg.shake()
            return false
        }
        
        return true
    }
    
    @IBAction func helpAction(_ sender: Any) {
        //let url = URL(string: "tel:09175434594")
        
        
        
        
        //let url = URL(string: "tel:022119792")
        
//        if UIApplication.shared.canOpenURL(url!) {
//            UIApplication.shared.openURL(url!)
//        }
        
        let popup = PopupDialog(title: "Contact Support", message: "Carrier charges may apply")
        let vc = popup.viewController as! PopupDialogDefaultViewController
        
        vc.titleFont = UIFont(name: "OpenSans", size: 16)!
        vc.titleColor = #colorLiteral(red: 0.3365190625, green: 0.3370802701, blue: 0.3537014127, alpha: 1)
        vc.titleTextAlignment = .center
        vc.messageFont = UIFont(name: "OpenSans", size: 14)!
        vc.messageColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        vc.messageTextAlignment = .center

        let sendSMSButton = DefaultButton(title: "Send us an SMS", height: 50) {
            
            let messageVC = MFMessageComposeViewController()
            
            if MFMessageComposeViewController.canSendText() {
                messageVC.body = "Enter a message";
                messageVC.recipients = ["09175434594"]
                messageVC.messageComposeDelegate = self;
                
                self.present(messageVC, animated: false, completion: nil)
            }
        }
        sendSMSButton.titleFont = UIFont(name: "OpenSans", size: 14)!
        
        let callMobileButton = DefaultButton(title: "Call Mobile", height: 50) {
            let url = URL(string: "tel:09175434594")
            
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.openURL(url!)
            }
        }
        callMobileButton.titleFont = UIFont(name: "OpenSans", size: 14)!
        
        let callLandlineButton = DefaultButton(title: "Call Landline", height: 50) {
            let url = URL(string: "tel:022119792")
            
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.openURL(url!)
            }
        }
        callLandlineButton.titleFont = UIFont(name: "OpenSans", size: 14)!
        
        popup.addButtons([sendSMSButton, callMobileButton, callLandlineButton])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
}

extension ContactViewController: MFMessageComposeViewControllerDelegate
{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}
