//
//  UIManager.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 25/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class UIManager: NSObject {
    
    class var mainBackgroundColor : UIColor {
        return UIColor(netHex: 0x111a31)
    }
    
    class var navBarBackgroundColor : UIColor {
        return UIColor(netHex: 0x0d152c)
    }
    
    class var fbButtonBackgroundColor : UIColor {
        return UIColor(netHex: 0x2e44e5)
    }
    
    class var watchButtonGray : UIColor {
        return UIColor(netHex: 0x67707A)
    }
    
    class var buttonRed : UIColor {
        return UIColor(netHex: 0xda2424)
    }
    
    class var seatmapBg : UIColor {
        return UIColor(netHex: 0xfafafa)
    }
    
    class var seatAvailableBg : UIColor {
        return UIColor(netHex: 0xc4cad3)
    }
    
    class var seatSelectedBg : UIColor {
        return UIColor(netHex: 0xef4042)
    }
    
    class var seatReservedBg: UIColor {
        return UIColor(netHex: 0x3f6eb6)
    }
    
    class var screenBorderColor : UIColor {
        return UIColor(netHex: 0x9ba2b9)
    }
    
    class var seatmapFontColor: UIColor {
        return UIColor(netHex: 0x76797b)
    }
    
    class func roundify(_ b : UIButton) {
        let frame = b.frame
        b.layer.cornerRadius = frame.size.height/2
    }
    
    class func buttonBorder(button b : UIButton, width w : CGFloat, color c : UIColor) {
        b.layer.borderColor = c.cgColor
        b.layer.borderWidth = w
    }
    
    class func seatmapDateFormatter(date : Date) -> String {
        
        let month : DateFormatter = DateFormatter()
        month.locale = NSLocale.current
        month.dateFormat = "MMM"
        let df : DateFormatter = DateFormatter()
        df.locale = NSLocale.current
        df.dateFormat = "dd MMM, EEE"
        
        let returnString = df.string(from: date)
        
        return returnString.replacingOccurrences(of: month.string(from: date), with: month.string(from: date).uppercased(), options: String.CompareOptions.caseInsensitive, range: returnString.range(of: month.string(from: date)))
    }
    
    class func ticketListDateFormatter(date : Date) -> NSAttributedString {
        let month : DateFormatter = DateFormatter()
        month.locale = NSLocale.current
        month.dateFormat = "MMM"
        let df : DateFormatter = DateFormatter()
        df.locale = NSLocale.current
        df.dateFormat = "MMM\ndd"
        
        let returnString = df.string(from: date)
        let formattedReturnString : NSMutableAttributedString = NSMutableAttributedString(string: returnString.replacingOccurrences(of: month.string(from: date), with: month.string(from: date).uppercased(), options: String.CompareOptions.caseInsensitive, range: returnString.range(of: month.string(from: date))), attributes: [NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName : UIFont(name: "OpenSans-Semibold", size: 17.0)!])
        
        formattedReturnString.addAttribute(NSFontAttributeName, value: UIFont(name: "OpenSans", size: 11.0)!, range: NSString(string: formattedReturnString.string).range(of: month.string(from: date).uppercased()))
        
        return formattedReturnString
    }
    
    class func scheduleDateStringToDate(dateString : String) -> Date {
        
        let df : DateFormatter = DateFormatter()
        df.locale = NSLocale.current
        df.dateFormat = "yyyy-MM-dd"
        
        return df.date(from: dateString)!
    }
    
    class func watchlistDateFormatter(dateString : String) -> String {
        let date : Date = scheduleDateStringToDate(dateString: dateString)
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "MMMM dd yyyy"
        df.locale = NSLocale.current
        
        return "Showing in \(df.string(from: date))"
    }
    
    class func comingSoonDateFormatter(dateString : String) -> String {
        let date : Date = scheduleDateStringToDate(dateString: dateString)
        
        let df : DateFormatter = DateFormatter()
        df.dateFormat = "MMMM d"
        df.locale = NSLocale.current
        
        return "\(df.string(from: date))"
    }
    
    class func paymentLoader() -> [UIImage] {
        var frames : [UIImage]? = []
        
        for i in (0 ..< 5) {
            frames?.append(UIImage(named: String(format: "payment_frame_%02d", i+1))!)
        }
        
        return frames!
    }
    
    class func genericLoader() -> [UIImage] {
        var frames : [UIImage]? = []
        
        for i in (0 ..< 15) {
            frames?.append(UIImage(named: String(format: "frame_%02d", i+1))!)
        }
        
        return frames!
    }
    
    class func generateDateId(date : Date) -> Int {
        var calendar : Calendar = Calendar.current
        calendar.locale = NSLocale.current
        let comp : DateComponents = calendar.dateComponents([.year, .month, .day], from: date)
        
        return Int("\(comp.year!)\(String(format: "%02d", comp.month!))\(String(format: "%02d", comp.day!))")!
    }
    
    class func configureErrorView(parentController : UIViewController, superview : UIView? = nil) -> ErrorView {
        
        let sp = superview == nil ? parentController.view : superview
        
        let errorView = Bundle.main.loadNibNamed("ErrorView", owner: parentController, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        sp?.layoutIfNeeded()
        
        errorView!.frame = (sp?.frame)!
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        sp?.addSubview(errorView!)
        sp?.addConstraints(constraints)
        
        sp?.layoutIfNeeded()
        
        
        return errorView!
    }
    
    class func refreshViewSpinner() -> UIImageView {
        let spinner = UIImageView(frame: CGRect(x: 0, y: 0, width: 60.0, height: 60.0))
        spinner.animationImages = genericLoader()
        
        return spinner
    }
}
