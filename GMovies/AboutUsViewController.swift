//
//  AboutUsViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 18/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import PopupDialog
import FBSDKShareKit

class AboutUsViewController: UITableViewController {

    @IBOutlet weak var privacyButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var faqButton: UIButton!
    @IBOutlet weak var rateButton: UIButton!
    
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        versionLabel.text = "Version \(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String) build \(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "ABOUT GMOVIES"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "about_us")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.title = " "
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    @IBAction func fbAction(_ sender: Any) {
        //AnalyticsHelper.sendButtonEvent(name: "about_us_fb")
        
        let alertController = UIAlertController(title: "Facebook", message: "", preferredStyle: .alert)
        
        let share = UIAlertAction(title: "Share on Facebook", style: .default) { (action) in
            
            let fbShare : FBSDKShareLinkContent = FBSDKShareLinkContent()
            fbShare.contentURL = URL(string:"https://www.facebook.com/GMoviesApp/")!
            fbShare.contentTitle = "Gmovies"
            fbShare.contentDescription = "Get inside, not in line!"
            
            let fbShareDialog : FBSDKShareDialog = FBSDKShareDialog()
            fbShareDialog.fromViewController = self
            fbShareDialog.shareContent = fbShare
            fbShareDialog.mode = .automatic
            fbShareDialog.show()
            fbShareDialog.delegate = self
        }
        
        let follow = UIAlertAction(title: "Follow on Facebook", style: .default) { (action) in
            UIApplication.shared.openURL(URL(string: "https://www.facebook.com/GMoviesApp")!)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
        }
        alertController.addAction(share)
        alertController.addAction(follow)
        alertController.addAction(cancel)
        
        
        present(alertController, animated: true, completion:nil)
    }
    
    @IBAction func tweetAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "about_us_twitter")
        UIApplication.shared.openURL(URL(string: "https://twitter.com/gmoviesapp")!)
    }

    @IBAction func instagramAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "about_us_instagram")
        UIApplication.shared.openURL(URL(string: "http://instagram.com/gmovies")!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWebContent" {
            
            var url : String = ""
            
            if (sender as! UIButton) == privacyButton {
                url = "\(Constants.gMoviesWebIP)/iframe/page/policy"
            } else if (sender as! UIButton) == termsButton {
                url = "\(Constants.gMoviesWebIP)/iframe/page/terms"
            } else if (sender as! UIButton) == rateButton {
                UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/id582901861")!)
            } else if (sender as! UIButton) == faqButton {
                url = "\(Constants.gMoviesWebIP)/iframe/page/faq"
            }
            
            (segue.destination as! AboutContentViewController).url = url
            (segue.destination as! AboutContentViewController).title = (sender as! UIButton).titleLabel?.text!
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showWebContent" {
            if (sender as! UIButton) == rateButton {
                UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/id582901861")!)
                return false
            }
        }
        
        return true
    }
}

extension AboutUsViewController: FBSDKSharingDelegate
{
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        if AccountHelper.shared.isRegisteredInRush {
            UserEngagementHelper.shared.earnBadgeFrom(engagement: UserEngagementCodes.shareFbPage)
        }
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
}
