//
//  BadgeCollectionViewCell.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 12/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

protocol BadgeCollectionViewCellDelegate {
    func onBadgeDetailsTapped(index: Int)
    func onBadgeTapped(index: Int)
}

class BadgeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var groupIconImageView: UIImageView!
    @IBOutlet weak var badgeIconImageView: EZUIImageView!
    
    @IBOutlet weak var badgeDetails: UIView!
    @IBOutlet weak var badge: UIView!
    
    var delegate: BadgeCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let badgeDetailsTap = UITapGestureRecognizer(target: self, action: #selector(badgeDetailsTapped(_:)))
        badgeDetails.addGestureRecognizer(badgeDetailsTap)
        
        let badgeTap = UITapGestureRecognizer(target: self, action: #selector(badgeTapped(_:)))
        badge.addGestureRecognizer(badgeTap)
        
    }
    
    func badgeTapped(_ sender: UITapGestureRecognizer) {
        
        self.badgeDetails.isHidden = false
        self.badgeDetails.alpha = 0
        
        let rot = CATransform3DRotate(self.badgeDetails.layer.transform, CGFloat(M_PI), 0, 1.0, 0)
        self.badgeDetails.layer.transform = rot
        
        let rot2 = CATransform3DRotate(self.badge.layer.transform, CGFloat(M_PI * 1.0), 0, 1.0, 0)
        self.badge.layer.transform = rot2
        
        UIView.animate(withDuration: 0.5, animations: {
            let rot = CATransform3DRotate(self.badgeDetails.layer.transform, CGFloat(M_PI), 0, 1.0, 0)
            self.badgeDetails.layer.transform = rot
            
            let rot2 = CATransform3DRotate(self.badge.layer.transform, CGFloat(M_PI), 0, 1.0, 0)
            self.badge.layer.transform = rot2
            
            self.badge.alpha = 0
            self.badgeDetails.alpha = 1
        }) { (completed) in
            self.badge.isHidden = true
        }
        self.delegate?.onBadgeTapped(index: self.tag)
    }
    
    func badgeDetailsTapped(_ sender: UITapGestureRecognizer) {
        
        self.badge.isHidden = false
        self.badge.alpha = 0
        
        let rot = CATransform3DRotate(self.badgeDetails.layer.transform, CGFloat(M_PI), 0, 1.0, 0)
        self.badgeDetails.layer.transform = rot
        
        let rot2 = CATransform3DRotate(self.badge.layer.transform, CGFloat(M_PI * 1.0), 0, 1.0, 0)
        self.badge.layer.transform = rot2
        
        UIView.animate(withDuration: 0.5, animations: {
            let rot = CATransform3DRotate(self.badgeDetails.layer.transform, CGFloat(M_PI), 0, 1.0, 0)
            self.badgeDetails.layer.transform = rot
            
            let rot2 = CATransform3DRotate(self.badge.layer.transform, CGFloat(M_PI), 0, 1.0, 0)
            self.badge.layer.transform = rot2
            
            self.badge.alpha = 1
            self.badgeDetails.alpha = 0
        }) { (completed) in
            self.badgeDetails.isHidden = true
        }
        self.delegate?.onBadgeDetailsTapped(index: self.tag)
    }
    
    func showDetails() {
        //reset
        self.badge.alpha = 1
        self.badgeDetails.alpha = 1
        
        self.badge.isHidden = true
        self.badgeDetails.isHidden = false
    }
    
    func hideDetails() {
        //reset
        self.badge.alpha = 1
        self.badgeDetails.alpha = 1
        
        self.badge.isHidden = false
        self.badgeDetails.isHidden = true
    }
}
