//
//  TheaterListInnerViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 10/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TheaterListInnerViewController: UIViewController, CinemaResultsProtocol {
    
    @IBOutlet weak var resultsTableView: UITableView!
    private var resultsController : CinemaResultsTableViewController?
    var theaters : [TheaterModel]?
    var movieObject : MovieModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        resultsController = CinemaResultsTableViewController()
        resultsController?.parent = self
        resultsController?.tableView = resultsTableView
        resultsTableView.delegate = resultsController
        resultsTableView.dataSource = resultsController
        resultsController?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        resultsController?.setData(data: theaters!)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Cinema results protocol
    
    func didSelectTheater(theater: TheaterModel) {
        var storyboard = UIStoryboard(name: "Booking", bundle: nil)
        let nav : ProgressNavigationViewController = storyboard.instantiateInitialViewController() as! ProgressNavigationViewController
        let seatSelection : SeatSelectionViewController = storyboard.instantiateViewController(withIdentifier: "SelectSeatsView") as! SeatSelectionViewController
        seatSelection.movieObject = movieObject
        seatSelection.theaterObject = theater
        seatSelection.tabImageOn = UIImage(named: "schedule-active")
        seatSelection.tabImageOff = UIImage(named: "schedule")
        
        let payment : PaymentViewController = storyboard.instantiateViewController(withIdentifier: "PaymentView") as! PaymentViewController
        payment.movieObject = movieObject
        payment.theaterObject = theater
        payment.tabImageOn = UIImage(named: "payment-active")
        payment.tabImageOff = UIImage(named: "payment")
        
        storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
        let ticket : TicketViewController = storyboard.instantiateViewController(withIdentifier: "TicketView") as! TicketViewController
        
        ticket.tabImageOn = UIImage(named: "ticket-active")
        ticket.tabImageOff = UIImage(named: "ticket")
        
        nav.viewControllers = [seatSelection, payment, ticket];
        
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
