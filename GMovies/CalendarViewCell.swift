//
//  CalendarViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CalendarViewCell: UICollectionViewCell {

    @IBOutlet weak var dayNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dayNumber.layer.cornerRadius = 3.0
        dayNumber.clipsToBounds = true
    }

}
