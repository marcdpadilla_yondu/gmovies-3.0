//
//  TheaterModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 22/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import CoreLocation

class TheaterModel: NSObject {
    /*
 
     "theater_id": "S6rmQuoKRiWBpqIeNyaiyA~5629499534213120",
     "address": "Rockwell Center, Makati City",
     "name": "Power Plant Mall",
     "slug": "power-plant-mall",
     "city": "Makati City",
     "region": "Metro Manila",
     "city_key": "makati_city",
     "region_key": "metro_manila",
     "active": true
     
    */
    
    /*
     {
     "allow_paynamics_rebill" = 0;
     "allow_reservation" = 0;
     "convenience_fees" =             {
     };
     "convenience_fees_message" =             {
     };
     name = "Greenbelt 3";
     "payment_options" =             (
     );
     slug = "greenbelt-3";
     };
    */
    
    var theaterId : String?
    var address : String?
    var name : String?
    var slug : String?
    var city : String?
    var region : String?
    var cityKey : String?
    var regionKey : String?
    var isActive : Bool = false
    var location : CLLocationCoordinate2D?
    var isPreferred : Bool = false
    
    var allowPaynamicsRebill : Bool = false
    var allowReseravation : Bool = false
    var paymentOptions : [PaymentOptionModel]?
    var cinemas : [CinemaModel]?
    var convenienceFees : NSDictionary?
    var convenienceFeesMessage : NSDictionary?
    
    
    init(data : NSDictionary) {
//        print(data)
        theaterId = data.object(forKey: "theater_id") as? String
        address = data.object(forKey: "address") as? String
        name = data.object(forKey: "name") as? String
        slug = data.object(forKey: "slug") as? String
        city = data.object(forKey: "city") as? String
        region =  data.object(forKey: "region") as? String
        cityKey = data.object(forKey: "city_key") as? String
        regionKey = data.object(forKey: "region_key") as? String
        if data.object(forKey: "active_ios") != nil {
            isActive = (data.object(forKey: "active_ios") as? Bool)!
        }
        
        let loc : NSDictionary? = data.object(forKey: "location") as? NSDictionary
        if loc != nil {
            location = CLLocationCoordinate2D(latitude: CLLocationDegrees(loc!.object(forKey: "lat") as! Double), longitude: CLLocationDegrees(loc!.object(forKey: "lon") as! Double))
        }
        
        cinemas = []
        paymentOptions = []
    }
}
