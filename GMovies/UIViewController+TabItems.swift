//
//  UIViewController+TabItems.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 6/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

private var tabImageOnAssociateKey : UInt8 = 0
private var tabImageOffAssociateKey : UInt8 = 0
private var tabTextColorOnAssociateKey : UInt8 = 0
private var tabTextColorOffAssociateKey : UInt8 = 0
private var tabBgColorOnAssociateKey : UInt8 = 0
private var tabBgColorOffAssociateKey : UInt8 = 0

extension UIViewController {
    
    var tabImageOn : UIImage? {
        get {
            return objc_getAssociatedObject(self, &tabImageOnAssociateKey) as? UIImage
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &tabImageOnAssociateKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var tabImageOff : UIImage? {
        get {
            return objc_getAssociatedObject(self, &tabImageOffAssociateKey) as? UIImage
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &tabImageOffAssociateKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var tabTextColorOn : UIColor? {
        get {
            return objc_getAssociatedObject(self, &tabTextColorOnAssociateKey) as? UIColor
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &tabTextColorOnAssociateKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var tabTextColorOff : UIColor? {
        get {
            return objc_getAssociatedObject(self, &tabTextColorOffAssociateKey) as? UIColor
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &tabTextColorOffAssociateKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var tabBgColorOn : UIColor? {
        get {
            return objc_getAssociatedObject(self, &tabBgColorOnAssociateKey) as? UIColor
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &tabBgColorOnAssociateKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var tabBgColorOff : UIColor? {
        get {
            return objc_getAssociatedObject(self, &tabBgColorOffAssociateKey) as? UIColor
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &tabBgColorOffAssociateKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
}
