//
//  ShareViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FBSDKShareKit
import Social

class ShareViewController: UITableViewController {
    
    private let identifier : String = "schedCinemaIdentifier"
    private var items : [String] = ["Share on Facebook", "Send to Messenger", "Tweet"]
    
    var mode : Int = 0 //default
    
    /*
     1: tickets
    */
    
    var mo : MovieModel?
    var t : TicketModel?
    var parentController : UIViewController?
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.register(UINib(nibName: "SchedCinemaCell", bundle: nil), forCellReuseIdentifier: identifier)
        
        if mode == 1 {
            if mo?.movieId != nil {
                if (mo?.movieId?.characters.count)! > 0 {
                    items.append("Write a review")
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "share_list")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SchedCinemaCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SchedCinemaCell

        cell.cinemaName.text = items[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let pop : PopDropViewController = self.parent as! PopDropViewController
        let cell = tableView.cellForRow(at: indexPath) as! SchedCinemaCell
        
        if indexPath.row == 0 {
            AnalyticsHelper.sendButtonEvent(name: "share_list_facebook")
            let fbShare : FBSDKShareLinkContent = FBSDKShareLinkContent()
            if t != nil {
                fbShare.contentURL = URL(string: t!.share!.value(forKey: "url") as! String)!
                fbShare.contentTitle = t!.share!.value(forKey: "title") as! String
                fbShare.contentDescription = t!.share!.value(forKey: "message") as! String
            } else {
                fbShare.contentURL = URL(string: mo!.share!.value(forKey: "url") as! String)!
                fbShare.contentTitle = mo!.share!.value(forKey: "title") as! String
                fbShare.contentDescription = mo!.share!.value(forKey: "message") as! String
            }
            
            let fbShareDialog : FBSDKShareDialog = FBSDKShareDialog()
            fbShareDialog.fromViewController = parentController!
            fbShareDialog.shareContent = fbShare
            fbShareDialog.mode = .automatic
            fbShareDialog.show()
            
        } else if indexPath.row == 1 {
            AnalyticsHelper.sendButtonEvent(name: "share_list_messenger")
            let fbShare : FBSDKShareLinkContent = FBSDKShareLinkContent()
            if t != nil {
                fbShare.contentURL = URL(string: t!.share!.value(forKey: "url") as! String)!
                fbShare.contentTitle = t!.share!.value(forKey: "title") as! String
                fbShare.contentDescription = t!.share!.value(forKey: "message") as! String
            } else {
                fbShare.contentURL = URL(string: mo!.share!.value(forKey: "url") as! String)!
                fbShare.contentTitle = mo!.share!.value(forKey: "title") as! String
                fbShare.contentDescription = mo!.share!.value(forKey: "message") as! String
            }
            
            let fbShareDialog : FBSDKMessageDialog = FBSDKMessageDialog()
            fbShareDialog.shareContent = fbShare
            fbShareDialog.show()
        } else if indexPath.row == 2 {
            AnalyticsHelper.sendButtonEvent(name: "share_list_twitter")
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
                
                let tweetShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                
                if t != nil {
                    tweetShare.add(URL(string: t!.share!.value(forKey: "url") as! String)!)
                    tweetShare.setInitialText(t!.share!.value(forKey: "message") as! String)
                } else {
                    tweetShare.add(URL(string: mo!.share!.value(forKey: "url") as! String)!)
                    tweetShare.setInitialText(mo!.share!.value(forKey: "message") as! String)
                }
                
                parentController?.present(tweetShare, animated: true, completion: nil)
                
            } else {
                
                let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to tweet.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    if let url = URL(string:UIApplicationOpenSettingsURLString) {
                        UIApplication.shared.openURL(url)
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                parentController?.present(alert, animated: true, completion: nil)
            }
        }
        
        if cell.cinemaName.text == "Write a review" {
            AnalyticsHelper.sendButtonEvent(name: "ticket_list_review")
            let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
            let reviewForm : WriteReviewViewController = storyboard.instantiateViewController(withIdentifier: "ReviewForm") as! WriteReviewViewController
            
            reviewForm.movieObject = mo
            parentController?.navigationController?.pushViewController(reviewForm, animated: true)
        }
        
        pop.hide()
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
