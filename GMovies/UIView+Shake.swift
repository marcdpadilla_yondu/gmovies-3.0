//
//  UIView+Shake.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import AudioToolbox

extension UIView {
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.1
        animation.values = [-15, 15, 0]
        layer.add(animation, forKey: "shake")
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
}
