//
//  WriteReviewViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 16/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import Cosmos

class WriteReviewViewController: UITableViewController, UITextViewDelegate {
    
    var movieObject : MovieModel?
    
    @IBOutlet weak var posterBorder: UIView!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var advisory: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var runtime: UILabel!
    
    @IBOutlet weak var starRating: CosmosView!
    @IBOutlet weak var commentBg: UIView!
    @IBOutlet weak var commentField: UITextView!
    @IBOutlet weak var submitButton: MButton!
    
    private var loadingScreen : LoaderView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        if movieObject?.poster != nil {
            poster.image = movieObject?.poster
        } else {
            GMoviesAPIManager.downloadPoster(urlString: movieObject!.posterUrl!, completion: { (error, image) in
                if error == nil {
                    self.movieObject?.poster = image
                    DispatchQueue.main.async {
                        self.poster.image = image
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        }
        
        movieTitle.text = movieObject?.movieTitle!
        advisory.text = movieObject?.advisoryRatingString
        genre.text = movieObject?.genre!
        runtime.text = movieObject?.length!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "review")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    //MARK: Private functions
    
    private func configureView() {
        UIManager.roundify(submitButton)
        commentBg.layer.cornerRadius = 3.0
        posterBorder.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        posterBorder.layer.borderWidth = 0.5
        
        advisory.layer.borderWidth = 0.5
        advisory.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        advisory.layer.cornerRadius = 3.0
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }
    
    @IBAction func submitAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "review_submit")
        let comment = commentField.text == "Write your review" ? "" : commentField.text
        
        self.view.addSubview(loadingScreen!)
        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                  ])
        
        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
        
        GMoviesAPIManager.sendReview(movieId: movieObject!.movieId!, comment: comment!, rating: "\(starRating.rating)", completion: { (error, result) in
            if error == nil {
                let status = result?.value(forKey: "status") as! Int
                
                if status == 1 {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        
                        let alert = UIAlertController(title: "Success", message: result?.value(forKey: "message") as! String, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        
                        let alert = UIAlertController(title: "Error", message: result?.value(forKey: "message") as! String, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                            
                        })
                        
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            } else {
                print(error!.localizedDescription)
                DispatchQueue.main.async {
                    self.loadingScreen?.stopAnimating()
                    self.loadingScreen?.removeFromSuperview()
                }
            }
        })
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write your review" {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Write your review"
        }
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
