//
//  ImageLoader.swift
//  Leaders' Summit
//
//  Created by Erson Jay Mujar on 25/11/2016.
//  Copyright © 2016 section8. All rights reserved.
//

import Foundation

class EZImageLoader {
    
    var cache = NSCache<NSString, NSDictionary>()
    
    class var shared : EZImageLoader {
        struct Static {
            static let instance : EZImageLoader = EZImageLoader()
        }
        return Static.instance
    }
    
    func image(url: URL, cacheKey: String? = nil, completion:@escaping (UIImage?,URL) -> ()){
        
        let key: NSString = cacheKey == nil ? url.absoluteString as NSString : cacheKey! as NSString
        
        DispatchQueue.global(qos: .utility).async {
            
            if let cachedData: NSDictionary = self.cache.object(forKey: key) {
                let cachedDataUrl = cachedData.object(forKey: "url") as! String
                if cachedDataUrl == url.absoluteString {
                    if let imageData = cachedData.object(forKey: "image") as? Data {
                        let image = UIImage(data: imageData)
                        DispatchQueue.main.async {
                            completion(image, url)
                        }
                        return
                    }
                }
            }
            
            let downloadTask: URLSessionDataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    completion(nil, url)
                    return
                }
                
                if data != nil {
                    let image = UIImage(data: data!)
                    let data2: NSData = data! as NSData
                    let dict = NSDictionary(dictionary: ["url":url.absoluteString, "image":data2])
                    self.cache.setObject(dict, forKey: key)
                    DispatchQueue.main.async {
                        completion(image, url)
                    }
                    return
                }
            }
            downloadTask.resume()
        }
    }
    
    func image(urlString: String, cacheKey: String? = nil, completion:@escaping (UIImage?,String) -> ()){
        
        let url: URL = URL(string: urlString)!
        image(url: url, cacheKey: cacheKey) { (image, url) in
            DispatchQueue.main.async {
                completion(image, url.absoluteString)
            }
        }
    }
    
    func purge () {
        self.cache.removeAllObjects()
    }
}
