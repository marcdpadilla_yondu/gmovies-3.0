//
//  EarnedPointsViewController.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 19/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit
import Nuke

protocol EarnedPointsViewControllerDelegate {
    func onOkClicked()
}

class EarnedPointsViewController: UIViewController {

    @IBOutlet weak var earnedPointsLabel: UILabel!
    @IBOutlet weak var availablePointsLabel: UILabel!
    @IBOutlet weak var groupBadgeIconView: EZUIImageView!
    @IBOutlet weak var badgeIconView: EZUIImageView!
    @IBOutlet weak var stackView: UIStackView!
    
    let earnedPointsText1 = "Congratulations, you have earned "
    let earnedPointsText2 = " for unlocking the [Name of Badge] Badge"
    let availabePointsText = "Available Points is "
    
    var earnedPoints = "20"
    var availablePoints = "20"
    var badgeTitleText = ""
    var badgeGroupIconUrl = ""
    var badgeIconUrl = ""
    
    var shouldHideAvailablePoints = false
    
    var delegate: EarnedPointsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if shouldHideAvailablePoints {
            
            stackView.removeArrangedSubview(availablePointsLabel)
            availablePointsLabel.removeFromSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var earnedFontSize: CGFloat = 14
        var availableFontSize: CGFloat = 17
        
        if UIScreen.main.bounds.maxY == 568 {
            earnedFontSize = 12
            availableFontSize = 15
        }
        
        if UIScreen.main.bounds.maxY == 667 {
            earnedFontSize = 14
            availableFontSize = 17
        }
        
        if UIScreen.main.bounds.maxY == 736 {
            earnedFontSize = 14
            availableFontSize = 17
        }
        
        //Earned points
        let earnedPointsTextAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.3365190625, green: 0.3370802701, blue: 0.3537014127, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans", size: earnedFontSize) ?? UIFont.systemFont(ofSize: earnedFontSize)]
        let earnedPointsAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.06666666667, green: 0.5137254902, blue: 0.8392156863, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans-Semibold", size: earnedFontSize) ?? UIFont.systemFont(ofSize: earnedFontSize)]
        
        let earned1 = NSMutableAttributedString(string: earnedPointsText1, attributes: earnedPointsTextAttr)
        let earned2 = NSMutableAttributedString(string: "\(earnedPoints) points", attributes: earnedPointsAttr)
        let earned3 = NSMutableAttributedString(string: " for unlocking the \(badgeTitleText) Badge", attributes: earnedPointsTextAttr)
        
        let earnedCombi = NSMutableAttributedString()
        earnedCombi.append(earned1)
        earnedCombi.append(earned2)
        earnedCombi.append(earned3)
        
        earnedPointsLabel.attributedText = earnedCombi
        
        if !shouldHideAvailablePoints {
            //Available points
            let availablePointsTextAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.3365190625, green: 0.3370802701, blue: 0.3537014127, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans", size: availableFontSize) ?? UIFont.systemFont(ofSize: availableFontSize)]
            let availablePointsAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.06666666667, green: 0.5137254902, blue: 0.8392156863, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans-Semibold", size: availableFontSize) ?? UIFont.systemFont(ofSize: availableFontSize)]
            
            let available1 = NSMutableAttributedString(string: availabePointsText, attributes: availablePointsTextAttr)
            let available2 = NSMutableAttributedString(string: "\(availablePoints)", attributes: availablePointsAttr)
            
            let availableCombi = NSMutableAttributedString()
            availableCombi.append(available1)
            availableCombi.append(available2)
            
            availablePointsLabel.attributedText = availableCombi
        }
        
        if let url = URL(string: badgeGroupIconUrl) {
            Nuke.loadImage(with: url, into: groupBadgeIconView, handler: { [weak groupBadgeIconView](response, isFromCache) in
                
                if response.value != nil {
                    groupBadgeIconView?.image = response.value
                }
            })
        }
        
        if let url = URL(string: badgeIconUrl) {
            Nuke.loadImage(with: url, into: badgeIconView, handler: { [weak badgeIconView] (response, isFromCache) in
                
                if response.value != nil {
                    badgeIconView?.image = response.value
                }
            })
        }
    }
    
    @IBAction func onOkClicked(_ sender: Any) {
        delegate?.onOkClicked()
    }
}
