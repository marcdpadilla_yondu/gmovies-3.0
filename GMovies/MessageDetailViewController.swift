//
//  MessageDetailViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 28/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MessageDetailViewController: UIViewController {
    
    @IBOutlet weak var messageView : UITextView!
    
    var messageObject : MessageModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let closeButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.closeAction))
        self.navigationItem.leftBarButtonItem = closeButton
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        messageView.text = messageObject!.message!
        GMoviesAPIManager.openMessage(msgId: messageObject!.msgId!, completion: { (error, result) in
            if error == nil {
                let status = result?.value(forKey: "status") as! Int
                if status == 1 {
                    self.messageObject?.opened = true
                } else {
                    
                }
            } else {
                print(error!.localizedDescription)
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "message_open")
    }
    
    @objc private func closeAction(sender : UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
