//
//  SuccessfulTransactionVC.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 05/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit
import Nuke

@objc protocol SuccessfulTransactionVCDelegate {
    @objc func onViewTicket()
    @objc func onJoinLoyaltyProgram()
}

class SuccessfulTransactionVC: UIViewController {

    @IBOutlet weak var earnedPointsLabel: UILabel!
    @IBOutlet weak var availablePointsLabel: UILabel!
    @IBOutlet weak var messageLabel: EZUILabel!
    @IBOutlet weak var joinLoyaltyProgram: AttributedTextView!
    @IBOutlet weak var joinButton: EZUIButton!
    var delegate: SuccessfulTransactionVCDelegate?
    @IBOutlet weak var loyaltyProgramStackView: UIStackView!
    @IBOutlet weak var loyaltyProgramHolder: UIView!
    @IBOutlet weak var badgeMessage: EZUILabel!
    
    @IBOutlet weak var badgeHolder: UIView!
    @IBOutlet weak var badgeGroupImageView: EZUIImageView!
    @IBOutlet weak var badgeImageView: EZUIImageView!
    
    
    var paymentRoot: UIViewController?
    
    let earnedPointsText1 = "You have earned "
    let earnedPointsText2 = " for this transaction"
    let availabePointsText = "Available Points is "
    
    var earnedPoints = "20"
    var availablePoints = "20"
    var badgeMessageText = ""
    var badgeCaptionText = ""
    var badgeGroupIconUrl = ""
    var badgeIconUrl = ""
    
    var shouldHideEarnedPoints = false
    var shouldHideJoinLoyaltyProgram = false
    var shouldHideBottomContainer = false
    var shouldHideBadge = false
    var shouldHideBadgeMessage = true
    var shouldHideBadgeCaption = false

    @IBAction func onViewTicketClicked(_ sender: Any) {
        delegate?.onViewTicket()
    }
    @IBAction func onJoinClicked(_ sender: Any) {
        delegate?.onJoinLoyaltyProgram()
    }
    
    override func viewDidLoad() {
        
        if shouldHideJoinLoyaltyProgram && shouldHideEarnedPoints && shouldHideBadge && shouldHideBadgeCaption {
            
            //This is just to make the GMovies Banner centered
            
            loyaltyProgramStackView.removeArrangedSubview(earnedPointsLabel)
            earnedPointsLabel.removeFromSuperview()
            loyaltyProgramStackView.removeArrangedSubview(availablePointsLabel)
            availablePointsLabel.removeFromSuperview()
            
            loyaltyProgramStackView.removeArrangedSubview(badgeHolder)
            badgeHolder.removeFromSuperview()
            
            loyaltyProgramHolder.isHidden = true
            
            return
        }
        
        if shouldHideEarnedPoints {
            loyaltyProgramStackView.removeArrangedSubview(earnedPointsLabel)
            earnedPointsLabel.removeFromSuperview()
            loyaltyProgramStackView.removeArrangedSubview(availablePointsLabel)
            availablePointsLabel.removeFromSuperview()
        }
        
        if shouldHideBadge {
            loyaltyProgramStackView.removeArrangedSubview(badgeHolder)
            badgeHolder.removeFromSuperview()
        }
        
        if shouldHideBadgeCaption {
            loyaltyProgramStackView.removeArrangedSubview(messageLabel)
            messageLabel.removeFromSuperview()
        }
        
        if shouldHideBadgeMessage {
            loyaltyProgramStackView.removeArrangedSubview(badgeMessage)
            badgeMessage.removeFromSuperview()
        }
        
        if shouldHideJoinLoyaltyProgram {
            loyaltyProgramStackView.removeArrangedSubview(joinLoyaltyProgram)
            joinLoyaltyProgram.removeFromSuperview()
            loyaltyProgramStackView.removeArrangedSubview(joinButton)
            joinButton.removeFromSuperview()
        }
        
        if shouldHideBottomContainer {
            loyaltyProgramHolder.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.hidesBackButton = true
        
        if !shouldHideEarnedPoints {
            
            
            var earnedFontSize: CGFloat = 14
            var availableFontSize: CGFloat = 17
            
            if UIScreen.main.bounds.maxY == 568 {
                earnedFontSize = 12
                availableFontSize = 15
            }
            
            if UIScreen.main.bounds.maxY == 667 {
                earnedFontSize = 14
                availableFontSize = 17
            }
            
            if UIScreen.main.bounds.maxY == 736 {
                earnedFontSize = 14
                availableFontSize = 17
            }
            
            //Earned points
            let earnedPointsTextAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans", size: earnedFontSize) ?? UIFont.systemFont(ofSize: earnedFontSize)]
            let earnedPointsAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.06666666667, green: 0.5137254902, blue: 0.8392156863, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans-Semibold", size: earnedFontSize) ?? UIFont.systemFont(ofSize: earnedFontSize)]
            
            let earned1 = NSMutableAttributedString(string: earnedPointsText1, attributes: earnedPointsTextAttr)
            let earned2 = NSMutableAttributedString(string: "\(earnedPoints) Points", attributes: earnedPointsAttr)
            let earned3 = NSMutableAttributedString(string: earnedPointsText2, attributes: earnedPointsTextAttr)
            
            let earnedCombi = NSMutableAttributedString()
            earnedCombi.append(earned1)
            earnedCombi.append(earned2)
            earnedCombi.append(earned3)
            
            earnedPointsLabel.attributedText = earnedCombi
            
            //Available points
            let availablePointsTextAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans", size: availableFontSize) ?? UIFont.systemFont(ofSize: availableFontSize)]
            let availablePointsAttr: [String : Any] = [NSForegroundColorAttributeName : #colorLiteral(red: 0.06666666667, green: 0.5137254902, blue: 0.8392156863, alpha: 1), NSFontAttributeName : UIFont(name: "OpenSans-Semibold", size: availableFontSize) ?? UIFont.systemFont(ofSize: availableFontSize)]
            
            let available1 = NSMutableAttributedString(string: availabePointsText, attributes: availablePointsTextAttr)
            let available2 = NSMutableAttributedString(string: "\(availablePoints)", attributes: availablePointsAttr)
            
            let availableCombi = NSMutableAttributedString()
            availableCombi.append(available1)
            availableCombi.append(available2)
            
            availablePointsLabel.attributedText = availableCombi
            
        }
        
        if !shouldHideJoinLoyaltyProgram {
            
            var joinLoyaltyProgramFontSize:CGFloat = 13.0
            
            if UIScreen.main.bounds.maxY == 568 {
                joinLoyaltyProgramFontSize = 12.0
            }
            
            if UIScreen.main.bounds.maxY == 667 {
                joinLoyaltyProgramFontSize = 13.0
            }
            
            if UIScreen.main.bounds.maxY == 736 {
                joinLoyaltyProgramFontSize = 13.0
            }
            
            joinLoyaltyProgram.attributer = "You could have earned points for this transaction if you have joined the "
                .color(UIColor(hex: 11187148))
                .font(UIFont(name: "OpenSans", size: joinLoyaltyProgramFontSize))
                .append("Reel Rewards Program")
                .font(UIFont(name: "OpenSans-Semibold", size: joinLoyaltyProgramFontSize))
                .makeInteract({ (_) in
                    let loyaltyProgramVC = JoinLoyaltyProgramViewController()
                    loyaltyProgramVC.title = ""
                    loyaltyProgramVC.showJoinNowButton = false
                    loyaltyProgramVC.showNotNowButton = false
                    self.show(loyaltyProgramVC, sender: self)
                })
                .setLinkColor(UIColor.white)
                .range(NSMakeRange(0, 75))
                .paragraphAlignCenter.paragraphApplyStyling
            
        }
        
        if !shouldHideBadge {
           
            if let url = URL(string: badgeGroupIconUrl) {
                Nuke.loadImage(with: url, into: badgeGroupImageView, handler: { [weak badgeGroupImageView](response, isFromCache) in
                    
                    if response.value != nil {
                        badgeGroupImageView?.image = response.value
                    }else {
                        
                    }
                    
                })
            }
            
            if let url = URL(string: badgeIconUrl) {
                Nuke.loadImage(with: url, into: badgeImageView, handler: { [weak badgeImageView] (response, isFromCache) in
                    
                    if response.value != nil {
                        badgeImageView?.image = response.value
                    }else {
                        
                    }
                })
            }
        }
        
        if !shouldHideBadgeMessage {
            badgeMessage.text = badgeMessageText
        }
        
        if !shouldHideBadgeCaption {
            messageLabel.text = badgeCaptionText
        }
    }
    
    
    
}
