//
//  EZUIProperties.swift
//  Leaders' Summit
//
//  Created by Erson Jay Mujar on 24/11/2016.
//  Copyright © 2016 section8. All rights reserved.
//

import Foundation

@IBDesignable
class EZLayoutConstraint: NSLayoutConstraint {
    
    @IBInspectable
    //iPhone 4
    var 📱3¨5_constant: CGFloat = 0 {
        didSet {
            if UIScreen.main.bounds.maxY == 480 {
                constant = 📱3¨5_constant
            }
        }
    }
    
    //iPhone 5, 5C, 5S, iPod Touch 5g,SE
    @IBInspectable
    var 📱4¨0_constant: CGFloat = 0 {
        didSet {
            if UIScreen.main.bounds.maxY == 568 {
                constant = 📱4¨0_constant
            }
        }
    }
    
    //iPhone 6, iPhone 6s, iPhone 7
    @IBInspectable
    var 📱4¨7_constant: CGFloat = 0 {
        didSet {
            if UIScreen.main.bounds.maxY == 667 {
                constant = 📱4¨7_constant
            }
        }
    }
    
    //Phone 6 Plus, iPhone 6s Plus, iPhone 7 Plus
    @IBInspectable
    var 📱5¨5_constant: CGFloat = 0 {
        didSet {
            if UIScreen.main.bounds.maxY == 736 {
                constant = 📱5¨5_constant
            }
        }
    }
}

@IBDesignable
class EZUILabel: UILabel {

    
    @IBInspectable
    var 📱3¨5_fontSize: CGFloat {
        get {
            return font.pointSize
        }
        set {
            if UIScreen.main.bounds.maxY == 480 {
                font = font.withSize(newValue)
            }
        }
    }
    
    @IBInspectable
    var 📱4¨0_fontSize: CGFloat {
        get {
            return font.pointSize
        }
        set {
            if UIScreen.main.bounds.maxY == 568 {
                font = font.withSize(newValue)
            }
        }
    }
    
    @IBInspectable
    var 📱4¨7_fontSize: CGFloat {
        get {
            return font.pointSize
        }
        set {
            if UIScreen.main.bounds.maxY == 667 {
                font = font.withSize(newValue)
            }
        }
    }
    
    @IBInspectable
    var 📱5¨5_fontSize: CGFloat{
        get {
            return font.pointSize
        }
        set {
            if UIScreen.main.bounds.maxY == 736 {
                font = font.withSize(newValue)
            }
        }
    }
}

@IBDesignable
class EZUIImageView: UIImageView {
    
    fileprivate var isCircular: Bool = false
    
    @IBInspectable
    var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable
    var makeCircular: Bool {
        set {
            isCircular = newValue
            if isCircular {
                self.cornerRadius = self.frame.size.width/2
            }else{
                self.cornerRadius = 0
            }
        }
        get {
            return isCircular
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isCircular {
            makeCircular = true
        }
    }
}

class EZUIButton: UIButton {
    
    fileprivate var isRounded: Bool = false
    
    @IBInspectable
    var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable
    var 📱3¨5_fontSize: CGFloat {
        get {
            return (titleLabel?.font.pointSize)!
        }
        set {
            if UIScreen.main.bounds.maxY == 480 {
                titleLabel?.font = titleLabel?.font.withSize(newValue)
            }
        }
    }
    
    @IBInspectable
    var 📱4¨0_fontSize: CGFloat {
        get {
            return (titleLabel?.font.pointSize)!
        }
        set {
            if UIScreen.main.bounds.maxY == 568 {
                titleLabel?.font = titleLabel?.font.withSize(newValue)
            }
        }
    }
    
    @IBInspectable
    var 📱4¨7_fontSize: CGFloat {
        get {
            return (titleLabel?.font.pointSize)!
        }
        set {
            if UIScreen.main.bounds.maxY == 667 {
                titleLabel?.font = titleLabel?.font.withSize(newValue)
            }
        }
    }
    
    @IBInspectable
    var 📱5¨5_fontSize: CGFloat {
        get {
            return (titleLabel?.font.pointSize)!
        }
        set {
            if UIScreen.main.bounds.maxY == 736 {
                titleLabel?.font = titleLabel?.font.withSize(newValue)
            }
        }
    }
    
    @IBInspectable
    var makeCornersRounded: Bool {
        set {
            self.isRounded = newValue
            if isRounded {
                self.cornerRadius = self.frame.size.height/2
            }else{
                self.cornerRadius = 0
            }
        }
        get {
            return self.isRounded
        }
    }
    
    func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if makeCornersRounded {
            makeCornersRounded = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

class EZUIView: UIView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

open class EZUITextView: UITextView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

extension UILabel {
    
    func addImage(named: String, afterLabel: Bool = false) {
        let attachment: NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: named)
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
        
        if afterLabel {
            let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        }else {
            let strLabelText: NSAttributedString = NSAttributedString(string: self.text!)
            let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }
    }
}

protocol EZProgressLayerProtocol {
    func progressUpdated(progress: CGFloat)
}

class EZProgressLayer : CALayer
{
    var progress: CGFloat = 0.0
    var progressDelegate: EZProgressLayerProtocol?
    
    override init (layer: Any) {
        super.init(layer: layer)
        if let other = layer as? EZProgressLayer {
            self.progress = other.progress
            self.progressDelegate = other.progressDelegate
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        progressDelegate = aDecoder.decodeObject(forKey: "progressDelegate") as? EZProgressLayerProtocol
        progress = aDecoder.decodeObject(forKey: "progress") as! CGFloat
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(progress, forKey: "progress")
        aCoder.encode(progressDelegate, forKey: "progressDelegate")
    }
    
    init(progressDelegate: EZProgressLayerProtocol) {
        super.init()
        self.progressDelegate = progressDelegate
    }

    
    override class func needsDisplay(forKey key: String) -> Bool
    {
        if key == "progress" {
            return true
        }else {
            return super.needsDisplay(forKey: key)
        }
    }
    
    override func draw(in ctx: CGContext) {
        self.progressDelegate?.progressUpdated(progress: progress)
    }
}
