//
//  MoviesTableViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 31/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MoviesTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var moviesCarousel: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
