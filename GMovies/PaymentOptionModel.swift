//
//  PaymentOptionModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 11/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PaymentOptionModel: NSObject {
    /*
     {
     id = paynamics;
     "payment_option" =                     {
     "allow_discount_codes" = 1;
     "default_params" =                         (
     );
     "instruction_url" = "";
     "is_required_discount_codes" = 0;
     name = "Credit Card";
     "payment_gateway" = paynamics;
     "payment_option_image" = "http://lh3.googleusercontent.com/Zz5pnY89wk0q33nKM6Jg2uH3QFtsq74MvXCj9yPUYCJeccNMFlbC9nmm0-WRJrHmt-o9P2OtdVNBLmfYTEhaAEdMQP5CB8HZHkE";
     "promo_header_image" = "";
     type = "credit_card";
     };
    */
    
    
    var optionId : String?
    var allowDiscountCodes : Bool = false
    var defaultParams : [NSDictionary]?
    var instructionUrl : String?
    var requireDiscountCode : Bool = false
    var name : String?
    var paymentGateway : String?
    var iconUrl : String?
    var icon : UIImage?
    var promoImageUrl : String?
    var promoImage : UIImage?
    var type : String?
    
    init (data : NSDictionary) {
        super.init()
        
        optionId = data.object(forKey: "id") as? String
        allowDiscountCodes = (data.value(forKeyPath: "payment_option.allow_discount_codes") as? Bool)!
        defaultParams = data.value(forKeyPath: "payment_option.default_params") as? [NSDictionary]
        instructionUrl = data.value(forKeyPath: "payment_option.instruction_url") as? String
        requireDiscountCode = (data.value(forKeyPath: "payment_option.is_required_discount_codes") as? Bool)!
        name = data.value(forKeyPath: "payment_option.name") as? String
        paymentGateway = data.value(forKeyPath: "payment_option.payment_gateway") as? String
        iconUrl = data.value(forKeyPath: "payment_option.payment_option_image") as? String
        promoImageUrl = data.value(forKeyPath: "payment_option.promo_header_image") as? String
        type = data.value(forKeyPath: "payment_option.type") as? String
    }
}
