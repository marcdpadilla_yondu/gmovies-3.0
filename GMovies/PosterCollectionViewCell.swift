//
//  PosterCollectionViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 31/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PosterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var advanceBookingLabel: UIButton!
    @IBOutlet weak var advanceBookingBg: UIView!
    @IBOutlet weak var watchListButton: UIButton!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var releaseaDate: UILabel!
    @IBOutlet weak var releaseDateHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor(netHex: 0xdcdcdc).cgColor
        self.layer.borderWidth = 0.5
        
//        if UserDefaults.standard.bool(forKey: "loggedin") == false {
//            watchListButton.isHidden = true
//        }
    }
}
