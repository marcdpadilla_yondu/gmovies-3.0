//
//  GMoviesAPIManager.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 22/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import CoreLocation

class GMoviesAPIManager: NSObject {
    
    //MARK: GMovies API
    
    class func checkOperationExistence(identifier : String) -> Bool {
        if GMoviesQueueManager.sharedInstance.getQueue().operations.filter({ (operation) -> Bool in
            return (operation as! GMoviesOperation).identifier == identifier
        }).count > 0 {
            return true
        }
        
        return false
    }
    
    class func authenticateAPI(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "tokenOperation") == true {
            return
        }
        
        let tokenOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/token/get", parameters: nil, completion: completion)
        tokenOperation.identifier = "tokenOperation"
        tokenOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: tokenOperation)
    }
    
    class func getCarousel(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getBannersHome") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/carousel", parameters: nil, completion: completion)
        moviesOperation.identifier = "getBannersHome"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getMovies(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getMoviesHome") == true {
            print("found duplicate")
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/movies", parameters: ["status" : "", "theaters" : ""], httpMethod: "POST", completion: completion)
        moviesOperation.identifier = "getMoviesHome"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getMoviesNowShowing(theaterId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getMoviesNowShowing") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/movies", parameters: ["status" : "now_showing", "theaters" : theaterId], httpMethod: "POST", completion: completion)
        moviesOperation.identifier = "getMoviesNowShowing"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getMoviesComingSoon(theaterId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getMoviesComingSoon") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/movies", parameters: ["status" : "coming_soon", "theaters" : theaterId], httpMethod: "POST", completion: completion)
        moviesOperation.identifier = "getMoviesComingSoon"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getTheaters(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        if checkOperationExistence(identifier: "getTheaters") == true {
            return
        }
        
        let theaterOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/theaters", parameters: ["" : ""], httpMethod: "POST", completion: completion)
        theaterOperation.identifier = "getTheaters"
        theaterOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: theaterOperation)
    }
    
    class func getTheaters(coords : CLLocationCoordinate2D, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getTheatersNearby") == true {
            return
        }
        
        let theaterOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/theaters/nearby", parameters: ["long" : coords.longitude, "lat" : coords.latitude], httpMethod: "POST", completion: completion)
        theaterOperation.identifier = "getTheatersNearby"
        theaterOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: theaterOperation)
    }
    
    class func downloadPoster(urlString : String, completion:@escaping (_ error : Error?, _ image : UIImage?) -> Void) {
        let moviesOperation = GMoviesImageOperation(URL: urlString, completion: completion)
        
        GMoviesQueueManager.sharedInstance.addDownloadOperation(op: moviesOperation)
    }
    
    class func getMovieDetail(movieId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getMovieDetail") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/movies/\(movieId)", parameters: nil,  completion: completion)
        moviesOperation.identifier = "getMovieDetail"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getSpotlight(page : Int, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        if checkOperationExistence(identifier: "getSpotlight") == true {
            return
        }
        
        let theaterOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/spotlight/list/\(page)", parameters: nil, httpMethod: "POST", completion: completion)
        theaterOperation.identifier = "getSpotlight"
        theaterOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: theaterOperation)
    }
    
    class func getMovieSchedule(movieId : String, theaterId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getMovieSchedule") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/movies/\(movieId)/schedule/ios/filter", parameters: ["theater" : theaterId], httpMethod: "POST", completion: completion)
        
        print(theaterId)
        
        moviesOperation.identifier = "getMovieSchedule"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getMovieSeatMap(scheduleId : String, theaterId : String, cinemaId : String, time : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getSeatMap") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/movies/seatmap", parameters: ["cinema" : cinemaId, "schedule" : scheduleId, "theater" : theaterId, "time" : time], httpMethod: "POST", completion: completion)
        moviesOperation.identifier = "getSeatMap"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getMovieSeatMap(promoId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getSeatMap") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/block_screening/\(promoId)/seatmap", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "getSeatMap"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getWatchList(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getWatchList") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/watch_list", parameters: nil,  completion: completion)
        moviesOperation.identifier = "getWatchList"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func addToWatchList(movieId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "addToWatchlist") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/watch_list/\(movieId)/add", parameters: nil,  completion: completion)
        moviesOperation.identifier = "addToWatchlist"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func removeFromWatchList(movieId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "removeFromWatchlist") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/watch_list/\(movieId)/delete", parameters: nil,  completion: completion)
        moviesOperation.identifier = "removeFromWatchlist"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getPreferred(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getPreferred") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/preferred_cinema", parameters: nil,  completion: completion)
        moviesOperation.identifier = "getPreferred"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func addToPreferred(theaterId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "addToPreferred") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/preferred_cinema/\(theaterId)/add", parameters: nil,  completion: completion)
        moviesOperation.identifier = "addToPreferred"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func removeFromPreferred(theaterId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "removeFromPreferred") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/preferred_cinema/\(theaterId)/delete", parameters: nil,  completion: completion)
        moviesOperation.identifier = "removeFromPreferred"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func sendPayment(paymentOptionId : String, params : NSDictionary, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "sendPayment") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/payment/\(paymentOptionId)", parameters: params, httpMethod: "POST", completion: completion)
        moviesOperation.identifier = "sendPayment"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func checkState(paymentOptionId : String, transactionId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "checkState") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/payment/\(paymentOptionId)/\(transactionId)/state", parameters: nil, completion: completion)
        moviesOperation.identifier = "checkState"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getTicket(paymentOptionId : String, transactionId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getTicket") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/payment/\(paymentOptionId)/\(transactionId)", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "getTicket"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getTickets(page : Int, type : String = "all", completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getTickets") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/tickets/\(type == "all" ? "list" : type)/\(page)", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "getTickets"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getReviews(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getReviews") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/reviews", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "getReviews"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func sendReview(movieId : String, comment : String, rating : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "sendReview") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/reviews/\(movieId)/submit", parameters: ["comment" : comment, "rating" : rating], httpMethod: "POST", completion: completion)
        moviesOperation.identifier = "sendReview"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func sendConcern(name : String, email : String, subject : String, category : String, message : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "sendConcern") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/support/submit", parameters: ["name" : name, "email" : email, "subject" : subject, "category" : category, "message" : message], httpMethod: "POST", completion: completion)
        moviesOperation.identifier = "sendConcern"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func getMessages(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getMessages") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/messages", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "getMessages"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func openMessage(msgId : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "OpenMessage") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServer)/v1/user/messages/\(msgId)/open", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "OpenMessage"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func loadCustomEndpoint(endpoint : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        //result?.value(forKeyPath: "data.url") as! String
        if checkOperationExistence(identifier: "customEndpoint") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "\(Constants.gMoviesServerIP)/\(endpoint)", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "customEndpoint"
        moviesOperation.headers = ["gmovies-platform" : "ios"]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    
    class func getServices(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "getServices") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "https://globe-gmovies.appspot.com/api/3/services/?platform=ios", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "getServices"
        moviesOperation.headers = ["X-GMovies-DeviceId" : Constants.UUID]
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    class func checkAppStoreVersion(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        if checkOperationExistence(identifier: "checkAppStoreVersion") == true {
            return
        }
        
        let moviesOperation = GMoviesDataOperation(url: "http://itunes.apple.com/lookup?id=582901861", parameters: nil, httpMethod: "GET", completion: completion)
        moviesOperation.identifier = "checkAppStoreVersion"
        
        GMoviesQueueManager.sharedInstance.addOperation(op: moviesOperation)
    }
    
    //MARK: Core API
    
    class func login(mobileNum : String, smsEmail : String,completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/login/send/otp", parameters: ["mobile" : mobileNum, "device_type" : "ios", "send_by" : smsEmail], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func verifyOTP(mobileNum : String, pin : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/login/verify/otp", parameters: ["mobile" : mobileNum, "device_type" : "ios", "pin" : pin, "device_id" : Constants.UUID, "device_token" : Constants.deviceToken ?? ""], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func gcashLogin(mobileNum : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/sms/otp/generate", parameters: ["mobile" : mobileNum, "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func verifyGCashOTP(mobileNum : String, pin : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/sms/otp/verify", parameters: ["mobile" : mobileNum, "device_type" : "ios", "pin" : pin], httpMethod: "POST", completion: completion)
        
        print(["mobile" : mobileNum, "device_type" : "ios", "pin" : pin])
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func verifyFbId(fbid : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/user/verify/facebook", parameters: ["fbid" : fbid, "device_id" : Constants.UUID, "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func register(mobileNum : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/register/send/otp", parameters: ["mobile" : mobileNum, "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func register(mobileNum : String, pin : String, firstName : String, lastName : String, email : String, fbid : String = "0", loyalty:Bool, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/register/verify/otp", parameters: ["mobile" : mobileNum, "device_type" : "ios", "pin" : pin, "device_id" : Constants.UUID, "device_token" : Constants.deviceToken ?? "", "first_name" : firstName, "last_name" : lastName, "email" : email, "fbid" : fbid, "loyalty" : loyalty], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func editPhoto(lbid : String, photo : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/profile/photo", parameters: ["photo" : photo, "token" : lbid, "type" : "base64", "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func removePhoto(lbid : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/profile/photo/remove", parameters: ["token" : lbid, "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func editNotification(action : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/ios/notification/set/\(action)", parameters: ["device_token" : Constants.deviceToken ?? ""], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func editProfile(lbid : String, mobileNum : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/profile/otp/send", parameters: ["mobile" : mobileNum, "token" : lbid, "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func editProfile(lbid : String, mobileNum : String, pin : String, firstName : String, lastName : String, email : String, fbid : String = "0", bday : String = "0000-00-00", completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/profile/edit", parameters: ["token" : lbid, "mobile" : mobileNum, "device_type" : "ios", "pin" : pin, "device_id" : Constants.UUID, "device_token" : Constants.deviceToken ?? "", "first_name" : firstName, "last_name" : lastName, "email" : email, "fbid" : fbid, "bday" : bday], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func delete(mobileNum : String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/user/delete/\(mobileNum)", parameters: nil, completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func verifyPromoCode(userId : String, promoCode : String, mobile : String, seatCount: String, theaterId : String, movieId : String, ticketPrice: String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/claim-code/verify", parameters: ["user_id" : userId, "mobile" : mobile, "seat_count" : seatCount, "theater" : theaterId, "movie" : movieId, "ticket_price" : ticketPrice , "code": promoCode, "device_id" : Constants.UUID, "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func verifyBankPromoCode(userId : String, promoCode : String, mobile : String, seatCount: String, theaterId : String, bankPromoId : String, ticketPrice: String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/\(bankPromoId)/verify", parameters: ["user_id" : userId, "mobile" : mobile, "seat_count" : seatCount, "theater" : theaterId, "code": promoCode, "ticket_price": ticketPrice, "device_id" : Constants.UUID, "device_type" : "ios"], httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func getMaintenanceMode(completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let coreOperation = LBAPIDataOperation(url: "\(Constants.LBAPIServer)/gmovies/maintenance", parameters: nil, httpMethod: "GET", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func checkDidRate(lbid:String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {

        let urlString = "\(Constants.LBAPIServer)/gmovies/feedback/\(lbid)/verify"
        
        let coreOperation = LBAPIDataOperation(url: urlString , parameters: nil, httpMethod: "GET", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func send (rating:Int, comment:String, userId:String, mobile:String, transactionId:String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let urlString = "\(Constants.LBAPIServer)/gmovies/feedback/submit"
        
        let parameters:[String : Any] = ["user_id":userId, "mobile":mobile, "comment":comment, "rating":rating, "txid":transactionId, "device_type":"ios"]
        
        let coreOperation = LBAPIDataOperation(url: urlString , parameters: parameters as NSDictionary?, httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func verifyPromoCode2(mobile: String, userId: String, promoCode: String, theaterId: String, seatCount: Int, movie: String, ticketPrice: Int, rushId: String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let urlString = "\(Constants.LBAPIServer)/gmovies/claim-code/promo-verify"
        
        let parameters:[String : Any] = ["user_id":userId, "mobile":mobile, "code":promoCode, "theater":theaterId, "seat_count":seatCount, "movie":movie, "ticket_price": ticketPrice, "rush_id":rushId, "device_type": "ios"]
        
        let coreOperation = LBAPIDataOperation(url: urlString, parameters: parameters as NSDictionary?, httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
    
    class func verifyPromoCode2(mobile: String, userId: String, promoCode: String, theaterId: String, seatCount: Int, movie: String, ticketPrice: Int, rushId: String, variant: String, completion:@escaping (_ error : Error?, _ result : NSDictionary?) -> Void) {
        
        let urlString = "\(Constants.LBAPIServer)/gmovies/claim-code/promo-verify"
        
        let parameters:[String : Any] = ["user_id":userId, "mobile":mobile, "code":promoCode, "theater":theaterId, "seat_count":seatCount, "movie":movie, "ticket_price": ticketPrice, "rush_id":rushId, "variant": variant, "device_type": "ios", "api_type": "new"]
        
        let coreOperation = LBAPIDataOperation(url: urlString, parameters: parameters as NSDictionary?, httpMethod: "POST", completion: completion)
        
        GMoviesQueueManager.sharedInstance.addOperation(op: coreOperation)
    }
}
