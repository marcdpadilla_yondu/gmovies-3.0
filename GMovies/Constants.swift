//
//  Constants.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 7/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    public enum BuildType: Int {
        case Development
        case QualityAssurance
        case Production
    }
    
    static let BUILD_TYPE: BuildType = .Production
    
    static let LBAPIServerIP_PROD = "http://core.digitalventures.ph"
    static let LBAPIServerIP_QA = "http://core.qa.digitalventures.ph"
    static let LBAPIServerIP_DEV = "http://core.dev.digitalventures.ph"
    
    static let GMoviesServerIP_PROD = "http://api.gmovies.ph"
    static let GMoviesServerIP_QA = "http://gmovies.api.qa.digitalventures.ph"
    static let GMoviesServerIP_DEV = "http://gmovies.api.dev.digitalventures.ph"
    
    class var GoogleMapsKey : String {
        return "AIzaSyDCGXNnt6F-qjJqRIL1AR1E5D3KFMVnRxY"
    }
    
    class var UUID : String {
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    
    class var LBAPIServerIP : String {
        
        switch BUILD_TYPE {
        case .Development:
            return LBAPIServerIP_DEV
        case .QualityAssurance:
            return LBAPIServerIP_QA
        case .Production:
            return LBAPIServerIP_PROD
        }
    }
    
    class var LBAPIServer : String {
        return "\(LBAPIServerIP)/api"
    }
    
    class var gMoviesWebIP : String {
        return "http://www.gmovies.ph"
//        return "http://GMovies-Web-1750847780.ap-southeast-1.elb.amazonaws.com"
    }
    
    class var gMoviesServerIP : String {
        
        switch BUILD_TYPE {
        case .Development:
            return GMoviesServerIP_DEV
        case .QualityAssurance:
            return GMoviesServerIP_QA
        case .Production:
            return GMoviesServerIP_PROD
        }
    }
    
    class var LBAPIToken : String {
        return "Mc78beJQcwdEbWWcBqkF4yT7gQ38eWVH2Ws5VeUJT6jEpuGYmYDj4jYAwCT3mjpRr6QW2b6S2VZsS7QzuMcJPhzF"
    }
    
    class var gMoviesServer : String {
        return "\(gMoviesServerIP)/api"
    }
    
    class var GMoviesAPIToken : String? {
        return UserDefaults.standard.string(forKey: "GMoviesAPIToken")
    }
    
    class var deviceToken : String? {
        return UserDefaults.standard.string(forKey: "deviceToken")
    }
    
    class var patience : Int {
        return 8
    }
    
    class var maxRetry : Int {
        return 3
    }
    
    class var maxSeatsPerTransaction : Int {
        return 10
    }
}

public enum SeatMapState : Int {
    case taken
    case available
    case selected
    case aisle
    case disabled
}
