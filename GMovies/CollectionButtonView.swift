//
//  CollectionButtonView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 10/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class CollectionButtonView: UICollectionReusableView {
    
    @IBOutlet weak var okayButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        okayButton.layer.cornerRadius = 3.0
    }
    
}
