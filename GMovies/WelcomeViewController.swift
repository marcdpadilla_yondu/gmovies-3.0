//
//  WelcomeViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 25/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class WelcomeViewController: UIViewController, UITextFieldDelegate, MTabBarProtocol {

    @IBOutlet weak var facebookButton: MButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    
    private var mobileFieldVisible: Bool = false
    
    @IBOutlet weak var facebookCenterYConstraint: NSLayoutConstraint!
    private var facebookButtonCenter : CGFloat?
    
    @IBOutlet weak var loginContainer: UIView!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var prompt: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var prefix: UILabel!
    
    @IBOutlet weak var labelFieldConstraint : NSLayoutConstraint!
    @IBOutlet weak var prefixLeftConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var logoBottom: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var progress: UIProgressView!
    
    
    
    var placeholder : String?
    var pre : String?
    var maxLength : Int = 0 //infinite
    
    var isExpanded : Bool = false
    
    private var fromFacebook : Bool = false
    private var myAccountTab : MTabBarViewController?
    
    var modalMode : Bool = false
    var joinReelRewardsAutomatically = false
    var previousViewController : UIViewController?
    var nextViewController : UIViewController?
    
    private let paymentsdk : Ipay = Ipay()
    private let payment : IpayPayment = IpayPayment()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginContainer.alpha = 0
//        if FBSDKAccessToken.current() != nil {
//            FBSDKLoginManager().logOut()
//            UserDefaults.standard.set(false, forKey: "loggedin")
//            UserDefaults.standard.set(nil, forKey: "facebookDetails")
//            UserDefaults.standard.set(nil, forKey: "user")
//            UserDefaults.standard.synchronize()
//        }
        
//        if UserDefaults.standard.bool(forKey: "loggedin") == true {
//            self.navigationController?.setNavigationBarHidden(true, animated: true)
//            loadHomeStoryboard(false)
//        }
        
        facebookButton.alpha = 0.0
        loginButton.alpha = 0.0
        createButton.alpha = 0.0
        guestButton.alpha = 0.0
        
//        loaderView.animationImages = UIManager.paymentLoader()
        
        if modalMode == true {
            backButton.isHidden = false
            backgroundImageView.image = nil
            guestButton.isHidden = true
            
            logoBottom.constant = -103.5
            self.view.layoutIfNeeded()
            self.view.isUserInteractionEnabled = true
            
            facebookButton.alpha = 1.0
            loginButton.alpha = 1.0
            createButton.alpha = 1.0
            guestButton.alpha = 1.0
            progress.isHidden = true
            
        } else {
            backButton.isHidden = true
            backgroundImageView.image = UIImage(named: "splash-bg")
            
//            loaderView.startAnimating()
//            loaderView.isHidden = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveNotification), name: NSNotification.Name(rawValue: "didInitialize"), object: nil)
        
        (UIApplication.shared.delegate as! AppDelegate).addObserver(self, forKeyPath: "loadProgress", options: .new, context: nil)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidAppear), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        AnalyticsHelper.sendScreenShowEvent(name: "splash")
        
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.SplashScreen)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: nil, object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        inputField.text = ""
        
        isExpanded = false
        
        self.loginContainer.layoutIfNeeded()
        
        self.labelFieldConstraint.constant = 0
        self.inputField.attributedPlaceholder = NSAttributedString(string: "")
        
        if self.pre == nil {
            self.prefixLeftConstraint.constant = 0.0
        } else {
            self.prefixLeftConstraint.constant = 14.0
            self.prefix.alpha = 0.0
        }
        
        self.loginContainer.layoutIfNeeded()
        
        if self.pre != nil {
            self.prefix.isHidden = true
        }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loadProgress" {
            let progress = change![NSKeyValueChangeKey.newKey] as! Int
            DispatchQueue.main.async {
                self.progress.setProgress(Float(Float(progress)/5.0), animated: true)
            }
        }
    }
    
    fileprivate func configureView() {
        
        UIManager.roundify(facebookButton)
        UIManager.roundify(loginButton)
        UIManager.buttonBorder(button: createButton, width: 0.25, color: UIColor(netHex: 0xbac2da))
        UIManager.roundify(createButton)
        
        prompt.isHidden = true
        label.text = "MOBILE NO."
        
        separator.layer.borderWidth = 0.25
        separator.layer.borderColor = UIColor(netHex: 0x777f99).cgColor
        
        placeholder = "917XXXXXXX"
        pre = "+63"
        prefix.text = pre
        maxLength = 10
    }
    
    @objc private func keyboardDidAppear(notif : Notification) {
        let kbFrame = (notif.userInfo as! NSDictionary).value(forKey: UIKeyboardFrameEndUserInfoKey) as! CGRect
        let intersection = loginButton.frame.intersection(kbFrame)
        
        UIView.animate(withDuration: 0.250, delay: 0.0, options: .curveEaseOut, animations: { () in
            self.view.frame.origin.y -= intersection.size.height + 10.0
        }, completion: { (_) in
            
        })
    }
    
    @objc private func keyboardDidDisappear(notif : Notification) {
        UIView.animate(withDuration: 0.250, delay: 0.0, options: .curveEaseOut, animations: { () in
            self.view.frame.origin.y = 0.0
        }, completion: { (_) in
            
        })
    }
    
    func didReceiveNotification(notif : Notification) {
        if notif.name.rawValue == "didInitialize" {
            self.progress.isHidden = true
            if UserDefaults.standard.bool(forKey: "maintenance") == false {
                if UserDefaults.standard.bool(forKey: "loggedin") == true {
                    self.navigationController?.setNavigationBarHidden(true, animated: true)
                    if UserDefaults.standard.bool(forKey: "showTutorial") == true {
                        performSegue(withIdentifier: "showTutorial", sender: nil)
                    } else {
                        loadHomeStoryboard(false)
                    }
                }
                
                DispatchQueue.main.async {
                    self.view.layoutIfNeeded()
                    self.logoBottom.constant = -103.5
                    UIView.animate(withDuration: 0.300, delay: 0.0, options: .curveEaseOut, animations: { 
                        self.view.layoutIfNeeded()
                        self.facebookButton.alpha = 1.0
                        self.loginButton.alpha = 1.0
                        self.createButton.alpha = 1.0
                        self.guestButton.alpha = 1.0
                    }, completion: { (_) in
                        
                    })
                }
            } else {
                let alert = UIAlertController(title: "Maintenance Mode", message: "Our servers are currently undergoing maintenance", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (frame) in
                    
                })
                
                alert.addAction(okAction)
                present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func loginAction(_ sender: AnyObject?) {
        
        UserDefaults.standard.setValue(nil, forKey: "facebookDetails")
        UserDefaults.standard.synchronize()
        FBSDKLoginManager().logOut()
        
        GAnalyticsHelper.sendEvent(category: GAEventCategory.SignUp, action: GASignUpCategory.Login)
        
        AnalyticsHelper.sendButtonEvent(name: "login_mobile")
        
        if mobileFieldVisible == true {
            if validateMobileNumber(mobileNumber: inputField.text!) {
                login()
            }
        } else {
            mobileFieldVisible = true
            
            view.layoutIfNeeded()
            facebookButtonCenter = facebookCenterYConstraint.constant
            facebookCenterYConstraint.constant = 0.0
            
            UIView.animate(withDuration: 0.250, delay: 0, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
                self.facebookButton.alpha = 0
                self.createButton.alpha = 0
                self.guestButton.alpha = 0
                self.loginContainer.alpha = 1
                if self.modalMode == false {
                    self.backgroundImageView.image = UIImage(named: "splash-bg")!.applyBlurWithRadius(blurRadius: 8.0, tintColor: nil, saturationDeltaFactor: 1.0)
                }
                }, completion: { (_) in
                    self.inputField.becomeFirstResponder()
            })
        }
        
    }
    
    @IBAction func createAccountAction(_ sender: AnyObject?) {
        
        UserDefaults.standard.setValue(nil, forKey: "facebookDetails")
        UserDefaults.standard.synchronize()
        FBSDKLoginManager().logOut()
        fromFacebook = false
        
        AnalyticsHelper.sendButtonEvent(name: "create_acount_mobile")
        
        GAnalyticsHelper.sendEvent(category: GAEventCategory.SignUp, action: GASignUpCategory.CreateAccount)
        
        performSegue(withIdentifier: "showCreateAccount", sender: nil)
    }
    
    
    @IBAction func tapAction(_ sender: AnyObject) {
        
        if facebookButtonCenter == nil {
            return
        }
        
        view.layoutIfNeeded()
        facebookCenterYConstraint.constant = facebookButtonCenter!
        mobileFieldVisible = false
        inputField.resignFirstResponder()
        inputField.text = ""
        
        UIView.animate(withDuration: 0.250, delay: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            self.facebookButton.alpha = 1
            self.createButton.alpha = 1
            self.guestButton.alpha = 1
            self.loginContainer.alpha = 0
            if self.modalMode == false {
                self.backgroundImageView.image = UIImage(named: "splash-bg")
            }
            
            }, completion: { (_) in
                if self.modalMode == false {
                    self.backgroundImageView.image = UIImage(named: "splash-bg")
                }
        })
    }
    
    
    @IBAction func guestButtonTapped(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "login_guest")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.SignUp, action: GASignUpCategory.Guest)
        UserDefaults.standard.setValue(nil, forKey: "facebookDetails")
        UserDefaults.standard.synchronize()
        FBSDKLoginManager().logOut()
        loadHomeStoryboard(true)
    }
    
    @IBAction func facebookButtonAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "facebook_button")
        
        GAnalyticsHelper.sendEvent(category: GAEventCategory.SignUp, action: GASignUpCategory.Facebook)
        
        UserDefaults.standard.setValue(nil, forKey: "facebookDetails")
        UserDefaults.standard.synchronize()
        FBSDKLoginManager().logOut()
        
        if FBSDKAccessToken.current() == nil {
            FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: { (result, error) in
                if error == nil {
                    if result?.isCancelled == false {
                        self.verifyFacebookId(fbid: FBSDKAccessToken.current().userID)
                    } else {
                        FBSDKLoginManager().logOut()
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        } else {
            self.verifyFacebookId(fbid: FBSDKAccessToken.current().userID)
        }
    }
    
    private func verifyFacebookId(fbid : String) {
        GMoviesAPIManager.verifyFbId(fbid: fbid) { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKeyPath: "response.status") as! Int
                if status == 0 {
                    self.fromFacebook = true
                    self.getUserDetailsFromFacebook()
                } else {
                    
                    let mobile = result?.value(forKeyPath: "response.user.mobile") as? String
                    
                    if mobile != nil {
                        if mobile?.characters.count == 0 || mobile == "0" {
                            self.fromFacebook = true
                            self.getUserDetailsFromFacebook()
                        } else {
                            
                            let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                            AccountHelper.shared.saveUser(data: userData)
                            
                            UserDefaults.standard.setValue(true, forKey: "loggedin")
                            
                            NotificationCenter.default.post(name: AccountHelper.shared.NotificationDidLoginToGMovies, object: nil)
                            
                            let rushUserData = result?.value(forKeyPath: "response.rush") as? NSDictionary
                            
                            if let responseCode = rushUserData?.value(forKey: "error_code") as? String {
                                if responseCode == RushAPIResponseCode.successful.rawValue {
                                    AccountHelper.shared.currentUser?.rushId = rushUserData?.value(forKeyPath: "data.uuid") as? String
                                    UserDefaults.standard.set(true, forKey: "registered_rush")
                                    AccountHelper.shared.saveUser()
                                    NotificationCenter.default.post(name: AccountHelper.shared.NotificationDidRegisterToRush, object: nil)
                                }
                            }
                            
                            UserDefaults.standard.synchronize()
                            
                            self.checkPhoto()
                            
                            
                            if AccountHelper.shared.isRegisteredInRush {
                                self.navigationController?.setNavigationBarHidden(true, animated: true)
                                self.goToNextScreen()
                            }else {
                                
                                if !AccountHelper.shared.didShowJoinLoyaltyProgram {
                                    
                                    let loyaltyVC = JoinLoyaltyProgramViewController()
                                    loyaltyVC.showJoinNowButton = true
                                    loyaltyVC.showNotNowButton = true
                                    loyaltyVC.delegate = self
                                    self.show(loyaltyVC, sender: self)
                                    AccountHelper.shared.setDidShowJoinLoyaltyProgram(show: true)
                                }else {
                                    self.goToNextScreen()
                                }
                            }
                        }
                    } else {
                        
                        let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                        AccountHelper.shared.saveUser(data: userData)
                        
                        UserDefaults.standard.setValue(true, forKey: "loggedin")
                        NotificationCenter.default.post(name: AccountHelper.shared.NotificationDidLoginToGMovies, object: nil)
                        
                        let rushUserData = result?.value(forKeyPath: "response.rush") as? NSDictionary
                        
                        if let responseCode = rushUserData?.value(forKey: "error_code") as? String {
                            if responseCode == RushAPIResponseCode.successful.rawValue {
                                AccountHelper.shared.currentUser?.rushId = rushUserData?.value(forKeyPath: "data.uuid") as? String
                                UserDefaults.standard.set(true, forKey: "registered_rush")
                                AccountHelper.shared.saveUser()
                                NotificationCenter.default.post(name: AccountHelper.shared.NotificationDidRegisterToRush, object: nil)
                            }
                        }
                        
                        UserDefaults.standard.synchronize()
                        
                        self.checkPhoto()
                        if AccountHelper.shared.isRegisteredInRush {
                            self.navigationController?.setNavigationBarHidden(true, animated: true)
                            self.goToNextScreen()
                        }else {
                            
                            if !AccountHelper.shared.didShowJoinLoyaltyProgram {
                                
                                let loyaltyVC = JoinLoyaltyProgramViewController()
                                loyaltyVC.showJoinNowButton = true
                                loyaltyVC.showNotNowButton = true
                                loyaltyVC.delegate = self
                                self.show(loyaltyVC, sender: self)
                                AccountHelper.shared.setDidShowJoinLoyaltyProgram(show: true)
                            }else {
                                self.goToNextScreen()
                            }
                        }
                    }
                }
                
            } else {
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                DispatchQueue.main.async {
                    UserDefaults.standard.setValue(nil, forKey: "facebookDetails")
                    UserDefaults.standard.synchronize()
                    FBSDKLoginManager().logOut()
                    appDelegate.showBanner(message: "Server Error", isPersistent: false)
                }
                
            }
        }
    }
    
    private func login() {
        
        AccountHelper.shared.requestOTP(mobileNumber: inputField.text!, successful: { data in
            
            self.fromFacebook = true
            self.performSegue(withIdentifier: "showOTP", sender: nil)
            
        }) { (error) in
            
            self.prompt.text = error
            self.prompt.isHidden = false
            self.loginContainer.shake()
        }
    }
    
    private func checkPhoto() {
        let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
        if (user.value(forKeyPath: "photo.mobile") as! String).characters.count == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let req : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "picture.type(large)"])
            req.start { (conn, result, error) in
                if error == nil {
                    let url = (result as! NSDictionary).value(forKeyPath: "picture.data.url") as! String
                    DispatchQueue.main.async {
                        GMoviesAPIManager.downloadPoster(urlString: url, completion: { (error, image) in
                            if error == nil {
                                let imgData = UIImageJPEGRepresentation(image!, 1.0)
                                let user : NSDictionary? = UserDefaults.standard.value(forKey: "user") as? NSDictionary
                                let newUser : NSMutableDictionary = user?.mutableCopy() as! NSMutableDictionary
                                let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                photo.setValue(imgData?.base64EncodedString(), forKey: "mobile")
                                newUser.setValue(photo, forKeyPath: "photo")
                                UserDefaults.standard.set(newUser, forKey: "user")
                                UserDefaults.standard.synchronize()
                                
                                DispatchQueue.main.async {
                                    GMoviesAPIManager.editPhoto(lbid: user?.value(forKey: "lbid") as! String, photo: user?.value(forKeyPath: "photo.mobile") as! String, completion: { (error, result) in
                                        
                                        print("woof!")
                                        
                                    })
                                }
                                
                            } else {
                                print(error!.localizedDescription)
                            }
                        })
                    }
                } else {
                    print(error!.localizedDescription)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        } else if (user.value(forKeyPath: "photo.mobile") as! String).characters.count != 0 {
            
        }
    }
    
    private func getUserDetailsFromFacebook() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let req : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "first_name,last_name,email,picture.type(large)"])
        req.start { (conn, result, error) in
            if error == nil {
                UserDefaults.standard.setValue(result!, forKey: "facebookDetails")
                UserDefaults.standard.synchronize()
                self.performSegue(withIdentifier: "showCreateAccount", sender: nil)
            } else {
                print(error!.localizedDescription)
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    fileprivate func loadHomeStoryboard(_ animated : Bool, selectRewards: Bool = false) {
        var storyboard : UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let tab : MTabBarViewController = storyboard.instantiateInitialViewController() as! MTabBarViewController
        
        let home : UINavigationController = storyboard.instantiateViewController(withIdentifier: "HomeViewNavigation") as! UINavigationController
        home.title = "Movies"
        home.tabImageOn = UIImage(named: "movies-active")
        home.tabImageOff = UIImage(named: "movies")
        home.tabTextColorOn = UIColor(netHex: 0x435082)
        home.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Cinemas", bundle: nil)
        let cinemas : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        cinemas.title = "Cinemas"
        cinemas.tabImageOn = UIImage(named: "cinemas-active")
        cinemas.tabImageOff = UIImage(named: "cinemas")
        cinemas.tabTextColorOn = UIColor(netHex: 0x435082)
        cinemas.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Spotlight", bundle: nil)
        let spotlight : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        spotlight.title = "Spotlight"
        spotlight.tabImageOn = UIImage(named: "spotlight-active")
        spotlight.tabImageOff = UIImage(named: "spotlight")
        spotlight.tabTextColorOn = UIColor(netHex: 0x435082)
        spotlight.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Rewards", bundle: nil)
        let vc2 : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        vc2.title = "Rewards"
        vc2.tabImageOn = UIImage(named: "myrewards-active")
        vc2.tabImageOff = UIImage(named: "myrewards")
        vc2.tabTextColorOn = UIColor(netHex: 0x435082)
        vc2.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
        let myAccount : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        myAccount.title = " "
        myAccount.tabImageOn = UIImage(named: "myaccount-active")
        myAccount.tabImageOff = UIImage(named: "myaccount")
        myAccount.tabTextColorOn = UIColor(netHex: 0x435082)
        myAccount.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        myAccountTab = (myAccount.topViewController as? MTabBarViewController)
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            myAccountTab?.initialViewControllerIndex = 2
        }
        myAccountTab!.delegate = self
        myAccountTab!.isIndicatorEnabled = true
        myAccountTab!.indicatorHugsContent = true
        let ticketList : TicketsListViewController = storyboard.instantiateViewController(withIdentifier: "TicketListView") as! TicketsListViewController
        myAccountTab!.title = "My Account"
        ticketList.tabImageOn = UIImage(named: "tickets-active")
        ticketList.tabImageOff = UIImage(named: "tickets")
        
        let watchList : WatchListViewController = storyboard.instantiateViewController(withIdentifier: "WatchListView") as! WatchListViewController
        watchList.tabImageOn = UIImage(named: "watchlist-active")
        watchList.tabImageOff = UIImage(named: "watchlist-1")
        
        let profileView : ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileView") as! ProfileViewController
        profileView.tabImageOn = UIImage(named: "profile-active")
        profileView.tabImageOff = UIImage(named: "profile")
        
        if selectRewards {
            tab.initialViewControllerIndex = 3
        }
        
        myAccountTab!.viewControllers = [ticketList, watchList, profileView]
        
        tab.viewControllers = [home, cinemas, spotlight, vc2, myAccount]
        
        self.navigationController?.pushViewController(tab, animated: animated)
    }
    
    //MARK : Login form
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        loginContainer.layoutIfNeeded()
        
        isExpanded = true
        
        prefix.alpha = 0.0
        
        UIView.animate(withDuration: 0.150, animations: {
            self.labelFieldConstraint.constant = -self.label.frame.size.height-5.0
            if self.pre == nil {
                self.prefixLeftConstraint.constant = 0
            } else {
                self.prefixLeftConstraint.constant = 14.0
                self.prefix.isHidden = false
                self.prefix.alpha = 1.0
            }
            self.loginContainer.layoutIfNeeded()
            
            }, completion: { (_) in
                
                if self.placeholder != nil {
                    self.inputField.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.lightGray, NSFontAttributeName : UIFont(name: "OpenSans", size: 11.0)!])
                }
        })
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (inputField.text?.characters.count)! > 0 {
            return
        }
        
        isExpanded = false
        
        self.loginContainer.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.150, animations: {
            self.labelFieldConstraint.constant = 0
            self.inputField.attributedPlaceholder = NSAttributedString(string: "")
            
            if self.pre == nil {
                self.prefixLeftConstraint.constant = 0.0
            } else {
                self.prefixLeftConstraint.constant = 14.0
                self.prefix.alpha = 0.0
            }
            
            self.loginContainer.layoutIfNeeded()
            
            }, completion: { (_) in
                
                if self.pre != nil {
                    self.prefix.isHidden = true
                }
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.characters.count)! + 1 > maxLength && string != "" && maxLength > 0 {
            return false
        }
        
        prompt.text = ""
        prompt.isHidden = true
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOTP" {
            let verify : VerifyMobileViewController = segue.destination as! VerifyMobileViewController
            verify.mobileNumber = inputField.text!
            verify.fromLogin = true
            verify.modalMode = modalMode
            verify.previousViewController = previousViewController
            verify.nextViewController = nextViewController
            verify.joinLoyaltyProgramAutomatically = joinReelRewardsAutomatically
        } else if segue.identifier == "showCreateAccount" {
            let register : RegisterViewController = segue.destination as! RegisterViewController
            if fromFacebook == true {
                register.facebookDetails = UserDefaults.standard.object(forKey: "facebookDetails") as? NSDictionary
            } else {
                UserDefaults.standard.setValue(nil, forKey: "facebookDetails")
                UserDefaults.standard.synchronize()
                FBSDKLoginManager().logOut()
            }
            register.modalMode = modalMode
            register.previousViewController = previousViewController
            register.nextViewController = nextViewController
            if modalMode && !joinReelRewardsAutomatically {
                register.hideJoinLoyaltyProgramCheckbox = true
            }
            if joinReelRewardsAutomatically {
                register.disableJoinLoyaltyProgramCheckbox = true
                register.checkJoinLoyaltyProgram = true
            }
        }
    }
    
    //MARK: private methods
    
    private func validateMobileNumber(mobileNumber : String) -> Bool {
        if mobileNumber.characters.count < 10 {
            prompt.text = "Invalid mobile number"
            prompt.isHidden = false
            loginContainer.shake()
            return false
        }
        
        return true
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        
        if previousViewController == nil {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    //MARK: TabBar Delegate
    
    func shouldSelectViewControllerAtIndex(tabController: MTabBarViewController, index: Int) -> Bool {
        
        if tabController == myAccountTab {
            let loggedin : Bool = UserDefaults.standard.bool(forKey: "loggedin")
            if loggedin == false {
                if index != 2 {
                    let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginModal()
                    return false
                }
            }
        }
        
        return true
    }
    
    fileprivate func goToNextScreen(selectRewards:Bool = false)
    {
        if self.modalMode == false {
            if UserDefaults.standard.bool(forKey: "showTutorial") == true {
                
                self.performSegue(withIdentifier: "showTutorial", sender: nil)
            } else {
                self.loadHomeStoryboard(true, selectRewards: selectRewards)
            }
        } else {
            
            if self.previousViewController == nil {
                self.dismiss(animated: true, completion: nil)
            } else {
                if self.previousViewController != self.nextViewController {
                    self.previousViewController?.navigationController?.pushViewController(self.nextViewController!, animated: true)
                } else {
                    _ = self.previousViewController?.navigationController?.popToViewController(self.previousViewController!, animated: true)
                }
            }
        }
    }
}

extension WelcomeViewController: JoinLoyaltyProgramViewControllerProtocol
{
    func onJoinNowClicked() {
        
        AccountHelper.shared.registerToRush(successful: { data in
            
            let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
            successRegistration.delegate = self
            successRegistration.userName = AccountHelper.shared.currentUser?.firstName
            successRegistration.dismissButtonTitle = "Go to Rewards"
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.show(successRegistration, sender: self)
            
        }) { (error) in
            self.goToNextScreen()
        }
    }
    
    func onNotNowClicked() {
        goToNextScreen()
    }
}

extension WelcomeViewController: LoyaltyRegistrationSuccessVCDelegate
{
    func didClickDismiss(sender: LoyaltyRegistrationSuccessVC) {
        self.loadHomeStoryboard(true, selectRewards: true)
    }
}
