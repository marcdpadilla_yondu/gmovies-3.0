//
//  GMoviesOperation.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 21/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class GMoviesOperation: Operation {
    
    private var _ready : Bool = true
    private var _isExecuting : Bool = false
    private var _isFinished : Bool = false
    private var _isCancelled : Bool = false
    private var _isAsynchronous : Bool = true
    
    var identifier : String?
    
    override var isReady: Bool {
        get {
            return self._ready
        }
        
        set (ready) {
            if _ready != ready {
                self.willChangeValue(forKey: NSStringFromSelector(#selector(getter: isReady)))
                self._ready = ready
                self.didChangeValue(forKey: NSStringFromSelector(#selector(getter: isReady)))
            }
        }
    }
    
    override var isExecuting: Bool {
        get {
            return self._isExecuting
        }
        
        set (executing) {
            if _isExecuting != executing {
                self.willChangeValue(forKey: NSStringFromSelector(#selector(getter: isExecuting)))
                self._isExecuting = executing
                self.didChangeValue(forKey: NSStringFromSelector(#selector(getter: isExecuting)))
            }
        }
    }
    
    override var isFinished: Bool {
        get {
            return self._isFinished
        }
        
        set (finished) {
            if _isFinished != finished {
                self.willChangeValue(forKey: NSStringFromSelector(#selector(getter: isFinished)))
                self._isFinished = finished
                self.didChangeValue(forKey: NSStringFromSelector(#selector(getter: isFinished)))
            }
        }
    }
    
    override var isAsynchronous: Bool {
        get {
            return true
        }
        
        set (asynchronous) {
            if _isAsynchronous != asynchronous {
                self.willChangeValue(forKey: NSStringFromSelector(#selector(getter: isAsynchronous)))
                self._isAsynchronous = asynchronous
                self.didChangeValue(forKey: NSStringFromSelector(#selector(getter: isAsynchronous)))
            }
        }
    }
    
    override func start() {
        if self.isExecuting != true {
            self.isReady = false
            self.isExecuting = true
            self.isFinished = false
        }
    }
    
    func finish() {
        if self.isExecuting {
            self.isExecuting = false
            self.isFinished = true
        }
    }
    
}
