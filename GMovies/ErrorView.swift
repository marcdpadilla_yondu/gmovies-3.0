//
//  ErrorView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 21/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class ErrorView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var errorIcon: UIImageView!
    @IBOutlet weak var errorTitle: UILabel!
    @IBOutlet weak var errorDesc: UILabel!
    
    @IBOutlet weak var retryButton: UIButton!

    @IBOutlet weak var retryButtonWidth: NSLayoutConstraint!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        UIManager.roundify(retryButton!)
        UIManager.buttonBorder(button: retryButton!, width: 0.5, color: UIColor(netHex: 0x0090ff))
        retryButton.addTarget(self, action: #selector(ErrorView.hide), for: .touchUpInside)
    }
    
    func hide() {
        UIView.animate(withDuration: 0.150, animations: { 
            self.alpha = 0.0
            }) { (_) in
                self.removeFromSuperview()
        }
    }
}
