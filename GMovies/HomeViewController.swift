//
//  HomeViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 25/8/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import CoreLocation
import ReachabilitySwift

class HomeViewController: UITableViewController, CLLocationManagerDelegate, PreferredTheaterProtocol {
    
    
    @IBOutlet weak var headerView: UIView!
    fileprivate var homeItems : [NSObject]?
    fileprivate var bannerScrollView : BannerScrollViewController?
    
    private var nowShowingItems : [MovieModel]?
    private var comingSoonItems : [MovieModel]?
    private var blockScreeningItems : [BlockScreeningModel]?
    private var preferred : [String]?
    
    private var timer : Timer?
    private var ticker : Int = 0
    
    private var locationManager : CLLocationManager?
    
    private var loadingScreen : LoaderView?
    var errorView : ErrorView?
    
    private var hideContent : Bool = false
    private var currentPopDrop : PopDropViewController?
    private var currentPreferredTheater : TheaterModel?
    
    private var fromDeepLink : Bool = false
    private var movieObjectDeepLink : MovieModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        homeItems = []
        nowShowingItems = []
        comingSoonItems = []
        blockScreeningItems = []
        preferred = []
        
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager?.delegate = self
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
//        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        /*
        GMoviesAPIManager.getMessages { (error, result) in
            if error == nil {
                let status = result?.value(forKey: "status") as! Int
                if status == 1 {
                    let data = result?.value(forKey: "data") as! [NSDictionary]
                    var msgs : [MessageModel] = []
                    for d in data {
                        let msg = MessageModel(data: d)
                        msgs.append(msg)
                    }
                    
                    let unopened = msgs.filter({ (m) -> Bool in
                        return m.opened == false
                    })
                    
                    if unopened.count > 0 {
                        //new messages
                    } else {
//                        let tab = self.parent?.parent as! MTabBarViewController
//                        let account = tab.tabButtons?[4]
//                        account?.badgeView.badgeColor = UIManager.buttonRed
//                        account?.badgeView.outlineWidth = 1.0
//                        account?.badgeView.outlineColor = tab.tabBar.backgroundColor
//                        account?.badgeView.minDiameter = 2.0
//                        account?.badgeView.position = MGBadgePosition.centerRight
//                        account?.badgeView.displayIfZero = true
//                        account?.badgeView.textColor = UIManager.buttonRed
//                        account?.badgeView.font = UIFont(name: "OpenSans", size: 5.0)
//                        account?.badgeView.horizontalOffset = -10.0
//                        tab.tabBar.layoutIfNeeded()
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
        }
 */
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.Movies)
        AnalyticsHelper.sendScreenShowEvent(name: "movies")
        UserDefaults.standard.addObserver(self, forKeyPath: "loggedin", options: .new, context: nil)
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.reachabilityChanged), name: ReachabilityChangedNotification,object: appDelegate.reachability)
        
        downloadMovies()
        getPreferred()
        
        let movieId = UserDefaults.standard.value(forKey: "launchedWithMovieId") as? String
        let spotlightLink = UserDefaults.standard.value(forKey: "launchedWithSpotlight") as? String
        
        if fromDeepLink == true {
            return
        } else {
            if movieId != nil {
                UserDefaults.standard.set(nil, forKey: "launchedWithMovieId")
                UserDefaults.standard.synchronize()
                self.fromDeepLink = true
                GMoviesAPIManager.getMovieDetail(movieId: movieId!, completion: { (error, result) in
                    if error == nil {
                        let status = result?.value(forKey: "status") as! Int
                        if status == 1 {
                            self.movieObjectDeepLink = MovieModel(data: result?.value(forKey: "data") as! NSDictionary)
                            DispatchQueue.main.async {
                                self.performSegue(withIdentifier: "showMovieDetail", sender: nil)
                            }
                        } else {
                            
                        }
                    } else {
                        
                    }
                })
            } else if spotlightLink != nil {
                UserDefaults.standard.set(nil, forKey: "launchedWithSpotlight")
                UserDefaults.standard.synchronize()
                let tab : MTabBarViewController = self.parent?.parent as! MTabBarViewController
                let spotlight = (tab.viewControllers?[2] as! MNavigationViewController).topViewController as! SpotlightViewController
                (tab.viewControllers?[2] as! MNavigationViewController).popToRootViewController(animated: false)
                spotlight.fromBanner = true
                spotlight.spotlightUrl = spotlightLink
                tab.setSelectedViewController(index: 2, animated: true)
            } else {
                let notification = UserDefaults.standard.value(forKey: "remoteNotification") as? NSDictionary
                UserDefaults.standard.set(nil, forKey: "remoteNotification")
                UserDefaults.standard.synchronize()
                let landingType = notification?.value(forKey: "landingType") as? String
                if landingType == nil {
                    return
                }
                if landingType! == "now showing" || landingType! == "coming soon" {
                    let movieId = notification?.value(forKey: "movie_id") as! String
                    let movieName = notification?.value(forKey: "movie_name") as! String
                    self.fromDeepLink = true
                    GMoviesAPIManager.getMovieDetail(movieId: movieId, completion: { (error, result) in
                        if error == nil {
                            let status = result?.value(forKey: "status") as! Int
                            if status == 1 {
                                self.movieObjectDeepLink = MovieModel(data: result?.value(forKey: "data") as! NSDictionary)
                                DispatchQueue.main.async {
                                    self.performSegue(withIdentifier: "showMovieDetail", sender: nil)
                                }
                            } else {
                                
                            }
                        } else {
                            
                        }
                    })
                } else if landingType! == "promotions" {
                    let linkName = notification?.value(forKey: "link_name") as! String
                    let tab : MTabBarViewController = self.parent?.parent as! MTabBarViewController
                    let spotlight = (tab.viewControllers?[2] as! MNavigationViewController).topViewController as! SpotlightViewController
                    spotlight.fromBanner = true
                    spotlight.spotlightUrl = "\(Constants.gMoviesWebIP)/iframe/spotlight/\(linkName)"
                    tab.setSelectedViewController(index: 2, animated: true)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.removeObserver(self, forKeyPath: "loggedin")
        self.title = ""
        if timer != nil {
            timer?.invalidate()
            ticker = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "MOVIES"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if hideContent == true {
            return 1
        }
        return (homeItems?.count)!
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return hideContent == true ? 0 : 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : MoviesTableViewCell? = nil
        
        let t = homeItems![indexPath.section]
        
        if t.isKind(of: BlockScreeningCarouselController.self) == true {
            cell  = tableView.dequeueReusableCell(withIdentifier: "blockIdentifier", for: indexPath) as? MoviesTableViewCell
        } else {
            cell  = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MoviesTableViewCell
        }

        configureMovieCell(Cell: cell!, IndexPath: indexPath)

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        print(((UIScreen.main.bounds.size.width/3.5) * 1.48) + 38.0)
        
        if hideContent == true {
            return 0.0
        }
        
//        print(indexPath.section < 3 ? "\(indexPath.section)test1" : "\(indexPath.section)test2")
        
        return indexPath.section == 0 ? ((UIScreen.main.bounds.size.width/2.75) * 1.48) + 38.0 : ((UIScreen.main.bounds.size.width/2.75) * 1.48) + 55.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if hideContent == true {
            return 0.0
        }
        
        return 50.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if hideContent == true {
            return nil
        }
        
        let sectionView : SectionLabelTableViewCell = tableView.dequeueReusableCell(withIdentifier: "sectionHeader") as! SectionLabelTableViewCell
        
        configureSection(view: sectionView, section: section)
        
        return sectionView
    }
    
    //MARK: Private Functions
    
    @IBAction func refreshAction(_ sender: AnyObject) {
        downloadMovies()
        getPreferred()
        refreshControl?.endRefreshing()
    }
    
    private func loadErrorView(titleString : String, description : String, image: UIImage = UIImage(named: "no-internet")!) {
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = tableView.subviews.first!
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addSubview(errorView!)
        self.view.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }
    
    @objc private func reachabilityChanged(note: NSNotification) {
        
        if note.name == ReachabilityChangedNotification {
            let reachability = note.object as! Reachability
            
            if reachability.isReachable {
                hideContent = false
                homeItems?.removeAll()
                
                nowShowingItems?.removeAll()
                comingSoonItems?.removeAll()
                blockScreeningItems?.removeAll()
                
                tableView.reloadData()

                downloadMovies()
                getPreferred()
                
                errorView?.removeFromSuperview()
                errorView = nil
                
            } else {
                DispatchQueue.main.async {
                    self.hideContent = true
                    self.configureBannerView()
                    self.loadErrorView(titleString: "No Internet", description: "Please check your network settings and try again")
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc private func applicationDidBecomeActive(notif : Notification) {
        if notif.name == Notification.Name.UIApplicationDidBecomeActive {
            let movieId = UserDefaults.standard.value(forKey: "launchedWithMovieId") as? String
            if movieId != nil {
                UserDefaults.standard.set(nil, forKey: "launchedWithMovieId")
                UserDefaults.standard.synchronize()
                self.fromDeepLink = true
                GMoviesAPIManager.getMovieDetail(movieId: movieId!, completion: { (error, result) in
                    if error == nil {
                        let status = result?.value(forKey: "status") as! Int
                        if status == 1 {
                            self.movieObjectDeepLink = MovieModel(data: result?.value(forKey: "data") as! NSDictionary)
                            DispatchQueue.main.async {
                                self.performSegue(withIdentifier: "showMovieDetail", sender: nil)
                            }
                        } else {
                            
                        }
                    } else {
                        
                    }
                })
            }
        }
    }
    
//    @objc private func notificationReceived(notif: Notification) {
//        if notif.name == UserDefaults.didChangeNotification {
//            homeItems?.removeAll()
//            
//            nowShowingItems?.removeAll()
//            comingSoonItems?.removeAll()
//            
//            tableView.reloadData()
//            
//            downloadMovies()
//            print("table changed")
//        }
//
//    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loggedin" {
            homeItems?.removeAll()
            
            nowShowingItems?.removeAll()
            comingSoonItems?.removeAll()
            blockScreeningItems?.removeAll()
            
            tableView.reloadData()
            
            downloadMovies()
            getPreferred()
            print("table changed")
        }
    }
    
    @objc private func filterButtonAction(sender : UIButton) {
//        if sender.tag == 0 {
        AnalyticsHelper.sendButtonEvent(name: "movies_preferred_button")
            let preferredCinemaCount : Int = (preferred?.count)!
            var pop : PopDropViewController? = nil
        
        if UserDefaults.standard.bool(forKey: "loggedin") == true {
            pop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 88.0 + (preferredCinemaCount > 3 ? CGFloat(3) * 44.0 : CGFloat(preferredCinemaCount) * 44.0))
        } else {
            pop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 44.0 + (preferredCinemaCount > 3 ? CGFloat(3) * 44.0 : CGFloat(preferredCinemaCount) * 44.0))
        }
            currentPopDrop = pop
            let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
            let preferredTable : PreferredDropViewController = storyboard.instantiateViewController(withIdentifier: "PreferredDropDownView") as! PreferredDropViewController
            preferredTable.delegate = self
            
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            preferredTable.dataSource = appDelegate.allCinemasController?.getAllCinemas()?.filter({ (to) -> Bool in
                return self.preferred?.contains(to.theaterId!) == true
            })
        
        if preferredTable.dataSource == nil {
            GMoviesAPIManager.getPreferred { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        self.preferred?.removeAll()
                        self.preferred = result?.value(forKey: "data") as? [String]
                    }
                } else {
                    print(error!.localizedDescription)
                }
                
                DispatchQueue.main.async {
                    var offset = self.tableView.rect(forSection: sender.tag)
                    offset.origin.y += 64.0
                    offset.origin.y -= self.tableView.contentOffset.y
                    pop?.showViewControllerFromView(viewController: preferredTable, originView: sender, offset: CGPoint(x: offset.origin.x, y: offset.origin.y))
                }
            }
        } else if preferredTable.dataSource!.count == 0{
            GMoviesAPIManager.getPreferred { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        self.preferred?.removeAll()
                        self.preferred = result?.value(forKey: "data") as? [String]
                    }
                } else {
                    print(error!.localizedDescription)
                }
                DispatchQueue.main.async {
                    var offset = self.tableView.rect(forSection: sender.tag)
                    offset.origin.y += 64.0
                    offset.origin.y -= self.tableView.contentOffset.y
                    pop?.showViewControllerFromView(viewController: preferredTable, originView: sender, offset: CGPoint(x: offset.origin.x, y: offset.origin.y))
                }
            }
        } else {
            var offset = self.tableView.rect(forSection: sender.tag)
            offset.origin.y += 64.0
            offset.origin.y -= self.tableView.contentOffset.y
            pop?.showViewControllerFromView(viewController: preferredTable, originView: sender, offset: CGPoint(x: offset.origin.x, y: offset.origin.y))
        }
        
//            print("preferred \(preferredTable.dataSource)")
        
        
//        }
    }
    
    private func configureSection(view : SectionLabelTableViewCell, section : Int) {
        
        let c = homeItems![section]
        
        if c.isKind(of: MovieCarouselCollectionViewController.self) == true {
            if section == 1 {
                view.filterButtonAction.isHidden = true
                view.filterButtonAction.tag = -1
            } else if section == 0 {
                view.filterButtonAction.isHidden = false
                if currentPreferredTheater != nil {
                    view.filterButtonAction.setTitle(currentPreferredTheater!.name!, for: .normal)
                } else {
                    view.filterButtonAction.setTitle("All Cinemas", for: .normal)
                }
                
                view.filterButtonAction.tag = section
                view.filterButtonAction.titleLabel?.lineBreakMode = .byTruncatingTail
                if view.filterButtonAction.target(forAction: #selector(HomeViewController.filterButtonAction), withSender: view.filterButtonAction!) == nil {
                    
                    view.filterButtonAction.addTarget(self, action: #selector(HomeViewController.filterButtonAction), for: .touchUpInside)
                }
                
                view.filterButtonAction.centerTextAndImage(spacing: 5)
            }
            
            view.sectionTitle.text = (c as! MovieCarouselCollectionViewController).sectionTitle
        } else {
            view.filterButtonAction.isHidden = true
            view.filterButtonAction.tag = -1
            
            view.sectionTitle.text = (c as! BlockScreeningCarouselController).sectionTitle
        }
    }
    
    private func configureMovieCell(Cell cell : MoviesTableViewCell, IndexPath indexPath : IndexPath) {
        let t = homeItems![indexPath.section]
        
        if t.isKind(of: BlockScreeningCarouselController.self) == true {
            cell.moviesCarousel.delegate = (t as! BlockScreeningCarouselController)
            cell.moviesCarousel.dataSource = (t as! BlockScreeningCarouselController)
            
            (t as! BlockScreeningCarouselController).collectionView = cell.moviesCarousel
            (t as! BlockScreeningCarouselController).setData(data: blockScreeningItems)
        } else {
            cell.moviesCarousel.delegate = (t as! MovieCarouselCollectionViewController)
            cell.moviesCarousel.dataSource = (t as! MovieCarouselCollectionViewController)
            
            (t as! MovieCarouselCollectionViewController).collectionView = cell.moviesCarousel
            if indexPath.section == 0 {
                (t as! MovieCarouselCollectionViewController).setData(data: nowShowingItems)
            } else if indexPath.section == 1 {
                (t as! MovieCarouselCollectionViewController).setData(data: comingSoonItems)
            }
        }
        
    }
    
    private func configureBannerView() {
        var viewFrame : CGRect = headerView.frame
        headerView.frame = viewFrame
        
        if hideContent == false {
            viewFrame.size.height = headerView.bounds.size.width * 0.6
        } else {
            viewFrame.size.height = 0.0
        }
        
        headerView.frame = viewFrame
        
        if bannerScrollView == nil {
            bannerScrollView = BannerScrollViewController(nibName: "BannerScrollViewController", bundle: nil)
            bannerScrollView!.view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        
        for v in headerView.subviews {
            v.removeFromSuperview()
        }
        
        if hideContent == false {
            bannerScrollView!.willMove(toParentViewController: self)
            self.addChildViewController(bannerScrollView!)
            bannerScrollView?.didMove(toParentViewController: self)
            
            headerView.addSubview(bannerScrollView!.view)
            
            let constraints : [NSLayoutConstraint] = [NSLayoutConstraint(item: headerView, attribute: .left, relatedBy: .equal, toItem: bannerScrollView!.view, attribute: .left, multiplier: 1.0, constant: 0.0),
                                                      NSLayoutConstraint(item: headerView, attribute: .top, relatedBy: .equal, toItem: bannerScrollView!.view, attribute: .top, multiplier: 1.0, constant: 0.0),
                                                      NSLayoutConstraint(item: headerView, attribute: .right, relatedBy: .equal, toItem: bannerScrollView!.view, attribute: .right, multiplier: 1.0, constant: 0.0),
                                                      NSLayoutConstraint(item: headerView, attribute: .bottom, relatedBy: .equal, toItem: bannerScrollView!.view, attribute: .bottom, multiplier: 1.0, constant: 0.0)]
            headerView.addConstraints(constraints)
        } else {
            bannerScrollView?.willMove(toParentViewController: nil)
            bannerScrollView?.removeFromParentViewController()
            bannerScrollView?.didMove(toParentViewController: nil)
            bannerScrollView?.view.removeFromSuperview()
            bannerScrollView = nil
        }
        
        view.layoutIfNeeded()
        headerView.layoutIfNeeded()
    }
    
    private func getPreferred() {
        
        var coords = locationManager?.location?.coordinate
        
        if coords == nil {
            
            coords = CLLocationCoordinate2D(latitude: 14.553449, longitude: 121.049992)
            
        }
        
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            
            GMoviesAPIManager.getTheaters(coords: coords!) { (error, result) in
                if error == nil {
                    let status : Int = result?.object(forKey: "status") as! Int
                    
                    if status == 1 {
                        //print(result)
                        let theaters : [NSDictionary] = result?.value(forKeyPath: "data.results.theaters") as! [NSDictionary]
                        self.preferred?.removeAll()
                        for theater : NSDictionary in theaters as [NSDictionary] {
                            let t : TheaterModel = TheaterModel(data: theater)
                            self.preferred?.append(t.theaterId!)
                        }
                        
                    }
                } else {
                    print(error!.localizedDescription)
                }
            }
            
            return
        }
        
        GMoviesAPIManager.getPreferred { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    self.preferred?.removeAll()
                    self.preferred = result?.value(forKey: "data") as? [String]
                    //print(self.preferred)
                    if self.preferred == nil {
                        GMoviesAPIManager.getTheaters(coords: coords!) { (error, result) in
                            if error == nil {
                                let status : Int = result?.object(forKey: "status") as! Int
                                
                                if status == 1 {
                                    //print(result)
                                    let theaters : [NSDictionary] = result?.value(forKeyPath: "data.results.theaters") as! [NSDictionary]
                                    self.preferred?.removeAll()
                                    for theater : NSDictionary in theaters as [NSDictionary] {
                                        let t : TheaterModel = TheaterModel(data: theater)
                                        self.preferred?.append(t.theaterId!)
                                    }
                                    
                                }
                            } else {
                                print(error!.localizedDescription)
                            }
                        }
                    } else if self.preferred!.count == 0 {
                        GMoviesAPIManager.getTheaters(coords: coords!) { (error, result) in
                            if error == nil {
                                let status : Int = result?.object(forKey: "status") as! Int
                                
                                if status == 1 {
                                    //print(result)
                                    let theaters : [NSDictionary] = result?.value(forKeyPath: "data.results.theaters") as! [NSDictionary]
                                    self.preferred?.removeAll()
                                    for theater : NSDictionary in theaters as [NSDictionary] {
                                        let t : TheaterModel = TheaterModel(data: theater)
                                        self.preferred?.append(t.theaterId!)
                                    }
                                    
                                }
                            } else {
                                print(error!.localizedDescription)
                            }
                        }
                    }
                } else {
                    let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    @objc private func downloadMovies() {
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager?.requestWhenInUseAuthorization()
            return
        } else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager?.startUpdatingLocation()
        }
        
        if (nowShowingItems?.count)! > 0 {
            return
        }
        
        if timer != nil {
            timer?.invalidate()
            ticker = 0
        }
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if appDelegate.reachability?.isReachable == false{
            self.configureBannerView()
            self.loadErrorView(titleString: "No Internet", description: "Please check your network settings and try again")
            self.tableView.reloadData()
            
            return
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(HomeViewController.tick), userInfo: nil, repeats: true)
        
        var viewFrame : CGRect = headerView.frame
        viewFrame.size.height = 0
        headerView.frame = viewFrame
        
        self.view.addSubview(loadingScreen!)
        loadingScreen?.frame = UIScreen.main.bounds
//        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
//            ])
//        
//        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
        
        GMoviesAPIManager.getMovies { (error, result) in
            if error == nil {
                
                let status : Int = result?.object(forKey: "status") as! Int
                
                if status == 1 {
                    
                    let data : NSDictionary = result?.object(forKey: "data") as! NSDictionary
                    let nowShowing : [NSDictionary]? = data.object(forKey: "now_showing") as? [NSDictionary]
                    let comingSoon : [NSDictionary]? = data.object(forKey: "coming_soon") as? [NSDictionary]
                    let blockScreening : [NSDictionary]? = data.object(forKey: "block_screening") as? [NSDictionary]
                    
                    
                    self.nowShowingItems?.removeAll()
                    for movieRaw : NSDictionary in nowShowing! as [NSDictionary] {
                        
                        let movie : MovieModel = MovieModel(data: movieRaw)
                        movie.showing = true
                        self.nowShowingItems?.append(movie)
                    }
                    
                    self.comingSoonItems?.removeAll()
                    for movieRaw : NSDictionary in comingSoon! as [NSDictionary] {
                        let movie : MovieModel = MovieModel(data: movieRaw)
                        movie.showing = false
                        self.comingSoonItems?.append(movie)
                        //print(movieRaw)
                    }
                    
                    self.blockScreeningItems?.removeAll()
                    for movieRaw : NSDictionary in blockScreening! as [NSDictionary] {
                        let movie : BlockScreeningModel = BlockScreeningModel(data: movieRaw)
                        self.blockScreeningItems?.append(movie)
                    }
                    
                    DispatchQueue.main.async {
                        self.errorView?.removeFromSuperview()
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        let nowShowing : MovieCarouselCollectionViewController = MovieCarouselCollectionViewController(sections: 1, reuseIdentifier: "posterCell")
                        nowShowing.sectionTitle = "NOW SHOWING"
                        let comingSoon : MovieCarouselCollectionViewController = MovieCarouselCollectionViewController(sections: 1, reuseIdentifier: "posterCell")
                        comingSoon.sectionTitle = "COMING SOON"
                        
                        self.homeItems = [nowShowing, comingSoon]
                        
                        if (self.blockScreeningItems?.count)! > 0 {
                            let block : BlockScreeningCarouselController = BlockScreeningCarouselController(sections: 1, reuseIdentifier: "blockPosterCell")
                            block.sectionTitle = "BLOCK SCREENING"
                            self.homeItems?.append(block)
                        }
                        
                        self.configureBannerView()
                        
                        self.tableView.reloadData()
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.hideBanner()
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.errorView?.removeFromSuperview()
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        self.loadErrorView(titleString: "No Internet", description: "Please check your network settings and try again")
                        appDelegate.hideBanner()
                    }
                    
                    let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                DispatchQueue.main.async {
                    self.errorView?.removeFromSuperview()
                    self.loadingScreen?.stopAnimating()
                    self.loadingScreen?.removeFromSuperview()
                    self.loadErrorView(titleString: "No Internet", description: "Please check your network settings and try again")
                    appDelegate.hideBanner()
                }
                print(error!.localizedDescription)
            }
            
            self.timer?.invalidate()
            self.timer = nil
            self.ticker = 0
            
        }
    }
    
    @objc private func tick() {
        self.ticker += 1

        if self.ticker == Constants.patience {
            self.timer?.invalidate()
            self.ticker = 0
            
            DispatchQueue.main.async {
                let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showBanner(message: "Taking longer than expected... Please Wait.", isPersistent: false)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetail" {
            let vc : MovieDetailViewController = (segue.destination as! UINavigationController).topViewController as! MovieDetailViewController
            
            if fromDeepLink == false {
                let p : UICollectionViewCell = sender as! UICollectionViewCell
                let c = (p.superview as! UICollectionView).delegate
                if c!.isKind(of: MovieCarouselCollectionViewController.self) {
                    let i : IndexPath =  (p.superview as! UICollectionView).indexPath(for: (p as! PosterCollectionViewCell))!
                    
                    let mo : MovieModel = (c as! MovieCarouselCollectionViewController).items![i.row]
                    
                    if (c as! MovieCarouselCollectionViewController).sectionTitle == "NOW SHOWING" {
                        vc.theaterObject = currentPreferredTheater
                    } else {
                        vc.theaterObject = nil
                    }
                    
                    vc.movieObject = mo
                } else {
                    let i : IndexPath =  (p.superview as! UICollectionView).indexPath(for: (p as! BlockPosterCollectionViewCell))!
                    
                    let mo : BlockScreeningModel = (c as! BlockScreeningCarouselController).items![i.row]
                    
                    vc.blockObject = mo
                }
            } else {
                fromDeepLink = false
                vc.movieObject = movieObjectDeepLink
            }
        }
    }
    
    // MARK: Location Delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
            break
            
        case .denied, .restricted:
            
            let alertController = UIAlertController(
                
                title: "Location Access Disabled",
                message: "In order to determine the cinemas near you, please open this app settings and set the location permission to 'When In Use'",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
            downloadMovies()
            getPreferred()
            break
            
        default:
            downloadMovies()
            getPreferred()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager?.stopUpdatingLocation()
    }
    
    //MARK: Preferred Cinema Delegate
    
    func didSelectTheater(to: TheaterModel) {
        currentPopDrop?.hide()
        
        GMoviesAPIManager.getMoviesNowShowing(theaterId: to.theaterId!) { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    
                    let data : NSDictionary = result?.object(forKey: "data") as! NSDictionary
                    
                    let nowShowing : [NSDictionary]? = data.object(forKey: "now_showing") as? [NSDictionary]
                    
                    if (nowShowing?.count)! > 0 {
                        self.nowShowingItems?.removeAll()
                    } else {
                        
                        let alert : UIAlertController = UIAlertController(title: "", message: "Sorry, the movie schedule is not yet available in this cinema. Try later or select a different cinema.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        return
                    }
                    
                    self.currentPreferredTheater = to
                    
                    for movieRaw : NSDictionary in nowShowing! as [NSDictionary] {
                        let movie : MovieModel = MovieModel(data: movieRaw)
                        movie.showing = true
                        self.nowShowingItems?.append(movie)
                    }
                    
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        let nowShowing : MovieCarouselCollectionViewController = self.homeItems![0] as! MovieCarouselCollectionViewController
                        nowShowing.setData(data: self.nowShowingItems!)
                        
                        self.tableView.reloadData()
                        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.hideBanner()
                    }
                } else {
                    let alert : UIAlertController = UIAlertController(title: "Error", message: result?.object(forKey: "message") as! String?, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    func didSelectAllTheaters() {
        homeItems?.removeAll()
        
        nowShowingItems?.removeAll()
        comingSoonItems?.removeAll()
        
        tableView.reloadData()

        currentPreferredTheater = nil
        
        downloadMovies()
        getPreferred()
        currentPopDrop?.hide()
    }
    
    func didSelectEdit() {
        let tab : MTabBarViewController = parent?.parent as! MTabBarViewController
        tab.setSelectedViewController(index: 1, animated: true)
        currentPopDrop?.hide()
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
