//
//  PopupViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 7/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

protocol PopupViewControllerDelegate {
    func onHidden(popup: PopupViewController)
}

class PopupViewController: UIViewController {
    
    private var window: UIWindow!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var overlay: UIView!
    @IBOutlet weak var alertViewCenterY: NSLayoutConstraint!
    @IBOutlet weak var alertViewCenterX: NSLayoutConstraint!
    
    @IBOutlet weak var alertViewHeight: NSLayoutConstraint!
    @IBOutlet weak var alertViewWidth: NSLayoutConstraint!
    
    private var mainWindow : UIWindow?
    private var childVc : UIViewController?
    
    private var height : CGFloat = 240.0
    private var width : CGFloat = 128.0
    
    var delegate: PopupViewControllerDelegate?
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        mainWindow = UIApplication.shared.keyWindow
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window.windowLevel = UIWindowLevelAlert
        window.rootViewController = self
    }
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, expandedHeight h : CGFloat) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        mainWindow = UIApplication.shared.keyWindow
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window.windowLevel = UIWindowLevelAlert
        window.rootViewController = self
        height = h
    }
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, expandedHeight h : CGFloat, expandedWidth w : CGFloat) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        mainWindow = UIApplication.shared.keyWindow
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window.windowLevel = UIWindowLevelAlert
        window.rootViewController = self
        height = h
        width = w
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        overlay.alpha = 0.0
        overlay.backgroundColor = UIColor.black
        alertViewCenterY.constant = 0.0
        alertViewCenterX.constant = 0.0
        alertViewHeight.constant = 0
        alertViewWidth.constant = 0
        
        self.contentView.layer.shadowColor = UIColor.black.cgColor
        self.contentView.layer.shadowRadius = 5.0
        self.contentView.layer.shadowOpacity = 0.7
        self.contentView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func outsideTapped(_ sender: AnyObject) {
        hide()
    }
    
    func show() {
        window.makeKeyAndVisible()
        
        self.view.layoutIfNeeded()
        
        alertViewHeight.constant = height
        alertViewWidth.constant = width
        
        UIView.animate(withDuration: 0.250, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            self.overlay.alpha = 0.4
            self.view.layoutIfNeeded()
        }) { (_) in
            
        }
    }
    
    func show(cornerRadius : CGFloat) {
        self.show()
        contentView.layer.cornerRadius = cornerRadius
    }
    
    func hide() {
        window.resignKey()
        self.view.layoutIfNeeded()
        
        alertViewWidth.constant = 0
        alertViewHeight.constant = 0
        
        
        UIView.animate(withDuration: 0.250, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            self.overlay.alpha = 0.0
            self.view.layoutIfNeeded()
        }) { (_) in
            self.window.isHidden = true
            
            self.mainWindow!.makeKeyAndVisible()
            self.delegate?.onHidden(popup: self)
        }
    }
    
    func showViewControllerFromView(viewController vc: UIViewController, offset: CGPoint?) {
        
        childVc = vc
        childVc?.view.translatesAutoresizingMaskIntoConstraints = false
        
        vc.willMove(toParentViewController: self)
        self.addChildViewController(childVc!)
        
        vc.didMove(toParentViewController: self)
        
        self.show()
        
        contentView.addSubview(childVc!.view)
        let constraints : [NSLayoutConstraint] = [NSLayoutConstraint(item: childVc!.view, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: childVc!.view, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: childVc!.view, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: childVc!.view, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1.0, constant: 0.0)]
        self.contentView.addConstraints(constraints)
        self.contentView.layoutIfNeeded()
    }
    
    func showView(view v: UIView, offset: CGPoint?) {
        
        self.show()
        
        contentView.backgroundColor = UIColor.clear
        contentView.addSubview(v)
        let constraints : [NSLayoutConstraint] = [NSLayoutConstraint(item: v, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: v, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: v, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: v, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1.0, constant: 0.0)]
        self.contentView.addConstraints(constraints)
        self.contentView.layoutIfNeeded()
        
        self.perform(#selector(self.hide), with: nil, afterDelay: 2.0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
