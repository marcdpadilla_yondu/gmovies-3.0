//
//  LoaderView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 11/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

@IBDesignable class LoaderView: UIView {
    
    @IBOutlet weak var LoaderImageView: UIImageView!
    @IBOutlet weak var loaderTitle: UILabel!
    @IBOutlet weak var loaderSubtitle: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    private var view : UIView?
    
    func loadViewFromNib() {
        view = UINib(nibName: "LoaderView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil).first as! UIView?
        view!.frame = bounds
        view!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view!)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    var loaderImages : [UIImage] {
        set (images) {
            LoaderImageView.animationImages = images
        }
        
        get { return LoaderImageView.animationImages!}
    }
    
    @IBInspectable var loaderTitleMessage : String? {
        get { return loaderTitle.text }
        set (string) { loaderTitle.text = string }
    }
    
    @IBInspectable var loaderSubtitleMessage : String? {
        get { return loaderSubtitle.text }
        set (string) { loaderSubtitle.text = string }
    }
    
    @IBInspectable var buttonMessage : String? {
        get { return actionButton.titleLabel?.text }
        set (string) { actionButton.setTitle(string, for: .normal) }
    }
    
    @IBInspectable var actionButtonHidden : Bool {
        get { return actionButton.isHidden }
        set (yesNo) { actionButton.isHidden = yesNo }
    }
    
    func startAnimating() {
        LoaderImageView.startAnimating()
    }
    
    func stopAnimating() {
        LoaderImageView.stopAnimating()
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        UIManager.roundify(actionButton)
    }

}
