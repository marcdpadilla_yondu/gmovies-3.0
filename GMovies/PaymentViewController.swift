//
//  PaymentViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 2/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import PopupDialog

class PaymentViewController: UIViewController, PaymentSelectionProtocol, PaymentLoaderProtocol, UITextFieldDelegate {
    
    var theaterObject : TheaterModel?
    var movieObject : MovieModel?
    var cinemaObject : CinemaModel?
    var timeObject : TimeModel?
    var dateObject : Date?
    var reservedSeating : Bool = false
    var seatCount : Int = 0
    var promoObject: PromoModel?
    
    var blockObject : BlockScreeningModel?
    
    fileprivate var successVC: SuccessfulTransactionVC?
    
    
    fileprivate var selectedPayment : PaymentOptionModel?
    private var convenienceFeeValue : Int = 0
    private var isPromoCodeValid : Bool = false
    
    fileprivate var transactionId: String?
    fileprivate var totalAmount: Int = 0
    
    @IBOutlet weak var posterBorder: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var theater: UILabel!
    @IBOutlet weak var cinema: UILabel!
    @IBOutlet weak var variant: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var selectPaymentButton: UIButton!
    @IBOutlet weak var seatsCollectionView: UICollectionView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var ticketPrice: UILabel!
    @IBOutlet weak var convenienceFee: UILabel!
    @IBOutlet weak var convenienceFeeLabel: UILabel!
    @IBOutlet weak var popcornLabel: UILabel!
    @IBOutlet weak var popcornFee: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var total: UILabel!
    
    @IBOutlet weak var seatCountLabel: UILabel!
    
    
    
    var selectedSeatsController : SeatSelectionViewController?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        email.text = UserDefaults.standard.value(forKeyPath: "user.email") as! String
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.title = " "
        
        posterBorder.layer.borderWidth = 0.5
        posterBorder.layer.borderColor = UIColor(netHex: 0x566184).cgColor
        
        if blockObject == nil {
            if movieObject?.poster == nil {
                GMoviesAPIManager.downloadPoster(urlString: movieObject!.posterUrl!, completion: { (error, image) in
                    if error == nil {
                        DispatchQueue.main.async {
                            self.movieObject?.poster = image
                            self.poster.image = self.movieObject?.poster
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            } else {
                poster.image = movieObject?.poster
            }
            movieTitle.text = movieObject?.movieTitle!.uppercased()
            theater.text = theaterObject?.name!
            cinema.text = cinemaObject?.name!
            variant.text = timeObject?.variantString
            timeLabel.text = timeObject?.name!
            dateLabel.text = UIManager.seatmapDateFormatter(date: dateObject!)
            
            reservedSeating = timeObject!.seatingType?.caseInsensitiveCompare("reserved seating") == .orderedSame
            
            if promoObject != nil {
                if promoObject!.paymentOptions.count == 1 {
                    selectedPayment = promoObject?.paymentOptions.first
                    didSelectPaymentOption(po: selectedPayment!)
                }
            }else if theaterObject!.paymentOptions!.count == 1 {
                selectedPayment = theaterObject?.paymentOptions?.first
                didSelectPaymentOption(po: selectedPayment!)
            }
            
        } else {
            if blockObject?.poster == nil {
                GMoviesAPIManager.downloadPoster(urlString: self.blockObject!.posterUrl!, completion: { (error, image) in
                    if error == nil {
                        DispatchQueue.main.async {
                            self.blockObject!.poster = image
                            self.poster.image = self.blockObject!.poster
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            } else {
                poster.image = self.blockObject!.poster
            }
            movieTitle.text = blockObject?.movie?.movieTitle!.uppercased()
            theater.text = blockObject?.theater?.name!
            cinema.text = blockObject?.theater?.cinemas?.first?.name!
            variant.text = ""
            timeLabel.text = blockObject?.screeningTimeAMPM
            dateLabel.text = UIManager.seatmapDateFormatter(date: blockObject!.screeningDateTime!)
            
            reservedSeating = true
            
            
            if promoObject != nil {
                if promoObject!.paymentOptions.count == 1 {
                    selectedPayment = promoObject?.paymentOptions.first
                    didSelectPaymentOption(po: selectedPayment!)
                }
            }else if blockObject!.theater!.paymentOptions!.count == 1 {
                selectedPayment = blockObject?.theater?.paymentOptions?.first
                didSelectPaymentOption(po: selectedPayment!)
            }
        }
        
        if reservedSeating == true {
            seatCount = selectedSeatsController!.selectedSeats!.count
            seatCountLabel.isHidden = true
        } else {
            seatCountLabel.isHidden = false
            seatCountLabel.text = "\(seatCount) seat\(seatCount == 1 ? "" : "s")"
        }
        
        seatsCollectionView.delegate = selectedSeatsController
        seatsCollectionView.dataSource = selectedSeatsController
        
        updatePrice()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "payment")
        selectedSeatsController?.centerSeats = false
        seatsCollectionView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectPaymentOption(po: PaymentOptionModel) {
        selectedPayment = po
        
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PayNow, action: po.name!)
        
        selectPaymentButton.setTitle(selectedPayment!.name!, for: .normal)
        selectPaymentButton.setImage(selectedPayment!.icon, for: .normal)
        
        if po.optionId!.localizedCaseInsensitiveContains("credit_card") == true || po.optionId!.localizedCaseInsensitiveContains("gcash") == true || po.optionId!.localizedCaseInsensitiveContains("bank_payment") == true {
            convenienceFee.text = "Php \(theaterObject?.convenienceFees?.value(forKey: po.optionId!))"
            
            let conFee = theaterObject?.convenienceFees?.value(forKey: po.optionId!)
            
            if let conFeeValue = conFee as? String {
                convenienceFeeValue = NSString(string: conFeeValue).integerValue
            }else if let conFeeValue = conFee as? Int {
                convenienceFeeValue = conFeeValue
            }else {
                convenienceFeeValue = 0
            }

            //convenienceFeeValue = Int(Float(theaterObject?.convenienceFees?.value(forKey: po.optionId!) as! String) ?? 0)
        } else if po.optionId!.localizedCaseInsensitiveContains("paynamics") == true || po.optionId!.localizedCaseInsensitiveContains("ipay88") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("pesopay") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("paynamics") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("ipay88") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("migs") == true{
            convenienceFee.text = "Php \(theaterObject?.convenienceFees?.value(forKey: "credit_card"))"
            
            let conFee = theaterObject?.convenienceFees?.value(forKey: "credit_card")
            
            if let conFeeValue = conFee as? String {
                convenienceFeeValue = NSString(string: conFeeValue).integerValue
            }else if let conFeeValue = conFee as? Int {
                convenienceFeeValue = conFeeValue
            }else {
                convenienceFeeValue = 0
            }
            
            convenienceFee.text = "Php \(theaterObject?.convenienceFees?.value(forKey: "credit_card"))"

            //convenienceFeeValue = Int(Float(theaterObject?.convenienceFees?.value(forKey: "credit_card") as! String)!)
            
        } else {
            convenienceFee.text = "Php 0.00"
            convenienceFeeValue = 0
        }
        
        discount.isHidden = true
        promoCodeLabel.isHidden = true
        isPromoCodeValid = false
        
        if selectedPayment != nil {
            if selectedPayment?.icon == nil {
                GMoviesAPIManager.downloadPoster(urlString: selectedPayment!.iconUrl!, completion: { (error, image) in
                    if error == nil {
                        DispatchQueue.main.async {
                            self.selectedPayment?.icon = image
                            self.selectPaymentButton.setImage(self.selectedPayment?.icon, for: .normal)
                        }
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            } else {
                self.selectPaymentButton.setImage(self.selectedPayment?.icon, for: .normal)
            }
        }
        
        updatePrice()
    }
    
    private func configureView() {
        scrollView.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height)
    }
    
    private func updatePrice() {
        convenienceFee.text = "Php \(convenienceFeeValue).00 x \(seatCount)"
        var sb : Float = 0.0
        
        var ticketCost:Float = 0
        
        if blockObject == nil {
            ticketPrice.text = "\(timeObject!.priceString) x \(seatCount)"
            sb = Float(timeObject!.price * seatCount) + Float(convenienceFeeValue * seatCount)
            ticketCost = Float(timeObject!.price)
        } else {
            ticketPrice.text = "Php \(blockObject!.price!) x \(seatCount)"
            sb = Float(Float(blockObject!.price!)! * Float(seatCount)) + Float(convenienceFeeValue * seatCount)
            ticketCost = Float(blockObject!.price!)!
        }
        
        popcornLabel.isHidden = true
        popcornFee.isHidden = true
        
        if let popcornPrice = timeObject?.popcornPrice {
            
            if popcornPrice > 0 {
                
                popcornLabel.isHidden = false
                popcornFee.isHidden = false
                popcornFee.text = String(format: "Php %.2f x %d", Float(popcornPrice), seatCount)
                popcornLabel.text = timeObject?.popcornLabel
                sb += Float(popcornPrice * seatCount)
            }
        }
        
        subTotal.text = String(format: "Php %.2f", sb)
        var discountValue : Float = 0.0
        
        if promoObject == nil || selectedPayment == nil {
            promoCodeLabel.isHidden = true
            discount.isHidden = true
            
        } else {
            
            let discountType = promoObject!.discountType
            
            discountValue = Float(promoObject!.discountValue) ?? Float(convenienceFeeValue)
            
            promoCodeLabel.text = "Discount"
            
            if discountType.caseInsensitiveCompare("percent") == .orderedSame {
                
                discountValue = Float(promoObject!.discountValue)!
                sb -= (sb * (discountValue/100.0))
                discount.text = "- \(Int(discountValue))%"
            } else if (discountType.caseInsensitiveCompare("convenience_fee") == .orderedSame) {
                let discountedSeats = promoObject!.discountedSeats
                
                sb -= Float(convenienceFeeValue * discountedSeats)
                discount.text = String(format:"-Php %.2f", Float(Int(convenienceFeeValue) * discountedSeats))
                //discount.text = "- Php \(Int(convenienceFeeValue) * seatCount).00"
            }else if discountType.caseInsensitiveCompare("special") == .orderedSame {
                
                let freeTickets = promoObject!.discountValue
                let freeArray = freeTickets.components(separatedBy: ";")
                if freeArray.count == 2 {
                    let freeCount = (freeArray[1] as NSString).integerValue
                    
                    let disc:Float = (ticketCost + Float(convenienceFeeValue)) * Float(freeCount)
                    
                    sb -= disc
                    discount.text = String(format:"-Php %.2f", disc)
                }
            } else if discountType.caseInsensitiveCompare("rush-rewards") == .orderedSame {
                
                let disc: Float = (ticketCost + Float(convenienceFeeValue)) * Float(promoObject!.discountedSeats)
                sb -= disc
                
                if promoObject!.discountedSeats > 1 {
                    promoCodeLabel.text = "\(promoObject!.name) (\(promoObject!.discountedSeats) Tickets)"
                }else {
                    promoCodeLabel.text = "\(promoObject!.name) (\(promoObject!.discountedSeats) Ticket)"
                }
                discount.text = String(format:"-Php %.2f", disc)
                
            } else {
                discount.text = String(format: "-Php %.2f", discountValue)
                //discount.text = "- Php \(Int(discountValue)).00"
                sb -= discountValue
            }
            
            promoCodeLabel.isHidden = false
            discount.isHidden = false
        }
        
        if selectedPayment == nil {
            convenienceFeeLabel.isHidden = true
            convenienceFee.isHidden = true
            print("1")
        } else if selectedPayment?.optionId?.localizedCaseInsensitiveContains("bank_payment") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("gcash") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("credit_card") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("pesopay") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("paynamics") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("ipay88") == true || selectedPayment?.optionId?.localizedCaseInsensitiveContains("migs") == true{
            convenienceFeeLabel.isHidden = false
            convenienceFee.isHidden = false
            print("2")
        } else {
            convenienceFeeLabel.isHidden = true
            convenienceFee.isHidden = true
            print(selectedPayment?.paymentGateway)
        }
        
        totalAmount = Int(sb)
        total.text = String(format: "Php %.2f", sb)
    }
    
    func clearCurrentSelectedPayment() {
        selectedPayment = nil
        if selectPaymentButton != nil {
            selectPaymentButton.setTitle("Choose your payment method", for: .normal)
            selectPaymentButton.setImage(nil, for: .normal)
        }
    }
    
    private func validate() -> Bool {
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if selectedPayment == nil {
            appDelegate.showBanner(message: "Please select payment method", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
            selectPaymentButton.shake()
            return false
        }
        
        if email.text?.contains("@") == false || email.text?.contains(".") == false {
            appDelegate.showBanner(message: "Invalid email", isPersistent: false, color: UIColor(netHex: 0x6547e6), textColor: UIColor.white)
            email.shake()
            emailLabel.shake()
            return false
        }
        
        return true
    }
    
    @IBAction func continueAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "payment_continue_button")
        if validate() == true {
            if selectedPayment?.type?.caseInsensitiveCompare("instruction") == ComparisonResult.orderedSame {
                self.performSegue(withIdentifier: "showPaymentGateway", sender: self)
            } else {
                
                print(selectedPayment?.optionId)
                
                if selectedPayment?.optionId?.caseInsensitiveCompare("gcash") == .orderedSame || selectedPayment?.optionId?.caseInsensitiveCompare("claim_code") == .orderedSame || selectedPayment?.optionId?.caseInsensitiveCompare("mpass") == .orderedSame || selectedPayment?.optionId?.caseInsensitiveCompare("blocked_screening_gcash") == .orderedSame || selectedPayment?.optionId?.caseInsensitiveCompare("blocked_screening_claim_code") == .orderedSame || selectedPayment?.optionId?.caseInsensitiveCompare("blocked_screening_mpass") == .orderedSame {
                    self.performSegue(withIdentifier: "showPaymentLogin", sender: self)
                } else {
                    self.performSegue(withIdentifier: "showPaymentLoader", sender: self)
                }
            }
        }
    }
    
    //MARK: Payment Loader delegate
    
    func didFinishTransaction(success: Bool, transactionId: String) {
        UserDefaults.standard.set(true, forKey: "refreshTickets")
        UserDefaults.standard.synchronize()
        
        self.transactionId = transactionId
        
        if success == true {
            
            GMoviesAPIManager.getTicket(paymentOptionId: self.selectedPayment!.optionId!, transactionId: self.transactionId!, completion: { (error, result) in
                
                if error == nil {
                    let status : Int = result?.value(forKey: "status") as! Int
                    if status == 1 {
                        
                        self.successVC = SuccessfulTransactionVC()
                        self.successVC?.delegate = self
                        self.successVC?.paymentRoot = self.parent
                        self.successVC?.title = self.selectedPayment?.name
                        
                        let rushPoints = result?.value(forKeyPath: "data.rush_points") as? NSDictionary
                        let badges = result?.value(forKeyPath: "data.badges") as? NSDictionary
                        
                        if AccountHelper.shared.isRegisteredInRush {
                            
                            self.successVC?.shouldHideJoinLoyaltyProgram = true
                            
                            if rushPoints != nil {
                                
                                let earnedPoints = rushPoints?.value(forKey: "points_earned") as? NSNumber ?? 0
                                let userPoints = rushPoints?.value(forKey: "user_points") as? NSNumber ?? 0
                                
                                self.successVC?.earnedPoints = String(format:"%d", earnedPoints.intValue)
                                self.successVC?.availablePoints = String(format:"%d", userPoints.intValue)
                                
                            }else {
                                self.successVC?.shouldHideEarnedPoints = true
                            }
                            
                            if badges == nil || badges?.count == 0 {
                                self.successVC?.shouldHideBadge = true
                                self.successVC?.shouldHideBadgeCaption = true
                            }else {
                                let badgeId = badges?.value(forKey: "badge_id") as? Int
                                if badgeId != nil {
                                    
                                    self.successVC?.shouldHideBadgeMessage = true
                                    let badgeIcon = badges?.value(forKey: "badge_icon") as! String
                                    let badgeGroupIcon = badges?.value(forKey: "group_badge_icon") as! String
                                    let badgeCaption = badges?.value(forKey: "badge_caption") as! String
                                    
                                    self.successVC?.badgeGroupIconUrl = badgeGroupIcon
                                    self.successVC?.badgeIconUrl = badgeIcon
                                    self.successVC?.badgeCaptionText = badgeCaption
                                    
                                }else {
                                    self.successVC?.shouldHideBadgeMessage = false
                                    self.successVC?.badgeMessageText = badges?.value(forKey: "badge_msg") as! String
                                    self.successVC?.shouldHideBadgeCaption = true
                                    //self.successVC?.badgeCaptionText = badges?.value(forKey: "badge_caption") as? String ?? ""
                                    self.successVC?.shouldHideBadge = true
                                }
                            }
                            
                        }else {
                            self.successVC?.shouldHideEarnedPoints = true
                            self.successVC?.shouldHideBadge = true
                        }
                        
                        self.navigationItem.setHidesBackButton(true, animated: false)
                        let ticketData : NSDictionary = result?.value(forKey: "data") as! NSDictionary
                        let ticket = TicketModel(data: ticketData)
                        let ticketView : TicketViewController = (self.parent as! ProgressNavigationViewController).viewControllers![2] as! TicketViewController
                        ticketView.ticket = ticket
                        ticketView.hideNav = true
                        ticketView.saveTickets = true
                        (self.parent as! ProgressNavigationViewController).setCurrentViewController(index: 2, animated: true)
                        
                        self.show(self.successVC!, sender: self)
                        
//                        self.checkRateUs(transactionId: self.transactionId!, completion: { didRate in
//                            
//                            self.navigationItem.setHidesBackButton(true, animated: false)
//                            let ticketData : NSDictionary = result?.value(forKey: "data") as! NSDictionary
//                            let ticket = TicketModel(data: ticketData)
//                            let ticketView : TicketViewController = (self.parent as! ProgressNavigationViewController).viewControllers![2] as! TicketViewController
//                            ticketView.ticket = ticket
//                            ticketView.hideNav = true
//                            ticketView.saveTickets = true
//                            (self.parent as! ProgressNavigationViewController).setCurrentViewController(index: 2, animated: true)
//                            
//                            self.show(self.successVC!, sender: self)
//                            
//                            if didRate {
//                                
//                                if AccountHelper.shared.isRegisteredInRush {
//                                    
//                                    UserEngagementHelper.shared.earnBadgeFrom(engagement: "in-app-feedback", hideAvailablePoints: true)
//                                }
//                            }
//                        })
                        
                    } else {
                        
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showPaymentSelection" {
            AnalyticsHelper.sendButtonEvent(name: "payment_select_payment_button")
            let paymentSelection : PaymentSelectionTableViewController = (segue.destination as! MNavigationViewController).topViewController as! PaymentSelectionTableViewController
            paymentSelection.delegate = self
            
            if promoObject != nil && promoObject!.paymentOptions.count > 0{
                paymentSelection.paymentOptions = promoObject?.paymentOptions
            }else if blockObject == nil {
                
                if var options:[PaymentOptionModel] = theaterObject?.paymentOptions {
                    
                    options = options.filter(){$0.type != "bank_promo"}
                    paymentSelection.paymentOptions = options
                }else {
                    paymentSelection.paymentOptions = theaterObject?.paymentOptions
                }
            } else {
                if var options:[PaymentOptionModel] = blockObject?.theater?.paymentOptions {
                    
                    options = options.filter(){$0.type != "bank_promo"}
                    paymentSelection.paymentOptions = options
                }else {
                    paymentSelection.paymentOptions = blockObject?.theater?.paymentOptions
                }
            }
        } else if segue.identifier == "showPaymentLoader" {
            let paymentLoader : PaymentLoaderViewController = segue.destination as! PaymentLoaderViewController
            
            paymentLoader.selectedPayment = selectedPayment
            if blockObject == nil {
                paymentLoader.theaterObject = theaterObject
                paymentLoader.movieObject = movieObject
                paymentLoader.cinemaObject = cinemaObject
                paymentLoader.timeObject = timeObject
                paymentLoader.dateObject = dateObject
            } else {
                paymentLoader.blockObject = blockObject
            }
            paymentLoader.reservedSeating = reservedSeating
            paymentLoader.seatCount = seatCount
            paymentLoader.selectedSeatsController = selectedSeatsController
            paymentLoader.email = email.text
            paymentLoader.promo = promoObject?.code
            paymentLoader.paymentRoot = self.parent
            paymentLoader.delegate = self
        } else if segue.identifier == "showPaymentGateway" {
            let paymentGateway : PaymentGatewayViewController = segue.destination as! PaymentGatewayViewController
            paymentGateway.url = selectedPayment!.instructionUrl
            paymentGateway.showBackButton = true
            paymentGateway.paymentRoot = self.parent
        } else if segue.identifier == "showPaymentLogin" {
            let paymentLogin : PaymentLoginViewController = segue.destination as! PaymentLoginViewController
            
            paymentLogin.selectedPayment = selectedPayment
            if blockObject == nil {
                paymentLogin.theaterObject = theaterObject
                paymentLogin.movieObject = movieObject
                paymentLogin.cinemaObject = cinemaObject
                paymentLogin.timeObject = timeObject
                paymentLogin.dateObject = dateObject
            } else {
                paymentLogin.blockObject = blockObject
            }
            paymentLogin.reservedSeating = reservedSeating
            paymentLogin.seatCount = seatCount
            paymentLogin.selectedSeatsController = selectedSeatsController
            paymentLogin.email = email.text
            paymentLogin.promo = promoObject?.code
            paymentLogin.paymentRoot = self.parent

        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
}

extension PaymentViewController {
    
    func checkRateUs (transactionId:String, completion: @escaping (Bool)->()) {
        
        RatingHelper.shared.checkDidRate(result: { (didRate) in
            if !didRate {
                // Create a custom view controller
                let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
                
                // Create the dialog
                let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
                
                
                // Create first button
                let askMeLaterButton = CancelButton(title: "Ask me later", height: 50) {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
                askMeLaterButton.titleFont = UIFont(name: "OpenSans", size: 14)
                
                // Create second button
                let submitNow = DefaultButton(title: "Submit now", height: 50) {
                    if ratingVC.rating >= 1 {
                        RatingHelper.shared.send(rating: ratingVC.rating, comment: ratingVC.comment, transactioId: transactionId, success: {
                            DispatchQueue.main.async {
                                popup.dismiss(animated: true, completion: nil)
                                completion(true)
                            }
                        }, failure: { (error) in
                            DispatchQueue.main.async {
                                popup.dismiss(animated: true, completion: nil)
                                completion(false)
                            }
                        })
                    }
                }
                submitNow.titleFont = UIFont(name: "OpenSans", size: 14)
                submitNow.dismissOnTap = false
                
                // Add buttons to dialog
                popup.addButtons([submitNow, askMeLaterButton])
                self.parent?.present(popup, animated: true)
            }else {
                completion(false)
            }
        })
    }
}

extension PaymentViewController: SuccessfulTransactionVCDelegate {
    
    func onViewTicket() {
        _ = self.successVC?.navigationController?.popToViewController((self.successVC?.paymentRoot)!, animated: true)
    }
    
    func onJoinLoyaltyProgram() {
        
        AccountHelper.shared.registerToRush(successful: { data in
            
            let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
            successRegistration.delegate = self
            successRegistration.userName = AccountHelper.shared.currentUser?.firstName
            successRegistration.dismissButtonTitle = "OK"
            successRegistration.modalTransitionStyle = .coverVertical
            self.successVC?.paymentRoot?.present(successRegistration, animated: true)
            
        }) { (error) in
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showBanner(message: error, isPersistent: false)
        }
    }
}

extension PaymentViewController: LoyaltyRegistrationSuccessVCDelegate {
    func didClickDismiss(sender: LoyaltyRegistrationSuccessVC) {
        successVC?.loyaltyProgramHolder.isHidden = true
        sender.dismiss(animated: true, completion: nil)
    }
}

extension PaymentViewController
{
    func topViewController(base: UIViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController!)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
