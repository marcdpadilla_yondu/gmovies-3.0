//
//  ReviewModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 17/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class ReviewModel: NSObject {
    /*
     {
     comment = Test;
     "created_at" = "2016-11-17 14:42:04";
     id = 3;
     "member_id" = "$2y$10$VESjfMCufJEpbmu7q0G08eFYy7RfMlYzuXKJGTyaKZ4SUAXFbp1pO";
     movie =     {
     "canonical_title" = "The Young Messiah";
     poster = "http://lh3.googleusercontent.com/p2A0JQ4AUU1BAHUQOdFn-i7M2dpRF1esuboqffrqmUMmCilh50C0fQo9XbiVVx3PyYhXs2LVmC_8AFPFV9mkV5B4yPW1hDs45KbKyw";
     "poster_landscape" = "http://lh3.googleusercontent.com/u4_32PAzI72oqD4ypMVL-XkqsJ1nrvCgwctmrArcfdL_oAXFFrU5nTNEjO7e3unoyQFqIsse8m-0Maj9nZP8nux4_PZ3d-YxaZRhvA";
     share =         {
     message = "So excited for The Young Messiah! Don\U2019t let the long lines ruin the experience. Download now po.st/GMoviesApp";
     title = "The Young Messiah";
     type = movie;
     url = "http://gmovies.web.qa.digitalventures.ph/movies/";
     };
     };
     "movie_id" = "d6e18307-4616-47b4-9d31-9b906a95ad00";
     rating = 3;
     status = 1;
     "updated_at" = "2016-11-17 14:42:04";
     }
    */
    
    var comment : String?
    var createdAt : String?
    var reviewId : Int = 0
    var movie : MovieModel?
    var rating : Int = 0
    
    var createdAtDate : Date? {
        get {
            let df = DateFormatter()
            df.locale = Locale.current
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            return df.date(from: createdAt!)
        }
    }
    
    var createdAtString : String {
        get {
            let df = DateFormatter()
            df.locale = Locale.current
            df.dateFormat = "MMMM dd yyyy"
            
            return "Created \(df.string(from: createdAtDate!))"
        }
    }
    
    init(data : NSDictionary) {
        comment = data.value(forKey: "comment") as? String
        createdAt = data.value(forKey: "created_at") as? String
        reviewId = data.value(forKey: "id") as! Int
        if let r = data.value(forKey: "rating") as? Int {
            rating = r
        } else {
            rating = Int(data.value(forKey: "rating") as! String)!
        }
        
        //print(data)
        if data.value(forKey: "movie") != nil {
            movie = MovieModel(data: data.value(forKey: "movie") as! NSDictionary)
        }
    }
}
