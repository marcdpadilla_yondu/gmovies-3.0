//
//  PaymentGatewayViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 3/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PaymentGatewayViewController: UIViewController, UIWebViewDelegate {
    
    var url : String?
    var showBackButton : Bool = false
    var paymentRoot : UIViewController?
    var ipay88View : UIView?
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if showBackButton == false {
//            self.navigationItem.setHidesBackButton(true, animated: true)
            backButton.isEnabled = false
            backButton.tintColor = UIColor.clear
        } else {
            backButton.isEnabled = true
            backButton.tintColor = UIColor.white
//             self.navigationItem.setHidesBackButton(false, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        print(url)
        AnalyticsHelper.sendScreenShowEvent(name: "payment_gateway")
        if ipay88View == nil {
            webView.loadRequest(URLRequest(url: URL(string: url!)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0))
        } else {
            self.view.addSubview(ipay88View!)
            let constraints : [NSLayoutConstraint] = [NSLayoutConstraint(item: ipay88View!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0),
                                                      NSLayoutConstraint(item: ipay88View!, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0.0),
                                                      NSLayoutConstraint(item: ipay88View!, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0.0),
                                                      NSLayoutConstraint(item: ipay88View!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 10.0)]
            self.view.addConstraints(constraints)
            self.view.layoutIfNeeded()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: WebView delegates
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if request.url?.absoluteString.contains("appspot.com/gmovies/") == true {
            self.navigationController?.popViewController(animated: true)
            return false
        }
        
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        let vcs = self.navigationController?.viewControllers.filter({ (vc) -> Bool in
            return vc.isKind(of: PaymentLoaderViewController.self)
        })
        
        if vcs != nil {
            if vcs!.count > 0 {
                let loader = vcs!.first as! PaymentLoaderViewController
                loader.cancelTransaction = true
            }
        }
        
        self.navigationController?.popToViewController(paymentRoot!, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
