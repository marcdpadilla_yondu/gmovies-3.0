//
//  LoginRegisterLoyaltyVC.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 04/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

@objc protocol LoginRegisterLoyaltyVCDelegate {
    @objc func onLogin()
    @objc func onRegister()
}

class LoginRegisterLoyaltyVC: UIViewController
{
    var delegate: LoginRegisterLoyaltyVCDelegate?
    
    @IBAction func onLoginClicked(_ sender: Any) {
        delegate?.onLogin()
    }
    
    @IBAction func onRegisterClicked(_ sender: Any) {
        delegate?.onRegister()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AccountHelper.shared.isRegisteredInRush {
            _ = self.navigationController?.popViewController(animated: false)
        }
    }
}
