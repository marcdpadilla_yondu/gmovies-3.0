//
//  MessageTableViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 18/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class MessageTableViewController: UITableViewController {
    
    private var messages : [MessageModel] = []
    private var errorView : ErrorView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        downloadMessages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.title = "MESSAGES"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "messages")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageIdentifier", for: indexPath)
        
        configureCell(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableCell(withIdentifier: "sectionIdentifier") as! SwitchCell
        
        configureSection(view: view, section: section)
        
        return view.contentView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let msg = messages[indexPath.row]
        
        let msgView = MessageDetailViewController(nibName: "MessageDetailViewController", bundle: nil)
        msgView.messageObject = msg
        let nav = MNavigationViewController(rootViewController: msgView)
        nav.navigationBar.titleTextAttributes = [NSFontAttributeName : UIFont(name: "OpenSans", size:15.0)!, NSForegroundColorAttributeName : UIColor.white]
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.barTintColor = UIColor(netHex: 0x28314E)
        nav.navigationBar.backgroundColor = UIColor(netHex: 0x28314E)
        nav.navigationBar.tintColor = UIColor.white
        nav.navigationBar.isTranslucent = false
        nav.extendedLayoutIncludesOpaqueBars = true
        self.present(nav, animated: true, completion: nil)
    }
    
    private func configureCell(cell : UITableViewCell, indexPath : IndexPath) {
        let msgModel = messages[indexPath.row]
        
        cell.textLabel?.text = msgModel.message!
        cell.detailTextLabel?.text = "\(msgModel.displayDate) | \(msgModel.displayTime)"
    }
    
    private func configureSection(view : SwitchCell, section : Int) {
        let config : Int? = UserDefaults.standard.value(forKey: "enableNotifications") as? Int ?? 1
        view.toggleSwitch.isOn = config! == 1 ? true : false
    }
    
    private func downloadMessages() {
        GMoviesAPIManager.getMessages { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    let data : [NSDictionary] = result?.value(forKey: "data") as! [NSDictionary]
                    
                    self.messages.removeAll()
                    for d in data {
                        self.messages.append(MessageModel(data: d))
                    }
                    
                    
                    DispatchQueue.main.async {
                        if self.messages.count == 0 {
                            self.loadErrorView(titleString: "No messages", description: "Your messages will be posted here")
                        }
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.loadErrorView(titleString: "No messages", description: "Your messages will be posted here")
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            } else {
                print(error!.localizedDescription)
                DispatchQueue.main.async {
                    self.loadErrorView(titleString: "No messages", description: "Your messages will be posted here")
                }
            }
        }
    }
    
    @IBAction func toggleAction(_ sender: Any) {
        print((sender as! UISwitch).isOn == true ? "enable" : "disable")
        GMoviesAPIManager.editNotification(action: (sender as! UISwitch).isOn == true ? "enable" : "disable", completion: { (error, result) in

            if error == nil {
                let status = result?.value(forKeyPath: "response.status") as! Int
//                print(result)
                if status == 1 {
                    UserDefaults.standard.set((sender as! UISwitch).isOn == true ? 1 : 0, forKey: "enableNotifications")
                    UserDefaults.standard.synchronize()
                }
            } else {
                print(error!.localizedDescription)
            }
            
        })
    }
    
    private func loadErrorView(titleString : String, description : String, image: UIImage = UIImage(named: "no-messages")!) {
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = tableView.subviews.first!
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 44.0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addSubview(errorView!)
        self.view.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
