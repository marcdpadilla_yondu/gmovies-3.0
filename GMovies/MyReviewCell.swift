//
//  MyReviewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 16/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import Cosmos

class MyReviewCell: UITableViewCell {
    
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var createdDate: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
