//
//  PromoCodeViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 9/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PromoCodeViewController: UIViewController, UITextFieldDelegate {
    
    var delegate : PromoCodeProtocol?
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var promoCodeBorder: UIView!
    @IBOutlet weak var promoCodeField: UITextField!
    
    var seatCount : String?
    var theaterId : String?
    var movieId : String?
    var ticketPrice: String?
    var paymentOption : PaymentOptionModel?
    
    private var timer : Timer?
    private var ticker : Int = 0
    private var isValidated : Bool = false
    private var details : NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        spinner.hidesWhenStopped = true
        spinner.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "promo_code_entry")
        promoCodeField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func applyAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "promo_code_entry_apply")
        if validate() {
            delegate?.didApplyPromoCode(promoCode: promoCodeField.text!, isValid: isValidated, details: details)
        } else {
            delegate?.didApplyPromoCode(promoCode: promoCodeField.text!, isValid: false, details: details)
        }
        
        let popup : PopupViewController = self.parent as! PopupViewController
        popup.hide()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "promo_code_entry_cancel")
        let popup : PopupViewController = self.parent as! PopupViewController
        popup.hide()
    }
    
    private func validate() -> Bool {
        
        if promoCodeField.text?.characters.count == 0 {
            return false
        }
        
        return true
    }
    
    private func checkPromoCode(promoCode : String) {
        let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
        
        isValidated = false
        spinner.startAnimating()
        spinner.isHidden = false
        if paymentOption?.type != "bank_promo" {
            GMoviesAPIManager.verifyPromoCode(userId: user.value(forKey: "uuid") as! String, promoCode: promoCode, mobile: user.value(forKey: "mobile") as! String, seatCount: seatCount!, theaterId: theaterId!, movieId: movieId!, ticketPrice: ticketPrice!,  completion: { (error, result) in
                
                if error == nil {
                    
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    DispatchQueue.main.async {
                        self.errorMessage.isHidden = false
                        if status == 1 {
                            self.errorMessage.text = "Promo is valid!"
                            self.isValidated = true
                            self.details = result?.value(forKeyPath: "response.details") as? NSDictionary
                        } else {
                            self.errorMessage.text = result?.value(forKeyPath: "response.msg") as? String
                            self.isValidated = false
                            self.details = nil
                        }
                    }
                } else {
                    self.isValidated = false
                    print(error!.localizedDescription)
                }
                DispatchQueue.main.async {
                    self.spinner.stopAnimating()
                    self.spinner.isHidden = true
                }
            })
        } else {
            
            let vals = paymentOption?.optionId?.components(separatedBy: "_")
            spinner.startAnimating()
            spinner.isHidden = false
            GMoviesAPIManager.verifyBankPromoCode(userId: user.value(forKey: "uuid") as! String, promoCode: promoCode, mobile: user.value(forKey: "mobile") as! String, seatCount: seatCount!, theaterId: theaterId!, bankPromoId: "\(vals!.first!)-promo",ticketPrice: ticketPrice!,  completion: { (error, result) in
                
                if error == nil {
                    
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    DispatchQueue.main.async {
                        self.errorMessage.isHidden = false
                        if status == 1 {
                            self.errorMessage.text = "Promo is valid!"
                            self.isValidated = true
                            self.details = result?.value(forKeyPath: "response.details") as! NSDictionary
                        } else {
                            self.errorMessage.text = result?.value(forKeyPath: "response.msg") as! String
                            self.isValidated = false
                            self.details = nil
                        }
                    }
                } else {
                    self.isValidated = false
                    print(error!.localizedDescription)
                }
                
                DispatchQueue.main.async {
                    self.spinner.stopAnimating()
                    self.spinner.isHidden = true
                }
            })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        timer?.invalidate()
        timer = nil
        ticker = 0
        
        errorMessage.isHidden = true
        isValidated = false
        details = nil
        
        
        if string == "" && textField.text?.characters.count == 1 {
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
                self.spinner.isHidden = true
            }
            return true
        }
        
        if string == "" {
            textField.deleteBackward()
        } else {
            textField.insertText(string.uppercased())
        }
        
        
        DispatchQueue.main.async {
            self.spinner.startAnimating()
            self.spinner.isHidden = false
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
        
        
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        promoCodeField.resignFirstResponder()
        
        return true
    }
    
    @objc private func tick() {
        self.ticker += 1
        
        if self.ticker == 3 {
            self.timer?.invalidate()
            self.ticker = 0
            
            DispatchQueue.main.async {
                self.checkPromoCode(promoCode: self.promoCodeField.text!)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol PromoCodeProtocol {
    func didApplyPromoCode(promoCode : String, isValid : Bool, details : NSDictionary?)
}
