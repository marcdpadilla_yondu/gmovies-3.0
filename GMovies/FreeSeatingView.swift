//
//  FreeSeatingView.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 26/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class FreeSeatingView: UIView {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        countLabel.layer.borderWidth = 0.5
        countLabel.layer.borderColor = instructionLabel.textColor.cgColor
        
        minusButton.tag = -1
        plusButton.tag = 1
        
    }

}
