//
//  PopDropViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 1/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PopDropViewController: UIViewController {
    
    
    fileprivate var window: UIWindow!
    @IBOutlet weak var overlay: UIView!
    @IBOutlet weak var contentView: UIView!
    let triangleView : UIView = UIView()
    
    
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewRight: NSLayoutConstraint!
    @IBOutlet weak var contentViewCenterY: NSLayoutConstraint!
    @IBOutlet weak var contentViewLeft: NSLayoutConstraint!
    
    fileprivate var collapseCenter : CGFloat = 0.0
    fileprivate var collapseLeft : CGFloat = 0.0
    fileprivate var collapseRight : CGFloat = 0.0
    
    fileprivate var mainWindow : UIWindow?
    fileprivate var childVc : UIViewController?
    
    var height : CGFloat = 170.0
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        mainWindow = UIApplication.shared.keyWindow
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window.windowLevel = UIWindowLevelAlert
        window.rootViewController = self
    }
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, expandedHeight h : CGFloat) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        mainWindow = UIApplication.shared.keyWindow
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window.windowLevel = UIWindowLevelAlert
        window.rootViewController = self
        height = h
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        overlay.alpha = 0.0
        overlay.backgroundColor = UIColor.black
        contentViewCenterY.constant = -contentViewHeightConstraint.constant/2.0
        contentViewHeightConstraint.constant = 0
        contentViewRight.constant = UIScreen.main.bounds.size.width
        
        collapseRight = contentViewRight.constant
        collapseCenter = contentViewCenterY.constant
        collapseLeft = contentViewLeft.constant
        
        self.contentView.layer.shadowColor = UIColor.black.cgColor
        self.contentView.layer.shadowRadius = 5.0
        self.contentView.layer.shadowOpacity = 0.7
        self.contentView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.view.addSubview(triangleView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : Custom methods
    
    @IBAction func outsideTapped(_ sender: AnyObject) {
        
        hide()
    }
    
    
    func show() {
        window.makeKeyAndVisible()
        
        self.view.layoutIfNeeded()
        
        contentViewHeightConstraint.constant = height
        contentViewRight.constant = 0
        contentViewCenterY.constant = 0
        
        collapseRight = contentViewRight.constant
        collapseCenter = contentViewCenterY.constant
        collapseLeft = contentViewLeft.constant
        
        UIView.animate(withDuration: 0.250, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            self.overlay.alpha = 0.4
            self.view.layoutIfNeeded()
        }) { (_) in
            
        }
    }
    
    
    func hide() {
        window.resignKey()
        self.view.layoutIfNeeded()
        
        contentViewCenterY.constant = collapseCenter
        contentViewHeightConstraint.constant = 0
        contentViewRight.constant = collapseRight
        contentViewLeft.constant = collapseLeft
        
        
        UIView.animate(withDuration: 0.250, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            self.overlay.alpha = 0.0
            self.triangleView.alpha = 0.0
            self.triangleView.frame = CGRect(x: self.triangleView.frame.origin.x, y: self.triangleView.frame.origin.y, width: 0.0, height: 0.0)
            self.view.layoutIfNeeded()
        }) { (_) in
            self.window.isHidden = true
            
            self.mainWindow!.makeKeyAndVisible()
        }
    }
    
    func showFromView(originView v: UIView, offset: CGPoint?) {
        
        let convertedFrame : CGRect = self.view.convert(v.frame, to: self.view)
        let screenMidY : CGFloat = UIScreen.main.bounds.size.height/2.0
        let yOffset = offset == nil ? 0.0 : offset!.y
        let xOffset = offset == nil ? 0.0 : offset!.x
        let vMidY : CGFloat = convertedFrame.origin.y + (convertedFrame.size.height/2.0)
        let vMidX : CGFloat = convertedFrame.origin.x + (convertedFrame.size.width/2.0) + xOffset
        
        window.makeKeyAndVisible()
        
        contentViewLeft.constant = vMidX
        contentViewRight.constant = UIScreen.main.bounds.size.width-vMidX
        contentViewHeightConstraint.constant = 0.0
        contentViewCenterY.constant = (vMidY + yOffset)-screenMidY
        self.view.layoutIfNeeded()
        
        collapseRight = contentViewRight.constant
        collapseCenter = contentViewCenterY.constant
        collapseLeft = contentViewLeft.constant
        
        
        var triangleInvert : Bool = false
        if convertedFrame.origin.y + height + 25.0 + convertedFrame.size.height + yOffset > UIScreen.main.bounds.size.height {
            
            contentViewLeft.constant = 0
            contentViewRight.constant = 0
            contentViewHeightConstraint.constant = height
            contentViewCenterY.constant -= (25.0+(contentViewHeightConstraint.constant/2.0))
            triangleInvert = true
            
        } else {
            
            contentViewLeft.constant = 0
            contentViewRight.constant = 0
            contentViewHeightConstraint.constant = height
            contentViewCenterY.constant += (25.0+(contentViewHeightConstraint.constant/2.0))
        }
        
        triangleView.center = CGPoint(x: vMidX, y: triangleInvert == false ? vMidY + yOffset : vMidY - yOffset)
        self.view.bringSubview(toFront: triangleView)
        UIView.animate(withDuration: 0.250, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            self.overlay.alpha = 0.4
            self.triangleView.alpha = 1.0
            self.drawTriangle(point: CGPoint(x: vMidX, y: vMidY + yOffset), inverted: triangleInvert)
            self.view.layoutIfNeeded()
            }) { (_) in
                
        }
        
    }
    
    func showViewControllerFromView(viewController vc: UIViewController, originView v: UIView, offset: CGPoint?) {
        
        childVc = vc
        childVc?.view.translatesAutoresizingMaskIntoConstraints = false
        
        vc.willMove(toParentViewController: self)
        self.addChildViewController(childVc!)
        
        vc.didMove(toParentViewController: self)
        
        self.showFromView(originView: v, offset: offset)
        
        contentView.addSubview(childVc!.view)
        let constraints : [NSLayoutConstraint] = [NSLayoutConstraint(item: childVc!.view, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: childVc!.view, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: childVc!.view, attribute: .right, relatedBy: .equal, toItem: contentView, attribute: .right, multiplier: 1.0, constant: 0.0),
                                                  NSLayoutConstraint(item: childVc!.view, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1.0, constant: 10.0)]
        self.contentView.addConstraints(constraints)
        self.contentView.layoutIfNeeded()
    }
    
    private func drawTriangle(point : CGPoint, inverted : Bool) {
        let path : UIBezierPath = UIBezierPath()
        triangleView.backgroundColor = UIColor.white
        if (inverted) {
            triangleView.frame = CGRect(origin: CGPoint(x: point.x - 9.0, y: point.y - 27.0), size: CGSize(width: 18.0, height: 18.0))
            path.move(to: CGPoint(x: 9.0, y: 18.0))
            path.addLine(to: CGPoint(x: 0.0, y: 0.0))
            path.addLine(to: CGPoint(x: 18.0, y: 0.0))
        } else {
            path.move(to: CGPoint(x: 9.0, y: 0.0))
            path.addLine(to: CGPoint(x: 0.0, y: 18.0))
            path.addLine(to: CGPoint(x: 18.0, y: 18.0))
            triangleView.frame = CGRect(origin: CGPoint(x: point.x - 9.0, y: point.y + 9.0), size: CGSize(width: 18.0, height: 18.0))
        }
        
        path.close()
        
        let mask : CAShapeLayer = CAShapeLayer()
        mask.frame = triangleView.bounds
        mask.path = path.cgPath
        
        triangleView.layer.mask = mask
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
