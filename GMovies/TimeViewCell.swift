//
//  TimeViewCell.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 12/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TimeViewCell: UITableViewCell {
    
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var variant: UILabel!
    @IBOutlet weak var price: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
