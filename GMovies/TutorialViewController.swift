//
//  TutorialViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 28/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MTabBarProtocol {
    
    private var layout = UICollectionViewFlowLayout()
    
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var laterButton: UIButton!
    @IBOutlet weak var selectPreferredButton: MButton!
    
    private var myAccountTab : MTabBarViewController?
    
    var newlyRegisteredLoyaltyUser: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        collectionView?.setCollectionViewLayout(layout, animated: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserDefaults.standard.set(false, forKey: "showTutorial")
        UserDefaults.standard.synchronize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : TutorialViewCell? = nil
        
        var identifier : String = ""
        
        if indexPath.row == 0 {
            identifier = "tutorialStep1Cell"
        } else if indexPath.row == 1 {
            identifier = "tutorialStep2Cell"
        } else if indexPath.row == 2 {
            identifier = "tutorialStep3Cell"
        } else if indexPath.row == 3 {
            identifier = "tutorialStep4Cell"
        } else if indexPath.row == 4 {
            identifier = "tutorialStep5Cell"
            
        }
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? TutorialViewCell
        
        if indexPath.row == 3 {
            let okButton = cell?.contentView.viewWithTag(990) as! EZUIButton
            okButton.addTarget(self, action: #selector(self.okButton), for: .touchUpInside)
        }
        
        if indexPath.row == 4 {
            let selectButton = cell?.contentView.viewWithTag(990) as! MButton
            let skipButton = cell?.contentView.viewWithTag(991) as! UIButton
            
            selectButton.addTarget(self, action: #selector(self.selectCinemas), for: .touchUpInside)
            skipButton.addTarget(self, action: #selector(self.skip), for: .touchUpInside)
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.300, delay: 0.0, options: .curveEaseOut, animations: {
            cell.alpha = 1.0
        }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height-2.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        pager.currentPage = page
    }
    
    func selectCinemas(_ sender: Any) {
        loadHomeStoryboard(true, selectCinemas: true)
    }
    
    func skip(_ sender: Any) {
        loadHomeStoryboard(true)
    }
    
    func okButton(_ sender: Any) {
        if newlyRegisteredLoyaltyUser {
            let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
            successRegistration.delegate = self
            successRegistration.userName = AccountHelper.shared.currentUser?.firstName
            successRegistration.dismissButtonTitle = "Go to Rewards"
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.show(successRegistration, sender: self)
        }else {
            loadHomeStoryboard(true)
        }
    }
    
    fileprivate func loadHomeStoryboard(_ animated : Bool, selectCinemas : Bool = false, selectRewards: Bool = false) {
        var storyboard : UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let tab : MTabBarViewController = storyboard.instantiateInitialViewController() as! MTabBarViewController
        
        let home : UINavigationController = storyboard.instantiateViewController(withIdentifier: "HomeViewNavigation") as! UINavigationController
        home.title = "Movies"
        home.tabImageOn = UIImage(named: "movies-active")
        home.tabImageOff = UIImage(named: "movies")
        home.tabTextColorOn = UIColor(netHex: 0x435082)
        home.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Cinemas", bundle: nil)
        let cinemas : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        cinemas.title = "Cinemas"
        cinemas.tabImageOn = UIImage(named: "cinemas-active")
        cinemas.tabImageOff = UIImage(named: "cinemas")
        cinemas.tabTextColorOn = UIColor(netHex: 0x435082)
        cinemas.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Spotlight", bundle: nil)
        let spotlight : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        spotlight.title = "Spotlight"
        spotlight.tabImageOn = UIImage(named: "spotlight-active")
        spotlight.tabImageOff = UIImage(named: "spotlight")
        spotlight.tabTextColorOn = UIColor(netHex: 0x435082)
        spotlight.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "Rewards", bundle: nil)
        let vc2 : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        vc2.title = "Rewards"
        vc2.tabImageOn = UIImage(named: "myrewards-active")
        vc2.tabImageOff = UIImage(named: "myrewards")
        vc2.tabTextColorOn = UIColor(netHex: 0x435082)
        vc2.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
        let myAccount : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        myAccount.title = " "
        myAccount.tabImageOn = UIImage(named: "myaccount-active")
        myAccount.tabImageOff = UIImage(named: "myaccount")
        myAccount.tabTextColorOn = UIColor(netHex: 0x435082)
        myAccount.tabTextColorOff = UIColor(netHex: 0xaeb5cb)
        
        myAccountTab = (myAccount.topViewController as? MTabBarViewController)
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            myAccountTab?.initialViewControllerIndex = 2
        }
        myAccountTab!.delegate = self
        myAccountTab!.isIndicatorEnabled = true
        myAccountTab!.indicatorHugsContent = true
        let ticketList : TicketsListViewController = storyboard.instantiateViewController(withIdentifier: "TicketListView") as! TicketsListViewController
        myAccountTab!.title = "My Account"
        ticketList.tabImageOn = UIImage(named: "tickets-active")
        ticketList.tabImageOff = UIImage(named: "tickets")
        
        let watchList : WatchListViewController = storyboard.instantiateViewController(withIdentifier: "WatchListView") as! WatchListViewController
        watchList.tabImageOn = UIImage(named: "watchlist-active")
        watchList.tabImageOff = UIImage(named: "watchlist-1")
        
        let profileView : ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileView") as! ProfileViewController
        profileView.tabImageOn = UIImage(named: "profile-active")
        profileView.tabImageOff = UIImage(named: "profile")
        
        if selectCinemas == true {
            tab.initialViewControllerIndex = 1
            UserDefaults.standard.set(true, forKey: "showHeartPopup")
            UserDefaults.standard.synchronize()
        }
        
        if selectRewards == true {
            tab.initialViewControllerIndex = 3
        }
        
        myAccountTab!.viewControllers = [ticketList, watchList, profileView]
        
        tab.viewControllers = [home, cinemas, spotlight, vc2, myAccount]
        
        
        
        self.navigationController?.pushViewController(tab, animated: animated)
    }
    
    func shouldSelectViewControllerAtIndex(tabController: MTabBarViewController, index: Int) -> Bool {
        
        if tabController == myAccountTab {
            let loggedin : Bool = UserDefaults.standard.bool(forKey: "loggedin")
            if loggedin == false {
                if index != 2 {
                    let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showLoginModal()
                    return false
                }
            }
        }
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TutorialViewController: LoyaltyRegistrationSuccessVCDelegate {
    func didClickDismiss(sender:LoyaltyRegistrationSuccessVC) {
        loadHomeStoryboard(true, selectCinemas: false, selectRewards: true)
    }
}
