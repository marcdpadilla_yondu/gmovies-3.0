//
//  PromoModel.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 11/05/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import Foundation

class PromoModel: NSObject
{
    var name: String
    var desc: String
    var discountType: String
    var discountValue: String
    var discountedSeats: Int
    var paymentOptions: [PaymentOptionModel]
    var promoType: String
    var bankType: String
    var code: String = ""
    
    init(data : NSDictionary) {
        
        name = data.value(forKey: "name") as! String
        desc = data.value(forKey: "description") as! String
        discountType = data.value(forKey: "discount_type") as! String
        if let discVal = data.value(forKey: "discount_value") as? String {
            discountValue = discVal
        }else {
            discountValue =  String(data.value(forKey: "discount_value") as! Int)
        }
        
        discountedSeats = (data.value(forKey: "discounted_seats") as? NSString)?.integerValue ?? 0
        
        let pOptions = data.value(forKey: "payment_options") as! [NSDictionary]
        paymentOptions = []
        
        for d in pOptions {
            paymentOptions.append(PaymentOptionModel(data: d))
        }
        
        promoType = data.value(forKey: "promo_type") as! String
        bankType = data.value(forKey: "bank_type") as! String
    }
}
