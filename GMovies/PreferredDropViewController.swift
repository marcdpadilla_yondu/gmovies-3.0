//
//  PreferredDropViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 24/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class PreferredDropViewController: UITableViewController {
    
    private var _dataSource : [TheaterModel]?
    var dataSource : [TheaterModel]? {
        set (ds) {
            _dataSource = ds
            self.tableView.reloadData()
        }
        
        get {
            return _dataSource
        }
    }
    
    var delegate : PreferredTheaterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.bool(forKey: "loggedin") == false {
            tableView.tableFooterView?.isHidden = true
        } else {
            tableView.tableFooterView?.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if _dataSource == nil {
            return 0
        }
        
        if (_dataSource?.count)! >= 3 {
            return 3
        }
        
        return (_dataSource?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "preferredIdentifier", for: indexPath)

        configureCell(cell: cell, indexPath: indexPath)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if _dataSource == nil {
            return
        }
        
        if delegate != nil {
            delegate!.didSelectTheater(to: _dataSource![indexPath.row])
        }
    }
    
    //MARK: Private functions
    
    private func configureCell(cell : UITableViewCell, indexPath : IndexPath) {
        
        if _dataSource == nil {
            return
        }
        
        let to = _dataSource![indexPath.row]
        
        cell.textLabel?.text = to.name!
    }
    
    @IBAction func allCinemasAction(_ sender: AnyObject) {
        delegate?.didSelectAllTheaters()
    }
    
    @IBAction func editCinemasAction(_ sender: AnyObject) {
        delegate?.didSelectEdit()
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol PreferredTheaterProtocol {
    func didSelectTheater(to : TheaterModel)
    func didSelectAllTheaters()
    func didSelectEdit()
}
