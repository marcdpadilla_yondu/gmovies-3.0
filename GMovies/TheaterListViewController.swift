//
//  TheaterListViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 10/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class TheaterListViewController: UIViewController, AllCinemasControllerProtocol {
    
    var movieObject : MovieModel?
    private let cellIdentifier = "valueCell"
    private let sectionIdentifier = "sectionCell"
    private let defaultIdentifier = "cell"
    private var theaters : [TheaterModel]?
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        tableView.dataSource = appDelegate.allCinemasController
        tableView.delegate = appDelegate.allCinemasController
        appDelegate.allCinemasController?.tableView = tableView
        appDelegate.allCinemasController?.movieObject = movieObject
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.allCinemasController?.delegate = self
        self.title = movieObject?.movieTitle!
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectDataSource(data: [TheaterModel]) {
        theaters = data
        //print(data)
        self.performSegue(withIdentifier: "showInnerList", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInnerList" {
            (segue.destination as! TheaterListInnerViewController).theaters = theaters
            (segue.destination as! TheaterListInnerViewController).movieObject = movieObject
            
            if theaters != nil {
                if movieObject == nil {
                    if theaters!.count > 0 {
                        segue.destination.title = theaters![0].city!
                    }
                } else {
                    segue.destination.title = movieObject!.movieTitle!
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
