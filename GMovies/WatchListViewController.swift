//
//  WatchListViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 15/10/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class WatchListViewController: UITableViewController {
    
    private var watchList : [MovieModel] = []
    
    private var errorView : ErrorView?
    
    private var loadingScreen : LoaderView?
    private var currentPopDrop : PopDropViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""
        
        self.loadErrorView(titleString: "No saved movies yet", description: "Tap the plus icon on the movie poster to save it for later")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "watchlist")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.Watchlist)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        downloadWatchlist()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        var count : Int = (NSArray(array: watchList).value(forKeyPath: "@distinctUnionOfObjects.showing") as! NSArray).count
        if count == 0 {
            count = 1
        }
        
        return count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        let showingCount : Int = (watchList.filter { (mo) -> Bool in
            return mo.showing == true
        } as NSArray).count
        
        let comingSoonCount : Int = (watchList.filter { (mo) -> Bool in
            return mo.showing == false
        } as NSArray).count
        
        var rowCount : Int = 0
        
        if section == 0 {
            rowCount = showingCount
            if showingCount == 0 {
                rowCount = comingSoonCount
            }
        } else {
            rowCount = comingSoonCount
        }
        
        return rowCount
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 145.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 35.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let count : Int = (NSArray(array: watchList).value(forKeyPath: "@distinctUnionOfObjects.showing") as! NSArray).count
        if count == 0 {
            return nil
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionCell")
        
        configureSectionHeader(view: cell as! RegionCell, section: section)
        
        return cell?.contentView
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WatchCell", for: indexPath)

        configureCell(cell: cell as! WatchViewCell, indexPath: indexPath)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard : UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let movieDetailNav : UINavigationController = storyboard.instantiateViewController(withIdentifier: "movieDetailNav") as! UINavigationController
        let movieDetail : MovieDetailViewController = movieDetailNav.topViewController as! MovieDetailViewController
        
        movieDetail.theaterObject = nil
        let filteredMovies = watchList.filter { (m) -> Bool in
            let sections = self.tableView.numberOfSections
            if sections == 2 {
                if indexPath.section == 1 {
                    return m.showing == false
                }
            } else {
                if m.showing == false {
                    return m.showing == false
                }
            }
            return m.showing == true
        }
        movieDetail.movieObject = filteredMovies[indexPath.row]
        
        self.present(movieDetailNav, animated: true, completion: nil)
    }
    
    //MARK: Private functions
    
    private func loadErrorView(titleString : String, description : String, image: UIImage = UIImage(named: "no-watchlist")!) {
        
        errorView?.removeFromSuperview()
        errorView = nil
        
        errorView = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as? ErrorView
        errorView!.translatesAutoresizingMaskIntoConstraints = false
        
        let sp : UIView = tableView.subviews.first!
        
        var constraints : [NSLayoutConstraint] = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .leading, relatedBy: .equal, toItem: sp, attribute: .leading, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .top, relatedBy: .equal, toItem: sp, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .trailing, relatedBy: .equal, toItem: sp, attribute: .trailing, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: errorView!, attribute: .bottom, relatedBy: .equal, toItem: sp, attribute: .bottom, multiplier: 1.0, constant: 0))
        
        self.view.addSubview(errorView!)
        self.view.addConstraints(constraints)
        
        errorView!.errorTitle.text = titleString
        errorView!.errorDesc.text = description
        errorView!.errorIcon.image = image
        errorView?.retryButton.isHidden = true
    }
    
    private func downloadWatchlist() {
        
        self.view.addSubview(loadingScreen!)
        self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                  ])
        
        self.view.layoutIfNeeded()
        
        loadingScreen?.startAnimating()
        
        
        GMoviesAPIManager.getWatchList { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKey: "status") as! Int
                if status == 1 {
                    self.watchList.removeAll()
                    
                    let movArray : [NSDictionary] = result?.value(forKey: "data") as! [NSDictionary]
                    
                    if movArray.count == 0 {
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            self.loadErrorView(titleString: "No saved movies yet", description: "Tap the plus icon on the movie poster to save it for later")
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            self.tableView.separatorStyle = .singleLine
                            self.errorView?.removeFromSuperview()
                            self.errorView = nil
                        }
                    }
                    
                    for movie in movArray {
                        
                        let mo : MovieModel = MovieModel(data: movie.value(forKey: "movie") as! NSDictionary)
                        mo.movieId = movie.value(forKey: "movie_id") as? String
                        mo.watched = true
                        
                        self.watchList.append(mo)
                    }
                    
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                        self.tableView.reloadData()
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.loadingScreen?.stopAnimating()
                        self.loadingScreen?.removeFromSuperview()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.loadingScreen?.stopAnimating()
                    self.loadingScreen?.removeFromSuperview()
                }
                print(error!.localizedDescription)
            }
        }
    }
    
    private func configureCell(cell : WatchViewCell, indexPath: IndexPath) {
        
        let showingCount : Int = (watchList.filter { (mo) -> Bool in
            return mo.showing == true
            } as NSArray).count
        
        var mo : MovieModel? = nil
        
        if indexPath.section == 0 {
            if showingCount == 0 {
                mo = (watchList.filter({ (mo) -> Bool in
                    return mo.showing == false
                }) as NSArray)[indexPath.row] as? MovieModel
            } else {
                mo = (watchList.filter({ (mo) -> Bool in
                    return mo.showing == true
                }) as NSArray)[indexPath.row] as? MovieModel
            }
        } else {
            mo = (watchList.filter({ (mo) -> Bool in
                return mo.showing == false
            }) as NSArray)[indexPath.row] as? MovieModel
        }
        
        cell.watchButton.tag = indexPath.row
        cell.watchButton.setValue(indexPath, forKey: "payload")
        
        if cell.menuButton.actions(forTarget: self, forControlEvent: .touchUpInside) == nil{
            cell.menuButton.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)
        }

        if cell.watchButton.actions(forTarget: self, forControlEvent: .touchUpInside) == nil {
            cell.watchButton.addTarget(self, action: #selector(WatchListViewController.watchlistButtonAction), for: .touchUpInside)
        }
        
        if indexPath.section == 1 {
            cell.rottenRating.isHidden = true
            cell.audienceRating.isHidden = true
            cell.runtime.text = UIManager.watchlistDateFormatter(dateString: (mo?.releaseDateString)!)
            print((mo?.releaseDateString)!)
            cell.runtime.numberOfLines = 0
            cell.runtime.textColor = UIColor(netHex: 0x0e84d6)
            
            if mo?.hasSchedule == true {
                cell.abBg.isHidden = false
                cell.advanceBooking.isHidden = false
            } else {
                cell.abBg.isHidden = true
                cell.advanceBooking.isHidden = true
            }
        } else {
            cell.rottenRating.isHidden = false
            cell.audienceRating.isHidden = false
            cell.runtime.textColor = UIColor.black
            cell.runtime.text = mo?.length!
            
            switch mo!.rottenSource! {
            case "certified fresh":
                cell.rottenRating.setImage(UIImage(named: "fresh"), for: .normal)
                break
            case "upright":
                cell.rottenRating.setImage(UIImage(named: "popcorn-full"), for: .normal)
                break
            case "spilled":
                cell.rottenRating.setImage(UIImage(named: "popcorn-spilled"), for: .normal)
                break
            case "rotten":
                cell.rottenRating.setImage(UIImage(named: "rotten"), for: .normal)
                break
            case "fresh":
                cell.rottenRating.setImage(UIImage(named: "tomatoe"), for: .normal)
                break
            default:
                cell.rottenRating.setImage(UIImage(named: "plus"), for: .normal)
            }
            
            switch mo!.audienceSource! {
            case "certified fresh":
                cell.audienceRating.setImage(UIImage(named: "fresh"), for: .normal)
                break
            case "upright":
                cell.audienceRating.setImage(UIImage(named: "popcorn-full"), for: .normal)
                break
            case "spilled":
                cell.audienceRating.setImage(UIImage(named: "popcorn-spilled"), for: .normal)
                break
            case "rotten":
                cell.audienceRating.setImage(UIImage(named: "rotten"), for: .normal)
                break
            case "fresh":
                cell.audienceRating.setImage(UIImage(named: "tomatoe"), for: .normal)
                break
            default:
                cell.audienceRating.setImage(UIImage(named: "plus"), for: .normal)
            }
            
            cell.rottenRating.setTitle("\(mo!.rottenRatingPercent!)%", for: .normal)
            cell.audienceRating.setTitle("\(mo!.audienceRatingPercent!)%", for: .normal)
            cell.abBg.isHidden = true
            cell.advanceBooking.isHidden = true
        }
        
        if mo?.watched == true {
            cell.watchButton.backgroundColor = UIManager.buttonRed
            cell.watchButton.setImage(UIImage(named: "watchlist-check"), for: .normal)
        } else {
            cell.watchButton.backgroundColor = UIManager.watchButtonGray
            cell.watchButton.setImage(UIImage(named: "watchlist-add"), for: .normal)
        }
        
        cell.movieTitle.text = mo?.movieTitle!
        cell.advisory.text = mo?.advisoryRatingString!
        cell.genren.text = mo?.genre
        
        if mo!.poster == nil {
            downloadPoster(mo: mo!, indexPath: indexPath)
        } else {
            cell.posterCell.image = mo!.poster
        }
    }
    
    @objc private func watchlistButtonAction(sender : MButton) {
        AnalyticsHelper.sendButtonEvent(name: "watchlist_plus_button")
        let indexPath : IndexPath = sender.value(forKey: "payload") as! IndexPath
        
        let showingCount : Int = (watchList.filter { (mo) -> Bool in
            return mo.showing == true
            } as NSArray).count
        
        var mo : MovieModel? = nil
        
        if indexPath.section == 0 {
            if showingCount == 0 {
                mo = (watchList.filter({ (mo) -> Bool in
                    return mo.showing == false
                }) as NSArray)[indexPath.row] as? MovieModel
            } else {
                mo = (watchList.filter({ (mo) -> Bool in
                    return mo.showing == true
                }) as NSArray)[indexPath.row] as? MovieModel
            }
        } else {
            mo = (watchList.filter({ (mo) -> Bool in
                return mo.showing == false
            }) as NSArray)[indexPath.row] as? MovieModel
        }
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if mo!.watched == true {
            let alert = UIAlertController(title: "", message: "You’re about to remove this movie from your watchlist", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
                //sender.isOn = true
            })
            let removeAction = UIAlertAction(title: "Remove", style: .default, handler: { (_) in
                sender.loadingIndicator(show: true)
                appDelegate.removeMovieFromWatchList(mo: mo!, completion: { (success) -> Void? in
                    DispatchQueue.main.async {
                        sender.loadingIndicator(show: false)
                        sender.backgroundColor = UIManager.buttonRed
                        sender.setImage(UIImage(named: "watchlist-check"), for: .normal)
                        
                        self.watchList = self.watchList.filter({ (mo1) -> Bool in
                            return mo1 != mo
                        })
                        
                        if self.tableView.numberOfRows(inSection: indexPath.section) == 1 {
                            self.tableView.beginUpdates()
                            if self.tableView.numberOfSections > 1 {
                                self.tableView.deleteSections(IndexSet(integer: indexPath.section), with: .fade)
                            } else {
                                self.tableView.deleteRows(at: [indexPath], with: .left)
                                self.loadErrorView(titleString: "No saved movies yet", description: "Tap the plus icon on the movie poster to save it for later")
                            }
                            self.tableView.endUpdates()
                            self.tableView.reloadData()
                        } else {
                            self.tableView.beginUpdates()
                            self.tableView.deleteRows(at: [indexPath], with: .left)
                            self.tableView.endUpdates()
                            self.tableView.reloadData()
                        }
                        
                        if self.watchList.count == 0 {
                            self.loadErrorView(titleString: "No saved movies yet", description: "Tap the plus icon on the movie poster to save it for later")
                        } else {
                            self.tableView.separatorStyle = .singleLine
                            self.errorView?.removeFromSuperview()
                            self.errorView = nil
                        }
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MovieModelUpdateRemove"), object: mo?.movieId!)
                        
                    }
                })
            })
            alert.addAction(cancelAction)
            alert.addAction(removeAction)
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        } else {
            sender.loadingIndicator(show: true)
            appDelegate.addMovieToWatchList(mo: mo!, completion: { (success) -> Void? in
                DispatchQueue.main.async {
                    sender.loadingIndicator(show: false)
                    sender.backgroundColor = UIManager.watchButtonGray
                    sender.setImage(UIImage(named: "watchlist-add"), for: .normal)
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MovieModelUpdateAdd"), object: mo?.movieId!)
                    
                    if self.watchList.count == 0 {
                        self.loadErrorView(titleString: "No saved movies yet", description: "Tap the plus icon on the movie poster to save it for later")
                    } else {
                        self.tableView.separatorStyle = .singleLine
                        self.errorView?.removeFromSuperview()
                        self.errorView = nil
                    }
                    let popup = PopupViewController(nibName: "PopupViewController", bundle: nil, expandedHeight: 140.0, expandedWidth: 140.0)
                    let confirmView = Bundle.main.loadNibNamed("ConfirmationView", owner: self, options: nil)?.first as! ConfirmationView
                    confirmView.translatesAutoresizingMaskIntoConstraints = false
                    confirmView.confirmImage.image = UIImage(named: "added-watchlist")
                    confirmView.confirmMessage.text = "Saved to Watchlist!"
                    
                    popup.delegate = self
                    popup.showView(view: confirmView, offset: CGPoint.zero)
                }
            })
        }
    }
    
    private func downloadPoster(mo: MovieModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: mo.posterUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    mo.poster = image
                    self.tableView?.reloadRows(at: [indexPath], with: .fade)
                }
            } else {
                
            }
        }
    }
    
    private func configureSectionHeader(view : RegionCell, section : Int) {
        if section == 0 {
            view.regionLabel.text = "Now Showing"
            let showingCount : Int = (watchList.filter { (mo) -> Bool in
                return mo.showing == true
            } as NSArray).count
            
            if showingCount == 0 {
                view.regionLabel.text = "Coming Soon"
            }
        } else {
            view.regionLabel.text = "Coming Soon"
        }
    }
    
    @objc private func menuButtonAction(sender : UIButton) {
        currentPopDrop?.hide()
        currentPopDrop = nil
        AnalyticsHelper.sendButtonEvent(name: "watch_list_menu_button")
        let cell = sender.superview?.superview as! UITableViewCell
        let indexPath = tableView.indexPath(for: cell)
        
        let share = ShareViewController(nibName: "ShareViewController", bundle: nil)
        share.parentController = self
        share.mo = watchList[indexPath!.row]
        
        currentPopDrop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 132.0)
        let rect = tableView.rectForRow(at: IndexPath(row: indexPath!.row, section: indexPath!.section))
        
        currentPopDrop?.showViewControllerFromView(viewController: share, originView: sender, offset: CGPoint(x: 0, y: 64.0+rect.origin.y-tableView.contentOffset.y))
    }
}

extension WatchListViewController: PopupViewControllerDelegate
{
    func onHidden(popup: PopupViewController) {
        
        if AccountHelper.shared.isRegisteredInRush {
            UserEngagementHelper.shared.earnBadgeFrom(engagement: UserEngagementCodes.addWatchList)
        }
    }
}
