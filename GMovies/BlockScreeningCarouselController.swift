//
//  BlockScreeningCarouselController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 18/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class BlockScreeningCarouselController: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {
    
    private var sectionsCount : Int
    var items : [BlockScreeningModel]?
    private var layout : UICollectionViewFlowLayout
    private var reuseIdentifier : String
    var sectionTitle : String = ""
    
    var collectionView : UICollectionView?
    
    override init() {
        sectionsCount = 1
        layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 2.5
        layout.minimumInteritemSpacing = 2.5
        
        reuseIdentifier = "Cell"
        items = [BlockScreeningModel]()
        
        super.init()
    }
    
    convenience init(sections sc : Int, reuseIdentifier ri : String) {
        self.init()
        
        sectionsCount = sc
        reuseIdentifier = ri
    }
    
    func setData(data : [BlockScreeningModel]?) {
        if data != nil {
            items = data
        }
        
        if collectionView != nil {
            collectionView?.setCollectionViewLayout(layout, animated: false)
            
            collectionView?.performBatchUpdates({
                self.collectionView?.reloadSections(IndexSet(integer: 0))
            }, completion: { (_) in
                
            })
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (items?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : UICollectionViewCell? = nil
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        configureCell(cell: cell as! BlockPosterCollectionViewCell, indexPath: indexPath)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.300, delay: 0.0, options: .curveEaseOut, animations: {
            cell.alpha = 1.0
        }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width/2.75, height: ((UIScreen.main.bounds.size.width/2.75) * 1.48) + 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 5.0, 0, 5.0)
    }
    
    //MARK: Private functions
    
    private func configureCell(cell : BlockPosterCollectionViewCell, indexPath: IndexPath) {
        let item = items![indexPath.row]
        
        if item.poster == nil {
            cell.posterImageView.image = nil
            downloadPoster(mo: item, indexPath: indexPath)
        } else {
            //print("display poster")
            cell.posterImageView.image = item.poster
        }
        
        cell.movieTitleLabel.text = item.movie?.movieTitle!
        cell.theaterName.text = item.theater?.name!
        cell.advanceBookingLabel.setTitle(item.screeningDateTimeString, for: .normal)
    }
    
    private func downloadPoster(mo: BlockScreeningModel, indexPath : IndexPath) {
        GMoviesAPIManager.downloadPoster(urlString: mo.posterUrl!) { (error, image) in
            if error == nil {
                DispatchQueue.main.async {
                    mo.poster = image
                    //print(mo.poster)
                    self.collectionView?.performBatchUpdates({ 
                        self.collectionView?.reloadItems(at: [indexPath])
                    }, completion: nil)
                }
            } else {
                
            }
        }
    }
    
}
