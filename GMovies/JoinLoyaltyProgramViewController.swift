//
//  LoyaltyProgramViewController.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 06/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

@objc protocol JoinLoyaltyProgramViewControllerProtocol {
    @objc optional func onJoinNowClicked()
    @objc optional func onNotNowClicked()
}

class JoinLoyaltyProgramViewController: UIViewController {

    @IBOutlet weak var joinNowButton: EZUIButton!
    @IBOutlet weak var noButton: EZUIButton!
    
    var showJoinNowButton:Bool = true
    var showNotNowButton:Bool = true
    weak var delegate: JoinLoyaltyProgramViewControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        joinNowButton.isHidden = !showJoinNowButton
        noButton.isHidden = !showNotNowButton
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AccountHelper.shared.isRegisteredInRush {
           _ = self.navigationController?.popViewController(animated: false)
        }
    }
    
    func buttons(show: Bool) {
        joinNowButton.isHidden = !show
        noButton.isHidden = !show
    }
    
    @IBAction func onJoinNowClicked(_ sender: Any) {
        self.delegate?.onJoinNowClicked?()
    }
    
    @IBAction func onNotNowClicked(_ sender: Any) {
        self.delegate?.onNotNowClicked?()
    }
}
