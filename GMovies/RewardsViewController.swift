//
//  RewardsViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 18/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import PopupDialog

class RewardsViewController: UIViewController {

    fileprivate var joinLoyaltyProgramVC: JoinLoyaltyProgramViewController?
    fileprivate var loginRegisterLoyaltyVC: LoginRegisterLoyaltyVC?
    fileprivate var loyaltyProgramVC: LoyaltyProgramViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Rewards"
        
        AnalyticsHelper.sendScreenShowEvent(name: "rewards")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.MyRewards)
        
        if AccountHelper.shared.isRegisteredInRush {
            showLoyaltyProgram()
        }else {
            showJoinLoyaltyProgram()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onLoggedInGMovies), name: AccountHelper.shared.NotificationDidLoginToGMovies, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onRegisteredRush), name: AccountHelper.shared.NotificationDidRegisterToRush, object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Rewards"
        
        AnalyticsHelper.sendScreenShowEvent(name: "rewards")
        GAnalyticsHelper.sendEvent(category: GAEventCategory.PageVisit, action: GAPageVisitCategory.MyRewards)
        
        if AccountHelper.shared.isRegisteredInRush {
            showLoyaltyProgram()
        }else {
            showJoinLoyaltyProgram()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

extension RewardsViewController {
    
    fileprivate func showJoinLoyaltyProgram () {
        
        if AccountHelper.shared.isLoggedInGMovies {
            if joinLoyaltyProgramVC == nil {
                joinLoyaltyProgramVC = JoinLoyaltyProgramViewController()
                joinLoyaltyProgramVC?.showNotNowButton = false
                joinLoyaltyProgramVC?.delegate = self
                joinLoyaltyProgramVC?.navigationItem.hidesBackButton = true
                joinLoyaltyProgramVC?.title = "Rewards"
                self.show(joinLoyaltyProgramVC!, sender: self)
            }
        }else {
            if loginRegisterLoyaltyVC == nil {
                loginRegisterLoyaltyVC = LoginRegisterLoyaltyVC()
                loginRegisterLoyaltyVC!.delegate = self
                loginRegisterLoyaltyVC?.navigationItem.hidesBackButton = true
                loginRegisterLoyaltyVC?.title = "Rewards"
                self.show(loginRegisterLoyaltyVC!, sender: self)
            }
        }
    }
    
    fileprivate func showLoyaltyProgram () {
        
        if loyaltyProgramVC == nil {
            loyaltyProgramVC = LoyaltyProgramViewController()
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.show(loyaltyProgramVC!, sender: self)
        }
    }
}

extension RewardsViewController: JoinLoyaltyProgramViewControllerProtocol
{
    func onJoinNowClicked() {
        AccountHelper.shared.registerToRush(successful: { data in
            
            let successRegistration: LoyaltyRegistrationSuccessVC = LoyaltyRegistrationSuccessVC()
            successRegistration.delegate = self
            successRegistration.userName = AccountHelper.shared.currentUser?.firstName
            successRegistration.dismissButtonTitle = "Go to Rewards"
            self.present(successRegistration, animated: true, completion: nil)
            
        }) { (error) in
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showBanner(message: error, isPersistent: false)
        }
    }
    
    func onNotNowClicked() {
        
    }
}

extension RewardsViewController: LoyaltyRegistrationSuccessVCDelegate
{
    func didClickDismiss(sender: LoyaltyRegistrationSuccessVC) {
        self.dismiss(animated: true, completion: nil)
        _ = self.navigationController?.popViewController(animated: false)
        self.showLoyaltyProgram()
    }
}

extension RewardsViewController: LoginRegisterLoyaltyVCDelegate
{
    func onLogin() {
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showLoginModal() { (welcome) in
            welcome.joinReelRewardsAutomatically = true
            welcome.loginAction(nil)
        }
    }
    
    func onRegister() {
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showLoginModal() { (welcome) in
            welcome.joinReelRewardsAutomatically = true
            welcome.createAccountAction(nil)
        }
    }
}

extension RewardsViewController
{
    func onLoggedInGMovies() {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func onRegisteredRush() {
        _ = self.navigationController?.popViewController(animated: false)
    }
}
