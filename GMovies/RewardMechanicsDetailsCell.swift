//
//  RewardMechanicsDetailsCell.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 21/03/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

class RewardMechanicsDetailsCell: UITableViewCell {

    static let ID = "RewardMechanicsDetailsCell"
    
    @IBOutlet weak var detailsLabel: UILabel!
    
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
    }
}
