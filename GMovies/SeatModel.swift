//
//  SeatModel.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 8/9/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class SeatModel: NSObject {
    var state : SeatMapState = SeatMapState.available
    var seatId : String?
    var seatName : String?
}
