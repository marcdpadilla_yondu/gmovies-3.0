//
//  FeedbackViewController.swift
//  GMovies
//
//  Created by Erson Jay Mujar on 02/02/2017.
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController {

    @IBOutlet weak var firstStar: UIButton!
    @IBOutlet weak var secondStar: UIButton!
    @IBOutlet weak var thirdStar: UIButton!
    @IBOutlet weak var fourthStar: UIButton!
    @IBOutlet weak var fifthStar: UIButton!
    @IBOutlet weak var feedbackTextView: RSKPlaceholderTextView!
    
    public fileprivate(set) var rating:Int = 0
    open var comment:String {
        get{
            return feedbackTextView.text
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedbackTextView.placeHolderFont = UIFont(name:"OpenSans-Italic", size:14)
    }
    
    @IBAction func didClickStar(_ sender: UIButton) {
        show(star: sender.tag)
        rating = sender.tag
    }
}

extension RatingViewController {
    
    func show(star count:Int) {
        switch count {
        case 1:
            firstStar.isSelected = true
            secondStar.isSelected = false
            thirdStar.isSelected = false
            fourthStar.isSelected = false
            fifthStar.isSelected = false
            break
        case 2:
            firstStar.isSelected = true
            secondStar.isSelected = true
            thirdStar.isSelected = false
            fourthStar.isSelected = false
            fifthStar.isSelected = false
            break
        case 3:
            firstStar.isSelected = true
            secondStar.isSelected = true
            thirdStar.isSelected = true
            fourthStar.isSelected = false
            fifthStar.isSelected = false
            break
        case 4:
            firstStar.isSelected = true
            secondStar.isSelected = true
            thirdStar.isSelected = true
            fourthStar.isSelected = true
            fifthStar.isSelected = false
            break
        case 5:
            firstStar.isSelected = true
            secondStar.isSelected = true
            thirdStar.isSelected = true
            fourthStar.isSelected = true
            fifthStar.isSelected = true
            break
        default: break
            
        }
    }
}
