//
//  LoyaltyProgramViewController.swift
//  GMovies
//  Copyright © 2017 Digital Ventures. All rights reserved.
//

import UIKit

class LoyaltyProgramViewController: UIViewController {
    
    @IBOutlet weak var selectionIndicatorTrailing: NSLayoutConstraint!
    @IBOutlet weak var selectionIndicatorLeading: NSLayoutConstraint!
    
    @IBOutlet weak var reelRewardsButton: UIButton!
    @IBOutlet weak var mechanicsButton: UIButton!
    @IBOutlet weak var container: UIView!
    
    var pageViewController: UIPageViewController?
    
    fileprivate var isReelRewardSegmentSelected = true
    
    private (set) lazy var orderedViewControllers: [UIViewController] = {
        return [ReelRewardsViewController(), RewardMechanicsViewController()]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageViewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        
        pageViewController?.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.addChildViewController(pageViewController!)
        container.addSubview(pageViewController!.view)
        pageViewController?.didMove(toParentViewController: self)
        
        let margins = self.container.layoutMarginsGuide
        pageViewController?.view.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 0).isActive = true
        pageViewController?.view.topAnchor.constraint(equalTo: margins.topAnchor, constant: 0).isActive = true
        pageViewController?.view.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: 0).isActive = true
        pageViewController?.view.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 0).isActive = true
        
        if let reelRewardsVC = orderedViewControllers.first {
            pageViewController?.setViewControllers([reelRewardsVC], direction: .forward, animated: false, completion: nil)
        }
    }
    
    @IBAction func onReelRewardsClicked(_ sender: Any) {
        self.setReelRewardSegment(active: true, animated: true)
    }
    
    @IBAction func onMechanicsClicked(_ sender: Any) {
        self.setReelRewardSegment(active: false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setReelRewardSegment(active: isReelRewardSegmentSelected, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    fileprivate func setReelRewardSegment(active: Bool, animated:Bool) {
        
        let halfWidth = self.view.frame.width / 2
        
        isReelRewardSegmentSelected = active
        
        if animated {
            
            self.reelRewardsButton.isSelected = active
            self.mechanicsButton.isSelected = !active
            
            if active {
                UIView.animate(withDuration: 0.3, animations: { [weak self] in
                    self?.selectionIndicatorTrailing.constant = halfWidth
                    self?.selectionIndicatorLeading.constant = 0
                    self?.view.layoutIfNeeded()
                    if (self != nil) {
                        self!.pageViewController?.setViewControllers([self!.orderedViewControllers[0]], direction: .forward, animated: animated, completion: nil)
                    }
                    }, completion: { (completed) in
                })
            }else {
                UIView.animate(withDuration: 0.3, animations: { [weak self] in
                    self?.selectionIndicatorTrailing.constant = 0
                    self?.selectionIndicatorLeading.constant = halfWidth
                    self?.view.layoutIfNeeded()
                    if (self != nil) {
                        self!.pageViewController?.setViewControllers([self!.orderedViewControllers[1]], direction: .reverse, animated: animated, completion: nil)
                    }
                    }, completion: { (completed) in
                })
            }
            
        }else {
            
            self.reelRewardsButton.isSelected = active
            self.mechanicsButton.isSelected = !active
            
            if active {
                self.selectionIndicatorTrailing.constant = halfWidth
                self.selectionIndicatorLeading.constant = 0
                self.pageViewController?.setViewControllers([self.orderedViewControllers[0]], direction: .forward, animated: animated, completion: nil)
            }else {
                self.selectionIndicatorTrailing.constant = 0
                self.selectionIndicatorLeading.constant = halfWidth
                self.pageViewController?.setViewControllers([self.orderedViewControllers[1]], direction: .reverse, animated: animated, completion: nil)
            }
            self.view.layoutIfNeeded()
        }
    }
}
