//
//  EditProfileViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 14/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class EditProfileViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, TextfieldCellDelegate, DatePickerProtocol {

    var fields : [FieldConfig]?
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImageWidth: NSLayoutConstraint!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var submitButton : UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var fbButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var logoutButton: UIButton!
    
    private var selectedImage : UIImage?
    private var currentPopDrop : PopDropViewController?
    
    private var base64Photo : String?
    private var selectedDate : Date?
    
    private var facebookDetails : NSDictionary?
    
    private var loadingScreen : LoaderView?
    private var removePhoto : Bool = false
    private var uploadPhotoStarted : Bool = false
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        loadingScreen = LoaderView(frame: UIScreen.main.bounds)
        loadingScreen?.translatesAutoresizingMaskIntoConstraints = false
        loadingScreen?.loaderImages = UIManager.paymentLoader()
        loadingScreen?.actionButton.isHidden = true
        loadingScreen?.loaderTitleMessage = ""
        loadingScreen?.loaderTitle.text = ""
        loadingScreen?.loaderSubtitle.text = ""

        
        fields = [FieldConfig(label: "SPACE", errorString: ""), FieldConfig(label: "FIRST NAME", errorString: "This field is required"),
                  FieldConfig(label: "LAST NAME", errorString: "This field is required"),
                  FieldConfig(label: "MOBILE NO", errorString: "This field is required", placeholder: "917xxxxxxxx", prefix: "+63"),
                  FieldConfig(label: "EMAIL", errorString: "This field is required"),FieldConfig(label: "BIRTHDATE", errorString: "Invalid Birthdate")]
        
//        print(UserDefaults.standard.value(forKeyPath: "user"))
        
        let userPhoto = UserDefaults.standard.value(forKeyPath: "user.photo.mobile") as! String
        if userPhoto.characters.count > 0 {
            base64Photo = userPhoto
            let photoData = NSData(base64Encoded: userPhoto, options: .ignoreUnknownCharacters)
            profileImage.image = UIImage(data: photoData as! Data)
        } else {
            base64Photo = ""
        }
        
        let fbid = UserDefaults.standard.value(forKeyPath: "user.fbid") as! String
        
        if fbid != "0" {
            //print(fbid)
            fbButtonHeight.constant = 0.0
            view.layoutIfNeeded()
            facebookButton.isHidden = true
            footerView.frame.size.height -= 70.0
            profileImage.isUserInteractionEnabled = false
        }
        
        let userDetails : NSDictionary? = UserDefaults.standard.value(forKey: "user") as? NSDictionary
        
        if userDetails != nil {
            for i in 1..<fields!.count {
                let fc = fields![i]
                if i == 1 {
                    fc.val = userDetails?.object(forKey: "first_name") as! String
                } else if i == 2 {
                    fc.val = userDetails?.object(forKey: "last_name") as! String
                } else if i == 3 {
                    let mobile = userDetails?.object(forKey: "mobile") as! String
                    fc.val = mobile.substring(from: mobile.index(mobile.startIndex, offsetBy: 1))
                } else if i == 4 {
                    fc.val = userDetails?.object(forKey: "email") as! String
                } else if i == 5 {
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd"
                    df.locale = Locale.current
                    selectedDate = df.date(from: userDetails!.object(forKey: "bday") as! String)
                    df.dateFormat = "d MMMM yyyy"
                    if selectedDate == nil {
                        selectedDate = Date()
                    }
                    fc.val = df.string(from: selectedDate!)
                }
            }
        }
        
        headerView.frame.size.height = 90.0
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        
        tableView.reloadData()
        
        UserDefaults.standard.addObserver(self, forKeyPath: "user.photo.mobile", options: .new, context: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "EDIT PROFILE"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "edit_profile")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        UserDefaults.standard.removeObserver(self, forKeyPath: "user.photo.mobile", context: nil)
    }
    
    fileprivate func configureView() {
        UIManager.roundify(submitButton)
        UIManager.roundify(facebookButton)
        profileImage.layer.cornerRadius = profileImageWidth.constant/2.0
        profileImage.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "user.photo.mobile" {
            let imgDataString = UserDefaults.standard.value(forKeyPath: "user.photo.mobile") as? String
            if imgDataString != nil {
                let imgData = NSData(base64Encoded: imgDataString!, options: .ignoreUnknownCharacters)
                if imgDataString!.characters.count > 0 {
                    //print("base64Photo altered")
                    if uploadPhotoStarted == true {
                        if removePhoto == true {
                            base64Photo = ""
                            DispatchQueue.main.async {
                                self.profileImage.image = UIImage(named: "guest-profile")
                                self.profileImage.clipsToBounds = true
                            }
                        } else {
                            base64Photo = imgDataString
                            DispatchQueue.main.async {
                                self.profileImage.image = UIImage(data: imgData as! Data)
                                self.profileImage.clipsToBounds = true
                            }
                        }
                        uploadPhotoStarted = false
                    }
                } else {
                    //print("base64Photo altered")
                    if uploadPhotoStarted == true {
                        if removePhoto == true {
                            base64Photo = ""
                            DispatchQueue.main.async {
                                self.profileImage.image = UIImage(named: "guest-profile")
                                self.profileImage.clipsToBounds = true
                            }
                        } else {
                            base64Photo = imgDataString
                            DispatchQueue.main.async {
                                self.profileImage.image = UIImage(data: imgData as! Data)
                                self.profileImage.clipsToBounds = true
                            }
                        }
                        uploadPhotoStarted = false
                    }
                }

            } else {
                DispatchQueue.main.async {
                    self.profileImage.image = UIImage(named: "guest-profile")
                    self.profileImage.clipsToBounds = true
                }
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (fields?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell?
        
        if ((indexPath as NSIndexPath).row == 0) {
            cell = tableView.dequeueReusableCell(withIdentifier: "space", for: indexPath) as! TextFieldTableViewCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! TextFieldTableViewCell
            
            configureCell(cell as! TextFieldTableViewCell, indexPath: indexPath)
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath as NSIndexPath).row == 0 {
            return 50.0
        }
        
        return 80.0
    }
    
//    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
////        let indexPath = tableView.indexPath(for: cell)! as IndexPath
//        let fc = fields![indexPath.row]
//        if indexPath.row == 0 {
//            return
//        }
//        fc.val = (cell as! TextFieldTableViewCell).inputField.text!
//    }
    
    fileprivate func configureCell(_ cell : TextFieldTableViewCell, indexPath : IndexPath) {
        let labelString = fields![(indexPath as NSIndexPath).row].label!
        let fc = fields![(indexPath as NSIndexPath).row]
        
        cell.prompt.isHidden = true
        cell.label.text = labelString
        
        cell.separator.layer.borderWidth = 0.25
        cell.separator.layer.borderColor = UIColor(netHex: 0x777f99).cgColor
        
        cell.placeholder = fields![(indexPath as NSIndexPath).row].placeholder
        cell.pre = fields![(indexPath as NSIndexPath).row].prefix
        cell.prefix.text = fields![(indexPath as NSIndexPath).row].prefix
        cell.delegate = self
        
        
        if (indexPath as NSIndexPath).row == 3 {
            cell.maxLength = 10
            cell.inputField.keyboardType = .numberPad
        } else {
            cell.maxLength = 0
            cell.inputField.keyboardType = .default
        }
        
        
        let userDetails : NSDictionary? = UserDefaults.standard.value(forKey: "user") as? NSDictionary
        
        if userDetails != nil {
            if indexPath.row == 1 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            } else if indexPath.row == 2 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            } else if indexPath.row == 3 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            } else if indexPath.row == 4 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            } else if indexPath.row == 5 {
                cell.inputField.text = fc.val
                cell.expand(state: true)
            }
            
        }
        
        fc.val = cell.inputField.text?.compare(fc.val) != .orderedSame ? cell.inputField.text! : fc.val
        cell.inputField.text = fc.val
    }
    
    private func validateFields() -> Bool {
        
        var isValid : Bool = true
        for i : Int in 1 ..< (fields?.count)! {
            tableView.scrollToRow(at: IndexPath(item: i, section: 0), at: .none, animated: false)
            let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: i, section: 0)) as! TextFieldTableViewCell
            if cell.inputField.text?.characters.count == 0 {
                cell.prompt.text = "Field is required"
                cell.prompt.isHidden = false
                cell.shake()
                isValid = false
            }
            
            if i == 3 {
                if cell.inputField.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) != nil {
                    cell.prompt.text = "Invalid mobile number"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                } else if (cell.inputField.text?.characters.count)! < 10 {
                    cell.prompt.text = "Invalid mobile number"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                }
            }
            
            if i == 4 {
                if cell.inputField.text?.contains("@") == false || cell.inputField.text?.contains(".") == false {
                    cell.prompt.text = "Invalid email"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                }
            }
            
            if i == 5 {
                if selectedDate == nil {
                    cell.prompt.text = "Please set your birthdate"
                    cell.prompt.isHidden = false
                    cell.shake()
                    isValid = false
                }
            }
        }
        
        return isValid
    }
    
    @IBAction func submitAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "edit_profile_submit")
        if validateFields() {
//            self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: 3, section: 0)) as! TextFieldTableViewCell
            let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
            let mobile = UserDefaults.standard.value(forKeyPath: "user.mobile") as? String
            
            if mobile!.substring(from: mobile!.index(mobile!.startIndex, offsetBy: 1)) != cell.inputField.text! {
                
                self.view.addSubview(loadingScreen!)
                self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                          ])
                
                self.view.layoutIfNeeded()
                
                loadingScreen?.startAnimating()
                
                GMoviesAPIManager.editProfile(lbid: user.value(forKey: "lbid") as! String, mobileNum: cell.inputField.text!, completion: { (error, result) in
                    if error == nil {
                        let status : Int = result?.value(forKeyPath: "response.status") as! Int
                        if status == 0 {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                                cell.prompt.text = (result?.value(forKeyPath: "response.msg") as! String)
                                cell.prompt.isHidden = false
                                cell.shake()
                                
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                                
                                if self.removePhoto == true {
                                    self.uploadPhotoStarted = true
                                    GMoviesAPIManager.removePhoto(lbid: user.value(forKey: "lbid") as! String, completion: { (error, result) in
                                        if error == nil {
                                            let status = result?.value(forKeyPath: "response.status") as! Int
                                            if status == 1 {
                                                let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                                                let newUser : NSMutableDictionary = user.mutableCopy() as! NSMutableDictionary
                                                let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                                
                                                photo.setValue("", forKey: "mobile")
                                                photo.setValue("", forKey: "website")
                                                newUser.setValue(photo, forKeyPath: "photo")
                                                UserDefaults.standard.setValue(newUser, forKey: "user")
                                                UserDefaults.standard.synchronize()
                                                
                                                self.removePhoto = false
                                            }
                                        } else {
                                            
                                        }
                                    })
                                }
                            }
                            self.performSegue(withIdentifier: "showVerifyOTP", sender: nil)
                            
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            self.loadingScreen?.stopAnimating()
                            self.loadingScreen?.removeFromSuperview()
                            
                        }
                        //print(error!.localizedDescription)
                    }
                })
            } else {
                let userDetails : NSMutableDictionary? = NSMutableDictionary()
                for i : Int in 1 ..< (fields?.count)! {
                    tableView.scrollToRow(at: IndexPath(item: i, section: 0), at: .none, animated: false)
                    let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: i, section: 0)) as! TextFieldTableViewCell
                    if i == 1 {
                        userDetails?.setValue(cell.inputField.text!, forKey: "first_name")
                    } else if i == 2 {
                        userDetails?.setValue(cell.inputField.text!, forKey: "last_name")
                    } else if i == 3 {
                        userDetails?.setValue("+63\(cell.inputField.text!)", forKey: "mobile")
                    } else if i == 4 {
                        userDetails?.setValue(cell.inputField.text!, forKey: "email")
                    } else if i == 5 {
                        let df = DateFormatter()
                        df.dateFormat = "yyyy-MM-dd"
                        df.locale = Locale.current
                        userDetails?.setValue(df.string(from: selectedDate!), forKey: "bday")
                    }
                }
                
                if facebookDetails != nil {
                    userDetails?.setValue(facebookDetails?.value(forKey: "id") as! String, forKey: "fbid")
                } else {
                    let fbid = UserDefaults.standard.value(forKeyPath: "user.fbid") as! String
                    userDetails?.setValue(fbid, forKey: "fbid")
                }
                
                self.view.addSubview(loadingScreen!)
                self.view.addConstraints([NSLayoutConstraint(item: loadingScreen!, attribute: .top, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .top, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .leading, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .leading, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .trailing, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .trailing, multiplier: 1.0, constant: 0.0), NSLayoutConstraint(item: loadingScreen!, attribute: .bottom, relatedBy: .equal, toItem: tableView.subviews.first!, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                                          ])
                
                self.view.layoutIfNeeded()
                
                loadingScreen?.startAnimating()
                
                if userDetails?.value(forKey: "fbid") as! String != "0" {
                    
                    GMoviesAPIManager.editProfile(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, mobileNum: userDetails?.value(forKey: "mobile") as! String, pin: "", firstName: userDetails?.value(forKey: "first_name") as! String, lastName: userDetails?.value(forKey: "last_name") as! String, email: userDetails?.value(forKey: "email") as! String, fbid: userDetails?.value(forKey: "fbid") as! String, bday: userDetails?.value(forKey: "bday") as! String, completion: { (error, result) in
                        if error == nil {
                            let status : Int = result?.value(forKeyPath: "response.status") as! Int
                            
                            if status == 1 {
                                
                                let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                                AccountHelper.shared.saveUser(data: userData)
                                
                                UserDefaults.standard.setValue(true, forKey: "loggedin")
                                UserDefaults.standard.synchronize()
                                
                                DispatchQueue.main.async {
                                    self.loadingScreen?.stopAnimating()
                                    self.loadingScreen?.removeFromSuperview()
                                    let alert = UIAlertController(title: "Success", message: "Your profile has been updated", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                        
                                    })
                                    
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    self.uploadPhotoStarted = true
                                    GMoviesAPIManager.editPhoto(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, photo: self.base64Photo!, completion: { (error, result) in
                                        
                                        if error == nil {
                                            let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                                            let newUser : NSMutableDictionary = user.mutableCopy() as! NSMutableDictionary
                                            let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                            
                                            photo.setValue(self.base64Photo!, forKey: "mobile")
                                            newUser.setValue(photo, forKeyPath: "photo")
                                            UserDefaults.standard.setValue(newUser, forKey: "user")
                                            UserDefaults.standard.synchronize()
                                            //print("woof!")
                                        } else {
                                            //print(error!.localizedDescription)
                                        }
                                        
                                    })
                                }
                                
                                DispatchQueue.main.async {
                                    self.loadingScreen?.stopAnimating()
                                    self.loadingScreen?.removeFromSuperview()
                                }
                            } else {
                                
                                DispatchQueue.main.async {
                                    
                                    self.loadingScreen?.stopAnimating()
                                    self.loadingScreen?.removeFromSuperview()
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                            }
                            //print(error!.localizedDescription)
                        }
                    })
                } else {
                    GMoviesAPIManager.editProfile(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, mobileNum: userDetails?.value(forKey: "mobile") as! String, pin: "", firstName: userDetails?.value(forKey: "first_name") as! String, lastName: userDetails?.value(forKey: "last_name") as! String, email: userDetails?.value(forKey: "email") as! String, bday: userDetails?.value(forKey: "bday") as! String , completion: { (error, result) in
                        if error == nil {
                            let status : Int = result?.value(forKeyPath: "response.status") as! Int
                            
                            if status == 1 {
                                
                                let userData = result?.value(forKeyPath: "response.user") as! NSDictionary
                                AccountHelper.shared.saveUser(data: userData)
                                
                                UserDefaults.standard.setValue(true, forKey: "loggedin")
                                UserDefaults.standard.synchronize()
                                
                                DispatchQueue.main.async {
                                    self.loadingScreen?.stopAnimating()
                                    self.loadingScreen?.removeFromSuperview()
                                    
                                    let alert = UIAlertController(title: "Success", message: "Your profile has been updated", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                        
                                    })
                                    
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    if self.removePhoto == true {
                                        self.uploadPhotoStarted = true
                                        GMoviesAPIManager.removePhoto(lbid: user.value(forKey: "lbid") as! String, completion: { (error, result) in
                                            //print(result)
                                            if error == nil {
                                                let status = result?.value(forKeyPath: "response.status") as! Int
                                                if status == 1 {
                                                    let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                                                    let newUser : NSMutableDictionary = user.mutableCopy() as! NSMutableDictionary
                                                    let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                                    
                                                    photo.setValue("", forKey: "mobile")
                                                    photo.setValue("", forKey: "website")
                                                    newUser.setValue(photo, forKeyPath: "photo")
                                                    UserDefaults.standard.setValue(newUser, forKey: "user")
                                                    UserDefaults.standard.synchronize()
                                                    
                                                    self.removePhoto = false
                                                }
                                            } else {
                                                
                                            }
                                        })
                                    } else {
                                        self.uploadPhotoStarted = true
                                        GMoviesAPIManager.editPhoto(lbid: UserDefaults.standard.value(forKeyPath: "user.lbid") as! String, photo: self.base64Photo!, completion: { (error, result) in
                                            
                                            if error == nil {
                                                let user : NSDictionary = UserDefaults.standard.value(forKey: "user") as! NSDictionary
                                                let newUser : NSMutableDictionary = user.mutableCopy() as! NSMutableDictionary
                                                let photo : NSMutableDictionary = NSMutableDictionary(dictionary: newUser.value(forKey: "photo") as! NSDictionary)
                                                
                                                photo.setValue(self.base64Photo!, forKey: "mobile")
                                                newUser.setValue(photo, forKeyPath: "photo")
                                                UserDefaults.standard.setValue(newUser, forKey: "user")
                                                UserDefaults.standard.synchronize()
                                                //print("woof!")
                                            } else {
                                                print(error!.localizedDescription)
                                            }
                                            
                                        })
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    self.loadingScreen?.stopAnimating()
                                    self.loadingScreen?.removeFromSuperview()
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.loadingScreen?.stopAnimating()
                                    self.loadingScreen?.removeFromSuperview()
                                }
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.loadingScreen?.stopAnimating()
                                self.loadingScreen?.removeFromSuperview()
                            }
                            print(error!.localizedDescription)
                        }
                    })
                }
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVerifyOTP" {
            let vc : VerifyEditProfileViewController = segue.destination as! VerifyEditProfileViewController
            vc.base64Photo = base64Photo
            vc.removePhoto = removePhoto
            
            let userDetails : NSMutableDictionary? = NSMutableDictionary()
            for i : Int in 1 ..< (fields?.count)! {
                tableView.scrollToRow(at: IndexPath(item: i, section: 0), at: .none, animated: false)
                let cell : TextFieldTableViewCell = tableView.cellForRow(at: IndexPath(item: i, section: 0)) as! TextFieldTableViewCell
                if i == 1 {
                    userDetails?.setValue(cell.inputField.text!, forKey: "first_name")
                } else if i == 2 {
                    userDetails?.setValue(cell.inputField.text!, forKey: "last_name")
                } else if i == 3 {
                    vc.mobileNumber = cell.inputField.text
                    userDetails?.setValue("+63\(cell.inputField.text!)", forKey: "mobile")
                } else if i == 4 {
                    userDetails?.setValue(cell.inputField.text!, forKey: "email")
                } else if i == 5 {
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd"
                    df.locale = Locale.current
                    userDetails?.setValue(df.string(from: selectedDate!), forKey: "bday")
                }
            }
            
            if facebookDetails != nil {
                userDetails?.setValue(facebookDetails?.value(forKey: "id") as! String, forKey: "fbid")
            } else {
                let fbid = UserDefaults.standard.value(forKeyPath: "user.fbid") as! String
                userDetails?.setValue(fbid, forKey: "fbid")
            }

            vc.userDetails = userDetails
        }
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "edit_profile_facebook")
        if FBSDKAccessToken.current() == nil {
            FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: { (result, error) in
                if error == nil {
                    if result?.isCancelled == false {
                       self.getUserDetailsFromFacebook()
                    } else {
                        FBSDKLoginManager().logOut()
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        } else {
            self.getUserDetailsFromFacebook()
        }
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "logout")
        GMoviesQueueManager.sharedInstance.getQueue().cancelAllOperations()
        GMoviesQueueManager.sharedInstance.getImageQueue().cancelAllOperations()
        FBSDKLoginManager().logOut()
        
        UserDefaults.standard.removeObserver(self, forKeyPath: "user.photo.mobile", context: nil)
        AccountHelper.shared.logout()
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        (appDelegate.window?.rootViewController as! MNavigationViewController).popToRootViewController(animated: true)
    }
    
    @IBAction func editPhoto(_ sender: Any) {
        AnalyticsHelper.sendButtonEvent(name: "edit_profile_photo")
        let actionSheet = UIAlertController(title: "Get photo from:", message: "", preferredStyle: .actionSheet)
        let photoAlbum = UIAlertAction(title: "Photo Album", style: .default, handler: {(_) in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        })
        
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_) in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .front
            self.present(imagePicker, animated: true, completion: nil)
        })
        
        let remove = UIAlertAction(title: "Remove", style: .destructive, handler: {(_) in
            self.profileImage.image = UIImage(named: "guest-profile")
            self.removePhoto = true
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {(_) in
            
        })
        
        actionSheet.addAction(photoAlbum)
        actionSheet.addAction(camera)
        if facebookDetails == nil {
            actionSheet.addAction(remove)
        }
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func getUserDetailsFromFacebook() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let req : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "first_name,last_name,email,picture.type(large)"])
        req.start { (conn, result, error) in
            if error == nil {
                
                self.facebookDetails = result as? NSDictionary
                //print(self.facebookDetails)
                
                DispatchQueue.main.async {
                    for i : Int in 1 ..< (self.fields?.count)! {
                        self.tableView.scrollToRow(at: IndexPath(item: i, section: 0), at: .none, animated: false)
                        let cell : TextFieldTableViewCell = self.tableView.cellForRow(at: IndexPath(item: i, section: 0)) as! TextFieldTableViewCell
                        let fc = self.fields![i]
                        if i == 1 {
                            cell.inputField.text = self.facebookDetails?.value(forKey: "first_name") as? String
                            fc.val = self.facebookDetails?.value(forKey: "first_name") as! String
                        } else if i == 2 {
                            cell.inputField.text = self.facebookDetails?.value(forKey: "last_name") as? String
                            fc.val =  self.facebookDetails?.value(forKey: "last_name") as! String
                        } else if i == 4 {
                            cell.inputField.text = self.facebookDetails?.value(forKey: "email") as? String
                            fc.val = self.facebookDetails?.value(forKey: "email") as! String
                        }
                    }
                    
                }
                
                DispatchQueue.main.async {
                    let url = (result as! NSDictionary).value(forKeyPath: "picture.data.url") as! String
                    GMoviesAPIManager.downloadPoster(urlString: url, completion: { (error, image) in
                        if error == nil {
                            let imgData = UIImageJPEGRepresentation(image!, 1.0)
                            
                            self.base64Photo = imgData?.base64EncodedString()
                            self.selectedImage = image
                            
                            DispatchQueue.main.async {
                                self.profileImage.image = image
                                self.profileImage.isUserInteractionEnabled = false
                            }
                            
                        } else {
                            print(error!.localizedDescription)
                        }
                    })
                }
                
            } else {
                print(error!.localizedDescription)
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        selectedImage = resizeImage(image: info[UIImagePickerControllerOriginalImage] as! UIImage, newWidth: 112.0)
        
        if selectedImage != nil {
            let imgData = UIImageJPEGRepresentation(selectedImage!, 0.9)
            base64Photo = imgData!.base64EncodedString()
            profileImage.image = selectedImage
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func shouldBeginEditing(cell: TextFieldTableViewCell) -> Bool {
        
        let indexPath = tableView.indexPath(for: cell)! as IndexPath
        
        if indexPath.row == 5 {
            
            currentPopDrop = PopDropViewController(nibName: "PopDropViewController", bundle: nil, expandedHeight: 216.0)
            let datePicker = DatePickerViewController(nibName: "DatePickerViewController", bundle: nil) 
            datePicker.delegate = self
            
            currentPopDrop?.showViewControllerFromView(viewController: datePicker, originView: cell, offset: CGPoint(x: 0.0, y: 64.0 - tableView.contentOffset.y))
            
            return false
        }
        
        return true
    }
    
    func didBeginEditing(cell: TextFieldTableViewCell) {
        
    }
    
    func didEndEditing(cell: TextFieldTableViewCell) {
        if tableView.indexPath(for: cell) != nil {
            let indexPath = tableView.indexPath(for: cell)! as IndexPath
            let fc = fields![indexPath.row]
            fc.val = cell.inputField.text!
        }
    }
    
    func didChangeDate(datePicker: DatePickerViewController, date: Date) {
        //print(date)
        selectedDate = date
        let df = DateFormatter()
        df.dateFormat = "d MMMM yyyy"
        
        let bdayCell : TextFieldTableViewCell? = tableView.cellForRow(at: IndexPath(row: 5, section: 0)) as? TextFieldTableViewCell
        bdayCell?.inputField.text = df.string(from: date)
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
