//
//  VerifyGCashViewController.swift
//  GMovies
//
//  Created by Marc Darren Padilla on 7/11/16.
//  Copyright © 2016 Digital Ventures. All rights reserved.
//

import UIKit

class VerifyGCashViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var otpField: UITextField!
    
    @IBOutlet weak var firstDigit: UILabel!
    @IBOutlet weak var secondDigit: UILabel!
    @IBOutlet weak var thirdDigit: UILabel!
    @IBOutlet weak var fourthDigit: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var resendPrompt: UILabel!
    
    
    @IBOutlet weak var firstLine: UIView!
    @IBOutlet weak var secondLine: UIView!
    @IBOutlet weak var thirdLine: UIView!
    @IBOutlet weak var fourthLine: UIView!
    
    
    @IBOutlet weak var indicator: UIButton!
    
    var mobileNumber : String?
    
    var paymentRoot : UIViewController?
    
    var selectedPayment : PaymentOptionModel?
    var theaterObject : TheaterModel?
    var movieObject : MovieModel?
    var cinemaObject : CinemaModel?
    var timeObject : TimeModel?
    var dateObject : Date?
    var reservedSeating : Bool = false
    var seatCount : Int = 0
    var selectedSeatsController : SeatSelectionViewController?
    var email : String?
    var promo : String?
    
    var blockObject : BlockScreeningModel?
    
    //gcash
    var gcash : String?
    var otp : String?
    
    var resendAttempt : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        indicator.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        otpField.becomeFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidBecomeActive), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AnalyticsHelper.sendScreenShowEvent(name: "gcash_otp")
        otpField.becomeFirstResponder()
        
        perform(#selector(self.showResend), with: nil, afterDelay: 8.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        NotificationCenter.default.removeObserver(self, name: nil, object: nil)
        resendAttempt = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc private func applicationDidBecomeActive(notif : Notification) {
        if notif.name == Notification.Name.UIApplicationDidBecomeActive {
            otpField.becomeFirstResponder()
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        indicator.isHidden = true
        
        if string == "" {
            if textField.text?.characters.count == 1 {
                firstDigit.text = string
            } else if textField.text?.characters.count == 2 {
                secondDigit.text = string
            } else if textField.text?.characters.count == 3 {
                thirdDigit.text = string
            } else if textField.text?.characters.count == 4 {
                fourthDigit.text = string
            }
        } else {
            if textField.text?.characters.count == 0 {
                firstDigit.text = string
            } else if textField.text?.characters.count == 1 {
                secondDigit.text = string
            } else if textField.text?.characters.count == 2 {
                thirdDigit.text = string
            } else if textField.text?.characters.count == 3 {
                fourthDigit.text = string
                sendMobile(pin: "\(textField.text!)\(string)")
            } else {
                return false
            }
        }
        
        return true
    }
    
    //MARK : Private methods
    
    private func sendDetails(pin : String) {
        /*
        if userDetails?.value(forKey: "fbid") != nil {
            GMoviesAPIManager.register(mobileNum: userDetails?.value(forKey: "mobile") as! String, pin: pin, firstName: userDetails?.value(forKey: "first_name") as! String, lastName: userDetails?.value(forKey: "last_name") as! String, email: userDetails?.value(forKey: "email") as! String, fbid: userDetails?.value(forKey: "fbid") as! String, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    DispatchQueue.main.async {
                        self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
                        self.indicator.isHidden = false
                    }
                    if status == 1 {
                        UserDefaults.standard.setValue(result?.value(forKeyPath: "response.user") as! NSDictionary, forKey: "user")
                        UserDefaults.standard.set(true, forKey: "loggedin")
                        UserDefaults.standard.synchronize()
                        DispatchQueue.main.async {
                            if self.modalMode == false {
                                self.loadHomeStoryboard()
                            } else {
                                
                                if self.previousViewController == nil {
                                    self.dismiss(animated: true, completion: nil)
                                } else {
                                    if self.previousViewController != self.nextViewController {
                                        self.previousViewController?.navigationController?.pushViewController(self.nextViewController!, animated: true)
                                    } else {
                                        self.previousViewController?.navigationController?.popToViewController(self.previousViewController!, animated: true)
                                    }
                                }
                            }
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            self.firstDigit.shake()
                            self.secondDigit.shake()
                            self.thirdDigit.shake()
                            self.fourthDigit.shake()
                            self.firstLine.shake()
                            self.secondLine.shake()
                            self.thirdLine.shake()
                            self.fourthLine.shake()
                            self.indicator.isHidden = false
                            self.indicator.shake()
                            
                        }
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        } else {
            GMoviesAPIManager.register(mobileNum: userDetails?.value(forKey: "mobile") as! String, pin: pin, firstName: userDetails?.value(forKey: "first_name") as! String, lastName: userDetails?.value(forKey: "last_name") as! String, email: userDetails?.value(forKey: "email") as! String, completion: { (error, result) in
                if error == nil {
                    let status : Int = result?.value(forKeyPath: "response.status") as! Int
                    DispatchQueue.main.async {
                        self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
                        self.indicator.isHidden = false
                    }
                    if status == 1 {
                        UserDefaults.standard.setValue(result?.value(forKeyPath: "response.user") as! NSDictionary, forKey: "user")
                        UserDefaults.standard.set(true, forKey: "loggedin")
                        UserDefaults.standard.synchronize()
                        DispatchQueue.main.async {
                            if self.modalMode == false {
                                self.loadHomeStoryboard()
                            } else {
                                
                                if self.previousViewController == nil {
                                    self.dismiss(animated: true, completion: nil)
                                } else {
                                    if self.previousViewController != self.nextViewController {
                                        self.previousViewController?.navigationController?.pushViewController(self.nextViewController!, animated: true)
                                    } else {
                                        self.previousViewController?.navigationController?.popToViewController(self.previousViewController!, animated: true)
                                    }
                                }
                            }
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            self.firstDigit.shake()
                            self.secondDigit.shake()
                            self.thirdDigit.shake()
                            self.fourthDigit.shake()
                            self.firstLine.shake()
                            self.secondLine.shake()
                            self.thirdLine.shake()
                            self.fourthLine.shake()
                            self.indicator.isHidden = false
                            self.indicator.shake()
                            
                        }
                    }
                } else {
                    print(error!.localizedDescription)
                }
            })
        }
         */
    }
    
    private func sendMobile(pin : String) {
        
        GMoviesAPIManager.verifyGCashOTP(mobileNum: mobileNumber!, pin: pin) { (error, result) in
            if error == nil {
                let status : Int = result?.value(forKeyPath: "response.status") as! Int
                if status == 1 {
                    DispatchQueue.main.async {
                        self.indicator.setImage(UIImage(named: "verify-correct"), for: .normal)
                        self.indicator.isHidden = false
                        self.performSegue(withIdentifier: "showPaymentLoader", sender: self)
                    }
                } else {
                    
                    DispatchQueue.main.async {
                        self.firstDigit.shake()
                        self.secondDigit.shake()
                        self.thirdDigit.shake()
                        self.fourthDigit.shake()
                        self.firstLine.shake()
                        self.secondLine.shake()
                        self.thirdLine.shake()
                        self.fourthLine.shake()
                        self.indicator.isHidden = false
                        self.indicator.shake()
                        self.indicator.setImage(UIImage(named: "verify-wrong"), for: .normal)
                        self.indicator.isHidden = false
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    private func configureView() {
        UIManager.buttonBorder(button: resendButton, width: 0.25, color: UIColor.gray)
        UIManager.roundify(resendButton)
        
        resendButton.alpha = 0
        resendPrompt.alpha = 0
        mobileLabel.text = "+63\(mobileNumber!)"
    }
    
    @objc private func showResend() {
        UIView.animate(withDuration: 0.250, animations: {
            self.resendButton.alpha = 1.0
            self.resendPrompt.alpha = 1.0
            }, completion: { (_) in
                
        })
    }
    
    private func hideResend() {
        UIView.animate(withDuration: 0.150, animations: {
            self.resendButton.alpha = 0.0
            self.resendPrompt.alpha = 0.0
            }, completion: { (_) in
                self.perform(#selector(self.showResend), with: nil, afterDelay: 8.0)
        })
    }
    
    @IBAction func resendAction(_ sender: AnyObject) {
        AnalyticsHelper.sendButtonEvent(name: "gcash_otp_resend")
        hideResend()
        
        if resendAttempt < 3 {
            GMoviesAPIManager.gcashLogin(mobileNum: mobileNumber!) { (error, result) in
                if error == nil {
                    
                } else {
                    print(error!.localizedDescription)
                }
            }
        } else {
            AnalyticsHelper.sendEvent(category: "event", action: "gcash_otp_resend", label: "exceeded_3_times", value: "")
            let alert = UIAlertController(title: "", message: "Having problems receiving the OTP? Please contact us at +63 917 5434594 or email us at support@gmovies.ph.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                
            })
            alert.addAction(yesAction)
            
            present(alert, animated: true, completion: nil)
        }
    
        resendAttempt += 1
    }


     // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPaymentLoader" {
            let paymentLoader : PaymentLoaderViewController = segue.destination as! PaymentLoaderViewController
            
            paymentLoader.selectedPayment = selectedPayment
            if blockObject == nil {
                paymentLoader.theaterObject = theaterObject
                paymentLoader.movieObject = movieObject
                paymentLoader.cinemaObject = cinemaObject
                paymentLoader.timeObject = timeObject
                paymentLoader.dateObject = dateObject
            } else {
                paymentLoader.blockObject = blockObject
            }
            paymentLoader.reservedSeating = reservedSeating
            paymentLoader.seatCount = seatCount
            paymentLoader.selectedSeatsController = selectedSeatsController
            paymentLoader.email = email
            paymentLoader.promo = promo
            paymentLoader.paymentRoot = paymentRoot
            paymentLoader.delegate = (paymentRoot as! ProgressNavigationViewController).viewControllers?[1] as! PaymentViewController
            paymentLoader.gcash = mobileNumber!
            paymentLoader.otp = otpField.text!
            
        }
    }

}
